

import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/conginito/secrets.dart';
import 'package:wellnest/pages/forgot_password/forgot_enter_code.dart';
import 'package:wellnest/pages/login/login_view.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class ForgotServices {

  static sendEmail(email) async{
    LoadingOrError.loading();
    var data;
    var cogUser = CognitoUser(email, 
    CogNitoWell.userPool);
    try{
      data = await cogUser.forgotPassword();
      Get.back();
     Get.to(()=>EneterCodeNewPassword(email: email));
    }catch(e){
      print(e);
    }
    print(data);
  }


  static setNewPassword(email,code,newPassword) async{
    LoadingOrError.loading();
    var cogUser = CognitoUser(email, 
    CogNitoWell.userPool);  
    bool? passwordConfirmd = false;
    try{
      passwordConfirmd =await cogUser.confirmPassword(
      code, newPassword);
      Get.back();
      Get.offNamedUntil(AppRouteName.startingpage!, (route) => false);
      LoadingOrError.errorloading("Successfully Change Password of your account $email",
       "Change Password");
      print(passwordConfirmd);
    }catch(e){
      Get.back();
      LoadingOrError.errorloading("Error $e",
       "Change Password");
      print(e);
    }
    

  }


}