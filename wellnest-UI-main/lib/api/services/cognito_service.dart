

import 'dart:convert';

import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/conginito/secrets.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/login/re_confirmEmail/re_confirm_email.dart';
import 'package:wellnest/pages/main/main_home.dart';
import 'package:wellnest/pages/onboarding/onboarding_page.dart';
import 'package:wellnest/pages/register/confirm_email/confirm_email.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class CognitoService{

  static signInCognito(data) async{
  LoadingOrError.loading();

  CognitoUserSession? session;
   final cogUser = CognitoUser(data['email'],
    CogNitoWell.userPool);  

    final authDetail = AuthenticationDetails(
      username: data['email'],
      password: data['password']
    );
    
    try {
  session = await cogUser.authenticateUser(authDetail);
  final attri = await cogUser.getUserAttributes();
  var bam= attri!.map((e) => '"${e.name}":"${e.value}"').toList();
  final json  = {bam.toString().replaceAll('[', '').replaceAll(']', '')};
  var de = jsonDecode(json.toString()); 
  await SharePref.setType(de['custom:type']);
  await SharePref.setProfile(jsonEncode(de));
  await SharePref.setPassword(data['password']);
  Get.back();
  //Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
  Get.offNamedUntil(AppRouteName.subscription!, (route) => false);
  print(de);
} on CognitoClientException catch (e) {

  if(e.message == "User is not confirmed."){
    Get.back();
    print("Result ${e.message}");
    resendEmailVerification(data['email']);
    Get.off(()=>ReConfrimEmail(data: data));
  }else{
    print("Rexux ${e.message}");
  Get.back();
   LoadingOrError.errorloading(e.message, "Try Again!");
  }
    
}catch (e) {
  Get.back();
//  LoadingOrError.errorloading(e.toString(), "Try Again!");
print(e);
} 
  
// print(session!.getAccessToken().getJwtToken());

  }

  static signUpCognito(data, type) async{
    // print(data);
      var dataPool;
      final userAtrri = [
        AttributeArg(
          name: "given_name",
          value: data['firstname'],
        ),
        AttributeArg(
          name: "family_name",
          value: data['lastname'],
        ),
        AttributeArg(
          name: "phone_number",
          value: "+63"+data['phone'],
        ),
        AttributeArg(
          name: "email",
          value: data['email'],
        ),
        AttributeArg(
          name: "custom:type",
          value: data['type'],
        ),
        
      ];

     try{
        dataPool = await CogNitoWell.userPool.signUp(
        data['email'],
        data['password'],
        userAttributes: userAtrri,
        // validationData: userAtrri
        );

        // Get.off(()=>ConfirmEmailCognito(data: data, type: type,));
        Get.offNamed(AppRouteName.onBoard!,parameters: data);
      print(data);
     }on CognitoClientException catch (e){
      LoadingOrError.errorloading(e.message, "Try Again!");
      print(data);
     }
  }

  static confirmEmail(email,verificationCode,data)async{
    LoadingOrError.loading();
    final cogniUser = CognitoUser(email, CogNitoWell.userPool); 

    final authDetail = AuthenticationDetails(
      username: data['email'],
      password: data['password']
    );

    bool confirmis = false;
    CognitoUserSession? session;
    try{
     
      confirmis = await cogniUser.confirmRegistration(verificationCode);
       session = await cogniUser.authenticateUser(authDetail);
      final attri = await cogniUser.getUserAttributes();
      var bam= attri!.map((e) => '"${e.name}":"${e.value}"').toList();
      final json  = {bam.toString().replaceAll('[', '').replaceAll(']', '')};
      var de = jsonDecode(json.toString()); 
      await SharePref.setType(de['custom:type']);
      await SharePref.setProfile(jsonEncode(de));
      await SharePref.setPassword(data['password']);
      print(de);
      print(confirmis);
      // Get.off(()=>OnBoardingPagew());
      Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
    }on CognitoClientException catch(e){
      Get.back();
       LoadingOrError.errorloading(e.message, "Try Again!");
    }
    
  }


  static resendEmailVerification(String? data) async{

    final cognitoUser = CognitoUser(data!,CogNitoWell.userPool);
    final String? status;
    try{
      status = await cognitoUser.resendConfirmationCode();
      print(status!);
    }catch(e){
      print("Result for $e");
    }
    
  }


 static logoutSingout(String? email) async{

   final cognitoUser = CognitoUser(email!,CogNitoWell.userPool);

   await cognitoUser.signOut();

  

 }   

}