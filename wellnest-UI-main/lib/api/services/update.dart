
import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/conginito/secrets.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class UpdateCognitoUser{


  static updateCognito(type) async{
    LoadingOrError.loading();
    final List<CognitoUserAttribute> attr = [];
    attr.add(CognitoUserAttribute(
      name: "custom:type" ,value: type 
    ));
    final cogUser = CognitoUser(
      SharePref.detailData['email'], CogNitoWell.userPool);
      final authDetail = AuthenticationDetails(
      username: SharePref.detailData['email'],
      password: SharePref.getPassword()
    );
   CognitoUserSession? session;
    try{
    session = await cogUser.authenticateUser(authDetail);
       await cogUser.updateAttributes(attr);
      print(session!.getAccessToken().getJwtToken());
      SharePref.setType(type);
      Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
    }on CognitoClientException catch (e){
      print(e.message);
      LoadingOrError.errorloading(e.message, "Try again!");
    }

  }

}