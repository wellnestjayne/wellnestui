
import 'package:dio/dio.dart' as dio;
import 'package:wellnest/api/api_string/api_string.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/user_view_book_events.dart';

class APICalendarCallEvent {

   static final dios = dio.Dio();
   static final String? userId= SharePref.detailData['id']!;
   static final tokenOption = dio.Options(
        headers: {
          "Authorization": "Bearer "+SharePref.getRefToken()!,
        }
  );


  static getUserCalendarEvent() async{

    try{
      final response = await dios.get(
        ApiWellnestHost.getUserBookEvent!+userId!,options: tokenOption);
      if(response.statusCode == 200){
        List<dynamic> list = response.data['data'];
        return list.map((e) => EventsUser.fromJson(e)).toList();
      }  
    }on dio.DioError catch(e){

    }

  }


}