

 import 'dart:convert';

import 'package:dio/dio.dart' as dio;
import 'package:get/get.dart';
import 'package:wellnest/api/api_string/api_string.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/coach_schedule_time.dart';
import 'package:wellnest/pages/coach/book_session/redirect/redirectPage_Web.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class ApiBookSession{

  static final dios = dio.Dio();
   static final tokenOption = dio.Options(
        headers: {
          "Authorization": "Bearer "+SharePref.getRefToken()!,
        }
      );

   static dataBookSession({List<Map<String,dynamic>>? list = const <Map<String,dynamic>>[],
    dynamic paymenthodID,
   }) async {
     try{

       final response = await dios.post(
         ApiWellnestHost.bookSession!,
       data: {
         "paymentMethodId": paymenthodID.toString(),
         "bookings":list!.map((e) => e).toList()
       },
       options: tokenOption
       );
        print("Log Date ${response.data}");
        if(response.statusCode == 200){
          if(response.data['data']['needsRedirect'] == true){
           Get.to(() => RedirectWebpageFromBook(
              url: response.data['data']['redirectUrl'],
              bookingId: response.data['data']['bookingId'],
            ));
        }else{
          return response.statusCode;
        }
        }

       
     }on dio.DioError catch(e){
       print(e.response!.data);
       LoadingOrError.errorloading(e.response!.data['responseException']['message'],"Can't Book");
     }

   }   

  static payBugoMongo({ dynamic datax }) async{
    final oneTime = "https://api.paymongo.com/v1/payment_methods";
    var sampleApi = "pk_test_YPQFb9nA3WK5bDkryEPwbXzW";
    var passwordNameBasicAuth = "";
    var basicAuth = 'Basic '+ base64Encode(utf8.encode('$sampleApi:$passwordNameBasicAuth'));
    var data ={
       "data":{
          "attributes":{
        "details":{
          "card_number": datax['card'],
          "exp_month": int.parse(datax['month']),
          "exp_year": int.parse(datax['year']),
          "cvc": datax['cvc']
        },
        "type": "card"
      },
       },
    };
    try{
      final response = await dios.post(oneTime,
      data: data,
      options: dio.Options(
        headers: {
          "authorization": basicAuth
        }
      ));
      if(response.statusCode == 200){
         return response.data; 
      }
    }on dio.DioError catch(e){
      print(e.response!.data);
    }

  }

  static paymentCallback()async{

  }

  static getSchedultDates({String? coachId})async{
    try{
      final response = await dios.get(ApiWellnestHost.getScheduleTime!+coachId!,
      options: tokenOption);
      print(response.data);
      List<dynamic> list = response.data['data'];
      return list.map((e) => 
      ScheduleTime.fromJson(e)
      ).toList()
      ..sort((ScheduleTime s1 , ScheduleTime s2)=>s1.day!.compareTo(s2.day!));
    }on dio.DioError catch(e){
      print(e.response!.data);
    }
  }

 }