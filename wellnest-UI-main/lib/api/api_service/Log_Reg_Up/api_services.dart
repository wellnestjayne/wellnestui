import 'dart:convert';
import 'package:dio/dio.dart' as dio;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_string/api_string.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/forgot_password/forgot_enter_code.dart';
import 'package:wellnest/pages/login/re_confirmEmail/re_confirm_email.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class UserCoachApiOnly {
  static final dioApi = dio.Dio();

  static loginUserOrCoach(data) async {
    LoadingOrError.loading();
    try {
      final response = await dioApi.post(
        ApiWellnestHost.login!,
        data: data,
      );
      if (response.statusCode == 200) {
        print(response.data['data']['userDetails']);
        if (response.data['data']['needsVerification'] == true) {
          print('need verification');
          Get.back();
          resendVerificationEmail(data['username']);
          Get.off(() => ReConfrimEmail(data: data));
        } else {
          await SharePref.setType(response.data['data']['userDetails']['type']);
          await SharePref.setProfile(jsonEncode(response.data['data']['userDetails']));
          await SharePref.setPassword(data['password']);
          await SharePref.setCoachId(response.data['data']['userDetails']['coachId'] != null
              ? response.data['data']['userDetails']['coachId']
              : "null");
          needTobeRefreshToken(response.data['data']['refreshToken']);
           await FirebaseMessaging.instance.subscribeToTopic('all');
          if(response.data['data']['userDetails']['coachId'] != null){
            await FirebaseMessaging.instance.subscribeToTopic("coach_${response.data['data']['userDetails']['coachId']}");
          }else{
            await FirebaseMessaging.instance.subscribeToTopic("user_${response.data['data']['userDetails']['id']}");
          }

          Get.back();
          Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
        }
      }
    } on dio.DioError catch (e) {
      Get.back();
      LoadingOrError.errorloading(e.response!.data['responseException']['message'], "Try Again!");
      print(e.response!.data);
    }
  }



  static needTobeRefreshToken(String? token) async {
    try {
      final responseToken = await dioApi.post(ApiWellnestHost.refreshToken!, data: {"refreshToken": token!});
      if (responseToken.statusCode == 200) {
        await SharePref.setTokenRefreshed(responseToken.data['data']['accessToken']);
      }
    } on dio.DioError catch (e) {
      print(e.error);
    }
  }

  static registerAccount(data) async {
    LoadingOrError.loading();
    try {
      final response = await dioApi.post(ApiWellnestHost.register!, data: data);
      if (response.statusCode == 200) {
        print(response.data);
        await FirebaseMessaging.instance.subscribeToTopic('all');
        Get.back();
        Get.offNamedUntil(AppRouteName.onBoard!, (route) => false, parameters: data);
      }
    } on dio.DioError catch (e) {
      Get.back();
      LoadingOrError.errorloading(e.response!.data['responseException']['message'], "Try Again!");
      print(e.response!.data['responseException']['message']);
    }
  }

  static verifyEmailUser(data) async {
    LoadingOrError.loading();
    try {
      final response = await dioApi.post(ApiWellnestHost.verifyemail!, data: data);
      if (response.statusCode == 200) {
        await SharePref.setType(response.data['data']['userDetails']['type']);
        await SharePref.setProfile(jsonEncode(response.data['data']['userDetails']));
        await SharePref.setPassword(data['password']);
        await SharePref.setCoachId(response.data['data']['userDetails']['coachId'] != null
            ? response.data['data']['userDetails']['coachId']
            : "null");
        await FirebaseMessaging.instance.subscribeToTopic("user_${response.data['data']['userDetails']['id']}");    
        needTobeRefreshToken(response.data['data']['refreshToken']);
        Get.back();
        Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
      }
    } on dio.DioError catch (e) {
      Get.back();
      LoadingOrError.errorloading(e.response!.data['responseException']['message'], "Try Again!");
      print(e.response!.data);
    }
  }

  static resendVerificationEmail(email) async {
    final data = email;
    try {
      final response = await dioApi.post(ApiWellnestHost.resendverifyemail!,
          data: '"' + data + '"', options: dio.Options(headers: {"Content-Type": "application/json"}));
      if (response.statusCode == 200) {
        print('Email Sent');
      }
    } on dio.DioError catch (e) {
      print(e.response!.data);
    }
  }

  static logoutUser() async {
    // try {
    //   final resposne = await dioApi.get(
    //     ApiWellnestHost.logout!,
    //     options: dio.Options(headers: {
    //       "Authorization": "Bearer " + SharePref.getRefToken()!,
    //     }),
    //   );
      SharePref.removeAll();
      Get.offNamedUntil(AppRouteName.startingpage!, (route) => false);
      // print(resposne.statusCode);
    // } on dio.DioError catch (e) {
    //   LoadingOrError.errorloading("Something Went Wrong! Please Try Again later. ", "Try Again!");
    // }
  }

  static requestForgotPass(String? email) async {
    try {
      final response = await dioApi.post(
        ApiWellnestHost.requestForgotpassword!, data:'"'+email!+'"');
      if (response.statusCode == 200) {
        // Get.back();
        Get.to(() => EneterCodeNewPassword(email: email));
      }
    } on dio.DioError catch (e) {
      print(e.response!.data);
      LoadingOrError.errorloading(e.response!.data['responseException']['message'], "Try Again!");
    }
  }

  static setNewPassword(email, code, password) async {
    LoadingOrError.loading();
    var data = {"username": email, "code": code, "newPassword": password};

    try {
      final response = await dioApi.post(ApiWellnestHost.confirmPassword!, data: data);
      if (response.statusCode == 200) {
        Get.back();
        Get.offNamedUntil(AppRouteName.startingpage!, (route) => false);
        LoadingOrError.errorloading("Successfully Change Password of your account $email", "Change Password");
      }
    } on dio.DioError catch (e) {
      Get.back();
      LoadingOrError.errorloading("Something went Wrong! Please try Again.", "Change Password");
    }
  }
}

