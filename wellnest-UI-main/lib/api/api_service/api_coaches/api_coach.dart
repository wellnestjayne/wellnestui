import 'package:dio/dio.dart' as dio;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wellnest/api/api_string/api_string.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/model/coash_services.dart';
import 'package:wellnest/pages/profile/models/category_class.dart';
import 'package:wellnest/pages/profile/models/inclussion.dart';
import 'package:wellnest/pages/profile/models/inclussion_class.dart';
import 'package:wellnest/pages/profile/models/userCoach.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';
import 'package:wellnest/standard_widgets/unorderedList_widget.dart';

class ApiCoach {
  static final diox = dio.Dio();
  static final tokenOption = dio.Options(headers: {
    "Authorization": "Bearer " + SharePref.getRefToken()!,
  });

  static getCoachonId(String? data) async {
    LoadingOrError.loading();
    if (data! == "") {
      Get.back();

      LoadingOrError.normalDialog(
        messageTop: "Don't have Coach Profile yet?",
        //contentText: "We Recommend to create a profile coach details and fill up what's needed and your good to go.",
        content: Column(
          children: [
            Text(
              "Thank you for your interest to apply as a coach. Before we start with the application process, below are the general requirements for coaches:",
              textAlign: TextAlign.justify,
              style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 12.0),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: UnorderedList([
                'has certification from local or internatioanl certification courses.',
                ' has accomplished at least 50 coaching hours.',
              ]),
            ),
            Text(
              "Please confirm if you qualify to these requirements by ticking the Confirm button below.",
              textAlign: TextAlign.justify,
              style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 12.0),
            ),
          ],
        ),
        function: () {
          Get.back();
          Get.toNamed(AppRouteName.createCoach!);
        },
      );
    } else {
      try {
        final response = await diox.get(
          ApiWellnestHost.getCoachDetails! + data,
          options: dio.Options(headers: {
            "Authorization": "Bearer " + SharePref.getRefToken()!,
          }),
        );
        if (response.statusCode == 200) {
          print(response.data);
          Get.back();
          //UpdateCognitoUser.updateCognito("Coach");
          await SharePref.setType("Coach");
          Get.keys.clear();
          Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
        }
      } on dio.DioError catch (e) {
        print(e.response!.statusCode);
        Get.back();
        LoadingOrError.normalDialog(
          messageTop: "Don't have Coach Profile yet?",
          //contentText: "We Recommend to create a profile coach details and fill up what's needed and your good to go.",
          content: Column(
            children: [
              Text(
                "Thank you for your interest to apply as a coach. Before we start with the application process, below are the general requirements for coaches:",
                textAlign: TextAlign.justify,
                style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 12.0),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: UnorderedList([
                  'has certification from local or internatioanl certification courses.',
                  ' has accomplished at least 50 coaching hours.',
                ]),
              ),
              Text(
                "Please confirm if you qualify to these requirements by ticking the Confirm button below.",
                textAlign: TextAlign.justify,
                style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 12.0),
              )
            ],
          ),
          function: () {
            Get.back();
            Get.toNamed(AppRouteName.createCoach!);
          },
        );
      }
    }
  }

  static createOrEditCoach(data, type) async {
    try {
      LoadingOrError.loading();
      final response = await diox.post(
        ApiWellnestHost.createOrEdit!,
        data: data,
        options: dio.Options(headers: {
          "Authorization": "Bearer " + SharePref.getRefToken()!,
        }),
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(response.data);
        await SharePref.setCoachId(response.data['data']);
        final getCoach = SharePref.getCoachId();
        await FirebaseMessaging.instance.subscribeToTopic("coach_$getCoach");
        if (type == 1) {
          // create profile
          Get.back();
          Get.toNamed(AppRouteName.createCoach! + AppRouteName.uploadImageCoach!,
              parameters: {'data': response.data['data']});
        } else if (type == 2) {
          // edit profile
          Get.back();
          Get.toNamed(AppRouteName.createCoach! + AppRouteName.editImageProfile!,
              parameters: {'data': response.data['data']});
        }
      }
    } on dio.DioError catch (e) {
      LoadingOrError.errorloading(e.response!.data['responseException']['message'], "Try Again!");
      print(e.response!.data);
    }
  }

  static uploadImage({List<XFile> file = const <XFile>[], String? uuid}) async {
    LoadingOrError.loading();

    return Future.wait(file
        .map((file) async => await diox.post(ApiWellnestHost.uploadImage! + uuid!,
            data: dio.FormData.fromMap(
                {'file': await dio.MultipartFile.fromFile(file.path, filename: file.path.split("/").last)}),
            options: tokenOption))
        .take(file.length));

    // try{
    //   var formData = dio.FormData.fromMap({
    //     'file': await dio.MultipartFile.fromFile(
    //     file.path
    //     ,filename: file.path.split("/").last)
    //   });

    //  final op = dio.Options(
    //     headers: {
    //       "Authorization": "Bearer "+SharePref.getRefToken()!,
    //      // "Content-Type" : "multipart/form-data"
    //     }
    //   );
    //   final response = await diox.post(ApiWellnestHost.uploadImage!+uuid!,
    //   data: formData,
    //   queryParameters: {
    //     'id': SharePref.detailData['id']
    //   },
    //   options: op
    //   );
    //   if(response.statusCode == 200){
    //     print(response.data);
    //     Get.back();
    //     Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
    //   }
    // }on dio.DioError catch(e){
    //   print(e.response!.data);

    // }
  }

  static getCoachProfileById() async {
    final coachId = SharePref.getCoachId()!;
    try {
      final response = await diox.get(
        ApiWellnestHost.getCoachDetails! + coachId,
        options: dio.Options(headers: {
          "Authorization": "Bearer " + SharePref.getRefToken()!,
        }),
      );

      if (response.statusCode == 200) {
        var body = response.data['data'];
        print(response.data['data']);
        return Data.fromJson(body);
      }
    } on dio.DioError catch (e) {
      print(e.response!.data);
    }
  }

  static getAllCoachCategory() async {
    try {
      final response = await diox.get(ApiWellnestHost.getCategories!, options: tokenOption);
      if (response.statusCode == 200) {
        List<dynamic> list = response.data['data'];

        return list.map((e) => Datum.fromJson(e)).toList()
          ..sort(
            (Datum n1, Datum n2) => n1.name!.compareTo(n2.name!),
          );
      }
    } on dio.DioError catch (e) {
      print(e.error);
    }
  }

  static getAllByCategory(categoryId) async {
    try {
      final response = await diox.get(
        ApiWellnestHost.getAllByCategory! + categoryId,
        options: tokenOption,
      );
      if (response.statusCode == 200) {
        List<dynamic> list = response.data['data'];
        print(list);
        return list.map((e) => Service.fromJson(e)).toList()
          ..sort(
            (Service n1, Service n2) => n1.name!.compareTo(n2.name!),
          );
        ;
      }
    } on dio.DioError catch (e) {
      print(e.error);
    }
  }

  static getCoachInclussion() async {
    try {
      final response = await diox.get(ApiWellnestHost.getInclussion!, options: tokenOption);
      if (response.statusCode == 200) {
        List<dynamic> list = response.data['data'];
        print(response.data['data']);
        return list.map((e) => Inclusion.fromJson(e)).toList()
          ..sort((Inclusion i1, Inclusion i2) => i1.name!.compareTo(i2.name!));
      }
    } on dio.DioError catch (e) {
      print(e.error);
    }
  }

  static coachCreateService({
    data,
    List<Inclussion> listInclussion = const <Inclussion>[],
  }) async {
    LoadingOrError.loading();
    //  try{

    //Input sako

    var formData = dio.FormData.fromMap({
      "CoachId": SharePref.getCoachId(), //SharePref ra nako ID
      "CategoryId": data['category'],
      "Type": data['type'],
      "Name": data['servicename'],
      "Description": data['description'],
      //"SelectedInclusionIds[]": listInclussion,
      "CoachingTOSUrl": data['link'],
      "NumberOfSessions": data['numberSession'],
      "TimeInSeconds": data['time'],
      "Price": data['price'],
      "Discount": data['discount'],
      "CanCancelFreeInDays": data['canceldays'],
      "BannerFile": await dio.MultipartFile.fromFile(data['imagePath'].toString(), filename: "bannerImage"),
      "CoachingTOSFile": await dio.MultipartFile.fromFile(data['coach_file'].toString(), filename: "fileTerm"),
      "CanRefund": data['refund'],
    });
    listInclussion.forEach((e) {
      formData.fields.add(MapEntry('SelectedInclusionIds[]', e.id.toString()));
    });

    final response = await diox.post(ApiWellnestHost.createCoachServices!, data: formData, options: tokenOption);

    if (response.statusCode == 200) {
      print(response.data);
      Get.back();
      //  controller.gotoCancelationx();
    }

    return response.statusCode;

    //  }on dio.DioError catch(e){
    //    Get.back();
    //   print(e.error);
    //    print(e.response!.data);

    //  }
  }

  static coachTrainingServices({
    data,
    List<Inclussion> listInclussion = const <Inclussion>[],
  }) async {
    LoadingOrError.loading();
    //  try{

    var formData = dio.FormData.fromMap({
      "CoachId": SharePref.getCoachId(), //SharePref ra nako ID
      "CategoryId": data['category'],
      "Type": data['type'],
      "Name": data['servicename'],
      "Description": data['description'],
      "TrainingTOSUrl": data['link'],
      "Price": data['price'],
      "Discount": data['discount'],
      "CanCancelFreeInDays": data['canceldays'],
      "MaximumPeople": data['maximun'],
      "BannerFile": await dio.MultipartFile.fromFile(data['imagePath'].toString(), filename: "bannerImage"),
      "CoachingTOSFile": await dio.MultipartFile.fromFile(data['training_file'].toString(), filename: "fileTerm"),
      "CanRefund": data['refund'],
    });
    listInclussion.forEach((e) {
      formData.fields.add(MapEntry('SelectedInclusionIds[]', e.id.toString()));
    });

    final response = await diox.post(ApiWellnestHost.createCoachServices!, data: formData, options: tokenOption);

    if (response.statusCode == 200) {
      print(response.data);
      Get.back();
      //  controller.gotoCancelationx();
    }

    return response.statusCode;
  }

  static coachEditServices({data}) async {
    try {
      LoadingOrError.loading();
      print("BBB ${data['serviceName']}");
      var formData = dio.FormData.fromMap({
        "Id": data['id'],
        "CoachId": SharePref.getCoachId(),
        "CategoryId": data['category'],
        "Type": data['type'],
        "Name": data['serviceName'],
        "Description": data['description'],
        "BannerUrl": data['imagePath'],
        "BannerFile": await dio.MultipartFile.fromFile(data['imageFile'].toString(), filename: "bannerImage"),
        "NumberOfSessions": data['numberOfSession'],
        "Price": data['price'],
        "Discount": data['discount'],
        "TimeInSeconds": Duration(hours: 1).inSeconds,
        "CanRefund": data['refund'],
      });

      final response = await diox.post(
        ApiWellnestHost.createCoachServices!,
        data: formData,
        options: tokenOption,
      );

      if (response.statusCode == 200) {
        print(response.data);
        Get.back();
        //  controller.gotoCancelationx();
      }

      return response.statusCode;
    } on dio.DioError catch (e) {
      print('HELLO ${e.response!.data}');
    }
  }

  static setCoachAvalability({dynamic data}) async {
    final coachId = SharePref.getCoachId();

    var datas = {
      "coachId": data['coachId'],
      "day": data['day'],
      "startTime": data['startTime'],
      "endTime": data['endTime'],
      "isActive": data['isActive']
    };

    try {
      final response = await diox.post(
        ApiWellnestHost.setAvalability!,
        data: datas,
        options: tokenOption,
      );
      return response.statusCode;
    } on dio.DioError catch (e) {
      return e.response!.data;
    }
  }
}
