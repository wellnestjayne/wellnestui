import 'package:dio/dio.dart' as dio;
import 'package:get/get.dart';
import 'package:wellnest/api/api_string/api_string.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/coach_user_schedule.dart';
import 'package:wellnest/model/coach_view_upcomming_session.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';

class ApiCoachUpcommingSession {
  static final diox = dio.Dio();
  static final tokenOption = dio.Options(headers: {
    "Authorization": "Bearer " + SharePref.getRefToken()!,
  });

  static getAllupcommingSessionwithFilter() async {
    final idCoach = SharePref.getCoachId()!;
    try {
      final response = await diox.get(ApiWellnestHost.upcommingSessionCoach! + idCoach, options: tokenOption);
      List<dynamic> list = response.data['data'];
      return list
          .where((e) => e['status'] == "Pending" || e['status'] == "Confirmed")
          .where((el) => DateTime.parse(el['startTime'].toString()).isAfter(DateTime.now()))
          .map((elm) => UpcommingSession.fromJson(elm))
          .toList()
        ..sort((UpcommingSession up1, UpcommingSession up2) => up1.startTime!.compareTo(up2.startTime!));
      // return list.map((e) => UpcommingSession.fromJson(e)).toList()
      // ..sort((UpcommingSession up1, UpcommingSession up2)=>
      //  up1.startTime!.compareTo(up2.startTime!)
      // );
    } on dio.DioError catch (e) {
      print(e.response!.data);
    }
  }

  static getAllBookingByUser() async {
    final userId = SharePref.detailData['id']!;
    try {
      final response = await diox.get(ApiWellnestHost.getUserBookEvent! + userId, options: tokenOption);
      List<dynamic> list = response.data['data'];
      print(list);
      return list
          .where((e) => e['status'] == "Pending" || e['status'] == "Confirmed")
          .where((el) => DateTime.parse(el['startTime'].toString()).isAfter(DateTime.now()))
          .map((elm) => UpcommingSession.fromJson(elm))
          .toList()
        ..sort((UpcommingSession up1, UpcommingSession up2) => up1.startTime!.compareTo(up2.startTime!));
    } on dio.DioError catch (e) {
      print(e.response!.data);
    }
  }

  static getCoachById(String? id) async {
    try {
      final response = await diox.get(ApiWellnestHost.getCoachDetails! + id!, options: tokenOption);
      if (response.statusCode == 200) {
        var body = response.data['data'];
        return CoachUserSchedule.fromJson(body);
      }
    } on dio.DioError catch (e) {}
  }

  static getConfirmBook({String? id}) async {
    try {
      final response = await diox.get(ApiWellnestHost.bookConfirm! + id!, options: tokenOption);
      print(response.data);
      return response.statusCode;
    } on dio.DioError catch (e) {
      print(e.response!.data);
      return e.response!.data;
    }
  }

  static getCancelBook({String? id}) async {
    print("ID $id");
    try {
      print("ID $id");
      final response = await diox.get(ApiWellnestHost.bookCancel! + id!, options: tokenOption);
      print(response.data);
      return response.statusCode;
    } on dio.DioError catch (e) {
      print(e.response!.data);
      return e.response!.data;
    }
  }
}
