import 'package:dio/dio.dart' as dio;
import 'package:wellnest/api/api_string/api_string.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/coach_featured_detail.dart';
import 'package:wellnest/model/coash_services.dart';
import 'package:wellnest/model/feature_class_view.dart';

class CoachGetLists {
  static final dios = dio.Dio();
  static final tokenOption = dio.Options(headers: {
    "Authorization": "Bearer " + SharePref.getRefToken()!,
  });

  static coachgetAll() async {
    try {
      final response = await dios.get(ApiWellnestHost.getCoachAll!, options: tokenOption);

      if (response.statusCode == 200) {
        List<dynamic> list = response.data['data'];
        return list.map((e) => Coachee.fromJson(e)).toList()
          ..sort((Coachee c1, Coachee c2) => c1.name!.compareTo(c2.name!));
      }
    } on dio.DioError catch (e) {
      print(e.response!.data);
    }
  }

  static getCoachDetailFeature(String? id) async {
    try {
      final response = await dios.get(ApiWellnestHost.getCoachDetails! + id!, options: tokenOption);
      if (response.statusCode == 200) {
        var body = response.data['data'];
        return CoachDetailFeatured.fromJson(body);
      }
    } on dio.DioError catch (e) {}
  }

  static getAllServicesOfCoach(String? id) async {
    try {
      final response = await dios.get(ApiWellnestHost.getCoachServices! + id!, options: tokenOption);

      if (response.statusCode == 200) {
        List<dynamic> list = response.data['data'];
        return list.map((e) => Service.fromJson(e)).toList();
      }
    } on dio.DioError catch (e) {}
  }
}
