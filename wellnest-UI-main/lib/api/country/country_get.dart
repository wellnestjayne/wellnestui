

import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:wellnest/model/country.dart';

class GetCountries{


  static  loadAssets() async{
    //return 
    final data =await rootBundle.loadString("lib/assets/country.json");
    final body = jsonDecode(data.toString());
    List<dynamic> list = body; 
    return list.map((e) => Country.fromJson(e))
    .toList()..sort((Country c1 , Country c2)=> c1.name!.compareTo(c2.name!));
   }

// "currency": "USD,ZAR,BWP,GBP,AUD,CNY,INR,JPY",
  // static List parseIt(String jsonString){
  //   List jsonded = jsonDecode(jsonString);
  //   return jsonded;
  // }

  // static Future loadCountry() async{
  //   String json = await loadAssets();
  //   parseIt(json)..forEach((element)=>
  //   countrys.add(new Country.fromJson(element))
  //   );
 // } 


}