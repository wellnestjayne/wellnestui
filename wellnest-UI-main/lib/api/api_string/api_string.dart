class ApiWellnestHost {
  static final String? urlbase = "http://ec2-18-142-159-98.ap-southeast-1.compute.amazonaws.com";

  //User
  static final String? login = "$urlbase/api/User/login";
  static final String? register = "$urlbase/api/User/Register";
  static final String? verifyemail = "$urlbase/api/User/verify-email";
  static final String? refreshToken = "$urlbase/api/User/refresh-token";
  static final String? logout = "$urlbase/api/User/logout";
  static final String? resendverifyemail = "$urlbase/api/User/resend-verify-email";
  static final String? requestForgotpassword = "$urlbase/api/User/request-forgotpassword";
  static final String? confirmPassword = "$urlbase/api/User/confirm-forgotpassword";

  //Coach
  static final String? getCoachDetails = "$urlbase/api/Coaches/get/";
  static final String? createOrEdit = "$urlbase/api/Coaches/createOrEdit";
  static final String? uploadImage = "$urlbase/api/Coaches/upload-photo/";
  static final String? getCoachAll = "$urlbase/api/Coaches/getAll";
  static final String? getCoachServices = "$urlbase/api/CoachServices/getAllByCoach/";
  static final String? getAllByCategory = "$urlbase/api/CoachServices/getAllByCategory/";
  static final String? setAvalability= "$urlbase/api/Coaches/createOrEditAvailabilityTimes";

  //Create Coach Services
  static final String? getCategories = "$urlbase/api/CoachServices/category/getAll";
  static final String? getInclussion = "$urlbase/api/CoachServices/inclusion/getAll";
  static final String? createCoachServices = "$urlbase/api/CoachServices/createOrEdit";

  //BookSession
  static final String? bookSession = "$urlbase/api/Bookings/book-service";
  static final String? getUserBookEvent = "$urlbase/api/Bookings/getAllByUser/";
  static final String? getScheduleTime = "$urlbase/api/Coaches/get-availability-times/";


  //UpcommingSession
  static final String? upcommingSessionCoach = "$urlbase/api/Bookings/getAllByCoach/";
  static final String? bookConfirm ="$urlbase/api/Bookings/confirm/";
  static final String? bookCancel = "$urlbase/api/Bookings/cancel/";

}
