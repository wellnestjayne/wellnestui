

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';

class NewsfeeTextmultiLine extends StatelessWidget {

  final TextEditingController? controller; 
  final Function()? ontap;

  const NewsfeeTextmultiLine({Key? key, this.controller, this.ontap}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: TextFormField(
          maxLines: null,
          expands:true,
          keyboardType: TextInputType.multiline,
          controller: controller,
          onTap: ontap,
          decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: WellNestColor.wncBlue)
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: WellNestColor.wncBlue)
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: WellNestColor.wncBlue)
          ),
          filled: true,
          fillColor: WellNestColor.wncWhite,
          hintText: "Hey, What do you want to talk about?",
          hintStyle: TextStyle(
            color: WellNestColor.wncLightgrey,
            fontSize: 14.sp,
          ),
        ),
          autofocus: false,

        ),
    );
  }
}