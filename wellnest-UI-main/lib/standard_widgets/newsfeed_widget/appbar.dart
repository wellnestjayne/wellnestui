import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:intl/intl.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppbarNewsfeedw extends StatelessWidget 
//implements PreferredSizeWidget
{

    final double? sizeHiegh;
    final bool? expand;
    final TextEditingController? controller; 
    final Function()? ontap;
    final Function()? outside;
    final String? hintext;

  const AppbarNewsfeedw({Key? key, this.sizeHiegh, this.expand, this.controller, this.ontap, this.outside, this.hintext}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final f = DateFormat("MMMM d y");
    return GestureDetector(
      onTap: outside,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        decoration: BoxDecoration(
          color: WellNestColor.wncAquaBlue,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)
          ),
        ),
        height: expand! ? sizeHiegh : 400,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 40.h,
                      width: 40.w,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: WellNestColor.wncWhite),
                        image: DecorationImage(
                          image: AssetImage(WellnestAsset.testimonial))
                      ),
                    ),
                    SizedBox(width: 50.w,),
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text("Newsfeed",
                            style: TextStyle(
                              fontSize: 16.sp,
                              fontFamily: 'NowMed',
                              shadows: [
                                Shadow(
                                  offset: Offset(3,2),
                                  blurRadius: 2,
                                  color: WellNestColor.wncGrey.withOpacity(0.4)
                                )
                              ],
                              color: WellNestColor.wncWhite,
                            ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: Text(f.format(DateTime.now()),
                              style: WellNestTextStyle.nowLight(WellNestColor.wncWhite, 10.sp)
                              ),
                            )
                          ],
                        ),
                      ),),
                      SizedBox(width: 5.w,),
                      Container(
                        height: 40.h,
                        width: 40.w,
                        child: Image.asset(WellnestAsset.bell),
                      ),
                      SizedBox(width: 5.w,),
                      // Container(
                      //   height: 40.h,
                      //   width: 40.w,
                      //   child: Image.asset(WellnestAsset.message),
                      // ),
                  ],
                ),
              ),
              Visibility(
                visible: expand!,
                child: Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Container(
                      height: 50,
                   child: Row(
                     children: [
                       FaIcon(FontAwesomeIcons.image,
                       color: WellNestColor.wncWhite,
                       size: 30.h,
                       ),
                       SizedBox(width: 10.w,),
                       FaIcon(FontAwesomeIcons.camera,
                       color: WellNestColor.wncWhite,
                       size: 30.h,
                       ),
                       SizedBox(width: 10.w,),
                       FaIcon(FontAwesomeIcons.video,
                       color: WellNestColor.wncWhite,
                       size: 30.h,
                       ),
                       Expanded(
                         child: Text("POST",
                         textAlign: TextAlign.right,
                         style: WellNestTextStyle.nowMedium(WellNestColor.wncInstaWhite, 16.sp)
                         ),
                       )
                       
                     ],
                   ),
                  ),
                ),
              ),
              Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(3, 2),
                    blurRadius: 3,
                    spreadRadius: 3,
                    color: WellNestColor.wncLightgrey.withOpacity(0.1)
                  )
                ]
              ),
              child: TextFormField(
                autofocus: false,
                textAlignVertical: TextAlignVertical.top,
                maxLines: null,
                expands:true,
                keyboardType: TextInputType.multiline,
                controller: controller,
                textInputAction: TextInputAction.none,
                onTap: ontap,
                decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: WellNestColor.wncAquaBlue)
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: WellNestColor.wncAquaBlue)
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: WellNestColor.wncAquaBlue)
                ),
                filled: true,
                fillColor: WellNestColor.wncWhite,
                hintText: hintext!,
                hintStyle: TextStyle(
                  color: expand! ? Colors.transparent : WellNestColor.wncLightgrey,
                  fontSize: 16.sp,
                ),
              ),
                  

              ),
            ),
          ),
      ) ,
            ],
          ),
          ),
      ),
    );
  }

  // @override
  // // TODO: implement preferredSize
  // Size get preferredSize => Size.fromHeight(sizeHiegh!);
}