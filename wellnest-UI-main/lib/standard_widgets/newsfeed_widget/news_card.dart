import 'package:emoji_picker_flutter/emoji_picker_flutter.dart' as emo;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/newsfeed/controller/newsfeed_controller.dart';

class NewsCardw extends StatelessWidget {
  final Function()? press;
  final bool? showIt;
  final Function()? icontap;
  final Function()? sendtap;
  final bool? iconSuffix;

  NewsCardw(
      {Key? key,
      this.press,
      this.showIt,
      this.icontap,
      this.sendtap,
      this.iconSuffix})
      : super(key: key);

  final controller = Get.put(NewsFeedController());

  @override
  Widget build(BuildContext context) {
    return SliverList(
        delegate: SliverChildListDelegate([
      Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          decoration: BoxDecoration(
              color: WellNestColor.wncWhite,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    blurRadius: 2,
                    spreadRadius: 3,
                    color: WellNestColor.wncLightgrey.withOpacity(0.1))
              ]),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            textDirection: TextDirection.ltr,
                            children: [
                              Text(
                                "James Smith",
                                style: TextStyle(
                                    color: WellNestColor.wncGrey,
                                    fontSize: 22.sp),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Text(
                                "3 mins ago",
                                style: TextStyle(
                                    color: WellNestColor.wncLightgrey
                                        .withOpacity(0.5),
                                    fontSize: 12.sp),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: 35.h,
                        width: 35.w,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncLightgrey.withOpacity(0.2)),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.solidBookmark,
                            color: WellNestColor.wncWhite,
                            size: 18,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Container(
                  height: 175.h,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.contain,
                          image: AssetImage(WellnestAsset.thirdImageWellNest))),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FaIcon(
                        FontAwesomeIcons.solidHeart,
                        color: WellNestColor.wncLightgrey.withOpacity(0.2),
                        size: 20.h,
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      Text(
                        "23 Likes",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: WellNestColor.wncLightgrey, fontSize: 16.sp),
                      ),
                      SizedBox(
                        width: 15.w,
                      ),
                      FaIcon(
                        FontAwesomeIcons.solidCommentAlt,
                        color: WellNestColor.wncLightgrey.withOpacity(0.2),
                        size: 20.h,
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      Text(
                        "12 Comments",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: WellNestColor.wncLightgrey, fontSize: 16.sp),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Divider(
                  thickness: 1,
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Obx(
                  ()=> Container(
                    child: Row(
                      children: [
                        Spacer(),
                        GestureDetector(
                          onTap: ()=>controller.like(),
                          child: Container(
                            child: Row(
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.solidThumbsUp,
                                  color: 
                                  controller.directLike.value ?
                                  WellNestColor.wncBlue
                                  : WellNestColor.wncLightgrey,
                                  size: 18.h,
                                ),
                                SizedBox(
                                  width: 10.w,
                                ),
                                Text(
                                  "Like",
                                  textAlign: TextAlign.center,
                                 style: WellNestTextStyle.nowRegular(
                                   controller.directLike.value ?
                                    WellNestColor.wncBlue
                                  : WellNestColor.wncLightgrey
                                   , 16.sp)
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15.w,
                        ),
                        InkWell(
                          splashColor: WellNestColor.wncAquaBlue,
                          onTap: press,
                          child: Container(
                            child: Row(
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.solidCommentAlt,
                                  color:
                                    controller.trial.value ?
                                    WellNestColor.wncBlue 
                                  : WellNestColor.wncLightgrey,
                                  size: 18.h,
                                ),
                                SizedBox(
                                  width: 10.w,
                                ),
                                Text(
                                  "Comment",
                                  textAlign: TextAlign.center,
                                  style: WellNestTextStyle.nowRegular(
                                    controller.trial.value ?
                                    WellNestColor.wncBlue 
                                  : WellNestColor.wncLightgrey
                                    , 16.sp)
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15.w,
                        ),
                        Container(
                          child: Row(
                            children: [
                              FaIcon(
                                FontAwesomeIcons.share,
                                color: WellNestColor.wncLightgrey,
                                size: 18.h,
                              ),
                              SizedBox(
                                width: 10.w,
                              ),
                              Text(
                                "Share",
                                textAlign: TextAlign.center,
                              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 16.sp)
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Divider(
                  thickness: 1,
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Visibility(
                  visible: showIt!,
                  child: AnimatedContainer(
                    width: MediaQuery.of(context).size.width,
                    duration: Duration(milliseconds: 450),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Wrap(
                            textDirection: TextDirection.ltr,
                            crossAxisAlignment: WrapCrossAlignment.start,
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: WellNestColor.wncInstaWhite,
                                      borderRadius: BorderRadius.circular(15)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      textDirection: TextDirection.ltr,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Ricardo Butits",
                                          style: TextStyle(
                                              color: WellNestColor.wncLightgrey,
                                              fontSize: 12.sp),
                                        ),
                                        SizedBox(
                                          height: 5.h,
                                        ),
                                        Text(
                                          "Learn essential skills ",
                                          style: TextStyle(
                                              color: WellNestColor.wncGrey,
                                              fontSize: 14.sp),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Wrap(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: WellNestColor.wncInstaWhite,
                                    borderRadius: BorderRadius.circular(15)),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    textDirection: TextDirection.ltr,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Ricardo Butits",
                                        style: TextStyle(
                                            color: WellNestColor.wncLightgrey,
                                            fontSize: 12.sp),
                                      ),
                                      SizedBox(
                                        height: 5.h,
                                      ),
                                      Text(
                                        "Learn essential skills from industry experts with short and concise learning sessions created",
                                        style: TextStyle(
                                            color: WellNestColor.wncGrey,
                                            fontSize: 14.sp),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Visibility(
                  visible: showIt!,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Row(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              child: SizedBox(
                            height: 50.h,
                            child: TextFormField(
                              // style: TextStyle(
                              //   fontSize:15.sp
                              // ),
                              autofocus: false,
                              maxLines: null,
                              expands:true,
                              controller: controller.newsFeed,
                              decoration: InputDecoration(
                                contentPadding: 
                                const EdgeInsets.all(8.0),
                                suffixIconConstraints:
                                    BoxConstraints(maxHeight: 50, maxWidth: 50),
                                suffixIcon: InkWell(
                                  onTap: icontap,
                                  child: Center(
                                    //  padding: const EdgeInsets.only(
                                    //   right: 20,
                                    // ),
                                    child: FaIcon(
                                      FontAwesomeIcons.solidSmile,
                                      color: WellNestColor.wncAquaBlue,
                                      size: 25.h,
                                    ),
                                  ),
                                ),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(100),
                                    borderSide: BorderSide(
                                        color: WellNestColor.wncWhite)),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(100),
                                    borderSide: BorderSide(
                                        color: WellNestColor.wncWhite)),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(100),
                                    borderSide: BorderSide(
                                        color: WellNestColor.wncWhite)),
                                filled: true,
                                fillColor: WellNestColor.wncInstaWhite,
                                hintText: "Write A Comment!",
                                hintStyle: TextStyle(
                                  color: WellNestColor.wncLightgrey,
                                  fontSize: 14.sp,
                                ),
                              ),
                            ),
                          )),
                          SizedBox(
                            width: 10.w,
                          ),
                          InkWell(
                            onTap: sendtap,
                            child: Center(
                              child: Text(
                                "Post",
                                style:
                                    TextStyle(color: WellNestColor.wncAquaBlue),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: iconSuffix! ?  showIt! :iconSuffix! ,
                  child: SizedBox(
                    height: 200.h,
                    // decoration: BoxDecoration(
                    //   borderRadius: BorderRadius.circular(10)
                    // ),
                    child: emo.EmojiPicker(
                      onEmojiSelected: (category, emoji) {
                        controller.emojiSel(emoji);
                      },
                      onBackspacePressed: controller.onBackpress(),
                      config: emo.Config(
                        columns: 7,
                        emojiSizeMax: 32.0.h,
                        verticalSpacing: 0,
                        horizontalSpacing: 0,
                        initCategory: emo.Category.RECENT,
                        bgColor: WellNestColor.wncInstaWhite,
                        indicatorColor: WellNestColor.wncAquaBlue,
                        iconColor: WellNestColor.wncLightgrey,
                        iconColorSelected: WellNestColor.wncAquaBlue,
                        progressIndicatorColor: WellNestColor.wncAquaBlue,
                        categoryIcons: const emo.CategoryIcons(),
                        showRecentsTab: true,
                        recentsLimit: 28,
                        noRecentsText: "No Recents",
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      //Half
    
    ]));
  }
}
