

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
class LineChoiceForgotorSome extends StatelessWidget {
  
  final Function()? forgot;
  final Function()? some;
  final String? title1;
  final String? title2;

  const LineChoiceForgotorSome({Key? key, 
  this.forgot, 
  this.some, 
  this.title1, 
  this.title2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
     width: 318.w,
      child: Row(
        children: [
          InkWell(
            onTap: forgot,
            child: Text(title1!,
            style: TextStyle(
              color: WellNestColor.wncBlue,
              decoration: TextDecoration.underline,
              fontSize: 12.sp
            ),
            ),
            ),
            Expanded(
              child:Container()),
           InkWell(
             onTap: some,
             child: Text(title2!,
             style: TextStyle(
               color: WellNestColor.wncBlue,
               decoration: TextDecoration.underline,
               fontSize: 12.sp
             ),
             )
             ),
        ],
      ),
    );
  }
}