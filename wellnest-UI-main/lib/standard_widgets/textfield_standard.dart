import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class TextFieldWellnestw extends StatelessWidget {
  final TextEditingController? controller;
  final String? Function(String?)? validator;
  final TextInputType? keyboardType;
  final bool? obsecure;
  final void Function(String?)? onchanged;
  final void Function(String?)? onsaved;
  final int? maxText;
  final String? hintText;
  final Widget? child;
  final double? hg;
  final TextAlignVertical? vertical;

  const TextFieldWellnestw({
    Key? key,
    this.controller,
    this.validator,
    this.keyboardType,
    this.obsecure,
    this.onchanged,
    this.onsaved,
    this.maxText,
    this.hintText,
    this.hg,
    this.vertical,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: hg == null ? 40 : hg,
      width: 349,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), boxShadow: [
        BoxShadow(color: WellNestColor.wncLightgrey.withOpacity(0.2), blurRadius: 1, offset: Offset(3, 4))
      ]),
      child: TextFormField(
        textCapitalization: TextCapitalization.sentences,
        autofocus: false,
        // expands: true,
        // maxLines: null,
        // minLines: null,
        //textAlignVertical: vertical == null ? TextAlignVertical.center : vertical,
        controller: controller!,
        onSaved: onsaved,
        onChanged: onchanged,
        keyboardType: keyboardType,
        obscureText: obsecure!,
        validator: validator,
        style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
        decoration: InputDecoration(
            prefixIcon: child,
            counterText: '',
            contentPadding: const EdgeInsets.only(top: 5.0, bottom: 5.0, right: 5.0, left: 10),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10), borderSide: BorderSide(color: WellNestColor.wncBlue)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10), borderSide: BorderSide(color: WellNestColor.wncBlue)),
            filled: true,
            fillColor: WellNestColor.wncWhite,
            hintText: hintText!,
            hintStyle: WellNestTextStyle.nowMedium(WellNestColor.wncGrey.withOpacity(0.3), 15.0)),
      ),
    );
  }
}
