import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/model/coach_user_schedule.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/booking_widgets/booking_schedule.dart';
import 'package:intl/intl.dart';

class ViewListCalendarw extends StatelessWidget {
  ViewListCalendarw({this.startTime, this.endTime, this.coachId});
  final String? startTime;
  final String? endTime;
  final String? coachId;

  final bookController = Get.put(BookingController());
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Column(
          children: [
            Container(
              width: 100.w,
              height: 10,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: WellNestColor.wncLightgrey.withOpacity(0.5)),
            ),
            SizedBox(
              height: 20.h,
            ),
            Container(
              height: 400.h,
              child: Obx(
                () => bookController.listBookingByUser.length == 0 || bookController.listBookingByUser.isEmpty
                    ? Center(
                        child: Text(
                        'You have no booked schedule Today',
                        style: TextStyle(fontSize: 20.sp, color: WellNestColor.wncAquaBlue),
                      ))
                    : BookingSchedule(
                        startTime: startTime,
                        endTime: endTime,
                        title: '',
                        coachId: coachId,
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
