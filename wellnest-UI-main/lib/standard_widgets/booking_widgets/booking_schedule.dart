import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';
import 'package:wellnest/route/route_page.dart';

class BookingSchedule extends StatelessWidget {
  BookingSchedule({Key? key, this.startTime, this.endTime, this.coachId, this.title, this.press, this.imageUrl})
      : super(key: key);

  final String? startTime;
  final String? endTime;
  final String? imageUrl;
  final String? coachId;
  final String? title;
  final Function()? press;

  final bookController = Get.put(BookingController());
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Obx(
        () => Container(
          height: 70,
          child: ListView.builder(
            itemCount: bookController.coachUserDetails.length,
            itemBuilder: (context, index) => bookController.coachUserDetails.isEmpty
                ? Container()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 15.w),
                      Text(
                        startTime!,
                        style: TextStyle(color: WellNestColor.wncAquaBlue),
                      ),
                      SizedBox(width: 15.w),
                      Expanded(
                        child: GestureDetector(
                          onTap: () =>
                              Get.toNamed(AppRouteName.homeView! + AppRouteName.cancelBooking!, id: 1, arguments: {
                            'id': coachId,
                            'name': bookController.coachUserDetails[index].name!,
                            'photo': bookController.coachUserDetails[index].photoUrls![0],
                          }),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: WellNestColor.wncWhite,
                              boxShadow: [
                                BoxShadow(
                                  spreadRadius: 4,
                                  blurRadius: 4,
                                  color: WellNestColor.wncGrey.withOpacity(0.1),
                                )
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                child: Row(
                                  children: [
                                    Container(
                                      height: 35.h,
                                      width: 5.w,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: WellNestColor.wncAquaBlue,
                                      ),
                                    ),
                                    SizedBox(width: 10.w),
                                    Container(
                                      width: 35,
                                      height: 35,
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            spreadRadius: 1,
                                            blurRadius: 1,
                                            offset: Offset(2, 6),
                                            color: WellNestColor.wncGrey.withOpacity(0.1),
                                          )
                                        ],
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          alignment: Alignment.topCenter,
                                          image: NetworkImage(bookController.coachUserDetails[index].photoUrls![0]),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 10.w),
                                    Container(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            bookController.coachUserDetails[index].name!,
                                            style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            "$startTime - $endTime",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: WellNestColor.wncGrey.withOpacity(0.5),
                                                fontWeight: FontWeight.normal),
                                          ),
                                          Text(
                                            title!,
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: WellNestColor.wncGrey.withOpacity(0.5),
                                                fontWeight: FontWeight.normal),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
