import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class StandardAppbaruse extends StatelessWidget implements PreferredSizeWidget {
  final double? topSize = 90.h;
  final Color? color;
  final String? title;
  final Widget? title2;
  final Function()? function;

  StandardAppbaruse({Key? key, this.color, this.title, this.function, this.title2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: topSize!,
      decoration: BoxDecoration(
        color: color!,
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            InkWell(
              onTap: function!,
              child: Container(
                width: 50,
                height: topSize!,
                child: Center(
                  child: FaIcon(
                    FontAwesomeIcons.chevronLeft,
                    color: WellNestColor.wncWhite,
                    size: 25,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 25.w,
            ),
            title2 ??
                Text(
                  title!,
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 18.sp),
                )
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(90);
}
