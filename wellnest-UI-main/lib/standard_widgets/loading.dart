import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class LoadingOrError {
  static loading() {
    Get.dialog(
      Center(
        child: Container(
          height: 80,
          width: 80,
          decoration: BoxDecoration(color: WellNestColor.wncWhite, borderRadius: BorderRadius.circular(10)),
          child: Center(
            child: SpinKitWave(
              color: WellNestColor.wncAquaBlue,
              size: 30,
              //controller: AnimationController(vsync: this),
            ),
          ),
        ),
      ),
      barrierColor: WellNestColor.wncAquaBlue.withOpacity(0.1),
      barrierDismissible: true,
    );
  }

  static errorloading(String? message, String? title) {
    Get.defaultDialog(
        backgroundColor: WellNestColor.wncWhite,
        barrierDismissible: false,
        title: title!,
        titleStyle: WellNestTextStyle.nowBold(WellNestColor.wncGrey, 18.0),
        onConfirm: () => Get.back(),
        confirmTextColor: WellNestColor.wncWhite,
        buttonColor: WellNestColor.wncAquaBlue,
        content: Container(
          height: 90,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    message!,
                    maxLines: 10,
                    textAlign: TextAlign.center,
                    style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 12.0),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  static logoutDialog(Function()? press) {
    return Get.defaultDialog(
        titlePadding: EdgeInsets.only(top :25.0,right: 15,left:15),
        contentPadding: EdgeInsets.all(10),
        backgroundColor: WellNestColor.wncWhite,
        barrierDismissible: true,
        title: "Are you sure want to logout?",
        content: Text(""),
        titleStyle: WellNestTextStyle.nowBold(WellNestColor.wncGrey, 12.0),
        // onConfirm: press,
        confirmTextColor: WellNestColor.wncWhite,
        buttonColor: WellNestColor.wncAquaBlue,
        confirm: Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: BlueButtonStandard(
          height: 40,
          width: 80,
          title: "Logout",
          sizeText: 13.0,
          textColor: WellNestColor.wncBlue,
          back: WellNestColor.wncWhite,
          border: WellNestColor.wncWhite,
          bluebutton: press!,
        ),
      ),
        //onCancel: ()=>Get.back(),
        //cancelTextColor: WellNestColor.wncGrey,
        // cancel: GestureDetector(
        //   onTap: () => Get.back(),
        //   child: Padding(
        //     padding: const EdgeInsets.only(top: 10, right: 15),
        //     child: Text(
        //       "Go Back!",
        //       style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 11.0),
        //     ),
        //   ),
        // ),
        // textConfirm: "Logout",
        // textCancel: "Go Back!",
        // content: Container(
        //   height: 50,
        //   child: Padding(
        //     padding: const EdgeInsets.all(10),
        //     child: SingleChildScrollView(
        //       child: Column(
        //         mainAxisAlignment: MainAxisAlignment.center,
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: [
        //           Text(
        //             "Please Check some thing you might forgotten.",
        //             maxLines: 10,
        //             textAlign: TextAlign.center,
        //             style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 12.0),
        //           )
        //         ],
        //       ),
        //     ),
        //   ),
        // )
        );
  }

  static normalDialog({String? messageTop, String? contentText, Widget? content, Function()? function}) {
    return Get.defaultDialog(
      barrierDismissible: false,
      backgroundColor: WellNestColor.wncWhite,
      title: messageTop!,
      titleStyle: WellNestTextStyle.nowBold(WellNestColor.wncGrey, 12.0),
      titlePadding: const EdgeInsets.only(top: 25),
      contentPadding: const EdgeInsets.all(15),
      content: Container(
        //height: 80,
        width: 240,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            content ??
                Text(
                  contentText!,
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 12.0),
                ),
          ],
        ),
      ),
      confirm: BlueButtonStandard(
        bluebutton: function,
        title: "Confirm",
        back: WellNestColor.wncBlue,
        border: WellNestColor.wncBlue,
        textColor: WellNestColor.wncWhite,
        sizeText: 12.0,
        width: 80,
        height: 50,
      ),
      cancel: BlueButtonStandard(
        bluebutton: () => Get.back(),
        title: "Cancel",
        back: WellNestColor.wncWhite,
        border: WellNestColor.wncWhite,
        textColor: WellNestColor.wncBlue,
        sizeText: 12.0,
        width: 80,
        height: 50,
      ),
    );
  }

  static snackBarMessage({String? title, String? message, Widget? icon}) {
    Get.snackbar(title!, message!,
        icon: Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: icon!,
        ),
        snackPosition: SnackPosition.TOP,
        backgroundColor: WellNestColor.wncBlue,
        borderRadius: 5,
        colorText: WellNestColor.wncWhite,
        shouldIconPulse: true,
        margin: EdgeInsets.all(16),
        dismissDirection: SnackDismissDirection.HORIZONTAL,
        forwardAnimationCurve: Curves.easeInOutBack,
        isDismissible: true);
  }

  static dialogWithNorm({
    Function()? yes,
    Function()? no,
    String? title,
    String? message,
  }) {
    Get.defaultDialog(
      backgroundColor: WellNestColor.wncWhite,
      barrierDismissible: false,
      title: title!,
      titleStyle: WellNestTextStyle.nowBold(WellNestColor.wncGrey, 18.0),
      content: Container(
        //height: 90,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  message!,
                  maxLines: 10,
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 12.0),
                )
              ],
            ),
          ),
        ),
      ),
      confirm: Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: BlueButtonStandard(
          height: 40,
          width: 80,
          title: "Yes",
          sizeText: 13.0,
          textColor: WellNestColor.wncWhite,
          back: WellNestColor.wncBlue,
          border: WellNestColor.wncBlue,
          bluebutton: yes!,
        ),
      ),
      cancel: Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: BlueButtonStandard(
          height: 40,
          width: 80,
          title: "No",
          sizeText: 13.0,
          textColor: WellNestColor.wncBlue,
          back: WellNestColor.wncWhite,
          border: WellNestColor.wncWhite,
          bluebutton: no!,
        ),
      ),
    );
  }
}
