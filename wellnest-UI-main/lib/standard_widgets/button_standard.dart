import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
class BlueButtonStandard extends StatelessWidget {
 

  final Function()? bluebutton;
  final String? title;
  final Color? back;
  final Color? border;
  final Color? textColor;
  final double? sizeText;
  final double? height;
  final double? width;

  

  const BlueButtonStandard({Key? key, this.bluebutton, this.title, this.back, this.border, this.textColor, this.sizeText, this.height, this.width}) : super(key: key);@override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: back,
          elevation: 2,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10)
          ),
          side: BorderSide(
            color: border!
          )
        ),
        onPressed: bluebutton, 
      child: Center(
        child: Text(title!,
        style: WellNestTextStyle.nowMedium(textColor, sizeText),
        ),
      )),
    );
  }
}