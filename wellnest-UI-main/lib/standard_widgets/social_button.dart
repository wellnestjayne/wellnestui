import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SocialButtonStandard extends StatelessWidget {
  final Function()? facebook;
  final Function()? google;
  final Function()? linkedin;
  final Function()? twitter;
  final String? bonusText;

  const SocialButtonStandard(
      {Key? key, this.facebook, this.google, this.linkedin, this.twitter,this.bonusText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Text(
            "Login via Social Media",
            style: TextStyle(
                color: WellNestColor.wncLightgrey,
                fontSize: 12.h,
                fontWeight: FontWeight.w300),
          ),
          SizedBox(
            height: 45.h,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: facebook,
                  child: Container(
                    height: 42.h,
                    width: 42.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: WellNestColor.wncBlue,
                    ),
                    child: Center(
                      child: FaIcon(
                        FontAwesomeIcons.facebookF,
                        color: WellNestColor.wncWhite,
                        size: 22.h,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 30.r,
                ),
                GestureDetector(
                  onTap: linkedin,
                  child: Container(
                    height: 42.h,
                    width: 42.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: WellNestColor.wncBlue,
                    ),
                    child: Center(
                      child: FaIcon(
                        FontAwesomeIcons.linkedin,
                        color: WellNestColor.wncWhite,
                        size: 22.h,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 30.r,
                ),
                GestureDetector(
                  onTap: google,
                  child: Container(
                    height: 42.h,
                    width: 42.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: WellNestColor.wncBlue,
                    ),
                    child: Center(
                      child: FaIcon(
                        FontAwesomeIcons.google,
                        color: WellNestColor.wncWhite,
                        size: 22.h,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 30.r,
                ),
                GestureDetector(
                  onTap: twitter,
                  child: Container(
                    height: 42.h,
                    width: 42.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: WellNestColor.wncBlue,
                    ),
                    child: Center(
                      child: FaIcon(
                        FontAwesomeIcons.twitter,
                        color: WellNestColor.wncWhite,
                        size: 22.h,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 45.h,
          ),
          Text(
          bonusText!,
            style: TextStyle(
                color: WellNestColor.wncLightgrey,
                fontSize: 12.h,
                fontWeight: FontWeight.w300),
          ),
        ],
      ),
    );
  }
}
