
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class Standardheader extends StatelessWidget {
  final Function()? press;
  final String? title;
  final bool? show;
  final Color? colorText;

  const Standardheader({Key? key, this.press, this.title, this.show, this.colorText}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Container(
      width: 349,
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: Text(title!,
              style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 16.0),
              ),
            )),
          Visibility(
            visible: show!,
            child: InkWell(
              onTap: press,
              child: Text("Show more",
              style: TextStyle(
                color: colorText!,
                fontFamily: 'NowMed',
                fontSize: 12.0,
                shadows: [
                  Shadow(
                    offset: Offset(3,2),
                    blurRadius: 2,
                    color: WellNestColor.wncGrey.withOpacity(0.2)
                )
                ]
              ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}