

import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';

class BottomBarOrigin extends StatelessWidget {
 final int? index1;
  final ValueChanged<int?>? onchanged;

  const BottomBarOrigin({Key? key, this.index1, this.onchanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: index1!,
      onTap: onchanged!,
      selectedItemColor: WellNestColor.wncBlue,
      unselectedItemColor: WellNestColor.wncAquaBlue,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      items: [

      ],
      
    );
  }
}