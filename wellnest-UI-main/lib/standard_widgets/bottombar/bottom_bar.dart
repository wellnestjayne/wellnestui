import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/standard_widgets/curved/curved_bar.dart';

class BottomCustombarw extends StatelessWidget {
  final int? index1;
  final ValueChanged<int?>? onchanged;
  final GlobalKey<CurvedNavigationBarState>? bottomNavigationKey;
  final Listenable? animationController;
  final bool? isShow;
  final Color? color;

  const BottomCustombarw(
      {Key? key,
      this.index1,
      this.onchanged,
      this.bottomNavigationKey,
      this.animationController,
      this.isShow,
      this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animationController!,
        builder: (_, __) => AnimatedContainer(
              duration: Duration(milliseconds: 450),
              height: isShow! ? 60 : 0,
              child: Wrap(
                children: [
                  CurvedNavigationBar(
                    key: bottomNavigationKey,
                    index: index1!,
                    height: 60,
                    letIndexChange: (index) => true,
                    onTap: onchanged!,
                    color: color!,
                    buttonBackgroundColor: color!,
                    animationCurve: Curves.easeInOut,
                    backgroundColor: WellNestColor.wncWhite,
                    animationDuration: Duration(milliseconds: 450),
                    items: [
                      ImageIcon(
                        AssetImage(WellnestAsset.home),
                        size: 18,
                        color: index1! == 0 ? WellNestColor.wncWhite : WellNestColor.wncLightgrey.withOpacity(0.6),
                      ),
                      ImageIcon(
                        AssetImage(WellnestAsset.sched),
                        size: 18,
                        color: index1! == 1 ? WellNestColor.wncWhite : WellNestColor.wncLightgrey.withOpacity(0.6),
                      ),
                      FaIcon(
                        FontAwesomeIcons.plus,
                        size: 18,
                        color: index1! == 2 ? WellNestColor.wncWhite : WellNestColor.wncLightgrey.withOpacity(0.6),
                      ),
                      // ImageIcon(
                      //   AssetImage(WellnestAsset.pen),
                      //   size: 18,
                      //   color: index1! == 3
                      //       ? WellNestColor.wncWhite
                      //       : WellNestColor.wncLightgrey.withOpacity(0.6),
                      // ),
                      FaIcon(
                        FontAwesomeIcons.cog,
                        size: 18,
                        color: index1! == 3 ? WellNestColor.wncWhite : WellNestColor.wncLightgrey.withOpacity(0.6),
                      ),
                      // ImageIcon(
                      //   AssetImage(WellnestAsset),
                      //   color: index1! == 4
                      //       ? WellNestColor.wncWhite
                      //       : WellNestColor.wncLightgrey.withOpacity(0.6),
                      // ),
                    ],
                  ),
                ],
              ),
            ));
  }
}
