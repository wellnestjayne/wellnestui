import 'package:flutter/material.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/standard_widgets/calendar/customization/calendar_builder.dart';
import 'package:wellnest/standard_widgets/calendar/customization/calendar_style.dart';

class CellContent extends StatelessWidget {
  final DateTime day;
  final DateTime focusedDay;
  final bool isTodayHighlighted;
  final bool isToday;
  final bool isSelected;
  final bool isRangeStart;
  final bool isRangeEnd;
  final bool isWithinRange;
  final bool isOutside;
  final bool isDisabled;
  final bool isHoliday;
  final bool isWeekend;
  final CalendarStyle calendarStyle;
  final CalendarBuilders calendarBuilders;

  const CellContent({
    Key? key,
    required this.day,
    required this.focusedDay,
    required this.calendarStyle,
    required this.calendarBuilders,
    required this.isTodayHighlighted,
    required this.isToday,
    required this.isSelected,
    required this.isRangeStart,
    required this.isRangeEnd,
    required this.isWithinRange,
    required this.isOutside,
    required this.isDisabled,
    required this.isHoliday,
    required this.isWeekend,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget? cell = calendarBuilders.prioritizedBuilder?.call(context, day, focusedDay);

    if (cell != null) {
      return cell;
    }

    final text = '${day.day}';
    final margin = calendarStyle.cellMargin;
    final duration = const Duration(milliseconds: 250);

    if (isDisabled) {
      cell = calendarBuilders.disabledBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration: calendarStyle.disabledDecoration,
            alignment: Alignment.center,
            child: Text(text, style: calendarStyle.disabledTextStyle),
          );
    } else if (isSelected) {
      final isTodaySelected = isToday && isTodayHighlighted;
      cell = calendarBuilders.selectedBuilder?.call(context, day, focusedDay) ??
          // AnimatedContainer(
          //   duration: duration,
          //   margin: margin,
          //   decoration: calendarStyle.selectedDecoration,
          //   alignment: Alignment.center,
          //   child: Text(text, style: calendarStyle.selectedTextStyle),
          // );
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(isTodaySelected ? WellnestAsset.dateback : WellnestAsset.selectedDate),
                    fit: BoxFit.contain)),
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(left: 0, right: 3, bottom: 3),
              child: Text(text,
                  textAlign: TextAlign.center,
                  style: isTodaySelected ? calendarStyle.selectedTextStyle : calendarStyle.selectedDateTextStyle),
            ),
          );
    } else if (isRangeStart) {
      cell = calendarBuilders.rangeStartBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration: calendarStyle.rangeStartDecoration,
            alignment: Alignment.center,
            child: Text(text, style: calendarStyle.rangeStartTextStyle),
          );
    } else if (isRangeEnd) {
      cell = calendarBuilders.rangeEndBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration: calendarStyle.rangeEndDecoration,
            alignment: Alignment.center,
            child: Text(text, style: calendarStyle.rangeEndTextStyle),
          );
    } else if (isToday && isTodayHighlighted) {
      cell = calendarBuilders.todayBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration:
                BoxDecoration(image: DecorationImage(image: AssetImage(WellnestAsset.dateback1), fit: BoxFit.contain)),
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(left: 0, right: 3, bottom: 3),
              child: Text(text, textAlign: TextAlign.center, style: calendarStyle.todayTextStyle),
            ),
          );
    } else if (isHoliday) {
      cell = calendarBuilders.holidayBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration: calendarStyle.holidayDecoration,
            alignment: Alignment.center,
            child: Text(text, style: calendarStyle.holidayTextStyle),
          );
    } else if (isWithinRange) {
      cell = calendarBuilders.withinRangeBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration: calendarStyle.withinRangeDecoration,
            alignment: Alignment.center,
            child: Text(text, style: calendarStyle.withinRangeTextStyle),
          );
    } else if (isOutside) {
      cell = calendarBuilders.outsideBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration: calendarStyle.outsideDecoration,
            alignment: Alignment.center,
            child: Text(text, style: calendarStyle.outsideTextStyle),
          );
    } else {
      cell = calendarBuilders.defaultBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            decoration: isWeekend ? calendarStyle.weekendDecoration : calendarStyle.defaultDecoration,
            alignment: Alignment.center,
            child: Text(
              text,
              style: isWeekend ? calendarStyle.weekendTextStyle : calendarStyle.defaultTextStyle,
            ),
          );
    }

    return cell;
  }
}
