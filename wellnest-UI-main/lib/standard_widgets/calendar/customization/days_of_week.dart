import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/standard_widgets/calendar/shared/utils.dart';

class DaysOfWeekStyle {
  final TextFormatter? dowTextFormatter;


  final Decoration decoration;


  final TextStyle weekdayStyle;


  final TextStyle weekendStyle;


  const DaysOfWeekStyle({
    this.dowTextFormatter,
    this.decoration = const BoxDecoration(),
    this.weekdayStyle = const TextStyle(
      
      //color: const Color(0xFF4F4F4F)
      color: WellNestColor.wncAquaBlue
      ),
    this.weekendStyle = const TextStyle(
      color: WellNestColor.wncAquaBlue
      //color: const Color(0xFF6A6A6A)
      ),
  });
}