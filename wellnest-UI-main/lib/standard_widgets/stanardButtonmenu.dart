
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class StandardButtonMenu extends StatelessWidget {
  
  final String? pathSvg;
  final String? title;
  final Function()? function;

  const StandardButtonMenu({Key? key, this.pathSvg, this.title, this.function}) : super(key: key);

  
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function,
      child: Container(
        child: Row(
          children: [
            Container(
              height: 20,
              width: 20,
              child: SvgPicture.asset(pathSvg!),
            ),
            SizedBox(width: 25,),
            Text(title!,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncGrey
              , 15.0),
            )
          ],
        ),
      ),
    );
  }
}