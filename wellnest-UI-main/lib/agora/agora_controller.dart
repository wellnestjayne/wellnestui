

import 'dart:async';
import 'dart:developer';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/foundation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:wellnest/agora/agora_string.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class AgoraController extends GetxController{
  
  late final RtcEngine engineRtc;
  final remoteUids = 0.obs;
  final isJoned = true.obs , switchCamera = true.obs , switchRender = true.obs
  ,videoLocal = true.obs, audioLocal = true.obs,
  remoteVideo = true.obs;


   final interval = const  Duration(seconds: 1).obs;
  RxInt timeMax = 3600.obs;
  RxInt currentSeconds = 0.obs;

  var press = false.obs;
  RxString get timerText
  => '${((timeMax.value - currentSeconds.value) ~/60).toString().padLeft(2,'0')}:${((timeMax.value - currentSeconds.value) % 60).toString().padLeft(2,'0')}'.obs;

   startTimeout(){
    var duration = interval.value;
    Timer.periodic(duration, (timer) {
      currentSeconds.value = timer.tick;
      if(timer.tick >= timeMax.value){
        timer.cancel();
        press(!press.value);
        Get.back();
      }
     });
  }
  
  @override
  void onInit() {
    super.onInit();
    this._initAgoraEngine();
  }

  _initAgoraEngine() async{
    engineRtc = await RtcEngine.createWithContext(RtcEngineContext(AgoraConfig.appIdAgora!));
    this.engineListerner();
    await engineRtc.enableAudio();
    await engineRtc.enableVideo();
    await engineRtc.startPreview();
    await engineRtc.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await engineRtc.setClientRole(ClientRole.Broadcaster);
    if(defaultTargetPlatform == TargetPlatform.android){
     await [Permission.microphone,Permission.camera,Permission.accessNotificationPolicy,
      Permission.criticalAlerts,Permission.notification
      ].request();
    }
   await engineRtc.joinChannel(AgoraConfig.appAgoraToken!, AgoraConfig.appChannel!, null, 0);


  }


  engineListerner(){
    engineRtc.setEventHandler(RtcEngineEventHandler(
      joinChannelSuccess: (channel, uuid,ellaped){
        print('joinChannel Success $channel $uuid $ellaped');
        isJoned(!isJoned.value);
      },
      userJoined:(uuid, ellaped){
        print('userJoined $uuid $ellaped');
        press(!press.value);
        remoteUids(uuid);
        LoadingOrError.snackBarMessage(
          title: "User Success Join",
          message: "User Coachee Joined id $uuid",
          icon: FaIcon(FontAwesomeIcons.userCheck,color: WellNestColor.wncWhite,)
        );
        if(remoteUids.value == 0){
          print("Timer Wont Start");
        }else{
          
          startTimeout();
        }
      },
      userOffline: (uuid,reason){
        print('userOffline $uuid $reason');
      },
      leaveChannel: (stats){
        print('leaveChannel ${stats.duration}');
        isJoned(!isJoned.value);
        remoteUids(0);
      },
      remoteVideoStateChanged: (uuid,state,reason,ellapsed){
        if(state == VideoRemoteState.Stopped){
          remoteVideo(!remoteVideo.value);
          print("Remote Stopped");
        }else if(state == VideoRemoteState.Decoding && 
        reason == VideoRemoteStateReason.RemoteUnmuted){
          remoteVideo(!remoteVideo.value);
          print("Remote Continue");
        }
      }
      ),
    );

  }

  leaveChannel()async{
    await engineRtc.leaveChannel();    
    Get.back();
  }


  hideVideoFace(){
    videoLocal(!videoLocal.value);
    engineRtc.enableLocalVideo(videoLocal.value);
  }

  micOff(){
    audioLocal(!audioLocal.value);
    engineRtc.enableLocalAudio(audioLocal.value);
  }

  switchCameraUserOrBrod(){
    engineRtc.switchCamera().then((value) => 
    switchCamera(!switchCamera.value)
    ).catchError((err){
      log('Switch Camera $err');
    });
  }

  switchRenders(){
    switchRender(!switchRender.value);
  }

  @override
  void onClose() {
    super.onClose();
    engineRtc.destroy();
  }

 


}