
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/agora/agora_vid_temp/agora_widget/buton_agora.dart';
import 'package:wellnest/constants/colors/colors.dart';

class AgoraButtonsLead extends StatelessWidget {

  final Function()? mic;
  final Function()? cameraSwitch;
  final Function()? video;
  final Function()? leaveChannle;
  final bool? iconMic;
  final bool? iconVideo;
  final bool? iconCamera;

  const AgoraButtonsLead({Key? key, this.mic, this.cameraSwitch, this.video, this.leaveChannle, this.iconMic, this.iconVideo, this.iconCamera}) : super(key: key);

  
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: 349,
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [             
            GestureDetector(
              onTap: mic,
              child: AgoraButton(
                boxColor: WellNestColor.wncLightgrey.withOpacity(0.26),
                boxColorOpa: WellNestColor.wncGrey.withOpacity(0.4),
                icons: iconMic! == false ?
                 FaIcon(FontAwesomeIcons.microphoneAltSlash,color: WellNestColor.wncWhite,size: 21,):
                FaIcon(FontAwesomeIcons.microphoneAlt,color: WellNestColor.wncWhite,size: 21,),
              ),
            ),
            GestureDetector(
              onTap: cameraSwitch,
              child: AgoraButton(
                boxColor: WellNestColor.wncLightgrey.withOpacity(0.26),
                boxColorOpa: WellNestColor.wncGrey.withOpacity(0.4),
                icons: iconCamera == false ? 
                 FaIcon(FontAwesomeIcons.mobile,color: WellNestColor.wncWhite,size: 21,) : 
                FaIcon(FontAwesomeIcons.camera,color: WellNestColor.wncWhite,size: 21,),
              ),
            ),
            GestureDetector(
              onTap: video,
              child: AgoraButton(
                boxColor: WellNestColor.wncLightgrey.withOpacity(0.26),
                boxColorOpa: WellNestColor.wncGrey.withOpacity(0.4),
                icons: iconVideo  == false ?
                FaIcon(FontAwesomeIcons.videoSlash,color:WellNestColor.wncWhite,size: 18,) :
                FaIcon(FontAwesomeIcons.video,color:WellNestColor.wncWhite,size: 18,),
              ),
            ),
            GestureDetector(
              onTap: leaveChannle,
              child: AgoraButton(
                boxColor: Colors.red,
                boxColorOpa: Colors.red.withOpacity(0.3),
                icons: FaIcon(FontAwesomeIcons.phoneAlt,color: WellNestColor.wncWhite,size: 18,),
              ),
            ),
          ],
        ),
      ),
    );
  }
}