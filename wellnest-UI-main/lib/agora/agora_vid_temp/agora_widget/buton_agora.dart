
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AgoraButton extends StatelessWidget {

    final Widget? icons;
    final Color? boxColor;
    final Color? boxColorOpa;

  const AgoraButton({Key? key, this.icons, this.boxColor, this.boxColorOpa}) : super(key: key);

 
  @override
  Widget build(BuildContext context) {
    return  Container(
                height: 50,
                width: 50,
                child: Center(child: icons!),
                decoration: BoxDecoration(
                  color:boxColor!,
                  shape: BoxShape.circle,
                ),
              );
  }
}