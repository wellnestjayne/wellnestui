

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/agora/agora_controller.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:wellnest/agora/agora_vid_temp/agora_widget/button_holder.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AgoraPageTrialViewing extends StatelessWidget {
  
  final agoraController = Get.put(AgoraController());

  @override
  Widget build(BuildContext context) {
    return
     Obx(
      ()=> 
      SafeArea(
          child: Scaffold(
            backgroundColor: WellNestColor.wncAquaBlue,
            body: Stack(
              children: [
                agoraController.remoteUids.value == 0  
            ||  agoraController.remoteVideo.isFalse ?
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncWhite,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(2,2),
                          blurRadius: 2,
                          spreadRadius: 2,
                          color: WellNestColor.wncLightgrey.withOpacity(0.3)
                        ),
                      ]
                    ),
                    child: Center(
                      child: Image.asset(WellnestAsset.logoWellNest)
                    )
                  ),
                ) : 
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.height,
                    child:  agoraController.remoteVideo.isFalse ? 
                    Center(
                      child: Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncWhite,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(2,2),
                          blurRadius: 2,
                          spreadRadius: 2,
                          color: WellNestColor.wncLightgrey.withOpacity(0.3)
                        ),
                      ]
                    ),
                    child: Center(
                      child: Image.asset(WellnestAsset.logoWellNest)
                    )
                  ),
                    ) :
                    RtcRemoteView.SurfaceView(uid: agoraController.remoteUids.value),
                  ),
                Padding(
                  padding: const EdgeInsets.only(top: 20,right: 10),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          agoraController.videoLocal.isFalse ? 
                          Container(
                            height: 160,
                            width: 120,
                            decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(10),
                               color: WellNestColor.wncWhite
                            ),
                            child: Center(
                              child: Image.asset(WellnestAsset.specialLogo,
                              fit: BoxFit.contain,),
                            ),
                          )
                          : Container(
                            height: 144,
                            width: 102,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: RtcLocalView.SurfaceView(),
                            )
                          ),
                           SizedBox(height: 10,),
                          // agoraController.press.isFalse ? Container(
                          //   width: 120,
                          //   child: Text("Timer 1:00",
                          //   textAlign: TextAlign.center,
                          //   style: WellNestTextStyle.nowMedium(
                          //     WellNestColor.wncGrey, 13.0),),
                          // )
                          // : Container(
                          //   width: 120,
                          //   child: Text("Timer ${agoraController.timerText}",
                          //   textAlign: TextAlign.center,
                          //   style: WellNestTextStyle.nowMedium(
                          //     WellNestColor.wncWhite, 13.0),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ),
                ),
              
                Padding(
                  padding: const EdgeInsets.only(top: 20,left: 20),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      height: 45,
                      width: 45,
                      // decoration: BoxDecoration(
                      //   shape: BoxShape.circle,
                      //   color: WellNestColor.wncWhite
                      // ),
                      child: Center(
                        child: FaIcon(FontAwesomeIcons.arrowLeft,
                        color: WellNestColor.wncWhite,
                        ),
                      )
                    ),
                  ),
                ),
                                  Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 130,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.transparent,
                          Colors.black.withOpacity(0.3)
                        ])
                    ),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom : 8),
                            child: Container(
                              width: 349,
                              child: Column(
                               // mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Coach Name Erchil",
                                  textAlign: TextAlign.center,
                                  style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite,15.0),
                                  ),
                                  SizedBox(height: 5,),
                                   agoraController.press.isFalse ? Container(
                                width: 120,
                                child: Text("Timer 1:00",
                                textAlign: TextAlign.center,
                                style: WellNestTextStyle.nowMedium(
                                  WellNestColor.wncWhite, 13.0),),
                              )
                              : Container(
                                width: 120,
                                child: Text("Timer ${agoraController.timerText}",
                                textAlign: TextAlign.center,
                                style: WellNestTextStyle.nowMedium(
                                  WellNestColor.wncWhite, 13.0),
                                ),
                              ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: AgoraButtonsLead(
                      iconCamera: agoraController.switchCamera.value,
                      iconMic: agoraController.audioLocal.value,
                      iconVideo: agoraController.videoLocal.value,
                      mic: ()=>agoraController.micOff(),
                      cameraSwitch: ()=>agoraController.switchCameraUserOrBrod(),
                      video: ()=>agoraController.hideVideoFace(),
                      leaveChannle: ()=>agoraController.leaveChannel(),
                    ),
                  ),
                ),

              ],
            )
          ),
     ),
    );
  }
}