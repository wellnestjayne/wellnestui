
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/agora/agora_vid_temp/agora_page.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class AgoraJoinPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: BlueButtonStandard(
            bluebutton: ()=>Get.to(()=>AgoraPageTrialViewing()),
            title: "Join Meet",
            back: WellNestColor.wncBlue,
            border: WellNestColor.wncBlue,
            textColor: WellNestColor.wncWhite,
            sizeText: 15.0,
            height: 40,
            width: 120,
          ),
        ),
      ),
    );
  }
}