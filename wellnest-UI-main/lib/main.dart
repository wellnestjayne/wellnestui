import 'package:device_preview/device_preview.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/controller.dart';
import 'package:wellnest/pages/starting/controller/start.dart';
import 'package:wellnest/route/route_name.dart';
import 'package:wellnest/route/route_page.dart';

final controllerRoot = Get.put(MainRootController());
const AndroidNotificationChannel channel = AndroidNotificationChannel(
  "id",
  "name",
  importance: Importance.max,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> messagehandling(RemoteMessage message) async {
  await Firebase.initializeApp();
  var initialization = AndroidInitializationSettings('@mipmap/launcher_icon');
  var initialSetting = InitializationSettings(android: initialization);
  flutterLocalNotificationsPlugin.initialize(initialSetting);
  flutterLocalNotificationsPlugin.show(
      message.notification.hashCode,
      message.notification!.title,
      message.notification!.body,
      NotificationDetails(
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          //icon: 'launch_background',
          fullScreenIntent: true,
          channelDescription: channel.description,
          priority: Priority.max,
          enableVibration: true,
          importance: Importance.high,
          largeIcon: DrawableResourceAndroidBitmap('@mipmap/launcher_icon'),
          // styleInformation: imageIcon
          styleInformation: BigTextStyleInformation(message.notification!.body!,
              contentTitle: message.notification!.title!,
              htmlFormatBigText: true,
              htmlFormatTitle: true,
              htmlFormatContent: true),
        ),
      ));
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(messagehandling);
  controllerRoot.localNotifProviderDetail();
  await SharePref.init();

  // runApp(
  //   DevicePreview(
  //     enabled: !kReleaseMode,
  //     builder: (context)=> MyApp()),
  // );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(375, 812),
      builder: () => GetMaterialApp(
        // locale: DevicePreview.locale(context),
        // builder: DevicePreview.appBuilder,
        debugShowCheckedModeBanner: false,
        defaultTransition: Transition.leftToRight,
        transitionDuration: Duration(milliseconds: 450),
        initialRoute: AppRouteName.splashScreen,
        getPages: Apppages.listRoute,
      ),
    );
  }
}
