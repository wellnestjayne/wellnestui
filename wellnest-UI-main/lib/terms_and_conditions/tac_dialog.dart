


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/terms_and_conditions/tac.dart';

class TacDialog{


  static tacDialogShow(){

    Get.defaultDialog(
      title: TacStrings.headTitle!,
      titleStyle: WellNestTextStyle.nowBold(
        WellNestColor.wncGrey, 15.0),
        titlePadding: const EdgeInsets.only(top: 25),
      backgroundColor: Colors.white,
      barrierDismissible: true,
      contentPadding: const EdgeInsets.only(
        left: 30,right: 30,top:30
      ),
      content:  Container(
        height: 330,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            textDirection: TextDirection.ltr,
            children: [
              Text(TacStrings.firstPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.intro!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.introPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.medicalService!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.medicalServicepara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.account!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.accountPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.contentLicense!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.contentpara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              Container(
                child: Row(
                  children: [
                    Container(
                      height: 15,
                      width: 15,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: WellNestColor.wncGrey
                      ),
                    ),
                    SizedBox(width: 5,),
                    Expanded(
                      child: Text(TacStrings.contentbulletpara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Container(
                      height: 15,
                      width: 15,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: WellNestColor.wncGrey
                      ),
                    ),
                    SizedBox(width: 5,),
                    Expanded(
                      child: Text(TacStrings.contentbulletpara1!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.propriety!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.proprietypara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.restriction!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.restrictionPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.pricing!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.pricingPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.privacy!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.privacyPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.modification!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.modiPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.changeTerm!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.changeTermPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.warranty!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.warrantyPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.limitation!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.limitationPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.indemnification!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.indiPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.thirdparty!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.thirdPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.thirdSoftware!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.thirdParaSoft!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.feedback!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.feedbackpara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.miscell!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.miscellPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.maintenace!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.maintenacePara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.locationData!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.locationDataPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.temThe!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.temThePara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.thirdparty!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.thirdPara!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
              SizedBox(height: 15,),
              Text(TacStrings.contactUs!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,13.0),
              ),
              Text(TacStrings.contactData!,
              textAlign: TextAlign.left,
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey,13.0),
              ),
            ],
          ),
        ),
      )
    );

  }


}