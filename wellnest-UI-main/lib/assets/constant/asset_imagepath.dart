class WellnestAsset {
  static const logoWellNest = "lib/assets/images/wellnest_logo.png";
  static const firstImageWellNest = "lib/assets/images/firstImage.png";
  static const secondImageWellNest = "lib/assets/images/secondImage.png";
  static const thirdImageWellNest = "lib/assets/images/thirdImage.png";
  static const fourthImageWellNest = "lib/assets/images/fourthImage.png";
  static const fifthImageWellNest = "lib/assets/images/fifthimage.png";
  static const home = "lib/assets/images/homer.png";
  static const sched = "lib/assets/images/sched.png";
  static const pen = "lib/assets/images/pen.png";
  static const money = "lib/assets/images/money.png";
  static const bell = "lib/assets/images/custom_bell.png";
  static const message = "lib/assets/images/custom_message.png";

  //Coach
  static const certificate = "lib/assets/images/coach/certificate.png";
  static const ellipse = "lib/assets/images/coach/ellipse.png";
  static const service = "lib/assets/images/coach/service.jpg";
  static const smilingMan = "lib/assets/images/coach/smiling_man.png";
  static const star = "lib/assets/images/coach/star.png";
  static const question = "lib/assets/images/coach/question.png";

  //badges
  static const challenge = "lib/assets/images/badges/challenge.png";
  static const challenge02 = "lib/assets/images/badges/challenge02.png";
  static const challenge03 = "lib/assets/images/badges/challenge03.png";
  static const challenge04 = "lib/assets/images/badges/challenge04.png";
  static const challenge05 = "lib/assets/images/badges/challenge05.png";
  static const challenge06 = "lib/assets/images/badges/challenge06.png";
  static const challenge07 = "lib/assets/images/badges/challenge07.png";
  static const challenge08 = "lib/assets/images/badges/challenge08.png";
  static const challenge09 = "lib/assets/images/badges/challenge09.png";
  static const challenge10 = "lib/assets/images/badges/challenge10.png";
  static const challenge11 = "lib/assets/images/badges/challenge11.png";
  static const challenge12 = "lib/assets/images/badges/challenge12.png";
  static const goal_Achieved = "lib/assets/images/badges/goal_achieved.png";
  static const gota_coach = "lib/assets/images/badges/gota_coach.png";
  static const master_scribe = "lib/assets/images/badges/master_scribe.png";
  static const model_coach = "lib/assets/images/badges/model_coach.png";
  static const ready = "lib/assets/images/badges/ready.png";

  //Categories
  static const fianancial = "lib/assets/images/categories/financial.png";
  static const lifeStyle = "lib/assets/images/categories/lifestyle.png";
  static const mental_health = "lib/assets/images/categories/mental_health.png";
  static const perfomace = "lib/assets/images/categories/performance.png";
  static const relationship = "lib/assets/images/categories/relationship.png";
  static const spiritual = "lib/assets/images/categories/spiritual.png";
  static const spiritual1 = "lib/assets/images/categories/spiritual1.png";
  static const lifeStyle1 = "lib/assets/images/categories/lifestyle1.png";

  //Categories SVG
  static const fianancialSvg = "lib/assets/images/categories/financial.svg";
  static const lifeStyleSvg = "lib/assets/images/categories/lifestyle.svg";
  static const mental_healthSvg = "lib/assets/images/categories/mental.svg";
  static const perfomaceSvg = "lib/assets/images/categories/performance.svg";
  static const relationshipSvg = "lib/assets/images/categories/relationship.svg";
  static const spiritualSvg = "lib/assets/images/categories/spiritual.svg";

  //Modes
  static const angry = "lib/assets/images/modes/angry.png";
  static const happy_mode = "lib/assets/images/modes/happy_mode.png";
  static const meh = "lib/assets/images/modes/meh.png";
  static const not_so_good = "lib/assets/images/modes/not_so_good.png";
  static const pleased = "lib/assets/images/modes/pleased.png";
  static const upset = "lib/assets/images/modes/upset.png";
  static const dateback = "lib/assets/images/modes/datebackground.png";
  static const dateback1 = "lib/assets/images/modes/datebackground1.png";
  static const selectedDate = "lib/assets/images/modes/selectedDate.png";

  // Services
  static const certificateIcon = "lib/assets/images/service/certificate.png";
  static const timeIcon = "lib/assets/images/service/time.png";
  static const assessmentIcon = "lib/assets/images/service/assessment.png";
  static const mountain = "lib/assets/images/service/mountain.png";
  static const check = "lib/assets/images/service/check.png";

  //Trash
  static const pssuy = "lib/assets/images/trash/pssuy.png";
  static const testimonial = "lib/assets/images/trash/test.png";
  static const extraTrash = "lib/assets/images/extraTrash.png";

  static const backBg = "lib/assets/images/backbg.png";
  static const flag = "lib/assets/images/flag.png";

  static const svgCoach = "lib/assets/images/coachSign.svg";
  static const svgUser = "lib/assets/images/userSign.svg";
  static const specialLogo = "lib/assets/images/logo_special.png";

  static const svgf = "lib/assets/images/f.svg";
  static const svgu = "lib/assets/images/u.svg";
  static const svgc = "lib/assets/images/c.svg";
  static const svgk = "lib/assets/images/k.svg";
  static const svgus = "lib/assets/images/us.svg";

  static const trash1 = "lib/assets/images/trash1.png";

  //Profile svg icons
  static const svgEdit = "lib/assets/images/edit.svg";
  static const svgShare = "lib/assets/images/share.svg";
  static const svgNotification = "lib/assets/images/notification.svg";
  static const svgMessaging = "lib/assets/images/messaging.svg";
  static const svgBluepike = "lib/assets/images/bluepike.svg";

  //Profile png
  static const editpng = "lib/assets/images/edit.png";
  static const sharepng = "lib/assets/images/share.png";

  static const approvedSesion = "lib/assets/images/approve_session.svg";
  static const cancelSession = "lib/assets/images/question_fill.svg";
  static const chipCol = "lib/assets/images/chip.png";
  static const starPol = "lib/assets/images/star.png";

  //Subcriptions
  static const backimagePng = "lib/assets/images/back_image.png";
  static const backImageSvg = "lib/assets/images/back_image.svg";
  static const subscription1 = "lib/assets/images/subcription_human1.svg";
  static const subscription2 = "lib/assets/images/subcription_human2.svg";

  //Inclusion Detail
  static const certificate_icon = "lib/assets/images/certificate_icon.svg";
  static const takeHomeassest = "lib/assets/images/takhome.svg";
  static const hoursession = "lib/assets/images/clock.svg";

  //Successful Create Service
  static const successFul = "lib/assets/images/successful.svg";

  //Settings
  static const accountSvg = "lib/assets/images/settings/accountsvg.svg";
  static const app_secureitySvg = "lib/assets/images/settings/app_security.svg";
  static const logoutSvg = "lib/assets/images/settings/logout.svg";
  static const manageSvg = "lib/assets/images/settings/manage_subs.svg";
  static const notificationSvg = "lib/assets/images/settings/notification.svg";
  static const referSvg = "lib/assets/images/settings/refer.svg";
  static const serviceSvg = "lib/assets/images/settings/service.svg";

  static const subbanner = "lib/assets/images/settings/benefit_svg.svg";
  static const subbannerPng = "lib/assets/images/settings/benefit_png.png";
  static const referIconSvg = "lib/assets/images/settings/refer_friend_icon.svg";
  static const referFriend = "lib/assets/images/settings/refer_friend.png";
  static const referFriendSuccess = "lib/assets/images/settings/refer_friend_success.png";
}
