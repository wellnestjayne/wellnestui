// To parse this JSON data, do
//
//     final getCoachDetail = getCoachDetailFromJson(jsonString);

import 'dart:convert';

GetCoachDetail getCoachDetailFromJson(String str) => GetCoachDetail.fromJson(json.decode(str));

String getCoachDetailToJson(GetCoachDetail data) => json.encode(data.toJson());

class GetCoachDetail {
    GetCoachDetail({
        this.message,
        this.coachUserDetails,
    });

    final String? message;
    final CoachUserDetails? coachUserDetails;

    factory GetCoachDetail.fromJson(Map<String, dynamic> json) => GetCoachDetail(
        message: json["message"] == null ? null : json["message"],
        coachUserDetails: json["data"] == null ? null : CoachUserDetails.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": coachUserDetails == null ? null : coachUserDetails!.toJson(),
    };
}

class CoachUserDetails {
    CoachUserDetails({
        this.userId,
        this.name,
        this.shortDescription,
        this.longDescription,
        this.tags,
        this.isVerified,
        this.photoUrls,
        this.nationalityTags,
        this.id,
    });

    final String? userId;
    final String? name;
    final String? shortDescription;
    final String? longDescription;
    final List<String>? tags;
    final bool? isVerified;
    final List<String>? photoUrls;
    final List<String>? nationalityTags;
    final String? id;

    factory CoachUserDetails.fromJson(Map<String, dynamic> json) => CoachUserDetails(
        userId: json["userId"] == null ? null : json["userId"],
        name: json["name"] == null ? null : json["name"],
        shortDescription: json["shortDescription"] == null ? null : json["shortDescription"],
        longDescription: json["longDescription"] == null ? null : json["longDescription"],
        tags: json["tags"] == null ? null : List<String>.from(json["tags"].map((x) => x)),
        isVerified: json["isVerified"] == null ? null : json["isVerified"],
        photoUrls: json["photoUrls"] == null ? null : List<String>.from(json["photoUrls"].map((x) => x)),
        nationalityTags: json["nationalityTags"] == null ? null : List<String>.from(json["nationalityTags"].map((x) => x)),
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "userId": userId == null ? null : userId,
        "name": name == null ? null : name,
        "shortDescription": shortDescription == null ? null : shortDescription,
        "longDescription": longDescription == null ? null : longDescription,
        "tags": tags == null ? null : List<dynamic>.from(tags!.map((x) => x)),
        "isVerified": isVerified == null ? null : isVerified,
        "photoUrls": photoUrls == null ? null : List<dynamic>.from(photoUrls!.map((x) => x)),
        "nationalityTags": nationalityTags == null ? null : List<dynamic>.from(nationalityTags!.map((x) => x)),
        "id": id == null ? null : id,
    };
}
