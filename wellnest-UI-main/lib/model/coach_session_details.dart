// To parse this JSON data, do
//
//     final sessionCoachDetail = sessionCoachDetailFromJson(jsonString);

import 'dart:convert';

SessionCoachDetail sessionCoachDetailFromJson(String str) => SessionCoachDetail.fromJson(json.decode(str));

String sessionCoachDetailToJson(SessionCoachDetail data) => json.encode(data.toJson());

class SessionCoachDetail {
    SessionCoachDetail({
        this.coachId,
        this.categoryId,
        this.type,
        this.name,
        this.description,
        this.bannerUrl,
        this.selectedInclusionIds,
        this.coachingTosUrl,
        this.trainingTosUrl,
        this.numberOfSessions,
        this.timeInSeconds,
        this.price,
        this.discount,
        this.canCancelFreeInDays,
        this.bannerFile,
        this.coachingTosFile,
        this.trainingTosFile,
        this.maximumPeople,
        this.canRefund,
        this.id,
    });

    final String? coachId;
    final String? categoryId;
    final String? type;
    final String? name;
    final String? description;
    final String? bannerUrl;
    final List<String>? selectedInclusionIds;
    final String? coachingTosUrl;
    final String? trainingTosUrl;
    final int? numberOfSessions;
    final int? timeInSeconds;
    final double? price;
    final double? discount;
    final int? canCancelFreeInDays;
    final String? bannerFile;
    final String? coachingTosFile;
    final String? trainingTosFile;
    final int? maximumPeople;
    final bool? canRefund;
    final String? id;

    factory SessionCoachDetail.fromJson(Map<String, dynamic> json) => SessionCoachDetail(
        coachId: json["coachId"] == null ? null : json["coachId"],
        categoryId: json["categoryId"] == null ? null : json["categoryId"],
        type: json["type"] == null ? null : json["type"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        bannerUrl: json["bannerUrl"] == null ? null : json["bannerUrl"],
        selectedInclusionIds: json["selectedInclusionIds"] == null ? null : List<String>.from(json["selectedInclusionIds"].map((x) => x)),
        coachingTosUrl: json["coachingTOSUrl"] == null ? null : json["coachingTOSUrl"],
        trainingTosUrl: json["trainingTOSUrl"] == null ? null : json["trainingTOSUrl"],
        numberOfSessions: json["numberOfSessions"] == null ? null : json["numberOfSessions"],
        timeInSeconds: json["timeInSeconds"] == null ? null : json["timeInSeconds"],
        price: json["price"] == null ? null : json["price"],
        discount: json["discount"] == null ? null : json["discount"],
        canCancelFreeInDays: json["canCancelFreeInDays"] == null ? null : json["canCancelFreeInDays"],
        bannerFile: json["bannerFile"] == null ? null : json["bannerFile"],
        coachingTosFile: json["coachingTOSFile"] == null ? null : json["coachingTOSFile"],
        trainingTosFile: json["trainingTOSFile"] == null ? null : json["trainingTOSFile"],
        maximumPeople: json["maximumPeople"] == null ? null : json["maximumPeople"],
        canRefund: json["canRefund"] == null ? null : json["canRefund"],
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "coachId": coachId == null ? null : coachId,
        "categoryId": categoryId == null ? null : categoryId,
        "type": type == null ? null : type,
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "bannerUrl": bannerUrl == null ? null : bannerUrl,
        "selectedInclusionIds": selectedInclusionIds == null ? null : List<dynamic>.from(
          selectedInclusionIds!.map((x) => x)),
        "coachingTOSUrl": coachingTosUrl == null ? null : coachingTosUrl,
        "trainingTOSUrl": trainingTosUrl == null ? null : trainingTosUrl,
        "numberOfSessions": numberOfSessions == null ? null : numberOfSessions,
        "timeInSeconds": timeInSeconds == null ? null : timeInSeconds,
        "price": price == null ? null : price,
        "discount": discount == null ? null : discount,
        "canCancelFreeInDays": canCancelFreeInDays == null ? null : canCancelFreeInDays,
        "bannerFile": bannerFile == null ? null : bannerFile,
        "coachingTOSFile": coachingTosFile == null ? null : coachingTosFile,
        "trainingTOSFile": trainingTosFile == null ? null : trainingTosFile,
        "maximumPeople": maximumPeople == null ? null : maximumPeople,
        "canRefund": canRefund == null ? null : canRefund,
        "id": id == null ? null : id,
    };
}
