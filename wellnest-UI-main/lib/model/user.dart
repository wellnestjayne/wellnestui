// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);


import 'package:amazon_cognito_identity_dart_2/cognito.dart';


class User {


     String? firstname;
     String? lastname;
     String? email;
     String? phone;
     String? customType;
     bool? confirmed = false;
     bool? hasAccess = false;
    
  User({this.firstname, this.lastname, this.email, this.phone, this.customType});

    factory User.fromUserAttributes(List<CognitoUserAttribute> json) {
      final user = User();
      json.forEach((element) { 
        if(element.getName() == 'first_name'){
          user.firstname = element.getValue();
        }
        else if(element.getName() == 'last_name'){
          user.lastname = element.getValue();
        }
       else if(element.getName() == 'email'){
          user.email = element.getValue();
        }
        else if(element.getName() == 'phone_number'){
          user.phone = element.getValue();
        }
        else if(element.getName() == 'custom:type'){
          user.customType = element.getValue();
        }else if(element.getName()!.toLowerCase().contains('verified')){
          if(element.getValue()!.toLowerCase() == 'true'){
            user.confirmed = true;
          }
        }
      });
      return user;
    }

}
