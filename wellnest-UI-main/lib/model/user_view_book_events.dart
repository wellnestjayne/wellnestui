// To parse this JSON data, do
//
//     final sessionCoachDetail = sessionCoachDetailFromJson(jsonString);

import 'dart:convert';

SessionCoachDetail sessionCoachDetailFromJson(String str) => SessionCoachDetail.fromJson(json.decode(str));

String sessionCoachDetailToJson(SessionCoachDetail data) => json.encode(data.toJson());

class SessionCoachDetail {
    SessionCoachDetail({
        this.message,
        this.eventsUser,
    });

    final String? message;
    final List<EventsUser>? eventsUser;

    factory SessionCoachDetail.fromJson(Map<String, dynamic> json) => SessionCoachDetail(
        message: json["message"] == null ? null : json["message"],
        eventsUser: json["data"] == null ? null : List<EventsUser>.from(json["data"].map((x) => EventsUser.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": eventsUser == null ? null : List<dynamic>.from(eventsUser!.map((x) => x.toJson())),
    };
}

class EventsUser {
    EventsUser({
        this.coachId,
        this.bookingId,
        this.serviceId,
        this.userId,
        this.sessionId,
        this.creationTime,
        this.startTime,
        this.endTime,
        this.status,
        this.id,
    });

    final String? coachId;
    final String? bookingId;
    final String? serviceId;
    final String? userId;
    final String? sessionId;
    final DateTime? creationTime;
    final DateTime? startTime;
    final DateTime? endTime;
    final String? status;
    final String? id;

    factory EventsUser.fromJson(Map<String, dynamic> json) => EventsUser(
        coachId: json["coachId"] == null ? null : json["coachId"],
        bookingId: json["bookingId"] == null ? null : json["bookingId"],
        serviceId: json["serviceId"] == null ? null : json["serviceId"],
        userId: json["userId"] == null ? null : json["userId"],
        sessionId: json["sessionId"] == null ? null : json["sessionId"],
        creationTime: json["creationTime"] == null ? null : DateTime.parse(json["creationTime"]),
        startTime: json["startTime"] == null ? null : DateTime.parse(json["startTime"]),
        endTime: json["endTime"] == null ? null : DateTime.parse(json["endTime"]),
        status: json["status"] == null ? null : json["status"],
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "coachId": coachId == null ? null : coachId,
        "bookingId": bookingId == null ? null : bookingId,
        "serviceId": serviceId == null ? null : serviceId,
        "userId": userId == null ? null : userId,
        "sessionId": sessionId == null ? null : sessionId,
        "creationTime": creationTime == null ? null : creationTime!.toIso8601String(),
        "startTime": startTime == null ? null : startTime!.toIso8601String(),
        "endTime": endTime == null ? null : endTime!.toIso8601String(),
        "status": status == null ? null : status,
        "id": id == null ? null : id,
    };
}
