// To parse this JSON data, do
//
//     final coachInclusions = coachInclusionsFromJson(jsonString);

import 'dart:convert';

CoachInclusions coachInclusionsFromJson(String str) => CoachInclusions.fromJson(json.decode(str));

String coachInclusionsToJson(CoachInclusions data) => json.encode(data.toJson());

class CoachInclusions {
    CoachInclusions({
        this.message,
        this.coachee,
    });

    final String? message;
    final List<Coachee>? coachee;

    factory CoachInclusions.fromJson(Map<String, dynamic> json) => CoachInclusions(
        message: json["message"] == null ? null : json["message"],
        coachee: json["data"] == null ? null : List<Coachee>.from(json["data"].map((x) => Coachee.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "coachee": coachee == null ? null : List<dynamic>.from(coachee!.map((x) => x.toJson())),
    };
}

class Coachee {
    Coachee({
        this.userId,
        this.name,
        this.shortDescription,
        this.longDescription,
        this.tags,
        this.isVerified,
        this.photoUrls,
        this.nationalityTags,
        this.isFeatured,
        this.id,
    });

    final String? userId;
    final String? name;
    final String? shortDescription;
    final String? longDescription;
    final List<String>? tags;
    final bool? isVerified;
    final List<String>? photoUrls;
    final List<String>? nationalityTags;
    final bool? isFeatured;
    final String? id;

    factory Coachee.fromJson(Map<String, dynamic> json) => Coachee(
        userId: json["userId"] == null ? null : json["userId"],
        name: json["name"] == null ? null : json["name"],
        shortDescription: json["shortDescription"] == null ? null : json["shortDescription"],
        longDescription: json["longDescription"] == null ? null : json["longDescription"],
        tags: json["tags"] == null ? null : List<String>.from(json["tags"].map((x) => x)),
        isVerified: json["isVerified"] == null ? null : json["isVerified"],
        photoUrls: json["photoUrls"] == null ? null : List<String>.from(json["photoUrls"].map((x) => x)),
        nationalityTags: json["nationalityTags"] == null ? null : List<String>.from(json["nationalityTags"].map((x) => x)),
        isFeatured: json["isFeatured"] == null ? null : json["isFeatured"],
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "userId": userId == null ? null : userId,
        "name": name == null ? null : name,
        "shortDescription": shortDescription == null ? null : shortDescription,
        "longDescription": longDescription == null ? null : longDescription,
        "tags": tags == null ? null : List<dynamic>.from(tags!.map((x) => x)),
        "isVerified": isVerified == null ? null : isVerified,
        "photoUrls": photoUrls == null ? null : List<dynamic>.from(photoUrls!.map((x) => x)),
        "nationalityTags": nationalityTags == null ? null : List<dynamic>.from(nationalityTags!.map((x) => x)),
        "isFeatured": isFeatured == null ? null : isFeatured,
        "id": id == null ? null : id,
    };
}
