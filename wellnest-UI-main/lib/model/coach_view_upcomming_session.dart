
// To parse this JSON data, do
//
//     final coachUpcommingSession = coachUpcommingSessionFromJson(jsonString);

import 'dart:convert';

CoachUpcommingSession coachUpcommingSessionFromJson(String str) => CoachUpcommingSession.fromJson(json.decode(str));

String coachUpcommingSessionToJson(CoachUpcommingSession data) => json.encode(data.toJson());

class CoachUpcommingSession {
    CoachUpcommingSession({
        this.message,
        this.upcommingSession,
    });

    final String? message;
    final List<UpcommingSession>? upcommingSession;

    factory CoachUpcommingSession.fromJson(Map<String, dynamic> json) => CoachUpcommingSession(
        message: json["message"] == null ? null : json["message"],
        upcommingSession: json["data"] == null ? null : List<UpcommingSession>.from(json["data"].map((x) => UpcommingSession.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": upcommingSession == null ? null : List<dynamic>.from(upcommingSession!.map((x) => x.toJson())),
    };
}

class UpcommingSession {
    UpcommingSession({
        this.coachId,
        this.bookingId,
        this.serviceId,
        this.userId,
        this.sessionId,
        this.creationTime,
        this.startTime,
        this.endTime,
        this.status,
        this.id,
    });

    final String? coachId;
    final String? bookingId;
    final String? serviceId;
    final String? userId;
    final String? sessionId;
    final DateTime? creationTime;
    final DateTime? startTime;
    final DateTime? endTime;
    final String? status;
    final String? id;

    factory UpcommingSession.fromJson(Map<String, dynamic> json) => UpcommingSession(
        coachId: json["coachId"] == null ? null : json["coachId"],
        bookingId: json["bookingId"] == null ? null : json["bookingId"],
        serviceId: json["serviceId"] == null ? null : json["serviceId"],
        userId: json["userId"] == null ? null : json["userId"],
        sessionId: json["sessionId"] == null ? null : json["sessionId"],
        creationTime: json["creationTime"] == null ? null : DateTime.parse(json["creationTime"]),
        startTime: json["startTime"] == null ? null : DateTime.parse(json["startTime"]),
        endTime: json["endTime"] == null ? null : DateTime.parse(json["endTime"]),
        status: json["status"] == null ? null : json["status"],
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "coachId": coachId == null ? null : coachId,
        "bookingId": bookingId == null ? null : bookingId,
        "serviceId": serviceId == null ? null : serviceId,
        "userId": userId == null ? null : userId,
        "sessionId": sessionId == null ? null : sessionId,
        "creationTime": creationTime == null ? null : creationTime!.toIso8601String(),
        "startTime": startTime == null ? null : startTime!.toIso8601String(),
        "endTime": endTime == null ? null : endTime!.toIso8601String(),
        "status": status == null ? null : status,
        "id": id == null ? null : id,
    };
}
