// To parse this JSON data, do
//
//     final country = countryFromJson(jsonString);

import 'dart:convert';

List<Country> countryFromJson(String str) => List<Country>.from(json.decode(str).map((x) => Country.fromJson(x)));

String countryToJson(List<Country> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Country {
    Country({
        this.code,
        this.name,
        this.native,
        this.phone,
        this.continent,
        this.capital,
        this.currency,
        //this.languages,
    });

    final String? code;
    final String? name;
    final String? native;
    final String? phone;
    final Continent? continent;
    final String? capital;
    final String? currency;
    //final List<String> languages;

    factory Country.fromJson(Map<String, dynamic> json) => Country(
        code: json["code"],
        name: json["name"],
        native: json["native"],
        phone: json["phone"],
        continent: continentValues.map[json["continent"]],
        capital: json["capital"],
        currency: json["currency"],
        //languages: List<String>.from(json["languages"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
        "native": native,
        "phone": phone,
        "continent": continentValues.reverse[continent],
        "capital": capital,
        "currency": currency,
        //"languages": List<dynamic>.from(languages.map((x) => x)),
    };
}

enum Continent { EU, AS, NA, AF, SA, OC }

final continentValues = EnumValues({
    "AF": Continent.AF,
    "AS": Continent.AS,
    "EU": Continent.EU,
    "NA": Continent.NA,
    "OC": Continent.OC,
    "SA": Continent.SA
});

class EnumValues<T> {
    Map<String, T> map;
    late Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
