
// To parse this JSON data, do
//
//     final coachSchedule = coachScheduleFromJson(jsonString);

import 'dart:convert';

CoachSchedule coachScheduleFromJson(String str) => CoachSchedule.fromJson(json.decode(str));

String coachScheduleToJson(CoachSchedule data) => json.encode(data.toJson());

class CoachSchedule {
    CoachSchedule({
        this.message,
        this.scheduleTime,
    });

    final String? message;
    final List<ScheduleTime>? scheduleTime;

    factory CoachSchedule.fromJson(Map<String, dynamic> json) => CoachSchedule(
        message: json["message"] == null ? null : json["message"],
        scheduleTime: json["data"] == null ? null : List<ScheduleTime>.from(json["data"].map((x) => ScheduleTime.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": scheduleTime == null ? null : List<dynamic>.from(scheduleTime!.map((x) => x.toJson())),
    };
}

class ScheduleTime {
    ScheduleTime({
        this.coachId,
        this.day,
        this.startTime,
        this.endTime,
        this.isActive,
        this.id,
    });

    final String? coachId;
    final int? day;
    final String? startTime;
    final String? endTime;
    final bool? isActive;
    final String? id;

    factory ScheduleTime.fromJson(Map<String, dynamic> json) => ScheduleTime(
        coachId: json["coachId"] == null ? null : json["coachId"],
        day: json["day"] == null ? null : json["day"],
        startTime: json["startTime"] == null ? null : json["startTime"],
        endTime: json["endTime"] == null ? null : json["endTime"],
        isActive: json["isActive"] == null ? null : json["isActive"],
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "coachId": coachId == null ? null : coachId,
        "day": day == null ? null : day,
        "startTime": startTime == null ? null : startTime,
        "endTime": endTime == null ? null : endTime,
        "isActive": isActive == null ? null : isActive,
        "id": id == null ? null : id,
    };
}
