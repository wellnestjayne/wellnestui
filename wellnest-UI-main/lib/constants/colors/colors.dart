import 'package:flutter/material.dart';

class WellNestColor{


  static const wncBlue = Color(0xFF00AEEF);
  static const wncAquaBlue = Color(0xFF75CDD3);
  static const wncPeach = Color(0xFFF6B797);
  static const wncOrange = Color(0xFFFAAC5E);
  static const wncWhite = Color(0xFFFFFFFF);
  static const wncGrey = Color(0xFF555555);
  static const wncLightgrey = Color(0xFF959292);
  static const wncLighBg = Color(0xFFF9F9F9);
  static const wncRedError = Color(0xFFBB0032);
  static const wncinsiderblue = Color(0xFF1554EF);
  static const wncLightmoreAque = Color(0xFFBFEBFB);
  static const wncInstaWhite = Color(0xFFF1F1F1);


  //Badges Color [BRONZE,SILVER,GOLD]

  static const wncBronze = Color(0xFFCD7F32);
  static const wncSilver = Color(0xFFC0C0C0);
  static const wncGold =  Color(0xFFFFD700);

}