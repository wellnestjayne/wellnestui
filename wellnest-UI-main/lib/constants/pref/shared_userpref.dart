import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SharePref {
  static late SharedPreferences _prefs;

  static const String? tokenKeySession = "session";
  static const String? typeUser = "user";
  static const String? profileDetails = "details";
  static const String? hidePassword = "password";
  static const String? refreshToken = "refreshToken";
  static const String? coachIdUse = "userCoachId";
  static const String? categoryIdUse = "categoryId";

  static Future init() async => _prefs = await SharedPreferences.getInstance();

  //Set pref
  static Future setSession(String? wells) async => await _prefs.setString(tokenKeySession!, wells!);

  static Future setType(String? type) async => await _prefs.setString(typeUser!, type!);

  static Future setProfile(String? data) async => await _prefs.setString(profileDetails!, data!);

  static Future setPassword(String? password) async => await _prefs.setString(hidePassword!, password!);

  static Future setTokenRefreshed(String? reftoken) async => await _prefs.setString(refreshToken!, reftoken!);

  static Future setCoachId(String? coachId) async => await _prefs.setString(coachIdUse!, coachId!);

  static Future setCategoryId(String? categoryId) async => await _prefs.setString(categoryIdUse!, categoryId!);

  //Get Pref

  static get detailData {
    var vardata = _prefs.getString(profileDetails!) ?? '';
    var user = jsonDecode(vardata);
    return user;
  }

  static String? getCategoryId() => _prefs.getString(categoryIdUse!);

  static String? getCoachId() => _prefs.getString(coachIdUse!);

  static String? getRefToken() => _prefs.getString(refreshToken!);

  static String? getSession() => _prefs.getString(tokenKeySession!)!;

  static String? getType() => _prefs.getString(typeUser!);

  static String? getPassword() => _prefs.getString(hidePassword!);

  //Remove one by one
  static Future<bool> removeSession() async => await _prefs.remove(tokenKeySession!);

  static Future<bool> removeType() async => await _prefs.remove(typeUser!);

  static Future<bool> removeDetailsProfile() async => await _prefs.remove(profileDetails!);

  //Remove All
  static Future<bool> removeAll() async => await _prefs.clear();
}
