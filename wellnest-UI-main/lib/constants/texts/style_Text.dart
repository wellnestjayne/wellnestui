
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
class WellNestTextStyle {
  //Fonts

  static final String? nowReg = "NowReg";
  static final String? nowBlk = "NowBlk";
  static final String? nowBol = "NowBol";
  static final String? nowLig = "NowLig";
  static final String? nowMed = "NowMed";

  //With Shadow
  static nowWithShad(color,fsize,family) 
  =>TextStyle(
    fontFamily: family,
    fontSize: fsize,
    color: color,
    shadows: [
            Shadow(
              offset: Offset(3,2),
              blurRadius: 4,
              color: WellNestColor.wncGrey.withOpacity(0.3)
    )
    ],
  );

  //Fix
  static  nowRegular(color,fsize) =>TextStyle(
    color: color,
    fontSize : fsize,
    fontFamily: 'NowReg'
  );

  static  nowBlack(color,fsize) =>TextStyle(
    color: color,
    fontSize : fsize,
    fontFamily: 'NowBlk'
  );

  static  nowBold(color,fsize) =>TextStyle(
    color: color,
    fontSize : fsize,
    fontFamily: 'NowBol'
  );

  static  nowLight(color,fsize) =>TextStyle(
    color: color,
    fontSize : fsize,
    fontFamily: 'NowLig'
  );

  static  nowMedium(color,fsize) =>TextStyle(
    color: color,
    fontSize : fsize,
    fontFamily: 'NowMed'
  );

  // static final textStyleBold = TextStyle(
  //   color: Colors.black,
  //   fontSize: 18.sp,
  //   fontWeight: FontWeight.bold
  // ); 

  // static final textStyleNoneUnderline = TextStyle(
  //   color: Colors.black,
  //   fontSize: 15.sp,
  //   fontWeight: FontWeight.normal,
  // ); 

  // shadows: [
  //                               Shadow(
  //                                 offset: Offset(3,2),
  //                                 blurRadius: 2,
  //                                 color: WellNestColor.wncGrey.withOpacity(0.4)
  //                               )
  //                             ],

}