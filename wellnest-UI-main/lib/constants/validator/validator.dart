String? email(String? value) {
  String? pattern = r'^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]';
  RegExp? regExp = RegExp(pattern);
  if (!regExp.hasMatch(value!))
    return 'Email is invalid';
  else
    return null;
}

String? password(String? value) {
  // String pattern = r'^.{6,}$';
  String pattern = r'^.{6,}[A-Za-z0-9_@./#&+-]*$';
  // String pattern = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$';
  RegExp regExp = RegExp(pattern);
  if (!regExp.hasMatch(value!))
    return 'Password must be at least 6 characters';
  else
    return null;
}

String? name(String? value) {
  String? pattern = r"^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$";
  RegExp? regex = RegExp(pattern);
  if (!regex.hasMatch(value!))
    return 'Invalid Characters';
  else
    return null;
}

String? nameWithDot(String? value) {
  if (value!.isEmpty)
    return 'Please input some description.';
  else
    return null;
}

String? mobilenumberX(String? value) {
  String? patternPhone1 = r'^(9|\9)\d{9}$';
  RegExp? regExp = RegExp(patternPhone1);
  if (!regExp.hasMatch(value!))
    return 'Invalid Mobile Number';
  else
    return null;
}

String? verification(String? value) {
  if (value!.isEmpty)
    return 'X';
  else
    return null;
}

String? phone(String? value) {
  if (value!.isEmpty)
    return "Please Don't leave it blank";
  else
    return null;
}

String? confirmPassword(String? value, String? confirm) {
  if (value! != confirm!)
    return "Password Not Match";
  else
    return null;
}
