import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/starting/controller/start.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class MainRootController extends GetxController {


//Extra Unused
  static const AndroidNotificationChannel? channel = AndroidNotificationChannel(
    "id",
    "name",
    importance: Importance.high,
  );

  final FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  Future<void> messagehandling(RemoteMessage message) async {
    await Firebase.initializeApp();
    var initialization = AndroidInitializationSettings('@mipmap/launcher_icon');
    var initialSetting = InitializationSettings(android: initialization);
    flutterLocalNotificationsPlugin!.initialize(initialSetting);
    print("Recieve Message ${message.notification!.body}");
    flutterLocalNotificationsPlugin!.show(
        message.notification.hashCode,
        message.notification!.title,
        message.notification!.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel!.id,
            channel!.name,
            //channel!.description,
            //icon: 'launch_background',
          ),
        ));
  }
// End of UnUsed

  @override
  void onInit() {
    super.onInit();
    // FirebaseMessaging.instance.getInitialMessage();
    firebaseMessageListen();
    getToken();
  }

  localNotifProviderDetail() async {
    await flutterLocalNotificationsPlugin!
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel!);
    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  firebaseMessageListen() {
    var initialization = AndroidInitializationSettings('@mipmap/launcher_icon');
    var initialSetting = InitializationSettings(android: initialization);

    var imageIcon = BigPictureStyleInformation(DrawableResourceAndroidBitmap("@mipmap/launcher_icon"),
        largeIcon: DrawableResourceAndroidBitmap("@mipmap/launcher_icon"),
        hideExpandedLargeIcon: true,
        htmlFormatContent: true,
        htmlFormatTitle: true);

    // var bigTextx = BigTextStyleInformation(bigText);

    // var otherStyle = MessagingStyleInformation(person)

    flutterLocalNotificationsPlugin!.initialize(initialSetting);
    //Foreground
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null && !kIsWeb) {
        print(message.notification!.body);
        print(message.notification!.title);
        flutterLocalNotificationsPlugin!.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel!.id,
                channel!.name,
                //icon: 'launch_background',
                fullScreenIntent: true,
                channelDescription: channel!.description,
                priority: Priority.max,
                enableVibration: true,
                importance: Importance.high,
                largeIcon: DrawableResourceAndroidBitmap('@mipmap/launcher_icon'),
                styleInformation: BigTextStyleInformation(notification.body!,
                    contentTitle: notification.title!,
                    htmlFormatBigText: true,
                    htmlFormatTitle: true,
                    htmlFormatContent: true),
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null && !kIsWeb) {
        flutterLocalNotificationsPlugin!.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel!.id,
                channel!.name,
                //icon: 'launch_background',
                fullScreenIntent: true,
                channelDescription: channel!.description,
                priority: Priority.max,
                enableVibration: true,
                importance: Importance.high,
                largeIcon: DrawableResourceAndroidBitmap('@mipmap/launcher_icon'),
                styleInformation: BigTextStyleInformation(notification.body!,
                    contentTitle: notification.title!,
                    htmlFormatBigText: true,
                    htmlFormatTitle: true,
                    htmlFormatContent: true),
              ),
            ));
      }
    });
  }

  final deviceToken = ''.obs;

  getToken() async {
    final result = await FirebaseMessaging.instance.getToken();
    await FirebaseMessaging.instance.subscribeToTopic('all');
    deviceToken(result);
    print(deviceToken);
  }
}
