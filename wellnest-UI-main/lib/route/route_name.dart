import 'package:flutter/animation.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/badges/badges_view.dart';
import 'package:wellnest/pages/booking/cancel_booking.dart';
import 'package:wellnest/pages/categories/category_all_service.dart';
import 'package:wellnest/pages/certificate/certificate_view.dart';
import 'package:wellnest/pages/coach/book_session/book_session.dart';
import 'package:wellnest/pages/coach/book_session/checkout/book_checkout.dart';
import 'package:wellnest/pages/coach/certificate/certificate_page.dart';
import 'package:wellnest/pages/coach/coach_view.dart';
import 'package:wellnest/pages/as_page/as_log.dart';
import 'package:wellnest/pages/as_page/controller/as_log_controller.dart';
import 'package:wellnest/pages/coach/featured_coach.dart';
import 'package:wellnest/pages/coach/review/review_coach.dart';
import 'package:wellnest/pages/coach/service/coach_service.dart';
import 'package:wellnest/pages/create_profile_coach/create_coach.dart';
import 'package:wellnest/pages/create_profile_coach/upload_image/upload_image.dart';
import 'package:wellnest/pages/featured_coach/feature_coach.dart';
import 'package:wellnest/pages/home/binding/binding.dart';
import 'package:wellnest/pages/home/home_view.dart';
import 'package:wellnest/pages/login/login_view.dart';
import 'package:wellnest/pages/main/binding/main_binding.dart';
import 'package:wellnest/pages/main/main_home.dart';
import 'package:wellnest/pages/notification/notification_view.dart';
import 'package:wellnest/pages/profile/create_service/create_service.dart';
import 'package:wellnest/pages/profile/edit_profile/edit_profile.dart';
import 'package:wellnest/pages/profile/edit_profile/widget/edit_image_profile.dart';
import 'package:wellnest/pages/profile/user_service/user_service.dart';
import 'package:wellnest/pages/profile/view_profile/view_profile.dart';
import 'package:wellnest/pages/refer_friend/refer_friend.dart';
import 'package:wellnest/pages/service/service_screen.dart';
import 'package:wellnest/pages/service/service_view.dart';
import 'package:wellnest/pages/onboarding/onboarding_page.dart';
import 'package:wellnest/pages/profile/profile_vieww.dart';
import 'package:wellnest/pages/register/confirm_email/confirm_email.dart';
import 'package:wellnest/pages/splash_screen/splash_view.dart';
import 'package:wellnest/pages/starting/start_page.dart';
import 'package:wellnest/pages/subscription/monthly/subscriptions_monthly.dart';
import 'package:wellnest/pages/subscription/subscription_view.dart';
import 'package:wellnest/pages/write/views/evening.dart';
import 'package:wellnest/pages/write/views/morning.dart';
import 'package:wellnest/pages/write/write.dart';
import 'package:wellnest/route/route_page.dart';

class Apppages {
  Apppages.__();

  static final listRoute = [
    GetPage(
      name: AppRouteName.splashScreen!,
      page: () => SplashScreenw(),
      transition: Transition.leftToRight,
      curve: Curves.easeIn,
      transitionDuration: 450.milliseconds,
    ),
    GetPage(
      name: AppRouteName.startingpage!,
      page: () => StartingPagew(),
      transition: Transition.leftToRight,
      curve: Curves.easeIn,
      transitionDuration: 450.milliseconds,
    ),
    GetPage(
      name: AppRouteName.loginPage!,
      page: () => LoginVieww(),
      transition: Transition.leftToRight,
      curve: Curves.easeIn,
      transitionDuration: 450.milliseconds,
    ),
    // GetPage(
    //   name: AppRouteName.registerPage!,
    //   page: () => LoginVieww(),
    //   transition: Transition.leftToRight,
    //   curve: Curves.easeIn,
    //   transitionDuration: 450.milliseconds,
    // ),
    GetPage(
      name: AppRouteName.dashBoard!,
      page: () => MainHomew(),
      binding: MainBinding(),
      transition: Transition.fade,
      curve: Curves.slowMiddle,
      transitionDuration: 450.milliseconds,
    ),
    GetPage(
      name: AppRouteName.loginAs!,
      page: () => LogAsCorUw(),
      transition: Transition.leftToRight,
      curve: Curves.easeIn,
      transitionDuration: 450.milliseconds,
    ),
    GetPage(
      name: AppRouteName.onBoard!,
      page: () => OnBoardingPagew(),
      transition: Transition.leftToRight,
      curve: Curves.elasticInOut,
      transitionDuration: 450.milliseconds,
    ),
    GetPage(
      name: AppRouteName.confirmCognito!,
      page: () => ConfirmEmailCognito(),
      transition: Transition.leftToRight,
      curve: Curves.elasticInOut,
      transitionDuration: 450.milliseconds,
    ),

    GetPage(
      name: AppRouteName.coachView!,
      page: () => CoachView(),
    ),
    GetPage(
      name: AppRouteName.certificateView!,
      page: () => CertificateView(),
    ),

    GetPage(
      name: AppRouteName.serviceView!,
      page: () => ServiceView(),
    ),

    GetPage(
      name: AppRouteName.serviceScreen!,
      page: () => ServiceScreen(),
    ),
    //),
    //Journals
    GetPage(
        name: AppRouteName.writeJournal!,
        page: () => WriteVieww(),
        transition: Transition.leftToRight,
        curve: Curves.easeIn,
        transitionDuration: 450.milliseconds,
        children: [
          //Extension children Pages
          GetPage(
            name: AppRouteName.morningQuestion!,
            page: () => JournalMorning(),
            transition: Transition.leftToRight,
            curve: Curves.elasticInOut,
            transitionDuration: 450.milliseconds,
          ),
          GetPage(
            name: AppRouteName.eveningQuestion!,
            page: () => JournalEvening(),
            transition: Transition.leftToRight,
            curve: Curves.elasticInOut,
            transitionDuration: 450.milliseconds,
          ),
        ]),

    //Home

    GetPage(
      name: AppRouteName.homeView!,
      page: () => HomeVieww(),
      binding: BindingHome(),
      children: [
        GetPage(
          name: AppRouteName.notification!,
          page: () => NotificationVieww(),
          transition: Transition.leftToRightWithFade,
          curve: Curves.elasticInOut,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.viewProfile!,
          page: () => ViewProfileUser(),
          transition: Transition.leftToRight,
          curve: Curves.easeInBack,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.createServicesCoach!,
          page: () => CreateServiceCoach(),
          transition: Transition.leftToRight,
          curve: Curves.easeInBack,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.userServiceCategory!,
          page: () => UserServiceCategory(),
          transition: Transition.leftToRight,
          curve: Curves.easeInBack,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.editProfile!,
          page: () => EditProfileCoach(),
          transition: Transition.leftToRight,
          curve: Curves.easeInBack,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.profileUser!,
          page: () => ProfileVieww(),
          transition: Transition.leftToRightWithFade,
          curve: Curves.elasticInOut,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.badges!,
          page: () => BadgesView(),
          transition: Transition.leftToRightWithFade,
          curve: Curves.elasticInOut,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.featureCoach!,
          page: () => FeatureCoach(),
          transition: Transition.leftToRightWithFade,
          curve: Curves.elasticInOut,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.categoryAllService!,
          page: () => CategoryAllService(),
          transition: Transition.leftToRightWithFade,
          curve: Curves.elasticInOut,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
            name: AppRouteName.coachFeaturedProfile!,
            page: () => FeaturedCoachProfile(),
            transition: Transition.leftToRight,
            curve: Curves.easeInOutBack,
            transitionDuration: 450.milliseconds,
            children: [
              GetPage(
                name: AppRouteName.coachCertificate!,
                page: () => CertificatePageProfile(),
                transition: Transition.leftToRight,
                curve: Curves.easeInOutBack,
                transitionDuration: 450.milliseconds,
              ),
              GetPage(
                  name: AppRouteName.coachServices!,
                  page: () => ServiceCoachDetails(),
                  transition: Transition.leftToRight,
                  curve: Curves.easeInOutBack,
                  transitionDuration: 450.milliseconds,
                  children: [
                    GetPage(
                      name: AppRouteName.coachBookSession!,
                      page: () => BookSessionToCoach(),
                      transition: Transition.leftToRight,
                      curve: Curves.easeInOutBack,
                      transitionDuration: 450.milliseconds,
                    ),
                  ]),
              GetPage(
                name: AppRouteName.coachReviews!,
                page: () => ReviewCoachpage(),
                transition: Transition.leftToRight,
                curve: Curves.easeInOutBack,
                transitionDuration: 450.milliseconds,
              ),
            ]),
        GetPage(
          name: AppRouteName.cancelBooking!,
          page: () => CancelBookingView(),
          transition: Transition.leftToRight,
          curve: Curves.easeInOutBack,
          transitionDuration: 450.milliseconds,
        ),
        GetPage(
          name: AppRouteName.referFriend!,
          page: () => ReferAFriendView(),
          transition: Transition.leftToRight,
          curve: Curves.easeInOutBack,
          transitionDuration: 450.milliseconds,
        ),
      ],
    ),

    //Subscription
    GetPage(
        name: AppRouteName.subscription!,
        page: () => SubscriptionView(),
        transition: Transition.leftToRight,
        curve: Curves.easeIn,
        transitionDuration: 450.milliseconds,
        children: [
          //Monthly
          GetPage(
            name: AppRouteName.subscriptionMonthly!,
            page: () => SubscriptionMonthy(),
            transition: Transition.leftToRight,
            curve: Curves.easeInOut,
            transitionDuration: 450.milliseconds,
          ),
        ]),

    //Checkout Session Page
    GetPage(
      name: AppRouteName.checkoutBook!,
      page: () => BookCheckout(),
      transition: Transition.leftToRight,
      curve: Curves.easeInBack,
      transitionDuration: 450.milliseconds,
    ),

    //Create Coach
    GetPage(
        name: AppRouteName.createCoach!,
        page: () => CreateProfileCoach(),
        transition: Transition.leftToRight,
        curve: Curves.easeInBack,
        transitionDuration: 450.milliseconds,
        children: [
          GetPage(
            name: AppRouteName.uploadImageCoach!,
            page: () => UploadImageForProfile(),
            transition: Transition.leftToRight,
            curve: Curves.easeInBack,
            transitionDuration: 450.milliseconds,
          ),
          GetPage(
            name: AppRouteName.editImageProfile!,
            page: () => EditImageProfile(),
            transition: Transition.leftToRight,
            curve: Curves.easeInBack,
            transitionDuration: 450.milliseconds,
          ),
        ]),
  ];
}
