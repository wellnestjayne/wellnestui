class AppRouteName {
  static const String? splashScreen = "/";
  static const String? startingpage = "/startingPage";
  static const String? loginPage = "/login";
  static const String? registerPage = "/register";
  static const String? dashBoard = "/dashoboard";
  static const String? coachView = "/coachView";
  static const String? loginAs = "/loginAs";
  static const String? certificateView = "/certificateView";
  static const String? serviceView = "/serviceView";
  static const String? serviceScreen = "/serviceScreen";

  static const String? writeJournal = "/journals";
  static const String? morningQuestion = "/morningQuest";
  static const String? eveningQuestion = "/eveningQuest";
  static const String? onBoard = "/onboard";
  static const String? confirmCognito = "/confirmation";

  //Home
  static const String? profileUser = "/profile";
  static const String? homeView = "/home";
  static const String? notification = "/notification";
  static const String? badges = "/badges";
  static const String? featureCoach = "/featureCoach";
  static const String? categoryAllService = "/allService";
  static const String? coachFeaturedProfile = "/coach-profile";
  static const String? coachCertificate = "/coachCertificate";
  static const String? coachServices = "/coachService";
  static const String? coachReviews = "/coachReviews";
  static const String? coachBookSession = "/coachBookSession";
  static const String? viewProfile = "/viewProfile";
  static const String? editProfile = "/editProfile";
  static const String? editImageProfile = "/editImageProfile";
  static const String? userServiceCategory = "/userServiceCategory";
  static const String? createServicesCoach = "/createServicecoach";

  //Subscription
  static const String? subscription = "/subscription";
  static const String? subscriptionMonthly = "/subscriptionMonthly";
  static const String? subscriptionAnnual = "/subscriptionAnnual";
  // static const String? subscriptionCheckout ="/checkout";
  // static const String? subscriptionEditPayment = "/editPayment";
  // static const String? subscriptionSuccessPayment = "/successPayment";

  //Booking
  static const String? checkoutBook = "/checkOutSession";
  static const String? pickadateCheck = "/pickADate";
  static const String? editpayment = "/editPayment";
  static const String? cancelBooking = "/cancelBooking";

  //Create Coach Profile
  static const String? createCoach = "/createCoach";
  static const String? uploadImageCoach = "/uploadImages";

  //Settings
  static const String? referFriend = "/referAFriend";
}
