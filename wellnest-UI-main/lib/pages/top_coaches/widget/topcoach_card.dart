
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class TopCoachCards extends StatelessWidget {
  
  final bool? change;
  final Function()? press;
  final String? name;
  final String? special;
  final double? follow;
  final double? follwing;
  final String? imagePath;

  const TopCoachCards({Key? key, this.change, this.press, this.name, this.special, this.follow, this.follwing, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: WellNestColor.wncWhite,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            offset: Offset(4,3),
            blurRadius: 3,
            spreadRadius: 3,
            color: WellNestColor.wncLightgrey.withOpacity(0.2),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 5.h,),
            Container(
              height: 90.h,
              width: 124.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  // /fit: BoxFit.contain,
                  image: AssetImage(imagePath!) )
              ),
            ),
            SizedBox(height: 15.h,),
            Text(name!,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncGrey, 16.sp),
            ),
            SizedBox(height: 5.h,),
            Text(special!,
            style: WellNestTextStyle.nowRegular(
              WellNestColor.wncLightgrey, 10.sp),
            ),
            SizedBox(height: 15.h,),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RichText(text: 
                  TextSpan(
                    children: [
                      TextSpan(
                        text:follow!.toStringAsFixed(follow!.truncate() == follow ? 0 : 1),
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 12.sp)
                      ),
                      TextSpan(
                        text:" Followers",
                        style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 12.sp)
                      ),
                    ]
                  )),
                  SizedBox(width: 5.w,),
                  RichText(text: 
                  TextSpan(
                    children: [
                      TextSpan(
                        text:follwing!.toStringAsFixed(follwing!.truncate() == follwing ? 0 : 1),
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 12.sp)
                      ),
                      TextSpan(
                        text:" Following",
                        style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 12.sp)
                      ),
                    ]
                  )),
                ],
              ),
            ),
            SizedBox(height: 15.h,),
            BlueButtonStandard(
              bluebutton: press,
              title:change! ?  "Followed" :"Follow",
              back: change! ? WellNestColor.wncWhite : WellNestColor.wncBlue,
              border: change! ? WellNestColor.wncWhite : WellNestColor.wncBlue,
              textColor: change! ? WellNestColor.wncBlue : WellNestColor.wncWhite,
              sizeText: 14.sp,
              height: 40.h,
              width: 120.w,
            ),
            SizedBox(height: 10.h,),
          ],
        ),
      ),
    );
  }
}