import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/categories/widget/top_coachCard.dart';
import 'package:wellnest/pages/top_coaches/controller/topcoahController.dart';
import 'package:wellnest/pages/top_coaches/widget/topcoach_card.dart';
import 'package:wellnest/standard_widgets/appbar.dart';
import 'package:wellnest/standard_widgets/header.dart';

class TopCoachView extends StatelessWidget {

  final topcoachController = Get.put(TopCoachController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
      child: Scaffold(
      appBar: StandardAppbaruse(
        function: ()=>Get.back(),
        color: topcoachController.mainController.colorType,
        title: "Top Coaches",
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GridView.builder(
        itemCount: topcoachController.listtop.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: MediaQuery.of(context).orientation == Orientation.landscape ? 4 : 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          childAspectRatio: 0.7
          ),
        itemBuilder: (context,index)
        => 
        Obx(
          ()=>
          Padding(
              padding: const EdgeInsets.only(
                left: 0.5,
                right: 0.5
              ),
              child: TopCoachCards(
                press: ()=>topcoachController.listtop[index].bools!(!topcoachController.listtop[index].bools!.value),
                follow: topcoachController.listtop[index].followers,
                follwing: topcoachController.listtop[index].following,
                name: topcoachController.listtop[index].name,
                special: topcoachController.listtop[index].special,
                imagePath: topcoachController.listtop[index].imagePath,
                change: topcoachController.listtop[index].bools!.value
                ,
              ),
          
          ),
        )
        ),
      )
      ), 
      onWillPop: ()async=>false),
    );
  }
}

//  TopCoachCards(
//             press: (){},
//             name: "Erchil",
//             special: "Rapist",
//             follow: 45,
//             follwing: 44,
//             change: true,
//             imagePath: WellnestAsset.pssuy,
//           ),