
import 'package:get/get.dart';

class TopCoachModel{


  final String? imagePath;
  final String? name;
  final String? special;
  final double? followers;
  final double? following;
  final RxBool? bools;

  TopCoachModel({this.imagePath, this.name, this.special, this.followers, this.following,this.bools});


}