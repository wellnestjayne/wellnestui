import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/onboarding/controllers/onboard_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/pages/onboarding/widgets/custom_appbar.dart';
import 'package:wellnest/pages/onboarding/widgets/image_page.dart';

class OnBoardingPagew extends StatelessWidget {

  final onboardController = Get.put(OnboardController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: WellNestColor.wncWhite,
      body: WillPopScope(
        child: SafeArea(
          child: PageView.builder(
            controller: onboardController.pageControl,
            onPageChanged: onboardController.selectedPageNumber,
            itemCount: onboardController.onboardPages.length,
            itemBuilder: (contentext, index) => Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Container(
                height: MediaQuery.of(context).size.height.h,
                width: MediaQuery.of(context).size.width.w,
                child: Column(
                  children: [
                    Spacer(),
                    ImageContainingw(
                      imagepath:
                          onboardController.onboardPages[index].imagepath,
                    ),
                    Text(onboardController.onboardPages[index].headtitle,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 28.sp,
                            fontWeight: FontWeight.bold)),
                    Spacer(),
                    Text(onboardController.onboardPages[index].description,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.sp,
                            fontWeight: FontWeight.normal)),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ),
        ),
        onWillPop: () async => false,
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Container(
          height: 75.h,
          width: MediaQuery.of(context).size.width.w,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Obx(
                    () => onboardController.isLastpage
                        ? AppbarWellnestw(
                            button: () => onboardController.forwardAct(Get.parameters),
                          )
                        : Container(),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                      onboardController.onboardPages.length,
                      (index) => Obx(
                            () => AnimatedContainer(
                              duration: 200.milliseconds,
                              margin: EdgeInsets.only(right: 5),
                              height: 10.h,
                              width:
                                  onboardController.selectedPageNumber.value ==
                                          index
                                      ? 25.w
                                      : 10.w,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: WellNestColor.wncBlue),
                            ),
                          )),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
