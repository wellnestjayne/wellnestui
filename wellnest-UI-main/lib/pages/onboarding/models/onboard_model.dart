
class OnboardModel{

  final imagepath;
  final headtitle;
  final description;

  OnboardModel({this.imagepath, this.headtitle, this.description});

}