

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/pages/login/login_view.dart';
import 'package:wellnest/pages/onboarding/models/onboard_model.dart';
import 'package:wellnest/route/route_page.dart';

class OnboardController extends GetxController{

  var selectedPageNumber = 0.obs;
  var showit = false.obs;
  
  var pageControl = PageController();
  bool get isLastpage => selectedPageNumber.value == onboardPages.length -1;


  forwardAct(data){
    if(isLastpage){
      showit(true);
      Get.offNamedUntil(AppRouteName.confirmCognito!
      , (route) => false,parameters: data);
      
    }
    else {
      pageControl.nextPage(duration: 300.milliseconds, curve: Curves.easeInCubic);
      }
  }


  List<OnboardModel> onboardPages =
  [
    OnboardModel(
      imagepath: WellnestAsset.firstImageWellNest,
      headtitle: "Welcome to our Well-being Community",
      description: "You have joined a community focused on becoming better together."
    ),
     OnboardModel(
      imagepath: WellnestAsset.secondImageWellNest,
      headtitle: "Find a Coach",
      description: "Choose a coach who can help you get the most out of life."
    ),
    OnboardModel(
      imagepath: WellnestAsset.thirdImageWellNest,
      headtitle: "Choose a Category",
      description: "Pick a category specific to your need or goal: mental wellness, performance, relationships, spiritual, lifestyle, or financial coaching."
    ),
   OnboardModel(
      imagepath: WellnestAsset.fourthImageWellNest,
      headtitle: "Get Coached in-app",
       description: "Enjoy the convenience of scheduling and conducting the session inside the app. Use the daily journal to record your experience today."
    ),
   OnboardModel(
      imagepath: WellnestAsset.fifthImageWellNest,
      headtitle: "Engage with the Community",
      description: "Interact with others to share ideas, tips and experiences about wellness. This is your safe space."
    ),
  ];


}