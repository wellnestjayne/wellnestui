
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class AppbarWellnestw extends StatelessWidget {
  
  final Function()? button;

  const AppbarWellnestw({Key? key, this.button}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 110.w,
      height: 35.h,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          primary: WellNestColor.wncBlue,
        ),
        onPressed: button,
        child: Center(
          child: Text("Get Started!",
          style: TextStyle(
            color: WellNestColor.wncWhite,
            fontSize: 12.sp,
            fontWeight: FontWeight.bold
          ),
          ),
        ),
      ),
    );
  }


}