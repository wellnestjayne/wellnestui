import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class ImageContainingw extends StatelessWidget {
  final String? imagepath;

  const ImageContainingw({Key? key, this.imagepath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 433.h,
      width: 433.w,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(imagepath!),
          fit: BoxFit.fitHeight
          ),
      ),
    );
  }
}
