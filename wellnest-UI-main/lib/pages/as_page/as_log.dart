

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/as_page/controller/as_log_controller.dart';
import 'package:wellnest/pages/as_page/widget/button.dart';
import 'package:wellnest/pages/login/login_view.dart';
import 'package:wellnest/pages/register/register_view.dart';
import 'package:wellnest/route/route_page.dart';

class LogAsCorUw extends StatelessWidget {
  

  final getAsLog = Get.put(AsLogInAs());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
       onWillPop: ()async=> await Get.offNamed(AppRouteName.startingpage!),
      child: SafeArea(
        child: Scaffold(
            body: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                 Spacer(),
                 Center(
                   child: Text("Welcome to Wellnest!",
                   style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 20.sp),
                   ),
                 ),
                 Spacer(flex: 1,),
                 ButtonAs(
                   press: ()=>Get.off(()=>RegisterVieww(typeUser: 'User',)),
                   textSize: 14.sp,
                   textColor: WellNestColor.wncWhite,
                   firstpara: "Signup as ",
                   secondpara: "User",
                   height: 40,
                   width: MediaQuery.of(context).size.width.w,
                   image: WellnestAsset.svgCoach,
                   color: WellNestColor.wncBlue,
                   picSizeh: 125.h,
                   picWidthw: 135.w,
                 ),
                 SizedBox(height: 15.h,),
                 ButtonAs(
                    press: ()=>Get.off(()=>RegisterVieww(typeUser: 'Coach',)),
                   textSize: 14.sp,
                   textColor: WellNestColor.wncWhite,
                   firstpara: "Signup as ",
                   secondpara: "Coach",
                   height: 40,
                   width: MediaQuery.of(context).size.width.w,
                   image: WellnestAsset.svgUser,
                   color: WellNestColor.wncBlue,
                    picSizeh: 125.h,
                   picWidthw: 135.w,
                 ),
                 SizedBox(height: 35.h,),
                 InkWell(
                        onTap: () => Get.off(() => LoginVieww()),
                        child: Text(
                          "Already have an Account? Login",
                          style: TextStyle(
                              color: WellNestColor.wncBlue,
                              fontSize: 18.sp,
                              fontWeight: FontWeight.normal,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                 Spacer(),
                ],
              ),
            ),
        ),
      ),
    );
  }
}