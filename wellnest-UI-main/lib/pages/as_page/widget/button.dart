

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class ButtonAs extends StatelessWidget {
  
  final Function()? press;
  final String? firstpara;
  final String? secondpara;
  final Color? textColor;
  final double? textSize;
  final String? image;
  final Color? color;
  final double? height;
  final double? width;
  final double? picSizeh;
  final double? picWidthw;

  const ButtonAs({Key? key, this.press, this.firstpara, this.secondpara, this.textColor, this.textSize, this.image, this.color, this.height, this.width, this.picSizeh, this.picWidthw}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Center(
            child: SvgPicture.asset(image!,
            semanticsLabel: '',
            height: picSizeh,
            width: picWidthw,
            ),
          ),
          SizedBox(height: 13.h,),
          Container(
            height: height,
            width: width,
            child: ElevatedButton(
              onPressed: press,
              style: ElevatedButton.styleFrom(
                primary: color,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)
                )
              ),
              child: Center(
                child: RichText(
                  text: TextSpan(
                   children: [
                     TextSpan(
                       text: firstpara!,
                       style: WellNestTextStyle.nowRegular(textColor, textSize),
                     ),
                     TextSpan(
                       text: secondpara!,
                       style: WellNestTextStyle.nowBold(textColor,textSize)
                     )
                   ] 
                  )),
              ),
            ),
          ),
        ],
      ),
    );
  }
}