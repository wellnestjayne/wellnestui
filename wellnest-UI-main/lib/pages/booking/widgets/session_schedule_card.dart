import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';

class SessionScheduleCard extends StatelessWidget {
  SessionScheduleCard({Key? key, this.date, this.time}) : super(key: key);

  final String? date;
  final String? time;

  final bookController = Get.put(BookingController());
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            height: 72,
            width: 337,
            decoration: BoxDecoration(
                color: WellNestColor.wncLighBg,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(2, 3),
                      blurRadius: 2,
                      spreadRadius: 1,
                      color: WellNestColor.wncGrey.withOpacity(0.3))
                ]),
            child: Padding(
              padding: const EdgeInsets.only(left: 10, top: 8, bottom: 8, right: 100),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      '$date\n$time',
                      textAlign: TextAlign.left,
                      style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 13.0),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
