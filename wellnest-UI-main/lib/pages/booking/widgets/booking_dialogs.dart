import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

final bookController = Get.put(BookingController());

class BookingDialog {
  static cancelBooking({Function()? press}) {
    return Get.defaultDialog(
      contentPadding: const EdgeInsets.all(20),
      title: '',
      content: Column(
        children: [
          Image.asset(WellnestAsset.question),
          SizedBox(height: 30),
          Text('Are you sure you want to cancel your session?',
              textAlign: TextAlign.center, style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 13.0)),
        ],
      ),
      cancel: BlueButtonStandard(
        height: 40,
        width: 92,
        sizeText: 15.0,
        textColor: WellNestColor.wncBlue,
        back: WellNestColor.wncWhite,
        border: WellNestColor.wncBlue,
        title: "Cancel",
        bluebutton: () => Get.back(),
      ),
      confirm: BlueButtonStandard(
        height: 40,
        width: 92,
        sizeText: 15.0,
        textColor: WellNestColor.wncWhite,
        back: WellNestColor.wncBlue,
        border: WellNestColor.wncBlue,
        title: "Confirm",
        bluebutton: press,
      ),
    );
  }

  static successBooking({Function()? press}) {
    return Get.defaultDialog(
      barrierDismissible: false,
      contentPadding: const EdgeInsets.all(20),
      title: '',
      content: Column(
        children: [
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(shape: BoxShape.circle, color: WellNestColor.wncAquaBlue),
            child: Center(
              child: FaIcon(FontAwesomeIcons.check, color: WellNestColor.wncWhite),
            ),
          ),
          SizedBox(height: 30),
          Text(
            'Booking Successfully Cancelled',
            textAlign: TextAlign.center,
            style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 13.0),
          ),
        ],
      ),
      confirm: BlueButtonStandard(
        height: 40,
        width: 92,
        sizeText: 15.0,
        textColor: WellNestColor.wncWhite,
        back: WellNestColor.wncBlue,
        border: WellNestColor.wncBlue,
        title: "Okay",
        bluebutton: () {
          Get.back();
          bookController.getBookingByUser();
          Get.back(id: bookController.shitme.current.value);
        },
      ),
    );
  }

  static errorBooking({Function()? press}) {
    return Get.defaultDialog(
      barrierDismissible: false,
      contentPadding: const EdgeInsets.all(20),
      title: '',
      content: Column(
        children: [
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(shape: BoxShape.circle, color: WellNestColor.wncAquaBlue),
            child: Center(
              child: FaIcon(FontAwesomeIcons.timesCircle, color: WellNestColor.wncWhite),
            ),
          ),
          SizedBox(height: 30),
          Text(
            "Sorry, 4 days has passed. You can't cancel your booking now.",
            textAlign: TextAlign.center,
            style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 13.0),
          ),
        ],
      ),
      confirm: BlueButtonStandard(
        height: 40,
        width: 92,
        sizeText: 15.0,
        textColor: WellNestColor.wncWhite,
        back: WellNestColor.wncBlue,
        border: WellNestColor.wncBlue,
        title: "Okay",
        bluebutton: () {
          Get.back();
          bookController.getBookingByUser();
          Get.back(id: bookController.shitme.current.value);
        },
      ),
    );
  }
}
