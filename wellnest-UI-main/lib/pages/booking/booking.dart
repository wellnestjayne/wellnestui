import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/model/coach_view_upcomming_session.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';
import 'package:wellnest/standard_widgets/booking_widgets/list_view_booking.dart';
import 'package:wellnest/standard_widgets/calendar/customization/calendar_style.dart';
import 'package:wellnest/standard_widgets/calendar/shared/utils.dart';
import 'package:wellnest/standard_widgets/calendar/table_calendar.dart';
import 'package:intl/intl.dart';

class BookingVieww extends GetView<BookingController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        backgroundColor: WellNestColor.wncWhite,
        body: SafeArea(
          child: Column(
            children: [
              controller.load.isTrue
                  ? Container()
                  : TableCalendar(
                      eventLoader: controller.getEventsList,
                      rowHeight: 90,
                      calendarFormat: CalendarFormat.week,
                      firstDay: DateTime.utc(2021, 8, 2),
                      lastDay: DateTime.utc(2030, 3, 14),
                      focusedDay: controller.focusDay.value,
                      calendarStyle: CalendarStyle(isTodayHighlighted: true),
                      selectedDayPredicate: (day) => isSameDay(controller.selectedDay.value, day),
                      onDaySelected: (date1, date2) => controller.onDaySelect(date1, date2),
                    ),
              SizedBox(height: 10.h),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                      color: WellNestColor.wncWhite,
                      boxShadow: [
                        BoxShadow(spreadRadius: 3, blurRadius: 3, color: WellNestColor.wncGrey.withOpacity(0.1))
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: controller.viewBydate.length == 0
                        ? Center(
                            child: Text(
                            'You have no booked session today.',
                            style: TextStyle(fontSize: 20.sp, color: WellNestColor.wncAquaBlue),
                          ))
                        : ListView.builder(
                            itemCount: controller.viewBydate.length,
                            itemBuilder: (context, index) {
                              final startTime = DateFormat("HH:mm aa")
                                  .format(DateTime.parse('${controller.viewBydate[index].startTime}'));
                              final endTime = DateFormat("HH:mm aa")
                                  .format(DateTime.parse('${controller.viewBydate[index].endTime}'));
                              print(controller.viewBydate.length);
                              return ViewListCalendarw(
                                startTime: startTime,
                                endTime: endTime,
                                coachId: controller.viewBydate[index].id,
                              );
                            },
                          ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
