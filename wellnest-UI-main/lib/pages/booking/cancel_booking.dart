import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';
import 'package:wellnest/pages/booking/widgets/booking_banner_card.dart';
import 'package:wellnest/pages/booking/widgets/booking_dialogs.dart';
import 'package:wellnest/pages/booking/widgets/session_schedule_card.dart';
import 'package:wellnest/pages/coach/book_session/view_calendar/view_calendar.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:intl/intl.dart';

class CancelBookingView extends StatelessWidget {
  CancelBookingView({Key? key}) : super(key: key);

  final bookController = Get.put(BookingController());
  final coachId = Get.arguments['id'];
  final coachName = Get.arguments['name'];
  final coachPhoto = Get.arguments['photo'];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              SizedBox(height: 10),
              Text(
                'Coaching Session',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20),
              Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(2, 6),
                      color: WellNestColor.wncGrey.withOpacity(0.1),
                    ),
                  ],
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    alignment: Alignment.topCenter,
                    image: NetworkImage(coachPhoto),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Text(
                coachName,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              BookingCardBanner(),
              SizedBox(height: 10),
              HeaderTitleSubs(
                showFunction: false,
                title: "Session Schedule",
                extra: "Remove",
                extraFunction: null,
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: bookController.listBookingByUser.length,
                  itemBuilder: (context, index) {
                    final date = DateFormat("EEEE MMMM dd, yyyy")
                        .format(DateTime.parse('${bookController.listBookingByUser[index].startTime}'));
                    final startTime = DateFormat("HH:mm aa")
                        .format(DateTime.parse('${bookController.listBookingByUser[index].startTime}'));
                    final endTime = DateFormat("HH:mm aa")
                        .format(DateTime.parse('${bookController.listBookingByUser[index].endTime}'));
                    return SessionScheduleCard(
                      date: date,
                      time: '$startTime - $endTime',
                    );
                  },
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(bottom: 20, left: 20, right: 20),
          child: Container(
            height: 90,
            child: Column(
              children: [
                Center(
                  child: BlueButtonStandard(
                    bluebutton: () {
                      Get.back();
                      Get.to(() => ViewCalendaroUser(), id: 0);
                    },
                    title: "View Calendar",
                    back: WellNestColor.wncBlue,
                    border: WellNestColor.wncBlue,
                    textColor: WellNestColor.wncWhite,
                    sizeText: 15.0,
                    height: 37,
                    width: 334,
                  ),
                ),
                SizedBox(height: 10),
                Center(
                  child: BlueButtonStandard(
                    bluebutton: () {
                      BookingDialog.cancelBooking(
                        press: () => bookController.cancelBooking(coachId.toString()),
                      );
                    },
                    title: "Cancel Booking",
                    back: WellNestColor.wncRedError,
                    border: WellNestColor.wncRedError,
                    textColor: WellNestColor.wncWhite,
                    sizeText: 15.0,
                    height: 37,
                    width: 334,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
