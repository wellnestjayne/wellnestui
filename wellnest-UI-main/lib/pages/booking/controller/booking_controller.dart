import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_coaches/api_get_upcomming_session.dart';
import 'package:wellnest/model/coach_user_schedule.dart';
import 'package:wellnest/model/coach_view_upcomming_session.dart';
import 'package:wellnest/pages/booking/widgets/booking_dialogs.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:intl/intl.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class BookingController extends GetxController {
  Rx<DateTime> focusDay = DateTime.now().obs;
  Rx<DateTime> selectedDay = DateTime.now().obs;
  final shitme = Get.find<MainController>();
  late ScrollController scroll = ScrollController();

  final List<UpcommingSession> listBookingByUser = <UpcommingSession>[].obs;
  final List<CoachUserSchedule> coachUserDetails = <CoachUserSchedule>[].obs;
  final List<UpcommingSession> viewBydate = <UpcommingSession>[].obs;
  Map<DateTime, List<UpcommingSession>> mapDummy = {};

  final load = false.obs;

  void ondaySelect(sel, foc) {
    focusDay(foc);
    selectedDay(sel);
  }

  @override
  void onInit() {
    getBookingByUser();
    dependecies();
    scroll.addListener(listen);
    super.onInit();
  }

  dependecies() {
    Get.lazyPut<MainController>(() => MainController());
  }

  void listen() {
    final direction = scroll.position.userScrollDirection;

    if (direction == ScrollDirection.forward) {
      shitme.listenMe(true);
    } else if (direction == ScrollDirection.reverse) {
      shitme.listenMe(false);
    }
  }

  getBookingByUser() async {
    load(!load.value);
    try {
      final result = await ApiCoachUpcommingSession.getAllBookingByUser();
      listBookingByUser.assignAll(result);

      listBookingByUser.map((e) async {
        final result = await ApiCoachUpcommingSession.getCoachById(e.coachId);
        if (result != null) {
          coachUserDetails.assign(result);
        }
      }).toList();
    } finally {
      load(!load.value);
      onDaySelect(selectedDay.value, focusDay.value);
      listBookingByUser.map((e) async {
        if (mapDummy.containsKey(e.startTime)) {
          mapDummy[DateTime.utc(e.startTime!.year, e.startTime!.month, e.startTime!.day)]!.add(UpcommingSession());
        } else {
          mapDummy[DateTime.utc(e.startTime!.year, e.startTime!.month, e.startTime!.day)] = [UpcommingSession()];
        }
      }).toList();
    }
  }

  cancelBooking(String? bookId) async {
    load(!load.value);
    try {
      listBookingByUser.map((e) async {
        var dateNow = DateTime.now();

        int daysBetween(DateTime from, DateTime to) {
          from = DateTime(from.year, from.month, from.day);
          to = DateTime(to.year, to.month, to.day);
          return (to.difference(from).inHours / 24).round();
        }

        final maxDaysToCancel = 4;
        //add [dateNow.day +5] to daysBetween to test the can't cancel booking
        final currentDays = daysBetween(e.creationTime!, DateTime(dateNow.year, dateNow.month, dateNow.day));

        if (currentDays <= maxDaysToCancel) {
          Get.back();
          LoadingOrError.loading();
          final result = await ApiCoachUpcommingSession.getCancelBook(id: bookId!);
          if (result == 200) {
            Get.back();
            BookingDialog.successBooking();

            mapDummy.removeWhere((key, value) => key
                .toString()
                .contains(DateTime.utc(e.startTime!.year, e.startTime!.month, e.startTime!.day).toString()));

            getBookingByUser();
          }
        } else if (currentDays > maxDaysToCancel) {
          Get.back();
          BookingDialog.errorBooking();
          print("Can't Cancel Now");
        } else {
          print("Error Cancel");
        }
      }).toList();
    } finally {
      load(!load.value);
    }
  }

  onDaySelect(date1, date2) {
    selectedDay(date1);
    focusDay(date2);

    viewBydate.assignAll(listBookingByUser.where((e) =>
        DateFormat("MM dd yyyy").format(e.startTime!).contains(DateFormat("MM dd yyyy").format(selectedDay.value))));
  }

  List<UpcommingSession> getEventsList(DateTime time) => mapDummy[time] ?? [];

  @override
  void onClose() {
    super.onClose();
    scroll.dispose();
  }
}
