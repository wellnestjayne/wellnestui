import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/certificate/certificate_view.dart';
import 'package:wellnest/pages/service/service_screen.dart';
import 'package:wellnest/pages/service/service_view.dart';
import 'package:wellnest/pages/write/widget/appbar.dart';

class CoachView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppbarJournal(
            back: ()=>Get.back(),
            name: '',
            welcoming: '',
          ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 40,
            ),

            // Hello

            child: Column(
              children: [
                CarouselSlider(
                    options:
                        CarouselOptions(height: 350.h, enlargeCenterPage: true),
                    items: [
                      Container(
                          decoration: BoxDecoration(
                        color: Color(0xFF00AEEF),
                        borderRadius: BorderRadius.circular(10.0),
                        image: DecorationImage(
                          image: AssetImage(WellnestAsset.smilingMan),
                          fit: BoxFit.contain,
                        ),
                      )),
                    ]),
                SizedBox(height: 32),
                Container(
                  height: 60,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: 20),
                      Expanded(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Juan Rodriguez",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 24)),
                              SizedBox(
                                height: 5,
                              ),
                              FittedBox(
                                fit: BoxFit.fitWidth,
                                child:
                                    Text("Psychiatrist with 8 Years Experience",
                                        style: TextStyle(
                                          fontSize: 16,
                                        )),
                              )
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () => Get.defaultDialog(
                            backgroundColor: WellNestColor.wncWhite,
                            barrierDismissible: false,
                            title: "",
                            onConfirm: () => Get.back(),
                            confirmTextColor: WellNestColor.wncWhite,
                            buttonColor: WellNestColor.wncAquaBlue,
                            content: Container(
                                height: 40,
                                child: Column(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            WellnestAsset.smilingMan),
                                        fit: BoxFit.contain,
                                      ),
                                    )
                                    ),
                                    Container(
                                      padding: const EdgeInsets.only(left:20, right: 20),
                                      child: Wrap(
                                        children:[
                                          Text("Are You Sure You Want to Remove this Category?")


                                        ]
                                      ),
                                    ),

                                  ],
                                  
                                )

                                
                                ),

                              textCancel: "Cancel",
                              textConfirm: "OK",
                              
                              ),
                        child: Padding(
                          padding:
                              const EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Stack(alignment: Alignment.center, children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                image: AssetImage(WellnestAsset.ellipse),
                                fit: BoxFit.fitHeight,
                              )),
                            ),
                            Container(
                              height: 30,
                              width: 30,
                              margin: const EdgeInsets.only(
                                right: 2,
                              ),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                image: AssetImage(WellnestAsset.star),
                                fit: BoxFit.fitHeight,
                              )),
                            ),
                          ]),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi mi ante, feugiat quis vehicula id, dictum vel diam. Morbi mi elit, aliquet eu nulla vitae, ornare convallis magna.",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.justify),
                ),

                Container(
                  alignment: Alignment.topLeft,
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Text("SPECIALIZATIONS",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      )),
                ),

                Container(
                  alignment: Alignment.topLeft,
                  padding: const EdgeInsets.all(20.0),
                  child: Wrap(spacing: 20.0, runSpacing: 10.0, children: [
                    ElevatedButton(
                        onPressed: () {},
                        child: Text("Executive",
                            style: TextStyle(
                              color: Color(0xFF555555),
                              fontSize: 16,
                            )),
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xFFF6B797),
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)))),
                    ElevatedButton(
                        onPressed: () {},
                        child: Text("Mental Health",
                            style: TextStyle(
                              color: Color(0xFF555555),
                              fontSize: 16,
                            )),
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xFFFAAC5E),
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)))),
                    ElevatedButton(
                        onPressed: () {},
                        child: Text("Care",
                            style: TextStyle(
                              color: Color(0xFF555555),
                              fontSize: 16,
                            )),
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xFFF6B797),
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)))),
                    ElevatedButton(
                        onPressed: () {},
                        child: Text("Spiritual",
                            style: TextStyle(
                              color: Color(0xFF555555),
                              fontSize: 16,
                            )),
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xFFF6B797),
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)))),
                  ]),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        alignment: Alignment.topLeft,
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text("CERTIFICATIONS",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left:20, right:20),
                      child: InkWell(
                        onTap: () => Get.to(()=> CertificateView()),
                        child: Text(
                          "Show More",
                          style: TextStyle(
                            color: WellNestColor.wncAquaBlue,
                          )
                          
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: CarouselSlider(
                      options: CarouselOptions(
                          height: 160,
                          enableInfiniteScroll: true,
                          enlargeCenterPage: true),
                      items: [
                        
                        Container(
                            decoration: BoxDecoration(
                          color: Color(0xFF00AEEF),
                          borderRadius: BorderRadius.circular(10.0),
                          image: DecorationImage(
                            image: AssetImage(WellnestAsset.certificate),
                            fit: BoxFit.cover,
                          ),
                        )),

                        Container(
                            decoration: BoxDecoration(
                          color: Color(0xFF00AEEF),
                          borderRadius: BorderRadius.circular(10.0),
                          image: DecorationImage(
                            image: AssetImage(WellnestAsset.certificate),
                            fit: BoxFit.cover,
                          ),
                        )),
                      ]),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        alignment: Alignment.topLeft,
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text("FEATURED SERVICES",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                    ),

                    Container(
                      padding: const EdgeInsets.only(left:20, right:20),
                      child: InkWell(
                        onTap: () => Get.to(()=> ServiceView()),
                        child: Text(
                          "Show More",
                          style: TextStyle(
                            color: WellNestColor.wncAquaBlue,
                          )
                          
                        ),
                      ),
                    )

                  ],
                ),
                Container(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: CarouselSlider(
                      options: CarouselOptions(
                          height: 160,
                          enableInfiniteScroll: true,
                          enlargeCenterPage: true),
                      items: [
                        InkWell(
                          onTap: () => Get.to(()=> ServiceScreen()),
                          child: Container(
                              decoration: BoxDecoration(
                            color: Color(0xFF00AEEF),
                            borderRadius: BorderRadius.circular(10.0),
                            image: DecorationImage(
                              image: AssetImage(WellnestAsset.service),
                              fit: BoxFit.cover,
                            ),
                          )),
                        ),
                        Container(
                            decoration: BoxDecoration(
                          color: Color(0xFF00AEEF),
                          borderRadius: BorderRadius.circular(10.0),
                          image: DecorationImage(
                            image: AssetImage(WellnestAsset.service),
                            fit: BoxFit.cover,
                          ),
                        )),
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: ElevatedButton(
                            onPressed: () {},
                            child: Text("Message",
                                style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontSize: 16,
                                )),
                            style: ElevatedButton.styleFrom(
                                primary: Color(0xFF75CDD3),
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)))),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: ElevatedButton(
                            onPressed: () {},
                            child: Text("Email",
                                style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontSize: 16,
                                )),
                            style: ElevatedButton.styleFrom(
                                primary: Color(0xFF75CDD3),
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)))),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, bottom: 20.0),
                  child: Container(
                    width: double.infinity,
                    child: ElevatedButton(
                        onPressed: () {},
                        child: Text("Book an Appointment",
                            style: TextStyle(
                              color: Color(0xFFFFFFFF),
                              fontSize: 16,
                            )),
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xFF00AEEF),
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)))),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
