import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/flag_d.dart';
import 'package:wellnest/pages/coach/widget/shimmer/shimmer_flag.dart';
import 'package:wellnest/pages/create_profile_coach/widget/add_natinality.dart';

class ViewFlagLanguage extends StatelessWidget {
  final coachController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => coachController.coachDetails.isEmpty
        ? ShimmerFlags()
        : Container(
            width: 349,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Wrap(
                    alignment: WrapAlignment.start,
                    runSpacing: 10,
                    spacing: 10,
                    children: coachController.flags
                        .map(
                          (e) => FlagWidget(
                            code: e!.toLowerCase(),
                          ),
                        )
                        .toList(),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  //height: 40,
                  child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: coachController.coachDetails.length,
                      itemBuilder: (_, __) => Text(
                            coachController.coachDetails[__].longDescription.toString(),
                            style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 15.0),
                          )),
                ),
              ],
            ),
          ));
  }
}
