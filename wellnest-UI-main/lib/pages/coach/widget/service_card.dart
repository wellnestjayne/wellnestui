import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/edit_service.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';
import 'package:wellnest/pages/profile/edit_service/edit_service.dart';

class ServiceCardCoach extends StatelessWidget {
  final String? imagePath;
  final String? name;
  final String? title;
  final double? hieght;
  final double? width;
  final Function()? press;
  final String? coachId;
  final String? id;

  ServiceCardCoach(
      {Key? key, this.imagePath, this.name, this.title, this.hieght, this.width, this.press, this.coachId, this.id})
      : super(key: key);

  final serviceController = Get.put(ViewOwnProfileController());
  final editServiceController = Get.put(EditServiceController());

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        height: hieght,
        width: width,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  offset: Offset(4, 2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  color: WellNestColor.wncLightgrey.withOpacity(0.2))
            ],
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(fit: BoxFit.cover, image: NetworkImage(imagePath!))),
        child: Container(
          height: hieght,
          width: width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                  begin: Alignment.center,
                  end: Alignment.bottomCenter,
                  colors: [Colors.transparent, Colors.black.withOpacity(0.4)])),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(
              children: [
                Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      height: 35,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        textDirection: TextDirection.ltr,
                        children: [
                          Text(title!, style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 14.sp)),
                          SizedBox(
                            height: 5.h,
                          ),
                          Text(name!, style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 10.sp))
                        ],
                      ),
                    )),
                Visibility(
                  visible: SharePref.getType() == "User"
                      ? false
                      : SharePref.getCoachId() == coachId
                          ? true
                          : false,
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      width: 50,
                      child: Row(
                        children: [
                          Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(color: WellNestColor.wncGrey, shape: BoxShape.circle),
                            child: Center(
                              child: FaIcon(
                                FontAwesomeIcons.trashAlt,
                                color: WellNestColor.wncWhite,
                                size: 15,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(color: WellNestColor.wncGrey, shape: BoxShape.circle),
                            child: InkWell(
                              onTap: () {
                                editServiceController.getSelected(id);
                                Get.to(() => EditService(id: id), id: 0);
                              },
                              child: Center(
                                child: FaIcon(
                                  FontAwesomeIcons.pen,
                                  color: WellNestColor.wncWhite,
                                  size: 15,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
