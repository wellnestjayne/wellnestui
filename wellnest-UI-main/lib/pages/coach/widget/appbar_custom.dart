import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppBarCustomFeatureCoach extends StatelessWidget implements PreferredSizeWidget{

    final double? topSize = 80;
    final Color? colors;
    final String? title;
    final Color? background;
    final Function()? function;

  const AppBarCustomFeatureCoach({Key? key, this.colors, this.title, this.background, this.function}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      height: topSize!,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Center(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: GestureDetector(
                  onTap: function,
                  child: SizedBox(
                    height: 40,
                    width: 40,
                    child: Center(
                      child: FaIcon(FontAwesomeIcons.arrowLeft,
                      size: 20,color: colors,
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(title!,
                style: WellNestTextStyle.nowMedium(
                  WellNestColor.wncGrey
                  ,18.0),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(topSize!);
}