
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/terms_and_conditions/tac_dialog.dart';

class TermsAndConditionsCheckout extends StatelessWidget {
  
  final controller = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return Obx(
    ()=> Container(
       width: 349,
       child: Column(
         children: [
           Container(
             child: Row(
               children: [
               Checkbox(
            checkColor: WellNestColor.wncWhite,
            activeColor: WellNestColor.wncBlue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2)
            ),
            value: controller.termsApp.value,
            onChanged: (bool? vol)=>controller.vals(vol!),),
            SizedBox(width: 5,),
            Expanded(
              child: GestureDetector(
                onTap: ()=>TacDialog.tacDialogShow(),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                  children: [
                    TextSpan(
                      text: "I agree to the Terms and Conditions of Wellnest. ",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncLightgrey
                        , 13.0)
                    ),
                    TextSpan(
                      text: "See More",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontFamily: WellNestTextStyle.nowMed,
                        fontSize: 13.0,
                        color: WellNestColor.wncGrey
                      )
                    )
                  ]
                )),
              ),
            ),
               ],
             ),
           ),
            Container(
             child: Row(
               children: [
               Checkbox(
            checkColor: WellNestColor.wncWhite,
            activeColor: WellNestColor.wncBlue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2)
            ),
            value: controller.termCoach.value,
            onChanged: (bool? vol)=>controller.vals1(vol!),),
            SizedBox(width: 5,),
            Expanded(
              child: RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                children: [
                  TextSpan(
                    text: "I agree to the Terms and Conditions of the coach/trainer. ",
                    style: WellNestTextStyle.nowMedium(
                      WellNestColor.wncLightgrey
                      , 13.0)
                  ),
                  TextSpan(
                    text: "See More",
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: WellNestTextStyle.nowMed,
                      fontSize: 13.0,
                      color: WellNestColor.wncGrey
                    )
                  )
                ]
              )),
            ),
               ],
             ),
           )
         ],
       ),    
      ),
    );
  }
}