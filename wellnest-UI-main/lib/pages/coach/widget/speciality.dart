
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/shimmer/shimmer_flag.dart';

class SpaicalityChips extends StatelessWidget {
  
  final coachController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return
     Obx(
       ()=>
       coachController.specials.isEmpty ?
       ShimmerFlags()
       :Container(
        width: MediaQuery.of(context).size.width,
        child: Wrap(
          spacing: 10,
          runSpacing: 10,
          alignment: WrapAlignment.center,
          children: coachController.specials.map((e) => 
          Container(
            decoration: BoxDecoration(
              color: WellNestColor.wncOrange,
              borderRadius: BorderRadius.circular(15),
              boxShadow:[
                BoxShadow(
                  offset: Offset(1, 2),
                  blurRadius: 1,
                  spreadRadius: 1,
                  color: WellNestColor.wncLightgrey.withOpacity(0.3)
                )
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                left: 10,
                right: 10,
                top: 5,
                bottom: 5
              ),
              child: Text(e.toString(),
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey, 13.0),
              ),
            ),
          )
          ).toList(),
        ),
    ),
     );
  }
}