
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wellnest/constants/colors/colors.dart';

class ShimmerListAll extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 145,
      width: 349,
        decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10)
            ),
      child: Shimmer.fromColors(child: 
      ListView.builder(
        itemCount: 3,
        scrollDirection: Axis.horizontal,
        itemBuilder: (_,__)
        =>Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: 292,
            height: 145,
           
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
               color: WellNestColor.wncLightgrey,
            ),
          ),
        ),
        ),
      baseColor: WellNestColor.wncLighBg, 
      highlightColor: WellNestColor.wncLightgrey
      ),
    );
  }
}