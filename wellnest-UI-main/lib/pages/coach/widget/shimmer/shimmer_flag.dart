

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wellnest/constants/colors/colors.dart';

class ShimmerFlags extends StatelessWidget {
  const ShimmerFlags({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 96,
      width: 349,
      child: Shimmer.fromColors(child: 
      ListView.builder(
        itemCount: 3,
        scrollDirection: Axis.horizontal,
        itemBuilder: (_,__)
        =>Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: 70,
            width: 120,
            color: WellNestColor.wncLightgrey,
          ),
        ),
        ),
      baseColor: WellNestColor.wncLighBg, 
      highlightColor: WellNestColor.wncLightgrey
      ),
    );
  }
}