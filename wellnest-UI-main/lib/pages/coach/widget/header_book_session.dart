import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class HeaderBookSession extends StatelessWidget {

  final Function()? goBack;
  final String? name;
  final String? title;
  final String? imagePathback;
  final String? imagePathIcon;

  const HeaderBookSession({Key? key, this.goBack, this.name, this.title, this.imagePathback, this.imagePathIcon}) : super(key: key);




  @override
  Widget build(BuildContext context) {
    return Container(
      height: 261,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.center,
          image: NetworkImage(imagePathback!)
        ),
      ),
      child: Container(
        height: 261,
        decoration: BoxDecoration(
           borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
            gradient: LinearGradient(
          begin: Alignment.center,
          end: Alignment.bottomCenter,
          colors: [
            Colors.transparent,
            Colors.black.withOpacity(0.7)
          ]),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.only(top:20,left: 20),
                child: GestureDetector(
                  onTap: goBack,
                  child: FaIcon(FontAwesomeIcons.chevronLeft,
                  color: WellNestColor.wncWhite,
                  size: 27,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(
                  bottom: 10,
                  right: 20,
                  left: 20
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 40,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          textDirection: TextDirection.ltr,
                          children: [
                            Text(title!,
                            overflow: TextOverflow.ellipsis,
                            style: WellNestTextStyle.nowMedium(
                              WellNestColor.wncWhite
                              ,16.0),
                            ),
                            SizedBox(height: 2,),
                            Text(name!,
                            style: WellNestTextStyle.nowMedium(
                              WellNestColor.wncWhite
                              , 13.0),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: WellNestColor.wncWhite,
                          width: 1),
                        image: DecorationImage( alignment: Alignment.center,
                         fit: BoxFit.cover,
                          image: NetworkImage( imagePathIcon!,))
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
