
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';

class SessionCardforPickDates extends StatelessWidget {
  
  final String? date;
  final int? lenght;
  final Function()? press;
  final bool? showTop;

  const SessionCardforPickDates({Key? key, this.date, this.lenght, this.press, this.showTop}) : super(key: key);

  
  // const SessionCardforPickDates({Key? key, this.date, this.press}) : super(key: key);
 
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 91,
      width: 349,
      child: Column(
        children: [
            HeaderTitleSubs(
                    showFunction: showTop == null ? true : showTop!,
                    title: "Session $lenght",
                    extra: "Remove",
                    extraFunction: press,
                    ),
         SizedBox(height: 2,),
         Container(
           height: 72,
           width: 337,
           decoration: BoxDecoration(
             color: WellNestColor.wncLighBg,
             borderRadius: BorderRadius.circular(10),
             boxShadow: [
               BoxShadow(
                 offset: Offset(2, 3),
                 blurRadius: 2,
                 spreadRadius: 1,
                 color: WellNestColor.wncGrey.withOpacity(0.3)
               )
             ]
           ),
           child: Padding(
             padding: const EdgeInsets.only(left: 10,
             top: 8,bottom: 8, right: 100
             ),
             child: Row(
               children: [
                 Expanded(
                   child: Text(date!,
                   textAlign: TextAlign.left,
                   style: WellNestTextStyle.nowMedium(
                     WellNestColor.wncGrey, 13.0),
                   ),
                 ),
               ],
             ),
           ),
         ),            
        ],
      ),    
    );
  }
}