
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class ReviewStat extends StatelessWidget {
  
  final String? ratenumber;
  final String? numberRate;
  final double? rate5;
  final double? rate4;
  final double? rate3;
  final double? rate2;
  final double? rate1;

  const ReviewStat({Key? key, this.ratenumber, this.numberRate, this.rate5, this.rate4, this.rate3, this.rate2, this.rate1}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 96,
      width: 318,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            offset: Offset(1,2),
            blurRadius: 2,  
            spreadRadius: 2,
            color: WellNestColor.wncLightgrey.withOpacity(0.3)
          )
        ],
        color:WellNestColor.wncBlue,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Container(
              width: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(ratenumber!,
                  style: WellNestTextStyle.nowBold(
                    WellNestColor.wncWhite, 41.0),
                  ),
                  SizedBox(height: 2,),
                  Text("Out of 5",
                  style: WellNestTextStyle.nowMedium(
                    WellNestColor.wncWhite, 10.0),)
                ],
              ),
            ),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                          Container(
                    height: 10,
                    width: 60,
                    child: ListView.builder(
                      itemCount: 5,
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index)
                      =>FaIcon(FontAwesomeIcons.solidStar,
                      color: WellNestColor.wncOrange,
                      size: 10,
                      )),
                  ),
                  SizedBox(height: 2,),
                  Container(
                    height: 10,
                    width: 60,
                    child: ListView.builder(
                      itemCount: 4,
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index)
                      =>FaIcon(FontAwesomeIcons.solidStar,
                      color: WellNestColor.wncOrange,
                      size: 10,
                      )),
                  ),
                  SizedBox(height: 2,),
                  Container(
                    height: 10,
                    width: 60,
                    child: ListView.builder(
                      itemCount: 3,
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index)
                      =>FaIcon(FontAwesomeIcons.solidStar,
                      color: WellNestColor.wncOrange,
                      size: 10,
                      )),
                  ),
                  SizedBox(height: 2,),
                  Container(
                    height: 10,
                    width: 60,
                    child: ListView.builder(
                      itemCount: 2,
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index)
                      =>FaIcon(FontAwesomeIcons.solidStar,
                      color: WellNestColor.wncOrange,
                      size: 10,
                      )),
                  ),
                  SizedBox(height: 2,),
                  Container(
                    height: 10,
                    width: 60,
                    child: ListView.builder(
                      itemCount: 1,
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index)
                      =>FaIcon(FontAwesomeIcons.solidStar,
                      color: WellNestColor.wncOrange,
                      size: 10,
                      )),
                  ),
                  SizedBox(height: 8,),
                  Align(
                            alignment: Alignment.bottomRight,
                            child: Text("",
                            style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncWhite, 10.0),
                            ),
                          ),
                      ],
                    ),
                  ),
                  
                ],
              ),
            ),
              Container(
                width: 130,
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Container(
                             height: 5,
                             width: 130,
                             child: ListView.builder(
                               itemCount: 1,
                               scrollDirection: Axis.horizontal,
                               itemBuilder: (context,index)
                               =>Container(
                                 height: 5,
                                 width: 130,
                                 decoration: BoxDecoration(
                                   color: WellNestColor.wncWhite,
                                   borderRadius: BorderRadius.circular(15),
                                 ),
                               )
                               ),
                           ),
                            SizedBox(height: 6,),
                           Container(
                             height: 5,
                             width: 130,
                             child: ListView.builder(
                               itemCount: 1,
                               scrollDirection: Axis.horizontal,
                               itemBuilder: (context,index)
                               =>Container(
                                 height: 5,
                                 width: 90,
                                 decoration: BoxDecoration(
                                   color: WellNestColor.wncWhite,
                                   borderRadius: BorderRadius.circular(15),
                                 ),
                               )
                               ),
                           ),
                            SizedBox(height: 7,),
                           Container(
                             height: 5,
                             width: 130,
                             child: ListView.builder(
                               itemCount: 1,
                               scrollDirection: Axis.horizontal,
                               itemBuilder: (context,index)
                               =>Container(
                                 height: 5,
                                 width: 60,
                                 decoration: BoxDecoration(
                                   color: WellNestColor.wncWhite,
                                   borderRadius: BorderRadius.circular(15),
                                 ),
                               )
                               ),
                           ),
                            SizedBox(height: 7,),
                           Container(
                             height: 5,
                             width: 130,
                             child: ListView.builder(
                               itemCount: 1,
                               scrollDirection: Axis.horizontal,
                               itemBuilder: (context,index)
                               =>Container(
                                 height: 5,
                                 width: 40,
                                 decoration: BoxDecoration(
                                   color: WellNestColor.wncWhite,
                                   borderRadius: BorderRadius.circular(15),
                                 ),
                               )
                               ),
                           ),
                            SizedBox(height: 7,),
                           Container(
                             height: 5,
                             width: 130,
                             child: ListView.builder(
                               itemCount: 1,
                               scrollDirection: Axis.horizontal,
                               itemBuilder: (context,index)
                               =>Container(
                                 height: 5,
                                 width: 20,
                                 decoration: BoxDecoration(
                                   color: WellNestColor.wncWhite,
                                   borderRadius: BorderRadius.circular(15),
                                 ),
                               )
                               ),
                           ),
                          SizedBox(height: 8,),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Text(numberRate!+" Ratings",
                            style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncWhite, 10.0),
                            ),
                          ),
                         ],
                       ),
            ),
           
          ],
        ),
      ),
    );
  }
}