
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class ReviewCardCoach extends StatelessWidget {

 final String? dateString;
  final String? understanding;
  final String? commentorname;
  final String? description;
  final double? starRate;

  const ReviewCardCoach({Key? key, this.dateString, this.understanding, this.commentorname, this.description, this.starRate}) : super(key: key); 

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 135,
      width: 292,
      decoration: BoxDecoration(
        color: WellNestColor.wncWhite,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            offset: Offset(0,2),
            blurRadius: 2,
            spreadRadius: 2,
            color: WellNestColor.wncLightgrey.withOpacity(0.2)
          )
        ]
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          top: 15,
        ),
        child: Column(
          children: [
            Container(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        textDirection: TextDirection.ltr,
                        children: [
                          Text(understanding!,
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncGrey, 15.0),
                          ),
                          IgnorePointer(
                            child: RatingBar(
                              initialRating: starRate!,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemPadding: EdgeInsets.zero,
                              ratingWidget: RatingWidget(
                                full: FaIcon(FontAwesomeIcons.solidStar,color: WellNestColor.wncOrange,size: 1,),
                                half:  FaIcon(FontAwesomeIcons.solidStarHalf,color: WellNestColor.wncOrange,size: 1,),
                                empty:  FaIcon(FontAwesomeIcons.solidStar,color: WellNestColor.wncGrey,size: 1,)),
                              onRatingUpdate: (rate){}),
                          )
                        ],  
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                        textDirection: TextDirection.ltr,
                      children: [
                        Text(dateString!,
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncGrey, 15.0),
                          ),
                          SizedBox(height: 15,),
                       Text(commentorname!,
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncGrey, 15.0),
                          ),   
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
             Text(description!,
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncLightgrey, 10.0),
                          ), 
          ],
        ),
      ),
    );
  }
}