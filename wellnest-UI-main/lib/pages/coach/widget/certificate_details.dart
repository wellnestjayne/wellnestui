
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class CertificateDetailList extends StatelessWidget {
  
  final String? imagePath;
  final String? name;
  final String? description;
  final String? certNam;
  final String? dateString;

  const CertificateDetailList({Key? key, this.imagePath, this.name, this.description, this.certNam, this.dateString}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 318,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 203,
            width: 318,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  offset: Offset(1,2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  color: WellNestColor.wncLightgrey.withOpacity(0.3)
                )
              ],
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(imagePath!,))
            ),
          ),
          SizedBox(height: 15.0,),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                children: [
                 Text(certNam!,
          style: WellNestTextStyle.nowMedium(
              WellNestColor.wncAquaBlue, 15.0),
          ),
          SizedBox(height: 5,),
           Text(dateString!,
          style: WellNestTextStyle.nowRegular(
              WellNestColor.wncLightgrey, 13.0),
          ),
          SizedBox(height: 10,),
          Text(description!,
          textAlign: TextAlign.justify,
          style: WellNestTextStyle.nowRegular(
              WellNestColor.wncLightgrey, 13.0),
          ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}