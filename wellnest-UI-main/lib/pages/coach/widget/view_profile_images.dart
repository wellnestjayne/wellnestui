import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/profile/view_profile/widget/shimmer/profile_imageLoad.dart';

class ViewProfilePageImage extends StatelessWidget {
  
  final coachConttroller = Get.put(FeatureCoachController());

 
  @override
  Widget build(BuildContext context) {
    return Obx(
      ()
    => coachConttroller.coachDetails.isEmpty ? 
    ImageLoadingProfile()
    : 
    Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
        Container(
           height: 303,
           width: 334,
         child: PageView.builder(
           controller: coachConttroller.pageController,
           onPageChanged: coachConttroller.selectedNumber,
           itemCount: coachConttroller.netWorkImage.length,
           itemBuilder: (context,index)
          =>Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 303,
                    width: 334,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncBlue,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(3,2),
                          blurRadius: 2,
                          spreadRadius: 2,
                          color: WellNestColor.wncLightgrey.withOpacity(0.3)
                        ),
                      ]
                    ),
                    child: ClipRRect(
                     borderRadius : BorderRadius.circular(10),
                      child: Image.network(
                        coachConttroller.netWorkImage[index].toString(),
                      fit: BoxFit.cover,
                      alignment: Alignment.topCenter
                      ),
                    ),
                  ),
                )),
            ),
            SizedBox(height: 15,),
             Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                  coachConttroller.netWorkImage.length,
                  (index) => Padding(
                    padding: const EdgeInsets.only(
                      left: 2,right: 2
                    ),
                    child: Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                        color: coachConttroller.selectedNumber.value == index ?
                        WellNestColor.wncBlue : WellNestColor.wncLightgrey.withOpacity(0.3),
                        shape: BoxShape.circle
                      ),
                    ),
                  )),
                ),
              ),
        ],
      ), 
    ),
  );
}
}