
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppBarDatePicker extends StatelessWidget implements PreferredSizeWidget {

    final double? topSize = 70;
    final Function()? goback;
    final String? title;

  const AppBarDatePicker({Key? key, this.goback, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: topSize!,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: Text(title!,
                style: WellNestTextStyle.nowMedium(
                  WellNestColor.wncGrey
                  , 15.0),
                ),
              ),
              GestureDetector(
                onTap: goback,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    height: 20,
                    width: 20,
                    child: Center(
                      child: FaIcon(FontAwesomeIcons.arrowLeft,
                      color: WellNestColor.wncGrey,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(topSize!);
}