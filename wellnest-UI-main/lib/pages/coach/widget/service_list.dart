

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/certificate_card.dart';
import 'package:wellnest/pages/coach/widget/service_card.dart';
import 'package:wellnest/pages/coach/widget/shimmer/shimmer_list.dart';
import 'package:wellnest/route/route_page.dart';

class ServiceListCoach extends StatelessWidget {
  
  final serviceController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return Container(
      height:160,
      width: 349,
      child: Obx(
        ()=> serviceController.serviceList.isEmpty ?
        ShimmerListAll() :
        Container(
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: serviceController.serviceList.length > 3 ? 3 : serviceController.serviceList.length,
            itemBuilder: (_,__)
            =>
            Padding(
              padding: const EdgeInsets.only(right: 10,
              bottom: 10,
              top: 10),
                child: ServiceCardCoach(
                  // press: ()=>Get.toNamed(
                  //    AppRouteName.homeView!+
                  //     AppRouteName.coachBookSession!,
                  //     id: 0,
                      // arguments: {
                      //   "title": serviceController.serviceList[__].name,
                      //  "back": serviceController.serviceList[__].bannerUrl,
                      //  "front": serviceController.netWorkImage.map((e) => e).first,
                      //  "name": serviceController.forNameService.value,
                      //  "long": serviceController.serviceList[__].description,
                      //  "cancel": serviceController.serviceList[__].canCancelFreeInDays,
                      //  "refund" :serviceController.serviceList[__].canRefund,
                      //  "session": serviceController.serviceList[__].numberOfSessions,
                      //  "price" : serviceController.serviceList[__].price,
                      //  "discount" : serviceController.serviceList[__].discount,
                      //  "service_ID": serviceController.serviceList[__].id,
                      //  "coachId" : serviceController.serviceList[__].coachId,
                      //  "maximum": serviceController.serviceList[__].maximumPeople,
                      //  "timeS" : serviceController.serviceList[__].timeInSeconds
                      // }
                   // ),
                   press: (){serviceController.sellMe(serviceController.serviceList[__].id);
                   Get.toNamed(
                     AppRouteName.homeView!+
                      AppRouteName.coachBookSession!,
                      id: 0);
                   },
                              imagePath: serviceController.serviceList[__].bannerUrl,
                              name: serviceController.forNameService.value,
                              title: serviceController.serviceList[__].name,
                              coachId: serviceController.serviceList[__].coachId,
                              width: 292,
                              hieght: 145,
                            ),
                          ),
            ),
        ),
      ),
    );
  }
}