import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/profile/view_profile/widget/shimmer/title_info.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class NameTagprofile extends StatelessWidget {
  final coachControlle = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => coachControlle.coachDetails.isEmpty
          ? TitleInfoLoading()
          : Container(
              height: 90,
              width: 349,
              clipBehavior: Clip.none,
              child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: coachControlle.coachDetails.length,
                  itemBuilder: (context, index) => Container(
                        width: 349,
                        height: 90,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 270,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                textDirection: TextDirection.ltr,
                                children: [
                                  Container(
                                    child: Wrap(
                                      runSpacing: 10,
                                      spacing: 10,
                                      crossAxisAlignment: WrapCrossAlignment.center,
                                      alignment: WrapAlignment.start,
                                      children: [
                                        Text(
                                          coachControlle.coachDetails[index].name.toString(),
                                          overflow: TextOverflow.ellipsis,
                                          style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 18.0),
                                        ),
                                        Obx(
                                          () => GestureDetector(
                                            onTap: () => coachControlle.changeBoolx(),
                                            child: Container(
                                              height: 28,
                                              width: 90,
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(5),
                                                  color: coachControlle.boolx.value
                                                      ? WellNestColor.wncBlue
                                                      : WellNestColor.wncWhite,
                                                  border: Border.all(color: WellNestColor.wncBlue)),
                                              child: Center(
                                                child: Text(
                                                  coachControlle.boolx.value ? "Follow" : "Following",
                                                  style: WellNestTextStyle.nowMedium(
                                                      coachControlle.boolx.value
                                                          ? WellNestColor.wncWhite
                                                          : WellNestColor.wncBlue,
                                                      13.0),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    //width: 270,
                                    // child: Row(
                                    //   children: [
                                    //     Container(
                                    //       width: 160,
                                    //       child: Text(coachControlle.coachDetails[index].name.toString(),
                                    //       overflow: TextOverflow.ellipsis,
                                    //       style: WellNestTextStyle.nowMedium(
                                    //         WellNestColor.wncGrey
                                    //         ,18.0),
                                    //       ),
                                    //     ),
                                    //     SizedBox(width: 10,),
                                    //     Obx(
                                    //       ()=> GestureDetector(
                                    //         onTap: ()=>coachControlle.changeBoolx(),
                                    //         child: Container(
                                    //           height: 28,
                                    //           width: 90,
                                    //           decoration: BoxDecoration(
                                    //             borderRadius: BorderRadius.circular(5),
                                    //             color: coachControlle.boolx.value ? WellNestColor.wncBlue :
                                    //             WellNestColor.wncWhite,
                                    //             border: Border.all(
                                    //               color:WellNestColor.wncBlue )
                                    //           ),
                                    //           child: Center(
                                    //             child: Text(coachControlle.boolx.value ? "Follow" : "Following",
                                    //             style: WellNestTextStyle.nowMedium(
                                    //              coachControlle.boolx.value ? WellNestColor.wncWhite :
                                    //             WellNestColor.wncBlue
                                    //               , 13.0),
                                    //             ),
                                    //           ),
                                    //         ),
                                    //       ),
                                    //     )
                                    //   ],
                                    // ),
                                  ),
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Text(
                                    coachControlle.coachDetails[index].shortDescription.toString(),
                                    style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 10.0),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: 55,
                              width: 45,
                              child: Stack(
                                children: [
                                  SizedBox(
                                    child: Image.asset(WellnestAsset.chipCol),
                                  ),
                                  Positioned(
                                    top: -1,
                                    left: 2,
                                    right: 4,
                                    //alignment: Alignment.topCenter,
                                    child: Image.asset(WellnestAsset.starPol),
                                  ),
                                  Positioned(
                                    top: 27,
                                    left: 8,
                                    right: 8,
                                    //alignment: Alignment.topCenter,
                                    child: Text(
                                      "5",
                                      textAlign: TextAlign.center,
                                      style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 10.0),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      )),
            ),
    );
  }
}
