import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FlagWidget extends StatelessWidget {
  final String? code;

  const FlagWidget({Key? key, this.code}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final urlImage = 'lib/assets/images/countries/$code.png';
    return Image.asset(
      urlImage,
      height: 36,
      width: 62,
      fit: BoxFit.cover,
      excludeFromSemantics : true,
      // semanticsLabel: code,
      // allowDrawingOutsideViewBox: true,
    );
  }
}