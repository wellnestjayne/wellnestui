import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/review_card.dart';
import 'package:wellnest/pages/coach/widget/review_stat.dart';
import 'package:wellnest/standard_widgets/appbar.dart';

class ReviewCoachpage extends StatelessWidget {
  
  final coachController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
        appBar: StandardAppbaruse(
          function: ()=>Get.back(),
          title: Get.parameters['name'],
          color: WellNestColor.wncBlue,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 20,right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 20,),
                ReviewStat(
                  rate1: 0.10,
                  rate2: 0.20,
                  rate3: 0.40,
                  rate4: 0.70,
                  rate5: 0.1,
                  ratenumber: "4.7",
                  numberRate: "247",
                ),
                SizedBox(height: 20,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: coachController.listReviews.length,
                    itemBuilder: (context,index)
                    =>Padding(
                          padding: const EdgeInsets.only(left: 5,right: 10,
                          bottom: 10,
                          top: 10
                          ),
                          child: ReviewCardCoach(
                            dateString: coachController.listReviews[index].dateString,
                            understanding: coachController.listReviews[index].understanding,
                            commentorname: coachController.listReviews[index].commentorname,
                            description: coachController.listReviews[index].description,
                            starRate: coachController.listReviews[index].starRate,
                          ),
                        )),
                ),
              ],
            ),
          ),
        ),
        ),
      onWillPop: ()async=>false),
    );
  }
}