import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/service_card.dart';
import 'package:wellnest/pages/coach/widget/shimmer/shimmer_list.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/appbar.dart';

class ServiceCoachDetails extends StatelessWidget {

  final serviceController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
          appBar: StandardAppbaruse(
            function: ()=>Get.back(id: 0),
            title: "${serviceController.forNameService.value} Services",
            color: WellNestColor.wncBlue,
          ),
          body:Obx(
            ()=> serviceController.serviceList.isEmpty ?
            ShimmerListAll() :
            Padding(
              padding: const EdgeInsets.only(
                top: 30,bottom: 20,
                right: 20,left: 20),
              child: ListView.builder(
                //scrollDirection: Axis.horizontal,
                itemCount: serviceController.serviceList.length > 3 ? 
                serviceController.serviceList.length : serviceController.serviceList.length,
                itemBuilder: (_,__)
                =>
                Padding(
                  padding: const EdgeInsets.only(right: 10,
                  bottom: 10,
                  top: 10),
                    child: ServiceCardCoach(
                       press: (){serviceController.sellMe(serviceController.serviceList[__].id);
                       Get.toNamed(
                         AppRouteName.homeView!+
                          AppRouteName.coachBookSession!,
                          id: 0);
                       },
                                  imagePath: serviceController.serviceList[__].bannerUrl,
                                  name: serviceController.forNameService.value,
                                  title: serviceController.serviceList[__].name,
                                  coachId: serviceController.serviceList[__].coachId,
                                  width: 292,
                                  hieght: 145,
                                ),
                              ),
                ),
            ),
          )
        ),
      onWillPop: ()async{ Get.back(id: 0); return true;}),
    );
  }
}