

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/certificate_details.dart';
import 'package:wellnest/standard_widgets/appbar.dart';

class CertificatePageProfile extends StatelessWidget {
  
  final coachController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
      child: Scaffold(
        appBar: StandardAppbaruse(
          function: ()=>Get.back(),
          title: Get.parameters['name'],
          color: WellNestColor.wncBlue,
        ),
       body: ListView.builder(
         itemCount: coachController.listCertificate.length,
         itemBuilder: (context,index)
         =>Padding(
           padding: const EdgeInsets.only(
             top: 5,
             bottom: 10
           ),
           child: CertificateDetailList(
             dateString: coachController.listCertificate[index].dateString,
             imagePath: coachController.listCertificate[index].imagePath,
             name: coachController.listCertificate[index].name,
             certNam: coachController.listCertificate[index].certNam,
             description: coachController.listCertificate[index].description,
           ),
         ),),
      ), 
    onWillPop: ()async=>false),
    );
  }
}