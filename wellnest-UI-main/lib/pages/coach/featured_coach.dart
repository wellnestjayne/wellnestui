
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/service/coach_service.dart';
import 'package:wellnest/pages/coach/widget/appbar_custom.dart';
import 'package:wellnest/pages/coach/widget/certificate_card.dart';
import 'package:wellnest/pages/coach/widget/flags.dart';
import 'package:wellnest/pages/coach/widget/name_tag.dart';
import 'package:wellnest/pages/coach/widget/review_card.dart';
import 'package:wellnest/pages/coach/widget/service_list.dart';
import 'package:wellnest/pages/coach/widget/speciality.dart';
import 'package:wellnest/pages/coach/widget/view_profile_images.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/header.dart';



class FeaturedCoachProfile extends StatelessWidget {
  

   final coachController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
          appBar: AppBarCustomFeatureCoach(
            background: Colors.transparent,
            colors: WellNestColor.wncGrey,
            title: "Featured Coach",
            function: ()=>Get.back(id: 0),),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 20,
                  right: 20
                ),
                child: AnimationLimiter(
                  child: Column(
                    children: AnimationConfiguration.toStaggeredList(
                      duration: const Duration(seconds: 1),
                      childAnimationBuilder: (widget)=>SlideAnimation(
                        verticalOffset: 150,
                        child: FadeInAnimation(
                          child: widget,
                        )),
                        children: [
                          ViewProfilePageImage(),
                          SizedBox(height: 25,),
                          NameTagprofile(),
                          SizedBox(height: 10,),
                          ViewFlagLanguage(),
                          SizedBox(height: 10,),
                          SpaicalityChips(),
                          SizedBox(height: 25,),
                          // Container(
                          //   width: 349,
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.center,
                          //     mainAxisAlignment: MainAxisAlignment.center,
                          //     children: [
                          //         BlueButtonStandard(
                          //   bluebutton: (){},
                          //   title: "Message",
                          //   back: WellNestColor.wncAquaBlue,
                          //   border: WellNestColor.wncAquaBlue,
                          //   textColor: WellNestColor.wncWhite,
                          //   sizeText: 15.0,
                          //   height: 37,
                          //   width: 155.59,
                          // ),
                          // SizedBox(width: 5,),
                          //   BlueButtonStandard(
                          //   bluebutton: (){},
                          //   title: "Email",
                          //   back: WellNestColor.wncAquaBlue,
                          //   border: WellNestColor.wncAquaBlue,
                          //   textColor: WellNestColor.wncWhite,
                          //   sizeText: 15.0,
                          //   height: 37,
                          //   width: 155.59,
                          // ),
                          //     ],
                          //   ),
                          // ),
                          SizedBox(height: 10,),
                            BlueButtonStandard(
                            bluebutton: (){},
                            title: "Book an Appointment",
                            back: WellNestColor.wncBlue,
                            border: WellNestColor.wncBlue,
                            textColor: WellNestColor.wncWhite,
                            sizeText: 15.0,
                            height: 37,
                            width: 334,
                          ),
                     SizedBox(height: 25,),
                      SizedBox(height: 15,),
                    // Standardheader(
                    // press: (){},
                    // title: "Certificates",
                    // show: true,
                    // colorText: WellNestColor.wncBlue,
                    // ),        
                    // SizedBox(height: 5,),
                    // Container(
                    //   height: 160,
                    //   child: ListView.builder(
                    //     scrollDirection: Axis.horizontal,
                    //     itemCount: coachController.listCertificate.length,
                    //     itemBuilder: (context,index)
                    //     =>Padding(
                    //       padding: const EdgeInsets.only(right: 10,
                    //       bottom: 10,
                    //       top: 10
                    //       ),
                    //       child: CertificateCardprofile(
                    //         imagePath: coachController.listCertificate[index].imagePath,
                    //         name: coachController.listCertificate[index].name,
                    //         width: 292,
                    //         hieght: 145,
                    //       ),
                    //     )),
                    // ),
                    SizedBox(height: 15,),
                         Standardheader(
                          press: ()=>Get.to(()
                          =>ServiceCoachDetails(),id:0),
                          title: "Services",
                          show: true,
                          colorText: WellNestColor.wncBlue,
                          ),        
                    SizedBox(height: 5,), 
                    ServiceListCoach(),
                     SizedBox(height: 15,),
                    // Standardheader(
                    // press: (){},
                    // title: "Reviews",
                    // show: true,
                    // colorText: WellNestColor.wncBlue,
                    // ),        
                    // SizedBox(height: 5,),
                    //  Container(
                    //   height: 160,
                    //   child: ListView.builder(
                    //     scrollDirection: Axis.horizontal,
                    //     itemCount: coachController.listReviews.length,
                    //     itemBuilder: (context,index)
                    //     =>Padding(
                    //       padding: const EdgeInsets.only(left: 5,right: 10,
                    //       bottom: 10,
                    //       top: 10
                    //       ),
                    //       child: ReviewCardCoach(
                    //         dateString: coachController.listReviews[index].dateString,
                    //         understanding: coachController.listReviews[index].understanding,
                    //         commentorname: coachController.listReviews[index].commentorname,
                    //         description: coachController.listReviews[index].description,
                    //         starRate: coachController.listReviews[index].starRate,
                    //       ),
                    //     )),
                    // ),
                   
                       SizedBox(height: 25,),   
                            BlueButtonStandard(
                            bluebutton: (){},
                            title: "Book an Appointment",
                            back: WellNestColor.wncBlue,
                            border: WellNestColor.wncBlue,
                            textColor: WellNestColor.wncWhite,
                            sizeText: 15.0,
                            height: 37,
                            width: 334,
                          ),
                           SizedBox(height: 25,),
                        ]),
                  )),
              ),
            ),
        ),
      onWillPop: ()async{Get.back(id: 0); return true;}),
    );
  }
}