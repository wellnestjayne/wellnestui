import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_book/api_book.dart';
import 'package:wellnest/api/api_service/api_coaches/api_get_all_coach.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/coach_featured_detail.dart';
import 'package:wellnest/model/coach_schedule_time.dart';
import 'package:wellnest/model/coach_session_details.dart';
import 'package:wellnest/model/coash_services.dart';
import 'package:wellnest/pages/coach/book_session/redirect/redirectPage_Web.dart';
import 'package:wellnest/pages/coach/book_session/success_page/success_page.dart';
import 'package:wellnest/pages/coach/book_session/validator/card_validator.dart';
import 'package:wellnest/pages/coach/model/certificate_model.dart';
import 'package:wellnest/pages/coach/model/profileImageSlide.dart';
import 'package:wellnest/pages/coach/model/review_model.dart';
import 'package:wellnest/pages/coach/model/services_model.dart';
import 'package:wellnest/pages/profile/models/UserCoach.dart';
import 'package:wellnest/standard_widgets/loading.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import 'package:intl/intl.dart';

class FeatureCoachController extends GetxController {
  var selectedNumber = 0.obs;
  var pageController = PageController();

  final TextEditingController? cardNumber = TextEditingController();
  final TextEditingController? expirationDateMonth = TextEditingController();
  final TextEditingController? expirationDateYear = TextEditingController();
  final TextEditingController? cvcOfCard = TextEditingController();
  final cardText = "".obs;

  savePayment() {
    CardValidator().validateCardInfo(
        numberCard: cardNumber!.value.text,
        expDate: expirationDateMonth!.value.text +
            "/" +
            expirationDateYear!.value.text,
        cvc: cvcOfCard!.value.text);
    cardText(cardNumber!.value.text);
    this.payBugoMethod();
    Get.back(id: 0);
  }

  //Certificate
  final listCertificate = [
    CertificateCoach(
      certNam: "PhD",
      dateString: "June 10",
      name: "Erchil",
      imagePath: WellnestAsset.certificate,
      description:
          "Sample text sample text sample text sample text sample text sample text sample text",
    ),
    CertificateCoach(
      certNam: "PhD",
      dateString: "June 10",
      name: "Erchil",
      imagePath: WellnestAsset.certificate,
      description:
          "Sample text sample text sample text sample text sample text sample text sample text",
    ),
    CertificateCoach(
      certNam: "PhD",
      dateString: "June 10",
      name: "Erchil",
      imagePath: WellnestAsset.certificate,
      description:
          "Sample text sample text sample text sample text sample text sample text sample text",
    ),
    CertificateCoach(
      certNam: "PhD",
      dateString: "June 10",
      name: "Erchil",
      imagePath: WellnestAsset.certificate,
      description:
          "Sample text sample text sample text sample text sample text sample text sample text",
    ),
  ];

  //Services
  final listService = [
    ServicCoach(
        imagePath: WellnestAsset.extraTrash,
        name: "Erchil Toturial",
        byname: "Erchil"),
    ServicCoach(
        imagePath: WellnestAsset.extraTrash,
        name: "Erchil Toturial",
        byname: "Erchil"),
    ServicCoach(
        imagePath: WellnestAsset.extraTrash,
        name: "Erchil Toturial",
        byname: "Erchil"),
    ServicCoach(
        imagePath: WellnestAsset.extraTrash,
        name: "Erchil Toturial",
        byname: "Erchil"),
  ];

  //Review
  final listReviews = [
    ReviewModel(
        dateString: "Sep 20",
        understanding: "Awesome",
        commentorname: "Erchil",
        description:
            "Great! Guidance is Given to me. Praise Erchil... AMEN. May Father of the understanding guide you.",
        starRate: 3.7),
    ReviewModel(
        dateString: "Sep 20",
        understanding: "Awesome",
        commentorname: "Erchil",
        description:
            "Great! Guidance is Given to me. Praise Erchil... AMEN. May Father of the understanding guide you.",
        starRate: 4.7),
    ReviewModel(
        dateString: "Sep 20",
        understanding: "Awesome",
        commentorname: "Erchil",
        description:
            "Great! Guidance is Given to me. Praise Erchil... AMEN. May Father of the understanding guide you.",
        starRate: 4.7),
    ReviewModel(
        dateString: "Sep 20",
        understanding: "Awesome",
        commentorname: "Erchil",
        description:
            "Great! Guidance is Given to me. Praise Erchil... AMEN. May Father of the understanding guide you.",
        starRate: 4.7),
  ];

  final boolx = false.obs;

  changeBoolx() {
    boolx(!boolx.value);
  }

  @override
  void onInit() {
    getCoachDetailbyId();
    getCoachAllService();
    super.onInit();
  }

  final YoutubePlayerController controll = YoutubePlayerController(
      initialVideoId: "aWjB-1mRP8M",
      params: YoutubePlayerParams(
          playsInline: false,
          loop: false,
          showControls: true,
          autoPlay: false,
          mute: false,
          showFullscreenButton: true));

  final List<CoachDetailFeatured> coachDetails = <CoachDetailFeatured>[].obs;
  final List<String?> netWorkImage = <String?>[].obs;
  final List<String?> flags = <String?>[].obs;
  final List<String?> specials = <String?>[].obs;
  final forNameService = ''.obs;

  getCoachDetailbyId() async {
    try {
      final result =
          await CoachGetLists.getCoachDetailFeature(Get.arguments.toString());
      if (result != null) {
        coachDetails.assign(result);
      }
    } finally {
      coachDetails
          .map(
            (e) => e.photoUrls!.map((e) => netWorkImage.add(e)).toList(),
          )
          .toList();
      coachDetails
          .map(
            (e) => e.nationalityTags!.map((e) => flags.add(e)).toList(),
          )
          .toList();
      coachDetails
          .map(
            (e) => e.tags!.map((e) => specials.add(e)).toList(),
          )
          .toList();
      forNameService(coachDetails.map((e) => e.name).first);
      this.getScheduleTime(coachId: coachDetails.map((e) => e.id).first);
    }
  }

  final List<ScheduleTime> timeSched = <ScheduleTime>[].obs;
  Map<DateTime, List<ScheduleTime>> mapDummy =
      <DateTime, List<ScheduleTime>>{}.obs;

  getScheduleTime({String? coachId}) async {
    try {
      final result = await ApiBookSession.getSchedultDates(coachId: coachId!);
      if (result != null) {
        timeSched.assignAll(result);
      }
    } finally {
      // print("Log Me Now ${timeSched.map((e) => e.id).toList()}");
      print(timeSched.map((e) => e.id).toList());
      timeSched
          .map((e) => getTimeSwitch(e.day, {
                "coachId": e.coachId!,
                "day": e.day!,
                "startTime": e.startTime,
                "endTime": e.endTime!,
                "isActive": e.isActive,
                "id": e.id
              }))
          .toList();
    }
  }

  DateTime weekDay(DateTime? time, int? weekDays) =>
      time!.add(Duration(days: weekDays!));

  DateTime weekDayNext(DateTime? time, int? weekDays) =>
      time!.add(Duration(days: weekDays! + time.weekday));

  DateTime lastofMonth(DateTime? time, int? weekDays) =>
      DateTime(time!.year, time.month + 1, weekDays!);

  DateTime dateOf(DateTime? time, int? week) => weekDay(time!, week);
  DateTime dateOfNext(DateTime? time, int? week) => weekDayNext(time!, week!);
  DateTime dateLasMonth(DateTime? time, int? week) => lastofMonth(time!, week);

  getTimeSwitch(int? day, dynamic data) {
    switch (day) {
      case 0:
        final DateTime wow = dateOf(DateTime.now(), day);
        final DateTime wowNext = dateOfNext(DateTime.now(), day);
        final DateTime wowLast = dateLasMonth(DateTime.now(), day);
        if (mapDummy.containsKey(wow)) {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
        } else {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
        }
        break;
      case 1:
        final DateTime wow = dateOf(DateTime.now(), day);
        final DateTime wowNext = dateOfNext(DateTime.now(), day);
        final DateTime wowLast = dateLasMonth(DateTime.now(), day);
        if (mapDummy.containsKey(wow)) {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
        } else {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
        }
        break;
      case 2:
        final DateTime wow = dateOf(DateTime.now(), day);
        final DateTime wowNext = dateOfNext(DateTime.now(), day);
        final DateTime wowLast = dateLasMonth(DateTime.now(), day);
        if (mapDummy.containsKey(wow)) {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
        } else {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
        }
        break;
      case 3:
        final DateTime wow = dateOf(DateTime.now(), day);
        final DateTime wowNext = dateOfNext(DateTime.now(), day);
        final DateTime wowLast = dateLasMonth(DateTime.now(), day);
        if (mapDummy.containsKey(wow)) {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
        } else {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
        }
        break;
      case 4:
        final DateTime wow = dateOf(DateTime.now(), day);
        final DateTime wowNext = dateOfNext(DateTime.now(), day);
        final DateTime wowLast = dateLasMonth(DateTime.now(), day);
        if (mapDummy.containsKey(wow)) {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
        } else {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
        }
        break;
      case 5:
        final DateTime wow = dateOf(DateTime.now(), day);
        final DateTime wowNext = dateOfNext(DateTime.now(), day);
        final DateTime wowLast = dateLasMonth(DateTime.now(), day);
        if (mapDummy.containsKey(wow)) {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
        } else {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
        }
        break;
      case 6:
        final DateTime wow = dateOf(DateTime.now(), day);
        final DateTime wowNext = dateOfNext(DateTime.now(), day);
        final DateTime wowLast = dateLasMonth(DateTime.now(), day);
        if (mapDummy.containsKey(wow)) {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)]!.add(
              ScheduleTime(
                  coachId: data['coachId'],
                  day: data['day'],
                  startTime: data['startTime'],
                  endTime: data['endTime'],
                  isActive: data['isActive'],
                  id: data['id']));
        } else {
          mapDummy[DateTime.utc(wow.year, wow.month, wow.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowNext.year, wowNext.month, wowNext.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
          mapDummy[DateTime.utc(wowLast.year, wowLast.month, wowLast.day)] = [
            ScheduleTime(
                coachId: data['coachId'],
                day: data['day'],
                startTime: data['startTime'],
                endTime: data['endTime'],
                isActive: data['isActive'],
                id: data['id'])
          ];
        }
        break;
    }
  }

  List<ScheduleTime> mapMEvoid(DateTime time) => mapDummy[time] ?? [];

  final List<Service> serviceList = <Service>[].obs;

  getCoachAllService() async {
    try {
      final result =
          await CoachGetLists.getAllServicesOfCoach(Get.arguments.toString());
      if (result != null) {
        serviceList.assignAll(result);
      }
    } finally {}
  }

  final List<Service> selectedCoach = <Service>[].obs;

  sellMe(String? serviceId) {
    final highFive =
        serviceList.where((e) => e.id!.contains(serviceId!)).toList();
    highFive.map((e) => selectedCoach.assign(e)).toList();
  }

  Rx<DateTime> focusDay = DateTime.now().obs;
  Rx<DateTime> selectedDay = DateTime.now().obs;
  Rx<DateTime> startTime = DateTime.now().obs;
  Rx<DateTime> endtime = DateTime.now().obs;
  final dateString = "".obs;
  final List<Map<String, dynamic>> sessions = <Map<String, dynamic>>[].obs;
  final List<Map<String, dynamic>> passdata = <Map<String, dynamic>>[].obs;
  final countMein = 0.obs;

  final List<ScheduleTime> timeSchedSearch = <ScheduleTime>[].obs;

  selectTimeWithdate(time, end) async {
    final dateOnly = DateFormat("yyyy-MM-dd").format(selectedDay.value);
    if (selectedDay.value.isAfter(DateTime.now())) {
      if (ifAlreadyAdded(mierda: dateOnly.toString())) {
        LoadingOrError.snackBarMessage(
            title: 'The Date and Time is Already Exist',
            message: "The Date you've pick is already Added.",
            icon: FaIcon(
              FontAwesomeIcons.exclamation,
              color: WellNestColor.wncWhite,
            ));
      } else {
        if (selectedCoach
            .map((e) => e.numberOfSessions)
            .contains(sessions.length)) {
          LoadingOrError.snackBarMessage(
              title: "You've Already Reach the limit of session",
              message: 'Please Refrain from exceeding of sessions.',
              icon: FaIcon(
                FontAwesomeIcons.exclamation,
                color: WellNestColor.wncWhite,
              ));
        } else {
          LoadingOrError.snackBarMessage(
              title: "Added on Session List",
              message: 'Schedule Added to your Session List.',
              icon: FaIcon(
                FontAwesomeIcons.thumbsUp,
                color: WellNestColor.wncWhite,
              ));
          final DateTime wow =
              DateFormat('yyyy-MM-dd HH:mm:ss').parse("$dateOnly $time");
          var dataReal = {
            "coachId": selectedCoach.map((e) => e.coachId).first,
            "serviceId": selectedCoach.map((e) => e.id).first,
            "userId": SharePref.detailData['id'],
            "startTime": DateFormat('yyyy-MM-dd HH:mm:ss')
                .parse("$dateOnly $time")
                .toString(),
            "endTime": DateFormat('yyyy-MM-dd HH:mm:ss')
                .parse("$dateOnly $end")
                .toString(),
          };
          var dataDisplay = {
            "startTime":
                DateFormat('yyyy-MM-dd hh:mm:ss').parse("$dateOnly $time"),
            "endTime": wow.add(Duration(seconds: 3600)),
            "mierda": dateOnly,
            "counter": 1 + sessions.map((e) => e).toList().length
          };

          print(dataDisplay);
          print(dataReal);
          sessions.add(dataDisplay);
          passdata.add(dataReal);
        }
      }
    } else {
      LoadingOrError.snackBarMessage(
          title: 'Please Choose a date is after from now',
          message: 'Pick a date does not behind from date from now.',
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    }
  }

  bool ifAlreadyAdded({String? mierda}) => sessions
      .where((e) => e['mierda']!.toString().contains(mierda!))
      .isNotEmpty;
  ondaySell(sel, foc) async {
    focusDay(foc);
    selectedDay(sel);
    timeSched
        .where((e) =>
            e.startTime.toString().contains(selectedDay.value.toString()))
        .toList()
        .map((ex) => timeSchedSearch.assign(ex))
        .toList();
    print(timeSched.map((e) => e.day).toList());
  }

  removeList(String? query, String? query1) {
    sessions.removeWhere((e) =>
        e['startTime'].toString().contains(query!) &&
        e['endTime'].toString().contains(query1!));
    passdata.removeWhere((e) =>
        e['startTime'].toString().contains(query!) &&
        e['endTime'].toString().contains(query1!));
  }

  final paymet = "".obs;

  payBugoMethod() async {
    final result = await ApiBookSession.payBugoMongo(datax: {
      "card": cardNumber!.value.text,
      "month": expirationDateMonth!.value.text,
      "year": expirationDateYear!.value.text,
      "cvc": cvcOfCard!.value.text,
    });
    paymet(result['data']['id']);
    print(paymet.value);
  }

  confirmBuy() async {
    // await ApiBookSession.payBugoMongo();
    if (passdata.map((e) => e).length ==
        selectedCoach.map((e) => e.numberOfSessions).first) {
      final result = await ApiBookSession.dataBookSession(
          list: passdata, paymenthodID: paymet.value);
      if (result == 200) {
        Get.back(id: 0);
        Get.to(() => SuccessBookingPage());
      } else {
        // Get.to(() => RedirectWebpageFromBook(
        //       url: result['data']['redirectUrl'],
        //       bookingId: result['data']['bookingId'],
        //     ));
      }
    } else {
      LoadingOrError.snackBarMessage(
          title: "Empty List Dates",
          message: "Hey!!",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    }
  }

  final termsApp = false.obs;
  final termCoach = false.obs;
  vals(bool? val) => termsApp(val);
  vals1(bool? val) => termCoach(val);
}
