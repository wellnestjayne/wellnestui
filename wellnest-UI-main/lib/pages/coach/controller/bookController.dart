import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:intl/intl.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class BookController extends GetxController {
  final mainController = Get.find<MainController>();

  Rx<DateTime> focusDay = DateTime.now().obs;
  Rx<DateTime> selectedDay = DateTime.now().obs;
  final dateString = "".obs;

  ondaySell(sel, foc, context, arguments) async {
    focusDay(foc);
    selectedDay(sel);
    if (selectedDay.value.isAfter(DateTime.now())) {
      dateString(DateFormat("EEEE, MMMM d y").format(selectedDay.value));
      //Show timePicker
      final TimeOfDay? pickTime = await showTimePicker(
          cancelText: "Back",
          confirmText: "Select Time",
          helpText: dateString.value,
          initialEntryMode: TimePickerEntryMode.input,
          context: context,
          initialTime: TimeOfDay.now());
      //End of Time Picker
      final time =
          "${pickTime!.hour}:${pickTime.minute} ${pickTime.period.toString().split(".").last.toString().toUpperCase()}";
      final dateOnly = DateFormat("yyyy-MM-dd").format(selectedDay.value);
      final timonly =
          pickTime.hour.toString() + ":" + pickTime.minute.toString();
      print(DateFormat('yyyy-MM-dd hh:mm:ss')
          .parse("$dateOnly $timonly:00")
          .toUtc());
      final DateTime wow = DateFormat('yyyy-MM-dd hh:mm:ss')
          .parse("$dateOnly $timonly:00")
          .toUtc();
      //  print(wow.add(Duration(seconds:arguments)));

    } else {
      LoadingOrError.snackBarMessage(
          title: 'Please Choose a date is after from now',
          message: 'Error encountered!',
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
