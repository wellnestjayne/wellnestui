import 'dart:collection';

import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_book/api_calendar.dart';
import 'package:wellnest/model/user_view_book_events.dart';
import 'package:wellnest/pages/coach/book_session/model/dummy.dart';
import 'package:wellnest/pages/write/calendar/shared/utils.dart';
import 'package:intl/intl.dart';

class CalendarController extends GetxController {
  Rx<DateTime> focusDay = DateTime.now().obs;
  Rx<DateTime> selectedDay = DateTime.now().obs;
  final List<EventsUser> eventUserList = <EventsUser>[].obs;
  final List<EventsUser> viewBydate = <EventsUser>[].obs;

  Map<DateTime, List<EventsUser>> mapDummy = <DateTime, List<EventsUser>>{}.obs;

  onDaySelect(date1, date2) {
    selectedDay(date1);
    focusDay(date2);

    viewBydate.assignAll(eventUserList.where((e) =>
        DateFormat("MM dd yyyy").format(e.startTime!).contains(DateFormat("MM dd yyyy").format(selectedDay.value))));
  }

  List<EventsUser> getEventsList(DateTime time) => mapDummy[time] ?? [];

  @override
  void onInit() {
    getEventOfUsers();
    super.onInit();
  }

  getEventOfUsers() async {
    try {
      final result = await APICalendarCallEvent.getUserCalendarEvent();
      eventUserList.assignAll(result);
    } finally {
      print('list ${eventUserList.map((e) => e.startTime).toList()}');
      onDaySelect(selectedDay.value, focusDay.value);
      eventUserList.map((e) async {
        if (mapDummy.containsKey(e.startTime)) {
          mapDummy[DateTime.utc(e.startTime!.year, e.startTime!.month, e.startTime!.day)]!.add(EventsUser());
        } else {
          mapDummy[DateTime.utc(e.startTime!.year, e.startTime!.month, e.startTime!.day)] = [EventsUser()];
        }
      }).toList();
      print('mapDummy $mapDummy');
    }
  }
}
