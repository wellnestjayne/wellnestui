
class ReviewModel{

  final String? dateString;
  final String? understanding;
  final String? commentorname;
  final String? description;
  final double? starRate;

  ReviewModel({this.dateString, this.understanding, this.commentorname, this.description,this.starRate});

}