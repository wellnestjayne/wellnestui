

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:wellnest/pages/coach/controller/webViewController.dart';

class RedirectWebpageFromBook extends StatelessWidget {
  final String? url;
  final String? bookingId;
  RedirectWebpageFromBook({Key? key, this.url, this.bookingId}) : super(key: key);
  final controller = Get.put(WebViewControllerRedicrect());
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: WebView(
            initialUrl: url,
            javascriptMode: JavascriptMode.unrestricted,
            //javascriptChannels: (){},
            onProgress: (progress){
              print(progress);
            },
          ),          
      ),
    );
  }
}