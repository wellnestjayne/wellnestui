import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';
import 'package:wellnest/pages/coach/book_session/view_calendar/view_calendar.dart';
import 'package:wellnest/pages/coach/book_session/widget/successBuyCard.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/session_card.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:intl/intl.dart';

class SuccessBookingPage extends StatelessWidget {
  final bookController = Get.put(FeatureCoachController());
  final controller = Get.put(BookingController());
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 60, left: 20, right: 20, bottom: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(shape: BoxShape.circle, color: WellNestColor.wncAquaBlue),
                  child: Center(
                    child: FaIcon(
                      FontAwesomeIcons.check,
                      color: WellNestColor.wncWhite,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "You have successfully booked your session",
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 20.0),
                ),
                SizedBox(
                  height: 45,
                ),
                SuccessBookCard(
                  imagePath: bookController.serviceList.map((e) => e.bannerUrl).first,
                  title: bookController.serviceList.map((e) => e.name).first,
                  name: bookController.forNameService.value,
                ),
                SizedBox(
                  height: 20,
                ),
                Obx(
                  () => bookController.sessions.isEmpty
                      ? Container()
                      : Container(
                          child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: bookController.sessions.length,
                              itemBuilder: (_, __) => Padding(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    child: SessionCardforPickDates(
                                      lenght: bookController.sessions[__]['counter'],
                                      date: DateFormat.yMMMMEEEEd().format(DateFormat("yyyy-MM-dd")
                                          .parse(bookController.sessions[__]['mierda'].toString())),
                                      press: () {},
                                      showTop: false,
                                    ),
                                  )),
                        ),
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(bottom: 20, left: 20, right: 20),
          child: Container(
            height: 50,
            child: Center(
              child: BlueButtonStandard(
                bluebutton: () {
                  controller.getBookingByUser();
                  Get.back();
                  Get.to(() => ViewCalendaroUser(), id: 0);
                },
                title: "View Calendar",
                back: WellNestColor.wncBlue,
                border: WellNestColor.wncBlue,
                textColor: WellNestColor.wncWhite,
                sizeText: 15.0,
                height: 37,
                width: 334,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
