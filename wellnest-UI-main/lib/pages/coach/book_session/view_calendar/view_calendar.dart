import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/model/user_view_book_events.dart';
import 'package:wellnest/pages/coach/book_session/model/dummy.dart';
import 'package:wellnest/pages/coach/book_session/widget/card_select.dart';
import 'package:wellnest/pages/coach/controller/calendar_event_controller.dart';
import 'package:wellnest/pages/coach/widget/app_bar_checkout.dart';
import 'package:wellnest/pages/write/calendar/customization/calendar_style.dart';
import 'package:wellnest/pages/write/calendar/shared/utils.dart';
import 'package:wellnest/pages/write/calendar/table_calendar.dart';
import 'package:intl/intl.dart';

class ViewCalendaroUser extends StatelessWidget {
  final eventController = Get.put(CalendarController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SafeArea(
        child: Scaffold(
          appBar: AppBarDatePicker(
            goback: () => Get.back(id: 0),
            title: "Calendar",
          ),
          body: Column(
            children: [
              TableCalendar<EventsUser>(
                  eventLoader: eventController.getEventsList,
                  rowHeight: 45,
                  calendarFormat: CalendarFormat.month,
                  firstDay: DateTime.utc(2021, 8, 2),
                  lastDay: DateTime.utc(2030, 3, 14),
                  focusedDay: eventController.focusDay.value,
                  calendarStyle: CalendarStyle(
                      markerDecoration: BoxDecoration(shape: BoxShape.circle, color: WellNestColor.wncBlue),
                      markersAlignment: Alignment(-0.10, 1),
                      markersAutoAligned: true,
                      canMarkersOverflow: true,
                      holidayDecoration: BoxDecoration(color: WellNestColor.wncLightmoreAque),
                      holidayTextStyle: WellNestTextStyle.nowMedium(WellNestColor.wncLighBg, 12.0),
                      isTodayHighlighted: true),
                  selectedDayPredicate: (day) => isSameDay(eventController.selectedDay.value, day),
                  onDaySelected: (date1, date2) =>
                      // bookingController.ondaySell(selected, focusselectd,context,Get.arguments.toString()),
                      eventController.onDaySelect(date1, date2)),
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                      color: WellNestColor.wncLighBg,
                      boxShadow: [
                        BoxShadow(spreadRadius: 3, blurRadius: 3, color: WellNestColor.wncGrey.withOpacity(0.1))
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, left: 5, right: 5),
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: eventController.viewBydate.length,
                        itemBuilder: (_, __) => CardSelectHolder(
                              timeStart: DateFormat("hh:mm:aa").format(eventController.viewBydate[__].startTime!),
                              xtimEnd: DateFormat("hh:mm:aa").format(eventController.viewBydate[__].endTime!),
                              xtimStart: DateFormat("hh:mm:aa").format(eventController.viewBydate[__].startTime!),
                            )),
                  ),
                ),
              ),
              //  Container(
              //    child: ListView.builder(
              //      shrinkWrap: true,
              //      itemCount: eventController.viewBydate.length,
              //      itemBuilder: (_,__)=>
              //    Text(eventController.viewBydate[__].creationTime.toString())
              //    ),
              //  ),
            ],
          ),
        ),
      ),
    );
  }
}
