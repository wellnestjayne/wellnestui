

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/standard_widgets/appbar.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class EditPaymentCheckbook extends StatelessWidget {
  
  final bookController = Get.put(FeatureCoachController());
  final GlobalKey<FormState> globeKeyPayment = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          appBar: StandardAppbaruse(
            title: "Edit Payment",
            color: WellNestColor.wncBlue,
            function: ()=>Get.back(id: 0),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Form(
                key: globeKeyPayment,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:[
                     TextFieldWellnestw(
                        controller:  bookController.cardNumber,
                        validator: (value) =>
                            phone(bookController.cardNumber!.text = value!),
                        keyboardType: TextInputType.number,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) =>
                             bookController.cardNumber!.text = value!,
                        hintText: "Card Number",
                      ),
                      SizedBox(height: 20),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                            Container(
                    width: 50,
                    child: TextFieldWellnestw(
                          controller:  bookController.expirationDateMonth,
                          validator: (value) =>
                              nameWithDot(bookController.expirationDateMonth!.text = value!),
                          keyboardType: TextInputType.text,
                          obsecure: false,
                          onchanged: (value) => false,
                          onsaved: (value) =>
                               bookController.expirationDateMonth!.text = value!,
                          hintText: "MM",
                        ),
                  ),
                  SizedBox(width: 10,),
                    Container(
                    width: 50,
                    child: TextFieldWellnestw(
                          controller:  bookController.expirationDateYear,
                          validator: (value) =>
                              nameWithDot(bookController.expirationDateYear!.text = value!),
                          keyboardType: TextInputType.text,
                          obsecure: false,
                          onchanged: (value) => false,
                          onsaved: (value) =>
                               bookController.expirationDateYear!.text = value!,
                          hintText: "YY",
                        ),
                  ),
                  SizedBox(width: 10,),
                  Container(
                    width: 150,
                    child: TextFieldWellnestw(
                          controller:  bookController.cvcOfCard,
                          validator: (value) =>
                              phone(bookController.cvcOfCard!.text = value!),
                          keyboardType: TextInputType.number,
                          obsecure: true,
                          onchanged: (value) => false,
                          onsaved: (value) =>
                               bookController.cvcOfCard!.text = value!,
                          hintText: "CVC",
                        ),
                  ),
                        ],
                      ),
                    ),
                   SizedBox(height: 20),
                  
                  SizedBox(height: 45,),
                  Container(
                    width: 337,
                    child: Row(
                      children: [
                        Container(
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                            color: WellNestColor.wncGrey,
                            shape: BoxShape.circle
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: Text("I acknowledge that by saving my account I acknowldge that by saving my account I xcacknowledge that by saving my account",
                          textAlign: TextAlign.justify,
                          maxLines: 6,
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncGrey
                            , 13.0),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 15,),
                   Container(
                    width: 337,
                    child: Row(
                      children: [
                        Container(
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                            color: WellNestColor.wncGrey,
                            shape: BoxShape.circle
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: Text("I acknowledge that by saving my account I acknowldge that by saving my account I xcacknowledge that by saving my account",
                          textAlign: TextAlign.justify,
                          maxLines: 6,
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncGrey
                            , 13.0),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 65,),
                  BlueButtonStandard(
                    height: 37,
                    width: 334,
                    bluebutton: ()=>callback(),
                    title: "Save",
                    textColor: WellNestColor.wncWhite,
                    border: WellNestColor.wncBlue,
                    back: WellNestColor.wncBlue,
                  ),
                  SizedBox(height: 15,),
                  ] 
                ),
              ),
            )
          ),
        ),
    );
  }

  callback(){
    if(globeKeyPayment.currentState!.validate()){
      bookController.savePayment();
    }
  }

}