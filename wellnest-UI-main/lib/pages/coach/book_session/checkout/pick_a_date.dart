
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/book_session/checkout/expanded_list.dart';
import 'package:wellnest/pages/coach/controller/bookController.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/app_bar_checkout.dart';
import 'package:wellnest/pages/coach/widget/session_card.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';
import 'package:wellnest/pages/write/calendar/customization/calendar_style.dart';
import 'package:wellnest/pages/write/calendar/shared/utils.dart';
import 'package:wellnest/pages/write/calendar/table_calendar.dart';
import 'package:intl/intl.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
class PickADateForBuying extends StatelessWidget {
  
  final bookingController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      ()=>
       SafeArea(
        child: Scaffold(
          appBar: AppBarDatePicker(
            goback: ()=>Get.back(id: 0),
            title: "Pick a Date",
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 20,right: 20,top: 5,bottom: 20
              ),
              child: Column(
                children: [
                   TableCalendar(
                          rowHeight: 50,
                          eventLoader: bookingController.mapMEvoid,
                          calendarFormat: CalendarFormat.month,
                          firstDay: DateTime.utc(2021,8,2),
                          lastDay: DateTime.utc(2030, 3, 14),
                          focusedDay: bookingController.focusDay.value,
                          calendarStyle: CalendarStyle(
                          markerDecoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncBlue
                          ),
                          isTodayHighlighted: true),
                          selectedDayPredicate: (day)=>isSameDay(bookingController.selectedDay.value, day),
                          onDaySelected: (selected,focusselectd)=>
                          bookingController.ondaySell(selected, focusselectd),
                        ),
                SizedBox(height: 15,),
                HeaderTitleSubs(
                    title: "Sessions",
                    extra: "Expand",
                    extraFunction: ()=>Get.to(()=>ExpandedSesionList(),id: 0)
                    ),  
                SizedBox(height: 20,),
              //   Container(
              //     width: 349,
              //     child: ListView.builder(
              //       shrinkWrap: true,
              //       itemCount: bookingController.timeSchedSearch.length,
              //       itemBuilder: (_,__)
              //       => Container(
              //         width: 220,
              //         height: 100,
              //         decoration: BoxDecoration(
              //           color: WellNestColor.wncWhite,
              //           borderRadius: BorderRadius.circular(10),
              //           boxShadow: [
              //             BoxShadow(
              //               offset: Offset(2, 3),
              //               blurRadius: 2,
              //               spreadRadius: 2,
              //               color: WellNestColor.wncLightgrey.withOpacity(0.3)
              //             ),
              //           ]
              //         ),
              //         child: Padding(
              //           padding: const EdgeInsets.all(12.0),
              //           child: Row(
              //             children: [
              //               Expanded(
              //                 child: Container(
              //                   child: Column(
              //                     crossAxisAlignment: CrossAxisAlignment.start,
              //                     children: [
              //                         Text(DateFormat.yMMMEd().format(bookingController.selectedDay.value)
              //                         .toString(),style:  WellNestTextStyle.nowMedium(
              //                           WellNestColor.wncGrey ,15.0),),
              //                       SizedBox(height: 10,),
              //                       RichText(
              //                         text: TextSpan(
              //                           children: [
              //                             TextSpan(
              //                               text: "Start Time: ",
              //                               style:  WellNestTextStyle.nowRegular(
              //                           WellNestColor.wncGrey ,13.0)
              //                             ),
              //                             TextSpan(
              //                               text: DateFormat.jm().format(
              //                                 DateFormat("hh:mm:ss").parse(bookingController.timeSchedSearch[__].startTime.toString())
              //                               ).toString(),
              //                               style:  WellNestTextStyle.nowBold(
              //                           WellNestColor.wncGrey ,13.0)
              //                             ),
              //                           ]
              //                         )),
              //                          SizedBox(height: 8,),
              //                       RichText(
              //                         text: TextSpan(
              //                           children: [
              //                             TextSpan(
              //                               text: "End Time: ",
              //                               style:  WellNestTextStyle.nowRegular(
              //                           WellNestColor.wncGrey ,13.0)
              //                             ),
              //                             TextSpan(
              //                               text: DateFormat.jm().format(
              //                                 DateFormat("hh:mm:ss").parse(bookingController.timeSchedSearch[__].endTime.toString())
              //                               ).toString(),
              //                               style:  WellNestTextStyle.nowBold(
              //                           WellNestColor.wncGrey ,13.0)
              //                             ),
              //                           ]
              //                         )),
              //                     ],
              //                   ),
              //                 ),
              //               )
              //               ,BlueButtonStandard(
              //   title: "Add",
              //   textColor: WellNestColor.wncWhite,
              //   height: 60,
              //   width: 60,
              //   back: WellNestColor.wncBlue,
              //   border: WellNestColor.wncBlue,
              //   bluebutton: ()=>bookingController.selectTimeWithdate(
              //           bookingController.timeSchedSearch[__].startTime,
              //           bookingController.timeSchedSearch[__].endTime
              //         ),
              // ),
              //             ],
              //           ),
              //         ),
              //       ),
                    
              //       ),
              //   ),     
              ...bookingController.mapMEvoid(bookingController.selectedDay.value)
              .map((e) => Container(
                      width: 349,
                     // height: 100,
                      decoration: BoxDecoration(
                        color: WellNestColor.wncWhite,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(2, 3),
                            blurRadius: 2,
                            spreadRadius: 2,
                            color: WellNestColor.wncLightgrey.withOpacity(0.3)
                          ),
                        ]
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                      Text(DateFormat.yMMMEd().format(bookingController.selectedDay.value)
                                      .toString(),style:  WellNestTextStyle.nowMedium(
                                        WellNestColor.wncGrey ,15.0),),
                                    SizedBox(height: 10,),
                                    RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: "Start Time: ",
                                            style:  WellNestTextStyle.nowRegular(
                                        WellNestColor.wncGrey ,13.0)
                                          ),
                                          TextSpan(
                                            text: DateFormat.jm().format(
                                              DateFormat("hh:mm:ss").parse(e.startTime.toString())
                                            ).toString(),
                                            style:  WellNestTextStyle.nowBold(
                                        WellNestColor.wncGrey ,13.0)
                                          ),
                                        ]
                                      )),
                                       SizedBox(height: 8,),
                                    RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: "End Time: ",
                                            style:  WellNestTextStyle.nowRegular(
                                        WellNestColor.wncGrey ,13.0)
                                          ),
                                          TextSpan(
                                            text: DateFormat.jm().format(
                                              DateFormat("hh:mm:ss").parse(e.endTime.toString())
                                            ).toString(),
                                            style:  WellNestTextStyle.nowBold(
                                        WellNestColor.wncGrey ,13.0)
                                          ),
                                        ]
                                      )),
                                  ],
                                ),
                              ),
                            )
                            ,BlueButtonStandard(
                title: "Add",
                textColor: WellNestColor.wncWhite,
                height: 60,
                width: 60,
                back: WellNestColor.wncBlue,
                border: WellNestColor.wncBlue,
                bluebutton: ()=>bookingController.selectTimeWithdate(
                        e.startTime,
                        e.endTime
                      ),
              ),
                          ],
                        ),
                      ),
                    ),).toList(), 
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}