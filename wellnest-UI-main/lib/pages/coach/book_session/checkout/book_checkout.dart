import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/coach/book_session/success_page/success_page.dart';
import 'package:wellnest/pages/coach/book_session/widget/card_session_number.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/session_card.dart';
import 'package:wellnest/pages/coach/widget/termsandcondition.dart';
import 'package:wellnest/pages/featured_coach/controller/featureController.dart';
import 'package:wellnest/pages/subscription/widget/edit_payment.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';
import 'package:wellnest/pages/subscription/widget/totalPayment_withoff.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/appbar.dart';
import 'package:intl/intl.dart';

class BookCheckout extends StatelessWidget {
  final bookController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
          child: Scaffold(
            appBar: StandardAppbaruse(
              function: () => Get.back(id: 0),
              title: "Checkout",
              color: WellNestColor.wncBlue,
            ),
            body: SingleChildScrollView(
              child: AnimationLimiter(
                  child: Column(
                children: AnimationConfiguration.toStaggeredList(
                    duration: const Duration(seconds: 1),
                    childAnimationBuilder: (widget) => SlideAnimation(
                        verticalOffset: 150,
                        child: FadeInAnimation(
                          child: widget,
                        )),
                    children: bookController.selectedCoach
                        .map(
                          (e) => Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 20, right: 20, top: 10),
                              child: Column(
                                children: [
                                  NumbberSessionCard(
                                      imagepath: e.bannerUrl,
                                      name: bookController.forNameService.value,
                                      title: e.name,
                                      price: e.price,
                                      session: e.numberOfSessions),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  HeaderTitleSubs(
                                    title: "Date and Time",
                                    extra: "Edit",
                                    extraFunction: () => Get.toNamed(
                                        AppRouteName.pickadateCheck!,
                                        id: 0),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Obx(
                                    () => bookController.sessions.isEmpty
                                        ? Container()
                                        : Container(
                                            height: 110,
                                            width: 349,
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: bookController
                                                    .sessions.length,
                                                itemBuilder: (_, __) => Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              bottom: 10),
                                                      child:
                                                          SessionCardforPickDates(
                                                        press: () => bookController
                                                            .removeList(
                                                                bookController
                                                                    .sessions[
                                                                        __][
                                                                        'startTime']
                                                                    .toString(),
                                                                bookController
                                                                    .sessions[
                                                                        __][
                                                                        'endTime']
                                                                    .toString()),
                                                        lenght: bookController
                                                                .sessions[__]
                                                            ['counter'],
                                                        date: DateFormat
                                                                .yMMMMEEEEd()
                                                            .format(DateFormat(
                                                                    "yyyy-MM-dd")
                                                                .parse(bookController
                                                                    .sessions[
                                                                        __][
                                                                        'mierda']
                                                                    .toString())),
                                                      ),
                                                    )),
                                          ),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  HeaderTitleSubs(
                                    title: "Payment",
                                    extra: "Edit",
                                    extraFunction: () => Get.toNamed(
                                        AppRouteName.editpayment!,
                                        id: 0),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Obx(
                                    () => EditPayment(
                                        cardText: bookController
                                                .cardText.isEmpty
                                            ? "Edit Payment"
                                            : "*" +
                                                bookController.cardText.value
                                                    .substring(bookController
                                                            .cardText
                                                            .toString()
                                                            .length -
                                                        4),
                                        sessionFee: "P " + e.price.toString(),
                                        //appFee: "P 200.00",
                                        discount: "% " +
                                            e.discount
                                                .toString()
                                                .replaceAll(".0", ""),
                                        voucher: "P 0.00",
                                        totalPayment:
                                            e.price! * e.numberOfSessions!),
                                  ),
                                  SizedBox(
                                    height: 25,
                                  ),
                                  // Text("Payment Method Id: ${bookController.paymet.isEmpty ? "" : bookController.paymet.value}"),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  TermsAndConditionsCheckout(),
                                  SizedBox(
                                    height: 25,
                                  ),
                                  TotalPaymentWithOff(
                                      goto: () => bookController.confirmBuy(),
                                      // goto: () {
                                      //   //Get.back(id: 0);
                                      //   Get.to(() => SuccessBookingPage());
                                      // },
                                      total:

                                          // e.price! *
                                          //     e.numberOfSessions! *
                                          //     (e.discount! *
                                          //         e.numberOfSessions! /
                                          //         100),

                                          e.price! * e.numberOfSessions! -
                                              ((e.price! *
                                                      e.numberOfSessions!) *
                                                  (e.discount! / 100))),
                                  SizedBox(
                                    height: 25,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                        .toList()),
              )),
            ),
          ),
          onWillPop: () async {
            Get.back(id: 0);
            return true;
          }),
    );
  }
}
