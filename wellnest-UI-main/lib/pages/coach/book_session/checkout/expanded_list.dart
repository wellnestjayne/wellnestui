

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/app_bar_checkout.dart';
import 'package:wellnest/pages/coach/widget/session_card.dart';
import 'package:intl/intl.dart';
class ExpandedSesionList extends StatelessWidget {
  final bookController = Get.put(FeatureCoachController());


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBarDatePicker(
            goback: ()=>Get.back(id: 0),
            title: "Session List",
          ),
        body:  Padding(
          padding: const EdgeInsets.all(30.0),
          child: Obx(()=>
                    bookController.sessions.isEmpty ? Container() :
                     Container(
                    width: 349,
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: bookController.sessions.length,
                      itemBuilder: (_,__)
                      =>Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: SessionCardforPickDates(
                         press: ()
                          =>bookController.removeList(
                            bookController.sessions[__]['startTime'].toString(),
                            bookController.sessions[__]['endTime'].toString()
                            ),
                          // timeStart: bookController.sessions[__]['startTime'].toString(),
                          // endTime:  bookController.sessions[__]['endTime'].toString(),
                          lenght: bookController.sessions[__]['counter'],
                          date:  DateFormat.yMMMMEEEEd().format(DateFormat("yyyy-MM-dd").parse(bookController.sessions[__]['mierda'].toString())),
                        ),
                      )
                      ),
                  ), 
                    ),
        ), 
      ),
    );
  }
}