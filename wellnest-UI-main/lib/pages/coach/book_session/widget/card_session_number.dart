
import 'package:flutter/material.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class NumbberSessionCard extends StatelessWidget {


  final String? imagepath;
  final String? name;
  final String? title;
  final double? price;
  final int? session;

  const NumbberSessionCard({Key? key, this.imagepath, this.name, this.title, this.price, this.session}) : super(key: key);
  


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 218,
      width: 337,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
        fit: BoxFit.cover,
        alignment: Alignment.topCenter,
        image: NetworkImage(imagepath!))
      ),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 111,
              width: 337,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                textDirection: TextDirection.ltr,
                                children: [
                                  Text(title!,
                                  style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncWhite,15.0),
                                  ),
                                  SizedBox(height: 2,),
                                  Text(name!,
                                  style: WellNestTextStyle.nowRegular(
                                    WellNestColor.wncWhite,10.0),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                textDirection: TextDirection.ltr,
                                children: [
                                  Text(price.toString(),
                                  style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncWhite,15.0),
                                  ),
                                  SizedBox(height: 2,),
                                  Text("save P100",
                                  style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncWhite,10.0),
                                  ),
                                ],
                              ),
                            ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Container(
                      child: Row(
                        children: [
                        Expanded(
                          child: Text("Number of Session",
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncWhite,18.0),
                          ),
                        ),
                        Text("x"+session.toString(),
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncWhite,18.0),
                          ),
                        
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}