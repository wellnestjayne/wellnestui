
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class CardSelectHolder extends StatelessWidget {

  final String? timeStart;
  final String? xtimStart;
  final String? xtimEnd;

  const CardSelectHolder({Key? key, this.timeStart, this.xtimStart, this.xtimEnd}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        width: 349,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(timeStart!,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncBlue, 13.0),
            ),
            SizedBox(width: 5,),
            Container(
              width: 261,
              height: 86,
              decoration: BoxDecoration(
                color: WellNestColor.wncWhite,
                boxShadow:[
                   BoxShadow(
                     offset: Offset(2, 2),
                     blurRadius: 1,
                     spreadRadius: 2,
                     color: WellNestColor.wncLightgrey.withOpacity(0.3)
                   )
                ],
                borderRadius: BorderRadius.circular(10)
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height: 36,
                      width: 5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40),
                        color: WellNestColor.wncAquaBlue
                      ),
                    ),
                    SizedBox(width: 10,),
                    Container(
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: WellNestColor.wncAquaBlue
                      ),
                    ),
                    SizedBox(width: 10,),
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          textDirection: TextDirection.ltr,
                          children: [
                            Text("Coaching Session with Anne Matt",
                            style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,13.0),
                            ),
                            Spacer(),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Text("$xtimStart - $xtimEnd",
                              textAlign: TextAlign.left,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey,13.0),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}