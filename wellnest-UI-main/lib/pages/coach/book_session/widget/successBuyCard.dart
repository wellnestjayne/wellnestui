
import 'package:flutter/material.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class SuccessBookCard extends StatelessWidget {
  
  final String? title;
  final String? name;
  final String? imagePath;

  const SuccessBookCard({Key? key, this.title, this.name, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 177,
      width: 349,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          fit: BoxFit.cover,  
          alignment: Alignment.topCenter,
          image: NetworkImage(imagePath!) 
          ),
      ),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 61,
              width: 349,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  textDirection: TextDirection.ltr,
                  children: [
                      Text(title!,
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncWhite, 15.0),
                      ),
                      SizedBox(height: 10,),
                      Text("By $name",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncWhite, 10.0),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}