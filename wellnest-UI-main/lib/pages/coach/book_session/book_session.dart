import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/header_book_session.dart';
import 'package:wellnest/pages/coach/widget/review_card.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/header.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class BookSessionToCoach extends StatelessWidget {
  // final bookSession = Get.put(BookController());
  final coachController = Get.put(FeatureCoachController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
          child: Scaffold(
            body: SingleChildScrollView(
              child: AnimationLimiter(
                  child: Column(
                      children: AnimationConfiguration.toStaggeredList(
                          duration: const Duration(seconds: 1),
                          childAnimationBuilder: (widget) => SlideAnimation(
                              verticalOffset: 150,
                              child: FadeInAnimation(
                                child: widget,
                              )),
                          children: coachController.selectedCoach
                              .map(
                                (e) => Container(
                                  child: Column(
                                    children: [
                                      HeaderBookSession(
                                        title: e.name,
                                        name: coachController.forNameService.value,
                                        imagePathIcon: coachController.netWorkImage.map((e) => e).first,
                                        imagePathback: e.bannerUrl,
                                        goBack: () => Get.back(id: 0),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                                        child: Container(
                                          width: 349,
                                          child: Text(
                                            e.description!,
                                            textAlign: TextAlign.justify,
                                            style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 25,
                                      ),
                                      Container(
                                        height: 156,
                                        width: 337,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(20),
                                            image: DecorationImage(
                                                fit: BoxFit.cover,
                                                alignment: Alignment.center,
                                                image: NetworkImage(e.bannerUrl.toString()))),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(20),
                                          child: YoutubePlayerIFrame(
                                            aspectRatio: 16 / 9,
                                            controller: coachController.controll,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 25,
                                          left: 20,
                                          right: 20,
                                        ),
                                        child: Column(
                                          children: [
                                            Visibility(
                                              visible: e.canCancelFreeInDays != null ? true : false,
                                              child: RichText(
                                                  text: TextSpan(children: [
                                                TextSpan(
                                                    text: "Refund Policy for refund for cancellations ",
                                                    style:
                                                        WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 13.0)),
                                                TextSpan(
                                                    text: " ${e.canCancelFreeInDays} days",
                                                    style: TextStyle(
                                                        decoration: TextDecoration.underline,
                                                        fontFamily: WellNestTextStyle.nowMed,
                                                        color: WellNestColor.wncGrey,
                                                        fontSize: 13.0)),
                                                TextSpan(
                                                    text: " before the scheduled session/s.",
                                                    style:
                                                        WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 13.0)),
                                              ])),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Visibility(
                                                visible: e.canRefund == false ? true : false,
                                                child: Text(
                                                  "Refund Policy: Fixed booking. Not Eligible for refund",
                                                  style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 13.0),
                                                ))
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20, right: 20),
                                        child: Standardheader(
                                          title: "Reviews",
                                          show: false,
                                          colorText: WellNestColor.wncBlue,
                                          press: () {},
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20, right: 20),
                                        child: Container(
                                          height: 160,
                                          child: ListView.builder(
                                              scrollDirection: Axis.horizontal,
                                              itemCount: coachController.listReviews.length,
                                              itemBuilder: (context, index) => Padding(
                                                    padding:
                                                        const EdgeInsets.only(left: 5, right: 10, bottom: 10, top: 10),
                                                    child: ReviewCardCoach(
                                                      dateString: coachController.listReviews[index].dateString,
                                                      understanding: coachController.listReviews[index].understanding,
                                                      commentorname: coachController.listReviews[index].commentorname,
                                                      description: coachController.listReviews[index].description,
                                                      starRate: coachController.listReviews[index].starRate,
                                                    ),
                                                  )),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20, right: 20, top: 35, bottom: 40),
                                        child: BlueButtonStandard(
                                          bluebutton: () => Get.toNamed(
                                            AppRouteName.homeView! + AppRouteName.checkoutBook!,
                                            id: 0,
                                          ),
                                          title: "Book Now",
                                          back: WellNestColor.wncBlue,
                                          border: WellNestColor.wncBlue,
                                          textColor: WellNestColor.wncWhite,
                                          sizeText: 15.0,
                                          height: 37,
                                          width: 334,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                              .toList()))),
            ),
          ),
          onWillPop: () async {
            Get.back(id: 0);
            return true;
          }),
    );
  }
}
