

import 'package:credit_card_validator/credit_card_validator.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class CardValidator{


  static CreditCardValidator _cc = CreditCardValidator();

  bool? validateCardInfo({String? numberCard,String? expDate,String? cvc}){

    final ccNumCardRes = _cc.validateCCNum(numberCard!);
    final ccExpDate = _cc.validateExpDate(expDate!);
    final cccvc = _cc.validateCVV(cvc!, ccNumCardRes.ccType);
    if(ccNumCardRes.isValid){

    }else{
      print(ccNumCardRes.message);
      LoadingOrError.errorloading("The Card you Input is Invalid", "Payment Method");
    }

  }

}