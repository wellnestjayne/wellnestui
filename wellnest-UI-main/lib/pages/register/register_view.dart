import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/login/login_view.dart';
import 'package:wellnest/pages/register/controller/register_controller.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/social_button.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class RegisterVieww extends StatelessWidget {

  final String? typeUser;

  final registerController = Get.put(RegisterControler());
  final GlobalKey<FormState> _forms = GlobalKey<FormState>();

  RegisterVieww({Key? key, this.typeUser}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final colorUse = typeUser == "user" ? WellNestColor.wncAquaBlue : WellNestColor.wncBlue;
    return WillPopScope(
      onWillPop: ()async=> await Get.offNamed(AppRouteName.startingpage!),
      child: Scaffold(
        backgroundColor: WellNestColor.wncWhite,
        body: SafeArea(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Obx(
                ()=> Form(
                  key: _forms,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 160.h,
                        width: 160.w,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                          WellnestAsset.logoWellNest,
                        ))),
                      ),
                      Text(
                        "Create An Account",
                        style: WellNestTextStyle.nowBold(WellNestColor.wncGrey, 16.sp),
                      ),
                      SizedBox(
                        height: 40.h,
                      ),
                      TextFieldWellnestw(
                        controller: registerController.firstname,
                        validator: (value) =>
                            name(registerController.firstname.text = value!),
                        keyboardType: TextInputType.text,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) =>
                            registerController.firstname.text = value!,
                        hintText: "Firstname",
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      TextFieldWellnestw(
                        controller: registerController.lastname,
                        validator: (value) =>
                            name(registerController.lastname.text = value!),
                        keyboardType: TextInputType.text,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) =>
                            registerController.lastname.text = value!,
                        hintText: "Lastname",
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      TextFieldWellnestw(
                        controller:  registerController.phone,
                        validator: (value) =>
                            phone(registerController.phone.text = value!),
                        keyboardType: TextInputType.number,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) =>
                             registerController.phone.text = value!,
                        hintText: "Mobile/Telephone",
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      // TextFieldWellnestw(
                      //   child: Container(
                      //     width: 90,
                      //     child: Center(
                      //       child: Padding(
                      //         padding: const EdgeInsets.all(10.0),
                      //         child: Row(
                      //           mainAxisAlignment: MainAxisAlignment.center,
                      //           crossAxisAlignment: CrossAxisAlignment.center,
                      //           children: [
                      //              Image.asset(WellnestAsset.flag,
                      //              width: 30,
                      //              height: 30,
                      //              ),
                      //              SizedBox(width: 5.h,),
                      //              Text("+63",
                      //              style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 14.sp),
                      //              )
                      //           ],
                      //         ),
                      //       ),
                      //     ),
                      //   ),
                      //   maxText: 10,
                      //   controller: registerController.phone,
                      //   validator: (value) => mobilenumberX(
                      //       registerController.phone.text = value!),
                      //   keyboardType: TextInputType.number,
                      //   obsecure: false,
                      //   onchanged: (value) => false,
                      //   onsaved: (value) =>
                      //       registerController.phone.text = value!,
                      //   hintText: "Phone",
                      // ),
                      // SizedBox(
                      //   height: 15.h,
                      // ),
                      TextFieldWellnestw(
                        controller: registerController.email,
                        validator: (value) =>
                            nameWithDot(registerController.email.text = value!),
                        keyboardType: TextInputType.emailAddress,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) =>
                            registerController.email.text = value!,
                        hintText: "Email",
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      TextFieldWellnestw(
                        controller: registerController.password,
                        validator: (value) =>
                            password(registerController.password.text = value!),
                        keyboardType: TextInputType.visiblePassword,
                        obsecure: registerController.showPass.isFalse ? true : false,
                        onchanged: (value) => false,
                        onsaved: (value) =>
                            registerController.password.text = value!,
                        hintText: "Password",
                      ),
                      Container(
                          width: 349,
                          child: Row(
                            children: [
                              Checkbox(
                              value: registerController.showPass.value,
                              tristate: true,
                              checkColor: WellNestColor.wncWhite,
                              activeColor: WellNestColor.wncBlue,
                              onChanged: (bolVal)=>registerController.showThePassword()),
                              Text("Show Password.",
                              style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey,
                              13.0),),
                            ],
                          ),
                        ),
                      SizedBox(
                        height: 55.h,
                      ),
                      // SocialButtonStandard(
                      //   facebook: () => print("this is f"),
                      //   google: () => print("this is g"),
                      //   linkedin: () => print("this is l"),
                      //   twitter: () => print("this is t"),
                      //   bonusText: "",
                      // ),
                      BlueButtonStandard(
                        bluebutton: () => callback(),
                        title: "Register",
                        textColor: WellNestColor.wncWhite,
                        border: colorUse,
                        back: colorUse,
                      ),
                      SizedBox( 
                        height: 10.h,
                      ),
                      InkWell(
                        onTap: () => Get.off(() => LoginVieww()),
                        child: Text(
                          "Already have an Account? Login",
                          style: TextStyle(
                              color: WellNestColor.wncBlue,
                              fontSize: 18.sp,
                              fontWeight: FontWeight.normal,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void callback() {
    if (_forms.currentState!.validate()) {
      registerController.signUp(typeUser);
    }
  }
}
