
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class CustomtextfieldVerification extends StatelessWidget {

  final TextEditingController? controller;
  final String? Function(String?)? validator;
  final void Function(String?)? onchanged;
  final void Function(String?)? onSave;
  final String? hintText;
  final FocusNode? nodes;
  final Color? color;
  final TextInputAction? action;

  const CustomtextfieldVerification({Key? key, this.controller, this.validator, this.onchanged, this.onSave, this.hintText, this.nodes, this.color, this.action}) : super(key: key);

  
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40.w,
      child: TextFormField(
       // autovalidateMode: AutovalidateMode.always,
       autofocus: false,
        cursorColor: color!,
        showCursor: true,
        textInputAction: action,
        maxLength: 1,
        textAlign: TextAlign.center,
        focusNode: nodes,
        style: WellNestTextStyle.nowRegular(color, 14.sp),
        controller: controller,
        onSaved: onSave,
        onChanged: onchanged!,
        keyboardType: TextInputType.number,
        validator: validator,
        decoration: InputDecoration(
          counterText: '',
          filled: true,
          fillColor: WellNestColor.wncInstaWhite.withOpacity(0.5),
          hintText: hintText,
          hintStyle: WellNestTextStyle.nowLight(color, 14.sp),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: color!)
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: color!)
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: color!)
          ),
        ),
      ),     
    );
  }
}