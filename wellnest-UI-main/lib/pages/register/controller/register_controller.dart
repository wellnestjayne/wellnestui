

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/Log_Reg_Up/api_services.dart';
import 'package:wellnest/api/services/cognito_service.dart';
import 'package:wellnest/pages/register/confirm_email/confirm_email.dart';

class RegisterControler extends GetxController{

  //Form Register
  TextEditingController firstname = TextEditingController();
  TextEditingController lastname = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  final showPass = false.obs;

   showThePassword(){
     showPass(!showPass.value);
   }

  void signUp(type) async{

    var data = <String,String>{
      "firstName": firstname.value.text,
      "lastName": lastname.value.text,
      "phone": "+63"+phone.value.text,
      "email": email.value.text,
      "password": password.value.text,
      "userType" : type
    };
    print(data);
    await UserCoachApiOnly.registerAccount(data);

    // await CognitoService.signUpCognito(data,type);

    // Get.off(()=>OnBoardingPagew());

    // Get.off(()=>ConfirmEmailCognito(data: data,type: type,)); 

  }


}