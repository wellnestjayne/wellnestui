
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/register/confirm_email/confirm_controller.dart';
import 'package:wellnest/pages/register/widget/custom_textfield.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class ConfirmEmailCognito extends StatelessWidget {
  
  // final String? type;
  // final data; 

  final confirmController = Get.put(ConfirmEmailController());
  final GlobalKey<FormState> keyts = GlobalKey<FormState>();

  // ConfirmEmailCognito({ this.type, this.data});

 

  @override
  Widget build(BuildContext context) {
    final colorUse = Get.parameters['type'] == "user" ? WellNestColor.wncAquaBlue : WellNestColor.wncBlue;
    // print(data['email'].toString());
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async=>false,
        child: Scaffold(
          backgroundColor: WellNestColor.wncWhite,
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Form(
                  key: keyts,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Center(
                        child: Container(
                        width: 150.w,
                        height: 150.h,
                        child: Image.asset(
                          WellnestAsset.logoWellNest,
                          fit: BoxFit.contain,
                        ),
                        ),
                      ),
                      SizedBox(height: 15.h,),
                      RichText(text: TextSpan(
                        children: [
                          TextSpan(
                            text: "We have sent you an Email Verification with code at your email ",
                            style: WellNestTextStyle.nowRegular(colorUse, 16.sp)
                          ),
                          TextSpan(
                           text: Get.parameters['email'] != null ? Get.parameters['email'] : '',
                           //text: 'a.chil666@gmail.com',
                           style: WellNestTextStyle.nowBold(colorUse, 16.sp)
                          ),
                        ]
                      )),
                      SizedBox(height: 15.h,),
                      Container(
                        width: MediaQuery.of(context).size.width.w,
                        height: 120.h,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            CustomtextfieldVerification(
                              color: colorUse,
                              controller: confirmController.n0,
                              onchanged: (value)=>confirmController
                              .nextField(value!,
                              confirmController.fn1, context),
                              onSave: (value)=>confirmController.n0!.text = value!,
                              hintText: "0",
                              nodes: confirmController.fn0,
                              action: TextInputAction.next,
                              validator: (value)=>verification(confirmController.n0!.text =value!),
                            ),

                            CustomtextfieldVerification(
                              color: colorUse,
                              controller: confirmController.n1,
                              onchanged: (value)=>confirmController
                              .nextField(value!,
                              confirmController.fn2, context),
                              onSave: (value)=>confirmController.n1!.text = value!,
                              hintText: "0",
                              nodes: confirmController.fn1,
                              action: TextInputAction.next,
                               validator: (value)=>verification(confirmController.n1!.text =value!),
                            ),

                            CustomtextfieldVerification(
                              color: colorUse,
                              controller: confirmController.n2,
                              onchanged: (value)=>confirmController
                              .nextField(value!,
                              confirmController.fn3, context),
                              onSave: (value)=>confirmController.n2!.text = value!,
                              hintText: "0",
                              nodes: confirmController.fn2,
                              action: TextInputAction.next,
                               validator: (value)=>verification(confirmController.n2!.text =value!),
                            ),

                            CustomtextfieldVerification(
                              color: colorUse,
                              controller: confirmController.n3,
                              onchanged: (value)=>confirmController
                              .nextField(value!,
                              confirmController.fn4, context),
                              onSave: (value)=>confirmController.n3!.text = value!,
                              hintText: "0",
                              nodes: confirmController.fn3,
                              action: TextInputAction.next,
                               validator: (value)=>verification(confirmController.n3!.text =value!),
                            ),

                            CustomtextfieldVerification(
                              color: colorUse,
                              controller: confirmController.n4,
                              onchanged: (value)=>confirmController
                              .nextField(value!,
                              confirmController.fn5, context),
                              onSave: (value)=>confirmController.n4!.text = value!,
                              hintText: "0",
                              nodes: confirmController.fn4,
                              action: TextInputAction.next,
                               validator: (value)=>verification(confirmController.n4!.text =value!),
                            ),

                            CustomtextfieldVerification(
                              color: colorUse,
                              controller: confirmController.n5,
                              onchanged: (value)=>confirmController
                              .lastInput(value!,
                              confirmController.fn5, context),
                              onSave: (value)=>confirmController.n5!.text = value!,
                              hintText: "0",
                              nodes: confirmController.fn5,
                              action: TextInputAction.next,
                               validator: (value)=>verification(confirmController.n5!.text =value!),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 50,),
                       BlueButtonStandard(
                        bluebutton: () => callBack(),
                        title: "Verify",
                        textColor: WellNestColor.wncWhite,
                      border: colorUse,
                      back: colorUse,
                      ),
                       SizedBox(height: 20,),
                        Obx(
                          ()=>confirmController.press.value ?
                         Text(
                            "Wait For the timer to finish to send again ${confirmController.timerText}",
                            style: WellNestTextStyle.nowMedium(colorUse, 14.sp),
                         ) :
                        Container(
                          child: GestureDetector(
                              onTap: ()=>
                               confirmController.resend(Get.parameters['email'].toString())
                              //print(data['email'].toString())
                              ,
                             child: Text("Didn't recieve any verification code? Click to Resend code",
                             style: WellNestTextStyle.nowRegular(colorUse, 14.sp),
                             ),
                          ),
                        ),
                        ),

                    ],
                  )),
              ),
          ),
        ),
      ),
    );
  }

  void callBack(){

    if(keyts.currentState!.validate()){
      confirmController.verifyandGoin(Get.parameters);
    }

  }

}