

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/Log_Reg_Up/api_services.dart';
import 'package:wellnest/api/services/cognito_service.dart';

class ConfirmEmailController extends GetxController{

  

  TextEditingController? n0 = TextEditingController();
  TextEditingController? n1 = TextEditingController();
  TextEditingController? n2 = TextEditingController();
  TextEditingController? n3 = TextEditingController();
  TextEditingController? n4 = TextEditingController();
  TextEditingController? n5 = TextEditingController();

  FocusNode fn0 = FocusNode();
  FocusNode fn1 = FocusNode();
  FocusNode fn2 = FocusNode();
  FocusNode fn3 = FocusNode();
  FocusNode fn4 = FocusNode();
  FocusNode fn5 = FocusNode();

  final interval = const  Duration(seconds: 1).obs;
  RxInt timeMax = 150.obs;
  RxInt currentSeconds = 0.obs;

  var press = false.obs;
  RxString get timerText
  => '${((timeMax.value - currentSeconds.value) ~/60).toString().padLeft(2,'0')}:${((timeMax.value - currentSeconds.value) % 60).toString().padLeft(2,'0')}'.obs;

  startTimeout(){
    var duration = interval.value;
    Timer.periodic(duration, (timer) {
      currentSeconds.value = timer.tick;
      if(timer.tick >= timeMax.value){
        timer.cancel();
        press(!press.value);
      }
     });
  } 

  void resend(String? data) async{
  press(!press.value);
    startTimeout();

    // await CognitoService.resendEmailVerification(data.toString());
    await UserCoachApiOnly.resendVerificationEmail(data!.toString());

  }

  nextField(String? values,nodes,context){
    if(values!.length == 1){
      nodes.requestFocus();
    }else{
      FocusScope.of(context).previousFocus();
    }
  }
  lastInput(String? value,nodes,context){
  if(value!.length == 1){
      fn5.unfocus();
    }else{
      FocusScope.of(context).previousFocus();
    }
  }

  void verifyandGoin(data) async{

    var mergeAll = 
    n0!.value.text
    +
    n1!.value.text
    +
    n2!.value.text
    +
    n3!.value.text
    +
    n4!.value.text
    +
    n5!.value.text;

    // await CognitoService.confirmEmail(data['email'],mergeAll,data);
    var datax = {
      "verificationCode" : mergeAll,
      "username": data['email'] != null ? data['email'] : data['username'],
      "password": data['password']
    };
    await UserCoachApiOnly.verifyEmailUser(datax);
    //await UserCoachApiOnly.resendVerificationEmail(data['email'].toString());
    print(data['email'] != null ? data['email'] : data['username']);

  }

}