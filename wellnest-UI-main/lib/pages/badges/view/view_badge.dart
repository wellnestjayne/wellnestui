import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/badges/controller/badgesController.dart';
import 'package:wellnest/pages/badges/widgets/appbar_badge.dart';
import 'package:wellnest/pages/badges/widgets/main_badges_holder.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class ViewBadgeDetail extends StatelessWidget {

  final badgeController = Get.put(BadgeController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async=>false,
        child: Scaffold(
          appBar: BadgeAppabr(
              color: badgeController.mainController.colorType,
              title: "Badges",
              function: ()=>Get.back(),
            ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 345.h,
                    width: 345.w,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(WellnestAsset.model_coach))
                    ),
                  ),
                  Text("Achieved Sep 6 2021",
                  style: WellNestTextStyle.nowBold(WellNestColor.wncGrey, 16.sp),
                  ),
                  SizedBox(height: 35.h,),
                  Text("Model Coach",
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncAquaBlue, 21.sp),
                  ),
                  SizedBox(height: 10.h,),
                  Text("Lorem ipsum dolor sit amet, consecteturadipis cing elit. Egestas mi tincidunt adipiscing vitae pretium",
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 15.sp)
                  ),
                   SizedBox(height: 35.h,),
                  Text("How to achieve",
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.sp),
                  ),
                  SizedBox(height: 10.h,),
                  Text("Lorem ipsum dolor sit amet, consecteturadipis cing elit. Egestas mi tincidunt adipiscing vitae pretium",
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 15.sp)
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Wrap(
                      spacing: -15,
                      alignment: WrapAlignment.start,
                      children: [
                        MainBadgesHolderWidget(
                          press: (){},
                          firstTitle: "Bronze",
                          description: "Badge",
                          opacity: 1,
                          borderColor: WellNestColor.wncBronze,
                          pathBadge: WellnestAsset.model_coach,
                        ),
                        MainBadgesHolderWidget(
                          press: (){},
                          firstTitle: "Silver",
                          description: "Badge",
                          opacity: 0.5,
                          borderColor: WellNestColor.wncSilver,
                          pathBadge: WellnestAsset.model_coach,
                        ),
                        MainBadgesHolderWidget(
                          press: (){},
                          firstTitle: "Gold",
                          description: "Badge",
                          opacity: 0.5,
                          borderColor: WellNestColor.wncGold,
                          pathBadge: WellnestAsset.model_coach,
                        ),
                      ],
                    ),
                  ),
                   SizedBox(height: 35.h,),
                  Text("Share you hardwork badge!",
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.sp),
                  ),
                  SizedBox(height: 15.h,),
                  BlueButtonStandard(
                    bluebutton: (){},
                    title: "Share",
                    back: WellNestColor.wncBlue,
                    textColor: WellNestColor.wncWhite,
                    sizeText: 14.sp,
                    height: 40.h,
                    width: 340.w,
                    border: WellNestColor.wncBlue,
                  ),
                  SizedBox(height: 35.h,),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}