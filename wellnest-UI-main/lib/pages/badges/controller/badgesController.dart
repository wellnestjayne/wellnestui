

import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/badges/model/challenges.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';

class BadgeController extends GetxController{


  final mainController = Get.find<MainController>();

  final listChallenges = [
    MonthlyChallengesClass(
      opacity: 1,
      first: "January",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge,
    ),
    MonthlyChallengesClass(
      opacity: 1,
      first: "February",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge02,
    ),
    MonthlyChallengesClass(
      opacity: 1,
      first: "March",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge03,
    ),
    MonthlyChallengesClass(
      opacity: 1,
      first: "April",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge04,
    ),
    MonthlyChallengesClass(
      opacity: 1,
      first: "May",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge05,
    ),
    MonthlyChallengesClass(
      opacity: 1,
      first: "June",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge06,
    ),
    MonthlyChallengesClass(
      opacity: 1,
      first: "July",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge07,
    ),
    MonthlyChallengesClass(
      opacity: 1,
      first: "August",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge08,
    ),
    MonthlyChallengesClass(
      opacity: 1,
      first: "September",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge09,
    ),
    MonthlyChallengesClass(
      opacity: 0.5,
      first: "October",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge10,
    ),
    MonthlyChallengesClass(
      opacity: 0.5,
      first: "November",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge11,
    ),
    MonthlyChallengesClass(
      opacity: 0.5,
      first: "December",
      description: "Challenge",
      borderColor: WellNestColor.wncGold,
      imagePath: WellnestAsset.challenge12,
    ),
  ];

}