

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/badges/controller/badgesController.dart';
import 'package:wellnest/pages/badges/widgets/appbar_badge.dart';
import 'package:wellnest/pages/badges/widgets/main_badges.dart';
import 'package:wellnest/pages/badges/widgets/monthly_challenges_badge.dart';

class BadgesView extends StatelessWidget {

  final badgeController = Get.put(BadgeController());  

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async=>false,
        child: Scaffold(
          appBar: BadgeAppabr(
            color: badgeController.mainController.colorType,
            title: "Badges",
            function: ()=>Get.back(),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 10,right: 10
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                children: [
                  SizedBox(height: 20.h,),
                  Text("Main Badges",
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,12.sp),
                  ),
                  SizedBox(height: 20.h,),
                  MainBadges(),
                   SizedBox(height: 20.h,),
                  Text("Monthly Challenges",
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,12.sp),
                  ),
                  SizedBox(height: 20.h,),
                  MontlyChallengesBadge(),
                   SizedBox(height: 50.h,),
                ],
              ),
              ),
          ),
        ),
      ),
    );
  }
}