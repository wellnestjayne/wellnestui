

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/badges/controller/badgesController.dart';
import 'package:wellnest/pages/badges/widgets/main_badges_holder.dart';

class MontlyChallengesBadge extends StatelessWidget {

  final controller = Get.put(BadgeController());

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Wrap(
        spacing: -20,
        alignment: WrapAlignment.spaceEvenly,
        children: controller.listChallenges.map((element) => 
        MainBadgesHolderWidget(
              opacity: element.opacity,
              press: (){},
              borderColor:element.borderColor,
              firstTitle: element.first,
              description: element.description,
              pathBadge: element.imagePath,
            ),
        ).toList(),
      ),
    );
  }
}