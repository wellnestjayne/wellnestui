
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/badges/view/view_badge.dart';
import 'package:wellnest/pages/badges/widgets/main_badges_holder.dart';

class MainBadges extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Wrap(
        //direction: Axis.horizontal,
        //spacing: 20,
        spacing: -20,
        alignment: WrapAlignment.spaceEvenly,
        children: [
           MainBadgesHolderWidget(
            press: ()=>Get.to(()=>ViewBadgeDetail()),
            borderColor: WellNestColor.wncBronze,
            firstTitle: "Model",
            description: "Coachee",
            pathBadge: WellnestAsset.model_coach,
            opacity: 1,
          ),
          MainBadgesHolderWidget(
            press: (){},
            borderColor: WellNestColor.wncBronze,
            firstTitle: "Master",
            description: "Scribe",
            pathBadge: WellnestAsset.master_scribe,
            opacity: 1,
          ),
          MainBadgesHolderWidget(
            press: (){},
            borderColor: WellNestColor.wncBronze,
            firstTitle: "Got a Coach",
            description: "Coachee",
            pathBadge: WellnestAsset.gota_coach,
            opacity: 1,
          ),
          MainBadgesHolderWidget(
            press: (){},
            borderColor: WellNestColor.wncBronze,
            firstTitle: "Ready",
            description: "",
            pathBadge: WellnestAsset.ready,
            opacity: 1,
          ),
          MainBadgesHolderWidget(
            press: (){},
            borderColor: WellNestColor.wncBronze,
            firstTitle: "Goal",
            description: "Achieved",
            pathBadge: WellnestAsset.goal_Achieved,
            opacity: 1,
          ),
        ],
      ),
    );
  }
}