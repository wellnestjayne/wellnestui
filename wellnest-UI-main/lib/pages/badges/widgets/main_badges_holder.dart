

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class MainBadgesHolderWidget extends StatelessWidget {

  final Color? borderColor;
  final Function()? press;
  final String? pathBadge;
  final String? firstTitle;
  final String? description;
  final double? opacity;

  const MainBadgesHolderWidget({Key? key, this.borderColor, this.press, this.pathBadge, this.firstTitle, this.description, this.opacity}) : super(key: key);

  

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: opacity!,
      child: GestureDetector(
        onTap: press,
        child: Container(
          child: Column(
            children: [
              Container(
                height: 120.h,
                width: 120.w,
                decoration: BoxDecoration(
                  // color: Colors.black
                ),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 90.h,
                        width: 90.w,
                        decoration: BoxDecoration(
                          color: WellNestColor.wncWhite,
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 1,
                            color: borderColor!
                          ),
                          boxShadow:[
                            BoxShadow(
                              offset: Offset(4,3),
                              spreadRadius: 3,
                              blurRadius: 3,
                              color: WellNestColor.wncLightgrey.withOpacity(0.2)
                            )
                          ] 
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 130.h,
                        width: 130.w,
                          child: Image.asset(
                            pathBadge!,
                            fit: BoxFit.contain,
                           
                          ),
                        ),
                    )
                    

                  ],
                ),
              ),
                    Text(firstTitle!,
                    style: WellNestTextStyle.nowRegular(
                      WellNestColor.wncGrey            
                      , 12.sp),
                    ),
                    SizedBox(height: 2.h,),
                    Text(description!,
                    style: WellNestTextStyle.nowRegular(
                      WellNestColor.wncGrey            
                      , 12.sp),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}