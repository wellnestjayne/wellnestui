
import 'package:flutter/material.dart';

class MonthlyChallengesClass{

  final String? imagePath;
  final double? opacity;
  final String? first;
  final String? description;
  final Color? borderColor;

  MonthlyChallengesClass({this.imagePath, this.opacity, this.first, this.description,this.borderColor});

}