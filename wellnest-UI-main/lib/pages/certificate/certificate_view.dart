

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/certificate/widget/certificate.dart';
import 'package:wellnest/pages/write/widget/appbar.dart';

class CertificateView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SafeArea(

      child: Scaffold(
        appBar: AppbarJournal(
            back: ()=>Get.back(),
            name: '',
            welcoming: '',
          ),

        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(

            children: [
              Certificate(),
              Certificate(),
              Certificate(),
              Certificate(),


            ],
          )
        )
      ) 
    );
  }
}