import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';

class Certificate extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Container(
      
      
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right:20, bottom: 20),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            textDirection: TextDirection.ltr,
          children: [
            Container(

              height: 308.h,
              width: 418.w,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                  fit: BoxFit.fitHeight,
                  
                  image: AssetImage(WellnestAsset.certificate),
                )
              ),

            ),
            Text(
              "PhD Sample Text",
              style: TextStyle(
                fontSize: 18,
                color: WellNestColor.wncBlue
              )
            ),
            Text(
              "July 2020",
              style: TextStyle(
                fontSize: 14,
                color: WellNestColor.wncLightgrey
              )
            ),

            SizedBox(
              height:10
            ),

            Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"

            ),

            SizedBox(
              height:10
            ),

          ],
        ),
      )
    );
    
  }
}