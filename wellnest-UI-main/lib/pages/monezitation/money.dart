import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/agora/agora_vid_temp/agora_join.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/monezitation/controller/money_controller.dart';
import 'package:wellnest/pages/refer_friend/refer_friend.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';
import 'package:wellnest/standard_widgets/stanardButtonmenu.dart';
import 'package:wellnest/terms_and_conditions/tac_dialog.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MoneyVieww extends GetView<MoneyController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 30, right: 30, top: 35, bottom: 25),
            child: Column(
              children: [
                StandardButtonMenu(
                  function: () => (
                    showToast()
                  ),
                  //()=>Get.to(()=>AgoraJoinPage()),
                  pathSvg: WellnestAsset.accountSvg,
                  title: "Account Details",
                ),
                SizedBox(
                  height: 15,
                ),
                Divider(
                  color: WellNestColor.wncLightgrey,
                ),
                SizedBox(
                  height: 15,
                ),
                StandardButtonMenu(
                  function: () => (
                    showToast()
                  ),
                  pathSvg: WellnestAsset.app_secureitySvg,
                  title: "App Security",
                ),
                SizedBox(
                  height: 15,
                ),
                Divider(
                  color: WellNestColor.wncLightgrey,
                ),
                SizedBox(
                  height: 15,
                ),
                StandardButtonMenu(
                  function: () => (
                    showToast()
                  ),
                  pathSvg: WellnestAsset.manageSvg,
                  title: "Manage Subscription",
                ),
                SizedBox(
                  height: 15,
                ),
                Divider(
                  color: WellNestColor.wncLightgrey,
                ),
                SizedBox(
                  height: 15,
                ),
                StandardButtonMenu(
                  function: () => (
                    showToast()
                  ),
                  pathSvg: WellnestAsset.notificationSvg,
                  title: "Notifications",
                ),
                SizedBox(
                  height: 15,
                ),
                Divider(
                  color: WellNestColor.wncLightgrey,
                ),
                SizedBox(
                  height: 15,
                ),
                StandardButtonMenu(
                  function: () => (
                    showToast()
                  ), // => Get.toNamed(AppRouteName.homeView! + AppRouteName.referFriend!, id: 3),
                  pathSvg: WellnestAsset.referSvg,
                  title: "Refer a friend",
                ),
                SizedBox(
                  height: 15,
                ),
                Divider(
                  color: WellNestColor.wncLightgrey,
                ),
                SizedBox(
                  height: 15,
                ),
                StandardButtonMenu(
                  function: () => (
                    showToast()
                  ),
                  pathSvg: WellnestAsset.referSvg,
                  title: "Refer a coach",
                ),
                SizedBox(
                  height: 15,
                ),
                Divider(
                  color: WellNestColor.wncLightgrey,
                ),
                SizedBox(
                  height: 15,
                ),
                StandardButtonMenu(
                  function: () => (
                    showToast()
                  ),
                  pathSvg: WellnestAsset.serviceSvg,
                  title: "Customer Support",
                ),
                SizedBox(
                  height: 15,
                ),
                Divider(
                  color: WellNestColor.wncLightgrey,
                ),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
              onTap: ()=>TacDialog.tacDialogShow(),
      child: Container(
        child: Row(
          children: [
            Container(
              height: 20,
              width: 20,
              child: FaIcon(FontAwesomeIcons.listAlt,color: WellNestColor.wncBlue,),
            ),
            SizedBox(width: 25,),
            Text("Terms & Conditions",
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncGrey
              , 15.0),
            )
          ],
        ),
      ),
    ),
    SizedBox(
                  height: 15,
                ),
    Divider(
                  color: WellNestColor.wncLightgrey,
                ),
                SizedBox(
                  height: 15,
                ),
                StandardButtonMenu(
                  function: () => LoadingOrError.logoutDialog(
                    () => controller.mainController.logoutinAnyPages(),
                  ),
                  pathSvg: WellnestAsset.logoutSvg,
                  title: "Logout",
                ),
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                            image: DecorationImage(fit: BoxFit.fill, image: AssetImage(WellnestAsset.specialLogo))),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "WellNest",
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        "Version 1.9.6",
                        style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey.withOpacity(0.7), 10.0),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 35,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  
  // methods ------------------------------------------------------
  void showToast() => Fluttertoast.showToast(
    msg: "Coming Soon...",
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 1,
    backgroundColor: Colors.grey[850],
    textColor: Colors.white,
    fontSize: 16.0
  );

}
