import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/service/package_view.dart';
import 'package:wellnest/pages/service/widget/service_on_view.dart';
import 'package:wellnest/pages/write/widget/appbar.dart';

class ServiceScreen extends StatelessWidget {
  const ServiceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppbarJournal(
            back: ()=>Get.back(),
            name: '',
            welcoming: '',
          ),
        body: SingleChildScrollView(
          child: Container(
              child: Column(
            children: [
              ServiceOnView(
                name: "Fitness",
                coach: "Juan Rodriguez"

              ),


              Padding(
                padding: const EdgeInsets.only(
                    left: 20.0, right: 20.0, bottom: 20.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,",
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(
                  left: 20.0,
                ),
                child: Text("PREVIEW",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  height: 200,
                  decoration: BoxDecoration(
                      color: WellNestColor.wncAquaBlue,
                      borderRadius: BorderRadius.circular(20)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.topLeft,
                      padding: const EdgeInsets.only(
                        left: 20.0,
                      ),
                      child: Text("INCLUSIONS",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                    image: AssetImage(WellnestAsset.timeIcon),
                                  ))),
                              Text("1 Hour Session"),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                    image: AssetImage(
                                        WellnestAsset.assessmentIcon),
                                  ))),
                              Text("Take Home Assessment"),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                    image: AssetImage(
                                        WellnestAsset.certificateIcon),
                                  ))),
                              Text("Certification"),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(left: 20.0, bottom: 20),
                child: Text("REVIEWS",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Container(
                padding: const EdgeInsets.only(top: 20, bottom: 20),
                child: CarouselSlider(
                    options: CarouselOptions(
                        height: 160,
                        enableInfiniteScroll: true,
                        enlargeCenterPage: true),
                    items: [
                      Container(
                        height: 200,
                        decoration: BoxDecoration(
                            color: WellNestColor.wncAquaBlue,
                            borderRadius: BorderRadius.circular(20)),
                      ),
                      Container(
                        height: 200,
                        decoration: BoxDecoration(
                            color: WellNestColor.wncAquaBlue,
                            borderRadius: BorderRadius.circular(20)),
                      ),
                    ]),
              ),
              
              
              
              
              Padding(
                padding: const EdgeInsets.only(
                    left: 20.0, right: 20.0, bottom: 20.0),
                child: Container(
                  width: double.infinity,
                  child: ElevatedButton(
                      onPressed: () => Get.to(()=> PackageView()),
                      child: Text("Book Now",
                          style: TextStyle(
                            color: Color(0xFFFFFFFF),
                            fontSize: 16,
                          )),
                      style: ElevatedButton.styleFrom(
                          primary: Color(0xFF00AEEF),
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)))),
                ),
              ),
            ],
          )),
        ),
      ),
    );
  }
}
