import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/pages/certificate/widget/certificate.dart';
import 'package:wellnest/pages/service/widget/service.dart';
import 'package:wellnest/pages/write/widget/appbar.dart';

class ServiceView extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppbarJournal(
            back: ()=>Get.back(),
            name: '',
            welcoming: '',
          ),
        body: SingleChildScrollView(
          child: Container(
            
            
            
            child: Column(
              children: [
                Service(
                  name: "Path to the Mountains",
                  coach: "Daniel Aguilar",
                  image: WellnestAsset.mountain

                ),

                Service(
                  name: "Fitness and Healthy Diet",
                  coach: "Daniel Aguilar",
                  image: WellnestAsset.service

                ),

                Service(
                  name: "Fitness and Healthy Diet",
                  coach: "Daniel Aguilar",
                  image: WellnestAsset.mountain

                ),
              ],
            )
            
            
          ),
        ),
      ),
    );
  }
}