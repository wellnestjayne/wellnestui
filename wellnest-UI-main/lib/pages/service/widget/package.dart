import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';

class Package extends StatelessWidget {

  final String? packageType;

  const Package({Key? key, this.packageType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom:20, left: 20, right: 20),
      child: 
      Stack(
        children: [
          Container(
            height: 95,
            width: double.infinity,
            decoration: BoxDecoration(
              color: WellNestColor.wncBlue,
              borderRadius: BorderRadius.circular(20)
            ),
          ),

          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          packageType!,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: WellNestColor.wncInstaWhite,
                            fontSize: 22,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(
                          "P500 Per Week",
                          style: TextStyle(
                            color: WellNestColor.wncInstaWhite,
                            fontSize: 13,
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          "Save P2000",
                          style: TextStyle(
                            color: WellNestColor.wncInstaWhite,
                            fontSize: 13,
                          ),
                        ),
                      ),
                      
                    ],
                    
                  ),
                ),
              ),

              

              

             
              Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    width: 60,
                    height: 60,
                    margin: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: WellNestColor.wncRedError,
                      borderRadius: BorderRadius.circular(40)
                    )
                  ),

                  Text(
                    "- 41%",
                    style: TextStyle(
                      color: WellNestColor.wncWhite,
                      fontWeight: FontWeight.bold,
                    )
                  )
                ]
              )
            ],
          ),

          
        ]
      ),
     
    );
  }
}