import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';

class Service extends StatelessWidget {

  final String? name;
  final String? coach;
  final String? image;

  const Service({Key? key, this.name, this.coach, this.image}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right:20, bottom: 20),
      child: Container(
        height: 308.h,


        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          image: DecorationImage(
            image: AssetImage(image!),
            fit: BoxFit.cover,
          )

          
        ),

        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomLeft,
              child: Opacity(
                opacity: 0.6,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
              
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [Colors.black, Colors.white] ,
                    ),
                  ),
                  
                ),
              )
            ),

            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left:20.0, right:20.0, bottom: 20.0),
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        height: 45.h,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom:5.0),
                              child: Text(
                                name!,
                                style: TextStyle(
                                  color: WellNestColor.wncWhite,
                                  fontSize: 18,
                                ),
                
                              ),
                            ),
                            Text(
                              coach!,
                              style: TextStyle(
                                color: WellNestColor.wncWhite,
                                fontSize: 12,
                
                              )
                              
                            ),
                          ],
                        ),
                      )
                      
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(left:20.0, right: 20.0, top: 190.0),
                  child: Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncAquaBlue,
                      borderRadius: BorderRadius.circular(50),
                      image: DecorationImage(
                        image: AssetImage(WellnestAsset.smilingMan),
                      )
                    ),
                  ),
                )
              ],
            )
          ]
        )
        

        
      ),
    );
  }
}