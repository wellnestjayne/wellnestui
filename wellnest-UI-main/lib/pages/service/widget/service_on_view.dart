import 'package:flutter/material.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';

class ServiceOnView extends StatelessWidget {

  
  final String? name;
  final String? coach;

  const ServiceOnView({Key? key, this.name, this.coach}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom:20),
      child: Stack(
        children: [

          Container( 
            height: 220,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(WellnestAsset.mountain),
                fit: BoxFit.fitWidth  
              ),
            )
          ),


          Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 150.0, left:20.0, right:20.0, bottom: 20.0),
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Container(
                          height: 45,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom:5.0),
                                child: Text(
                                  name!,
                                  style: TextStyle(
                                    color: WellNestColor.wncWhite,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                  
                                ),
                              ),
                              Text(
                                coach!,
                                style: TextStyle(
                                  color: WellNestColor.wncWhite,
                                  fontSize: 12,
                  
                                )
                                
                              ),
                            ],
                          ),
                        )
                        
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:20.0, right: 20.0, top: 125.0),
                    child: Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        color: WellNestColor.wncAquaBlue,
                        borderRadius: BorderRadius.circular(50),
                        image: DecorationImage(
                          image: AssetImage(WellnestAsset.smilingMan),
                        )
                      ),
                    ),
                  )
                ],
              )



        ]
      ),
    );
  }
}