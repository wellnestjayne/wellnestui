


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/write/widget/appbar.dart';

import 'widget/package.dart';

class PackageView extends StatelessWidget {
  const PackageView({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: WellNestColor.wncWhite,
        appBar: AppbarJournal(
            back: ()=>Get.back(),
            name: '',
            welcoming: '',
        ),

    
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [

                Stack(
                children: [
                  Container( 
                    height: 202,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(WellnestAsset.mountain),
                        fit: BoxFit.fitWidth
                      )
                    ),
                  ),
                  
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 202,
                      decoration: BoxDecoration(
                                            
                        gradient: LinearGradient(
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          colors: [Colors.white, Colors.white.withOpacity(0.2)],
                        ),
                      ),
                      
                    )
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top:50.0),
                    child: Container(
                      height: 100,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(WellnestAsset.specialLogo), 
                        )
                      ),
                    ),
                  ),
                  
                ]
                  
                ),




                Container(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    "Unlock Ideas that will Change Your Life",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),


                Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  width: 27,
                  height: 27,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(WellnestAsset.check),
                    )
                  ),

                ),

                Container(
                  width: 257,
                  child: Text(
                    "A user can cancel their appointment for free 1 day before the set date.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,  
                    ),
                  ),
                ),

                SizedBox(height:20),

                Package(
                  packageType: "Individual",
                ),
                Package(
                  packageType: "For 10 People",
                ),
                Package(
                  packageType: "For 20 People",
                ),
                
              ]
            ),
          ),
        ),
      )
    );
  }
}