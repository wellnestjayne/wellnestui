import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/coach_view.dart';
import 'package:wellnest/pages/home/widget/appbar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/pages/home/widget/categories.dart';
import 'package:wellnest/pages/home/widget/feature_card.dart';
import 'package:wellnest/pages/home/widget/featured_coach.dart';
import 'package:wellnest/pages/home/widget/list_person.dart';
import 'package:wellnest/pages/home/widget/modes.dart';
import 'package:wellnest/pages/notification/notification_view.dart';
import 'package:wellnest/pages/profile/widgets/header.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/header.dart';
import 'controller/home_controller.dart';
import 'widget/badges.dart';

class HomeVieww extends GetView<HomeController> {
  final int? index;

  HomeVieww({this.index});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: WellNestColor.wncLighBg,
        appBar: Appbarhomeuser(
          path: null,
          firstname: controller.letter[0],
          colors: SharePref.getType() == "User" ? WellNestColor.wncBlue : WellNestColor.wncAquaBlue,
          notif: () => Get.to(() => NotificationVieww(), id: index),
        ),
        body: SingleChildScrollView(
            child: SafeArea(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 20,
                  ),

                  // Text("Tutorial Completion (7/10)",
                  // style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 14.sp),
                  // ),
                  // SizedBox(height: 10,),
                  // LinearPercentIndicator(
                  //   width: MediaQuery.of(context).size.width - 50.w,
                  //   animation: true,
                  //   lineHeight: 30.0.w,
                  //   animationDuration: 2000,
                  //   percent: 0.7,
                  //   center: Text(""),
                  //   linearStrokeCap: LinearStrokeCap.roundAll,
                  //   progressColor: WellNestColor.wncAquaBlue,
                  // ),
                  // SizedBox(height: 10,),
                  // ListPersonsw(),
                  // SizedBox(height: 15,),
                  // Badgesw(
                  //   function: ()=>Get.toNamed(
                  //     AppRouteName.homeView!
                  //     +AppRouteName.badges!),
                  // ),
                  // FeatureCoachw(
                  //   press: ()=>Get.toNamed(
                  //     AppRouteName.homeView!+
                  //     AppRouteName.featureCoach!
                  //   ),
                  //   goTo:()=>Get.toNamed(
                  //     AppRouteName.homeView!+
                  //     AppRouteName.coachFeaturedProfile!
                  //   ),
                  //   ),
                  Standardheader(
                    press: () => Get.toNamed(AppRouteName.homeView! + AppRouteName.featureCoach!, id: 0),
                    show: true,
                    colorText: WellNestColor.wncBlue,
                    title: "Featured Coaches",
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  FeatureCoachw(),
                  // SizedBox(
                  //   height: 15,
                  // ),
                  // MoodsModew(),
                  SizedBox(
                    height: 15,
                  ),
                  Categoriesw(
                    press: () => Get.toNamed(AppRouteName.homeView! + AppRouteName.categoryAllService!),
                  ),
                  SizedBox(height: 20,),
                ],
              ),
            ),
          ),
        )));
  }
}
