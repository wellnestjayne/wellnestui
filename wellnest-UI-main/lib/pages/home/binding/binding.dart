

import 'package:get/get.dart';
import 'package:wellnest/pages/profile/controller/profileController.dart';

class BindingHome extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<ProfileController>(() => ProfileController());
  }



}