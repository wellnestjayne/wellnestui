import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/api/api_service/api_coaches/api_get_all_coach.dart';
import 'package:wellnest/api/services/update.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/feature_class_view.dart';
import 'package:wellnest/pages/categories/coach_by_category/controller/coachCategoryController.dart';
import 'package:wellnest/pages/home/model/menuItems_user.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/profile/models/category_class.dart';
import 'package:wellnest/pages/profile/profile_vieww.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class HomeController extends GetxController {
  final letter = SharePref.detailData['firstName'];
  final mainController = Get.find<MainController>();

  @override
  void onInit() {
    getCoachList();
    getCategories();
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  categoryIcon(String? string) {
    switch (string) {
      case "Mental Health":
        return WellnestAsset.mental_health;
      case "Performance":
        return WellnestAsset.perfomace;
      case "Relationship":
        return WellnestAsset.relationship;
      case "Lifestyle":
        return WellnestAsset.lifeStyle1;
      case "Spiritual":
        return WellnestAsset.spiritual1;
      case "Financial":
        return WellnestAsset.fianancial;
    }
  }

  void selectedItem(items) async {
    switch (items) {
      case MenuItemsUser.switchToCoach:
          LoadingOrError.dialogWithNorm(
           title: "Switch to Coach",
           message: "Do you want to switch account.",
           yes: ()=>trialFuntion(),
           no: ()=>Get.back()
         );
        // UpdateCognitoUser.updateCognito("Coach");
        break;
      case MenuItemsUser.coachProfile:
        trialFuntion();
        break;
    }
  }

  final activeShow = false.obs;

  itemtype() {
    final gotType = SharePref.getType();
    if (gotType == "Coach") {
      // Get.toNamed(AppRouteName.homeView!+
      // AppRouteName.profileUser!
      // );
      Get.to(() => ProfileVieww(), id: mainController.select.value);
    }
  }

  trialFuntion() async {
    // final idUserUuid = SharePref.detailData['id'];
    final idCoach = SharePref.getCoachId() != null ? SharePref.getCoachId() : "";

    await ApiCoach.getCoachonId(idCoach);
  }

  final List<Coachee> coachList = <Coachee>[].obs;
  final List<Coachee> coachFeatureDList = <Coachee>[].obs;
  getCoachList() async {
    try {
      final result = await CoachGetLists.coachgetAll();
      if (result != null) {

        coachList.assignAll(result);
      }
    } finally {
      final listCoach  = coachList.where((el) => el.isFeatured == true ).toList();
      coachFeatureDList.assignAll(listCoach);
    }
  }

  final List<Datum> coachCategories = <Datum>[].obs;
  getCategories() async {
    try {
      final result = await ApiCoach.getAllCoachCategory();
      if (result != null) {
        coachCategories.assignAll(result);
      }
    } finally {}
  }
}