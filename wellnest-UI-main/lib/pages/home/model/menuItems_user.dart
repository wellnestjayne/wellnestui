
import 'package:wellnest/pages/home/model/menuItem_user.dart';

class MenuItemsUser{

  static const switchToCoach = 
  MenuItemUser(
    menuTitle: "Switch to Coach"
  );

  static const settingTab = 
  MenuItemUser(
    menuTitle: "Settings"
  );

  static const coachProfile = 
  MenuItemUser(
    menuTitle: "Create Coach Profile"
  );

  static const List<MenuItemUser> listUserItem =
  [
    switchToCoach
    // settingTab
    
  ];

  static const List<MenuItemUser> listCoachItem =
  [
    coachProfile
  ];

}