
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
class ListPersonsw extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.h,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          Container(
            height: 65.h,
            width: 65.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 55.h,
                    width: 55.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                      image: DecorationImage(
                        image: AssetImage(WellnestAsset.testimonial),
                        )
                    ),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text("Mental Health",
                  overflow: TextOverflow.ellipsis,
                  ),
              ],
            ),
          ),

  Container(
            height: 70.h,
            width: 70.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 55.h,
                    width: 55.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                      image: DecorationImage(
                        image: AssetImage(WellnestAsset.testimonial),
                        )
                    ),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text("Executive",
                  overflow: TextOverflow.ellipsis,
                  ),
              ],
            ),
          ),

           Container(
            height: 70.h,
            width: 70.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 55.h,
                    width: 55.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                      image: DecorationImage(
                        image: AssetImage(WellnestAsset.testimonial),
                        )
                    ),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text("Spiritual",
                  overflow: TextOverflow.ellipsis,
                  ),
              ],
            ),
          ),
           Container(
            height: 70.h,
            width: 70.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 55.h,
                    width: 55.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                      image: DecorationImage(
                        image: AssetImage(WellnestAsset.testimonial),
                        )
                    ),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text("Relationship",
                  overflow: TextOverflow.ellipsis,
                  ),
              ],
            ),
          ),

           Container(
            height: 70.h,
            width: 70.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 55.h,
                    width: 55.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                      image: DecorationImage(
                        image: AssetImage(WellnestAsset.testimonial),
                        )
                    ),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text("Executive",
                  overflow: TextOverflow.ellipsis,
                  ),
              ],
            ),
          ),

          
        ],
      ),
    );
  }
}