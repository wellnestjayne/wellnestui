

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/home/controller/home_controller.dart';
import 'package:wellnest/pages/home/widget/pop_menu.dart';
class Appbarhomeuser extends StatelessWidget implements PreferredSizeWidget{

  final double? prefer = 90;

  final String? path;
  final String? firstname;
  final Color? colors;
  final Function()? press;
  final Function()? notif;
  
  

  Appbarhomeuser({Key? key, this.path, this.firstname, this.colors, this.press, this.notif}) : super(key: key);@override
  

    final homeController = Get.put(HomeController());
  
  Widget build(BuildContext context) {
    return Container(
      height: prefer!,
      width: MediaQuery.of(context).size.width.w,
      decoration: BoxDecoration(
        color: colors!,
        borderRadius: BorderRadius.only(
          bottomLeft: const Radius.circular(10),
          bottomRight: const Radius.circular(10)
        ),
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 20,
            right: 20
          ),
          child: Row(
            children: [
               SharePref.getCoachId() != "null" && SharePref.getType() == "Coach" ? 
               GestureDetector(
                 onTap: ()=>homeController.itemtype(),
                 child: Container(
                     width: 90,
                    child: Stack(
                      children: [
                      path != null 
                      ?  Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncWhite
                          ),
                          child: Image.network(path!,
                          loadingBuilder: (context,child,loading)
                          =>Center(
                            child: CircularProgressIndicator(
                              backgroundColor: WellNestColor.wncBlue,
                              value: loading!.expectedTotalBytes != null ? 
                                    loading.cumulativeBytesLoaded/loading.expectedTotalBytes! :null,
                            ),
                          ),
                          ),
                        ) :
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: WellNestColor.wncOrange,
                              border: Border.all(color: WellNestColor.wncWhite)
                            ),
                            child: Center(
                              child: Text(
                                firstname!,
                                style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 28.0),
                              ),
                            ),
                          ),
                        ),
                         Positioned(
                            left: 30,
                          bottom: 10,
                          child: Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: WellNestColor.wncPeach,
                              border: Border.all(color: WellNestColor.wncWhite)
                            ),
                            child: Center(
                              child: Text(
                                firstname!,
                                style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 12.0),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          left: 25,
                          bottom: 10,
                          child: Container(
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: WellNestColor.wncAquaBlue
                            ),
                            child: Center(
                              child: FaIcon(
                                FontAwesomeIcons.exchangeAlt,
                                size: 5,
                                color: WellNestColor.wncWhite,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
               )
               
               : PopMenuUser(
                  item: (items)=>homeController.selectedItem(items),
                  firstname: SharePref.detailData['firstName'][0],
                  path: null,
                ),
              Expanded(
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      // FaIcon(FontAwesomeIcons.solidBell,
                      // color: WellNestColor.wncWhite,
                      // ),
                      GestureDetector(
                        onTap: notif,
                        child: 
                        Container(
                          height: 40.h,
                          width: 40.w,
                          child: Image.asset(WellnestAsset.bell))
                        ),
                      SizedBox(width: 10,),
                      // FaIcon(FontAwesomeIcons.solidCommentDots,
                      // color: WellNestColor.wncWhite,
                      // ),
                    //  Container(
                    //    height: 40.h,
                    //       width: 40.w,
                    //    child: Image.asset(WellnestAsset.message))
                    ],
                  ),
                ) 
                ),
            ],
          ),
          ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(prefer!);
}