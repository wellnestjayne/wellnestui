


import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wellnest/constants/colors/colors.dart';

class ListShimmerCoach extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 349,
      height: 161,
      child: Shimmer.fromColors(
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 3,
        itemBuilder: (_,__)
        =>Padding(
          padding: const EdgeInsets.only(
            top: 5,
            bottom: 10,
            right: 10
          ),
          child: Container(
            height: 39,
            width: 96,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: WellNestColor.wncGrey.withOpacity(0.5)
            )
          ),
          ),
        ),
     baseColor: WellNestColor.wncLighBg,
        highlightColor: WellNestColor.wncGold,), //wncLightgrey
    );
  }
}