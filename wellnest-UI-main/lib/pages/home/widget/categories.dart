import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/categories/coach_by_category/controller/coachCategoryController.dart';
import 'package:wellnest/pages/home/controller/home_controller.dart';
import 'package:wellnest/pages/home/widget/categories_card.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/header.dart';

class Categoriesw extends StatelessWidget {
  final Function()? press;
  final homeController = Get.put(HomeController());
  final coachCatController = Get.put(CoachCategoryController());

  Categoriesw({Key? key, this.press}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          // Container(
          //   height: 20,
          //   width: MediaQuery.of(context).size.width.w,
          //   child: Stack(
          //     children: [
          //       Align(
          //         alignment: Alignment.centerLeft,
          //         child: Text("Categories"),
          //       ),
          //       Align(
          //         alignment: Alignment.centerRight,
          //         child: GestureDetector(
          //           onTap: press,
          //           child: Text(
          //             "Show More",
          //             style: WellNestTextStyle.nowWithShad(WellNestColor.wncBlue, 10.sp, WellNestTextStyle.nowMed),
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
           Standardheader(
                   // press: () => Get.toNamed(AppRouteName.homeView! + AppRouteName.featureCoach!, id: 0),
                   press: (){},
                    show: true,
                    colorText: WellNestColor.wncBlue,
                    title: "Categories",
                  ),
                  SizedBox(
                    height: 15,
          ),
          Container(
            height: 324,
            child: Obx(
              () => homeController.coachCategories.isEmpty
                  ? Container()
                  : GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: 4 / 5.5,
                      ),
                      itemCount: homeController.coachCategories.length,
                      itemBuilder: (context, index) {
                        return AnimationConfiguration.staggeredGrid(
                          position: index,
                          duration: const Duration(milliseconds: 375),
                          columnCount: 2,
                          child: ScaleAnimation(
                            child: FadeInAnimation(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 4, right: 4, bottom: 25),
                                child: CategoriesCard(
                                    name: homeController.coachCategories[index].name,
                                    image: homeController.categoryIcon(homeController.coachCategories[index].name),
                                    onPressed: () {
                                      coachCatController.getCatID(homeController.coachCategories[index].id);
                                      Get.toNamed(
                                        AppRouteName.homeView! + AppRouteName.coachServices!,
                                        id: 0,
                                        arguments: [
                                          homeController.coachCategories[index].name,
                                          homeController.coachCategories[index].id
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ),
                        );
                      }),
            ),
          ),
        ],
      ),
    );
  }
}
