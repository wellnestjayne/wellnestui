
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/home/model/menuItem_user.dart';
import 'package:wellnest/pages/home/model/menuItems_user.dart';
import 'package:wellnest/pages/profile/widgets/switchero.dart';

class PopMenuUser extends StatelessWidget {
  
  final Function(MenuItemUser?)? item;
  final String? path;
  final String? firstname;

  const PopMenuUser({Key? key, this.item, this.path, this.firstname}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: 
      SharePref.getCoachId() == "null" ?
       PopupMenuButton<MenuItemUser>(
                   onSelected: item,
                   child:  Container(
                   width: 90,
                  child: Stack(
                    children: [
                    path != null 
                    ?  Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: WellNestColor.wncWhite
                        ),
                        child: Image.network(path!,
                        loadingBuilder: (context,child,loading)
                        =>Center(
                          child: CircularProgressIndicator(
                            backgroundColor: WellNestColor.wncBlue,
                            value: loading!.expectedTotalBytes != null ? 
                                  loading.cumulativeBytesLoaded/loading.expectedTotalBytes! :null,
                          ),
                        ),
                        ),
                      ) :
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncOrange,
                            border: Border.all(color: WellNestColor.wncWhite)
                          ),
                          child: Center(
                            child: Text(
                              firstname!,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 28.0),
                            ),
                          ),
                        ),
                      ),
                       Positioned(
                          left: 30,
                        bottom: 10,
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncPeach,
                            border: Border.all(color: WellNestColor.wncWhite)
                          ),
                          child: Center(
                            child: Text(
                              firstname!,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 12.0),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 25,
                        bottom: 10,
                        child: Container(
                          height: 10,
                          width: 10,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncAquaBlue
                          ),
                          child: Center(
                            child: FaIcon(
                              FontAwesomeIcons.exchangeAlt,
                              size: 5,
                              color: WellNestColor.wncWhite,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                  offset: Offset(0.0,50.5),
                  elevation: 2,
                  
                  shape: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10),        
                  ),
          itemBuilder: (_)
          =>[
            ...MenuItemsUser.listCoachItem.map((e) =>  
            PopupMenuItem<MenuItemUser>(
                      value: e,
                      child: Visibility(
                        visible: SharePref.getCoachId() != "null" ? false : true,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(e.menuTitle!,
                              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 16.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                      
                      ),).toList(),  
          ],        
      )
      : 
      PopupMenuButton<MenuItemUser>(
                   onSelected: item,
                   child:  Container(
                   width: 90,
                  child: Stack(
                    children: [
                    path != null 
                    ?  Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: WellNestColor.wncWhite
                        ),
                        child: Image.network(path!,
                        loadingBuilder: (context,child,loading)
                        =>Center(
                          child: CircularProgressIndicator(
                            backgroundColor: WellNestColor.wncBlue,
                            value: loading!.expectedTotalBytes != null ? 
                                  loading.cumulativeBytesLoaded/loading.expectedTotalBytes! :null,
                          ),
                        ),
                        ),
                      ) :
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncOrange,
                            border: Border.all(color: WellNestColor.wncWhite)
                          ),
                          child: Center(
                            child: Text(
                              firstname!,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 28.0),
                            ),
                          ),
                        ),
                      ),
                       Positioned(
                          left: 30,
                        bottom: 10,
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncPeach,
                            border: Border.all(color: WellNestColor.wncWhite)
                          ),
                          child: Center(
                            child: Text(
                              firstname!,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 12.0),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 25,
                        bottom: 10,
                        child: Container(
                          height: 10,
                          width: 10,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncAquaBlue
                          ),
                          child: Center(
                            child: FaIcon(
                              FontAwesomeIcons.exchangeAlt,
                              size: 5,
                              color: WellNestColor.wncWhite,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                  offset: Offset(0.0,50.5),
                  elevation: 2,
                  
                  shape: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10),        
                  ),
          itemBuilder: (_)
          =>[
            ...MenuItemsUser.listUserItem.map((e) => 
           PopupMenuItem<MenuItemUser>(
                      value: e,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(e.menuTitle!,
                            style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 16.0),
                            ),
                          ],
                        ),
                      ),
                      
                      ),
            ).toList(),
          ],        
      ),
    );
  }
}