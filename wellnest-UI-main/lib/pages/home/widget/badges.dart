


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
class Badgesw extends StatelessWidget{
  
  final Function()? function;

  const Badgesw({Key? key, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 135.h,
      child: Column(
        children: [
          Container(
            height: 20,
            width: MediaQuery.of(context).size.width.w,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Badges"),
                ),
                GestureDetector(
                  onTap: function!,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text("Show More"),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 5.h,),
          Container(
            height: 80.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                  Container(
            height: 75.h,
            width: 75.w,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(2.0,2.0),
                )
              ]
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 75.h,
                    width: 75.w,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncWhite,
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                   
                    ),
                    child: Center(
                      child: Container(
                        height: 65.h,
                        width: 65.w,
                        decoration: BoxDecoration(
                          image: DecorationImage(image: 
                          AssetImage(WellnestAsset.model_coach)
                          ,fit: BoxFit.contain 
                          ),
                        ),
                    ),
                  ),
                  ),
              ],
            ),
          ),

            Container(
            height: 75.h,
            width: 75.w,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(2.0,2.0),
                )
              ]
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 75.h,
                    width: 75.w,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncWhite,
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                   
                    ),
                    child: Center(
                      child: Container(
                        height: 65.h,
                        width: 65.w,
                        decoration: BoxDecoration(
                          image: DecorationImage(image: 
                          AssetImage(WellnestAsset.master_scribe)
                          ,fit: BoxFit.contain 
                          ),
                        ),
                    ),
                  ),
                  ),
              ],
            ),
          ),
            Container(
            height: 75.h,
            width: 75.w,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(2.0,2.0),
                )
              ]
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 75.h,
                    width: 75.w,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncWhite,
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                   
                    ),
                    child: Center(
                      child: Container(
                        height: 65.h,
                        width: 65.w,
                        decoration: BoxDecoration(
                          image: DecorationImage(image: 
                          AssetImage(WellnestAsset.gota_coach)
                          ,fit: BoxFit.contain 
                          ),
                        ),
                    ),
                  ),
                  ),
              ],
            ),
          ),

              Container(
            height: 75.h,
            width: 75.w,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(2.0,2.0),
                )
              ]
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                  Container(
                    height: 75.h,
                    width: 75.w,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncWhite,
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      ),
                   
                    ),
                    child: Center(
                      child: Container(
                        height: 65.h,
                        width: 65.w,
                        decoration: BoxDecoration(
                          image: DecorationImage(image: 
                          AssetImage(WellnestAsset.ready)
                          ,fit: BoxFit.contain 
                          ),
                        ),
                    ),
                  ),
                  ),
              ],
            ),
          ),
          
              ],
            ),
          )
        ],
      ),
    );  
  }



}