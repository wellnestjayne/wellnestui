

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
class MoodsModew extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Container(
    height: 135.h,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
          Container(
            height: 20,
            width: MediaQuery.of(context).size.width.w,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Current Mood"),
                ),
              ],
            ),
          ),
          SizedBox(height: 5.h,),
          Container(
             height: 80.h,
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: [
                  Container(
            height: 75.h,
            width: 75.w,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(2.0,2.0),
                )
              ]
            ),
            child: Container(
              height: 75.h,
              width: 75.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncWhite,
                shape: BoxShape.circle,
                border: Border.all(
                  color: WellNestColor.wncBlue
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(1.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 50.h,
                      width: 50.w,
                      child: Image.asset(WellnestAsset.happy_mode),
                    ),
                    Text("Happy",
                    style: TextStyle(
                      fontSize: 8.sp
                    ),
                    ),
                      Text("10 : 00 am",
                    style: TextStyle(
                      color: WellNestColor.wncLightgrey.withOpacity(0.6),
                      fontSize: 4.sp
                    ),
                    )
                  ],
                ),
              ),
            ),
          ),

            Container(
            height: 75.h,
            width: 75.w,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(2.0,2.0),
                )
              ]
            ),
            child: Container(
              height: 75.h,
              width: 75.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncWhite,
                shape: BoxShape.circle,
                border: Border.all(
                  color: WellNestColor.wncinsiderblue
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(1.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 50.h,
                      width: 50.w,
                      child: Image.asset(WellnestAsset.upset),
                    ),
                    Text("Happy",
                    style: TextStyle(
                      fontSize: 8.sp
                    ),
                    ),
                      Text("10 : 00 am",
                    style: TextStyle(
                      color: WellNestColor.wncLightgrey.withOpacity(0.6),
                      fontSize: 4.sp
                    ),
                    )
                  ],
                ),
              ),
            ),
          ),
            Container(
            height: 75.h,
            width: 75.w,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(2.0,2.0),
                )
              ]
            ),
            child: Container(
              height: 75.h,
              width: 75.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncWhite,
                shape: BoxShape.circle,
                border: Border.all(
                  color: WellNestColor.wncRedError
                ),
              ),
              child: Padding(
              padding: const EdgeInsets.all(1.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 50.h,
                      width: 50.w,
                      child: Image.asset(WellnestAsset.angry),
                    ),
                    Text("Happy",
                    style: TextStyle(
                      fontSize: 8.sp
                    ),
                    ),
                      Text("10 : 00 am",
                    style: TextStyle(
                      color: WellNestColor.wncLightgrey.withOpacity(0.6),
                      fontSize: 4.sp
                    ),
                    )
                  ],
                ),
              ),
            ),
          ),
            Container(
            height: 75.h,
            width: 75.w,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: WellNestColor.wncLightgrey.withOpacity(0.2),
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(2.0,2.0),
                )
              ]
            ),
            child: Container(
              height: 75.h,
              width: 75.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncWhite,
                shape: BoxShape.circle,
                border: Border.all(
                  color: WellNestColor.wncBlue
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: Text("Add another mood",
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.fade,
                      style: TextStyle(
                        fontSize: 11.sp
                      ),
                      ),
                    ),
                     Container(
                      height: 20.h,
                      width: 20.w,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: WellNestColor.wncBlue
                      ),
                      child: Center(
                        child: FaIcon(
                          FontAwesomeIcons.plus,
                          color: WellNestColor.wncWhite,
                          size: 12.h,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

             ],
           ),  
          ),


      ],
    ),
    );
  }
}