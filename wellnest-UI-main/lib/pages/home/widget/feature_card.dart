


import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:flutter/foundation.dart'; // , uses debugPrint


class FeaturedCard extends StatelessWidget {
  
  final String? name;
  final String? description;
  final String? imagePath;
  final Function()? press;

  const FeaturedCard({Key? key, this.name, this.description, this.imagePath, this.press}) : super(key: key);

  
  @override
  Widget build(BuildContext context) {
    DebugPrintCallback debugPrint = debugPrintThrottled;
    debugPrint('hello');
    return GestureDetector(
      onTap: press,
      child: Container(
        width: 102,
        height: 132,
        decoration: BoxDecoration(
          color: WellNestColor.wncBlue,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              offset: Offset(2, 3),
              spreadRadius: 2,
              blurRadius: 2,
              color: WellNestColor.wncLightgrey.withOpacity(0.3),
            ),
          ],
          // image: DecorationImage(
          //   image: NetworkImage(imagePath!, 
          //   ),
          //   fit: BoxFit.cover,
          //   alignment: Alignment.topCenter
          //   ),
        ),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Image.network(imagePath!,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
                loadingBuilder: (_,__,___){
                if(___ == null)
                return __;
                return Align(
                  alignment: Alignment.center, //c
                  child: SpinKitRotatingCircle(
                    color: WellNestColor.wncOrange,//c
                    size: 40.0,
                    duration: Duration(milliseconds: 450),
                    
                  ),
                );
                }),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 39,
                  width: 96,
                  decoration: BoxDecoration(
                    color: WellNestColor.wncWhite,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      textDirection: TextDirection.ltr,
                      children: [
                        Text(name!,
                        overflow: TextOverflow.ellipsis,
                        style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncGrey
                          ,10.0),
                        ),
                        SizedBox(height: 2,),
                        Text(description!,
                        overflow: TextOverflow.ellipsis,
                        style: WellNestTextStyle.nowRegular(
                          WellNestColor.wncLightgrey
                          ,6.0),
                        ),
                       SizedBox(height: 2,),
                       Container(
                         child: Row(
                           children: [
                             Container(
                               height: 5,
                               width: 5,
                               decoration: BoxDecoration(
                                 shape: BoxShape.circle,
                                 color: Colors.red
                               ),
                             ),
                             SizedBox(width: 2,),
                             Text("Live",
                        style: WellNestTextStyle.nowLight(
                          WellNestColor.wncLightgrey
                          ,5.0),
                        ),
                           ],
                         ),
                       ),
                      ],
                    ),
                  ),
                )
              ),
            ),
          ]
        ),
      ),
    );
  }
}