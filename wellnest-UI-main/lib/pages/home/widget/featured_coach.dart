


import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/coach/featured_coach.dart';
import 'package:wellnest/pages/featured_coach/feature_coach.dart';
import 'package:wellnest/pages/home/controller/home_controller.dart';
import 'package:wellnest/pages/home/widget/feature_card.dart';
import 'package:wellnest/pages/home/widget/shimmer/list_shimmer.dart';
import 'package:wellnest/route/route_page.dart';

final controller = Get.put(HomeController());
class FeatureCoachw extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      return Obx(
        ()=> 
        controller.coachFeatureDList.isEmpty ?  
        ListShimmerCoach()
        : Container(
          width: 349,
          height: 161.34,
          child: AnimationLimiter(
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: controller.coachFeatureDList.length > 3 ? 3: controller.coachFeatureDList.length,
              itemBuilder: (_,__)
              =>AnimationConfiguration.staggeredList(
                position: __,
                duration: 375.milliseconds,
                child: SlideAnimation(
                  verticalOffset: 50.0,
                  child: FadeInAnimation(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 5,
                        bottom: 10,
                        right: 10
                      ),
                      child: FeaturedCard(
                        press: ()=>
                        Get.toNamed(
                          AppRouteName.homeView!+
                          AppRouteName.coachFeaturedProfile!,
                          arguments: controller.coachFeatureDList[__].id,
                          id: 0,
                          ),
                        name: controller.coachFeatureDList[__].name,
                        description: controller.coachFeatureDList[__].shortDescription,
                        imagePath: controller.coachFeatureDList[__].photoUrls!.map((e) =>e).first.toString(),
                      ),
                    ),
                    ),
                  ),
                ),
              ), 
          ),
        ),
      );   
  }
}