import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/models/category_class.dart';

class CategoriesCard extends StatelessWidget {
  const CategoriesCard({Key? key, required this.name, this.onPressed, this.image}) : super(key: key);
  final String? name;
  final String? image;
  final Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        height: 100.h,
        width: 120.w,
        decoration: BoxDecoration(
          color: WellNestColor.wncLightmoreAque,
          borderRadius: BorderRadiusDirectional.circular(10),
          boxShadow: [
            BoxShadow(
              color: WellNestColor.wncLightgrey.withOpacity(0.2),
              blurRadius: 2,
              spreadRadius: 2,
              offset: Offset(2.0, 2.0),
            )
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 40.h,
              width: 40.w,
              child: Image.asset(image!),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Text(
                name!,
                maxLines: 2,
                textAlign: TextAlign.center,
                style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 13.sp),
              ),
            )
          ],
        ),
      ),
    );
  }
}
