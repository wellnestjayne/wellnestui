import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/booking/booking.dart';
import 'package:wellnest/pages/home/home_view.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/main/widget/tabView.dart';
import 'package:wellnest/pages/monezitation/money.dart';
import 'package:wellnest/pages/newsfeed/newsfeed_view.dart';
import 'package:wellnest/pages/write/write.dart';
import 'package:wellnest/standard_widgets/bottombar/bottom_bar.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class MainHomew extends StatelessWidget {

  final mainController = Get.put(MainController());

  Widget _buildOff(int? tabName)
  => Offstage(
    offstage: mainController.current.value != tabName,
    child: TabNavigatorGet(
      tabItem: tabName,
    ),
  );

  @override
  Widget build(BuildContext context) {
    mainController.checkVersionofApp(context: context);
    return Obx(()
     => WillPopScope(
       onWillPop: ()async{
         final isFirst = !await Get.nestedKey(mainController.current.value)!
         .currentState!.maybePop();
         if(isFirst){
           if(mainController.current.value != 0){
             mainController.selectTab(0, 0);
             return false;
           }
         }
         return false;
       },
       child:  Scaffold(
         body: Stack(
           children: [
             _buildOff(0),
             _buildOff(1),
             _buildOff(2),
             _buildOff(3),
            //  _buildOff(4),
           ],
         ),
      //  body: mainController.listWidget[mainController.selectectIndex],
        // body: IndexedStack(
        //     index: mainController.selectectIndex,
        //     children: [
        //       HomeVieww(),
        //       BookingVieww(),
        //       NewFeedVieww(),
        //       WriteVieww(),
        //       MoneyVieww(),
        //     ],
        //   ),
        bottomNavigationBar: BottomCustombarw(
                color: mainController.colorType,
                animationController: mainController.listenable,
                isShow: mainController.bilatOten.value,
                bottomNavigationKey: mainController.bottomNavigationKey,
                index1: mainController.select.value,
               // onchanged: (index)=> mainController.selectedIndex = index,
                onchanged: (index)=>
                mainController.selectTab(mainController.pageKeys[index!], index),
              ),
              
          
      ),
     )
    );
    
  }
  
}
