
import 'package:get/get.dart';
import 'package:wellnest/pages/booking/controller/booking_controller.dart';
import 'package:wellnest/pages/home/controller/home_controller.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/monezitation/controller/money_controller.dart';
import 'package:wellnest/pages/newsfeed/controller/newsfeed_controller.dart';
import 'package:wellnest/pages/write/controller/write_controller.dart';

class MainBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<MainController>(() => MainController(),fenix: true);
    Get.lazyPut<HomeController>(() => HomeController(),fenix: true);
    Get.lazyPut<BookingController>(() => BookingController(),fenix: true);
    Get.lazyPut<NewsFeedController>(() => NewsFeedController(),fenix: true);
    Get.lazyPut<WriteController>(() => WriteController(),fenix: true);
    Get.lazyPut<MoneyController>(() => MoneyController(),fenix: true);
  }



}
