import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:new_version/new_version.dart';
import 'package:wellnest/api/api_service/Log_Reg_Up/api_services.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/booking/booking.dart';
import 'package:wellnest/pages/home/home_view.dart';
import 'package:wellnest/pages/monezitation/money.dart';
import 'package:wellnest/pages/newsfeed/newsfeed_view.dart';
import 'package:wellnest/pages/write/write.dart';
import 'package:wellnest/standard_widgets/curved/curved_bar.dart';
import 'package:wellnest/standard_widgets/loading.dart';
class MainController extends GetxController with SingleGetTickerProviderMixin{

 final bilatOten = true.obs;
  late Listenable listenable = AnimationController(vsync: this,
  
  duration: 450.milliseconds);
  late ScrollController scrollController;
  GlobalKey<CurvedNavigationBarState>? bottomNavigationKey =  GlobalKey<CurvedNavigationBarState>();
  final currentPage = 0.obs;

  get selectectIndex => this.currentPage.value;
  set selectedIndex(index){ 
    
    this.currentPage.value = index;

    // var fun = this.currentPage.value = index;    

    //   if(fun == 0){
    //     print(homeController);
    //   }

    } 

  final colorType = SharePref.getType() == "User" ? WellNestColor.wncBlue : WellNestColor.wncAquaBlue;

  final listWidget = 
  [
    HomeVieww(),
    BookingVieww(),
    NewFeedVieww(),
    // WriteVieww(),
    MoneyVieww(),
  ];


  @override
  void onInit() {
    super.onInit();
    scrollController = ScrollController();
  }


  // final homeController = Get.find<HomeController>().title;

  void listenMe(isForward){
     bilatOten(isForward);
  }

  
  void logoutinAnyPages() async{
    // var email = SharePref.detailData['email'];
    // await CognitoService.logoutSingout(email);
      await UserCoachApiOnly.logoutUser();
    // // print("User is Logout.");
  }

  final current = 0.obs;
  final select = 0.obs;
  List<int> pageKeys = [
   0,
   1,
   2,
   3,
   4
  ].obs;

  Map<int, String?> navKey = {
    0:"home",
    1:"book",
    2: "feed",
    3: "write",
    4: "settings"
  };

  selectTab(int? tabItem, int? index){
    if(tabItem == current.value){
      Get.nestedKey(navKey[tabItem])!.currentState!.popUntil((route) => route.isFirst);
    }else{
      current(pageKeys[index!]);
      select(index);
    } 

  }

  late final newVersion = NewVersion(
    iOSId: "com.WellNest.app", 
    androidId: "com.WellNest.app"
  );

  checkVersionofApp({BuildContext? context}) async{
      //LoadingOrError.loading();
      final statusApp = await newVersion.getVersionStatus();
if(statusApp!.localVersion == statusApp.storeVersion){
    print("The App is on the latest one!");
    //Get.back();
  }else{
    //Get.back();
   newVersion.showUpdateDialog(
   context: context!,
   versionStatus: statusApp,
   dialogTitle: "Update WellNest to Latest Version.",
   updateButtonText: "Update the app.",
   dialogText: "New version of the app please update the app from ${statusApp.localVersion} to ${statusApp.storeVersion} as the latest version of WellNest.",
   dismissAction: ()=>Get.back(),
   dismissButtonText: "Skip for now.",
   );
  }
 }


}