import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/booking/booking.dart';
import 'package:wellnest/pages/booking/cancel_booking.dart';
import 'package:wellnest/pages/categories/category_all_service.dart';
import 'package:wellnest/pages/categories/coach_by_category/coach_by_category.dart';
import 'package:wellnest/pages/categories/top_services/top_services.dart';
import 'package:wellnest/pages/coach/book_session/book_session.dart';
import 'package:wellnest/pages/coach/book_session/checkout/book_checkout.dart';
import 'package:wellnest/pages/coach/book_session/checkout/edit_payment.dart';
import 'package:wellnest/pages/coach/book_session/checkout/pick_a_date.dart';
import 'package:wellnest/pages/coach/featured_coach.dart';
import 'package:wellnest/pages/featured_coach/feature_coach.dart';
import 'package:wellnest/pages/home/home_view.dart';
import 'package:wellnest/pages/monezitation/money.dart';
import 'package:wellnest/pages/newsfeed/newsfeed_view.dart';
import 'package:wellnest/pages/profile/profile_vieww.dart';
import 'package:wellnest/pages/refer_friend/refer_friend.dart';
import 'package:wellnest/pages/write/write.dart';
import 'package:wellnest/route/route_page.dart';

class TabNavigatorGet extends StatelessWidget {
  final int? tabItem;

  const TabNavigatorGet({Key? key, this.tabItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: Get.nestedKey(tabItem),
      onGenerateRoute: (routeSetting) {
        Get.routing.args = routeSetting.arguments;
        if (tabItem == 0) {
          if (routeSetting.name!.contains(AppRouteName.homeView! + AppRouteName.featureCoach!))
            return GetPageRoute(
              routeName: AppRouteName.homeView! + AppRouteName.coachFeaturedProfile!,
              page: () => FeatureCoach(),
            );
          else if (routeSetting.name!.contains(AppRouteName.homeView! + AppRouteName.coachFeaturedProfile!))
            return GetPageRoute(
              routeName: AppRouteName.homeView! + AppRouteName.coachFeaturedProfile!,
              page: () => FeaturedCoachProfile(),
            );
          else if (routeSetting.name!.contains(AppRouteName.homeView! + AppRouteName.coachBookSession!))
            return GetPageRoute(
              routeName: AppRouteName.homeView! + AppRouteName.coachBookSession!,
              page: () => BookSessionToCoach(),
            );
          else if (routeSetting.name!.contains(AppRouteName.homeView! + AppRouteName.checkoutBook!))
            return GetPageRoute(
              routeName: AppRouteName.homeView! + AppRouteName.checkoutBook!,
              page: () => BookCheckout(),
            );
          else if (routeSetting.name!.contains(AppRouteName.pickadateCheck!))
            return GetPageRoute(
              routeName: AppRouteName.pickadateCheck!,
              page: () => PickADateForBuying(),
            );
          else if (routeSetting.name!.contains(AppRouteName.editpayment!))
            return GetPageRoute(
              routeName: AppRouteName.editpayment!,
              page: () => EditPaymentCheckbook(),
            );
          else if (routeSetting.name!.contains(AppRouteName.homeView! + AppRouteName.coachBookSession!))
            return GetPageRoute(
              routeName: AppRouteName.homeView! + AppRouteName.coachBookSession!,
              page: () => BookSessionToCoach(),
            );
          else if (routeSetting.name!.contains(AppRouteName.homeView! + AppRouteName.coachServices!))
            return GetPageRoute(
              routeName: AppRouteName.homeView! + AppRouteName.coachServices!,
              page: () => CoachByCategory(),
            );
        }
        if (tabItem == 1) {
          if (routeSetting.name!.contains(AppRouteName.homeView! + AppRouteName.cancelBooking!))
            return GetPageRoute(
              routeName: AppRouteName.homeView! + AppRouteName.cancelBooking!,
              page: () => CancelBookingView(),
            );
        }

        if (tabItem == 3) {
          if (routeSetting.name!.contains(AppRouteName.homeView! + AppRouteName.referFriend!))
            return GetPageRoute(
              routeName: AppRouteName.homeView! + AppRouteName.referFriend!,
              page: () => ReferAFriendView(),
            );
        }
        //Don't add in here on top only
         if(tabItem ==0 
         && SharePref.getType() == "Coach"
         && SharePref.getCoachId() == "null"){
          return GetPageRoute(
            routeName: routeSetting.name,
            page:()=> HomeVieww(
              index: tabItem,
            ),
          );
        }else if(tabItem ==0 
         && SharePref.getType() == "Coach"
         && SharePref.getCoachId() != null){
          return GetPageRoute(
            routeName: routeSetting.name,
            page:()=>ProfileVieww(
            ),
          );
        }
        else if (tabItem == 0 
          && SharePref.getType() == "User") {
          return GetPageRoute(
            routeName: routeSetting.name,
            page: () => HomeVieww(
              index: tabItem,
            ),
          );
        } else if (tabItem == 1) {
          return GetPageRoute(routeName: routeSetting.name, page: () => BookingVieww());
        } else if (tabItem == 2) {
          return GetPageRoute(routeName: routeSetting.name, page: () => NewFeedVieww());
        }
        // else if (tabItem == 2) {
        //   return GetPageRoute(routeName: routeSetting.name, page: () => WriteVieww());
        // }
        else if (tabItem == 3) {
          return GetPageRoute(routeName: routeSetting.name, page: () => MoneyVieww());
        }
      },
    );
  }
}