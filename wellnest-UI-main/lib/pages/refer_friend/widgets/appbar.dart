import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class ReferFriendAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double? topSize = 90.h;
  final Color? color;
  final String? title;
  final Widget? title2;
  final Function()? function;

  ReferFriendAppBar({Key? key, this.color, this.title, this.function, this.title2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: topSize!,
      decoration: BoxDecoration(
        color: color!,
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            InkWell(
              onTap: function!,
              child: Container(
                width: 50,
                height: topSize!,
                child: Center(
                  child: FaIcon(
                    FontAwesomeIcons.chevronLeft,
                    color: WellNestColor.wncLightgrey,
                    size: 25,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20.w,
            ),
            Container(
              width: MediaQuery.of(context).size.width * .7,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: WellNestColor.wncWhite,
                border: Border.all(color: WellNestColor.wncBlue),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(2, 1),
                    blurRadius: 1,
                    spreadRadius: 1,
                    color: WellNestColor.wncLightgrey.withOpacity(0.3),
                  )
                ],
              ),
              child: ListTile(
                dense: true,
                leading: SvgPicture.asset(WellnestAsset.referIconSvg),
                title: Text('Referrals'),
                trailing: Text('0'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(90);
}
