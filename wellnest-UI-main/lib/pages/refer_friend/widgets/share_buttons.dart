import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';

class ShareButtons extends StatelessWidget {
  const ShareButtons({Key? key, this.onPressedLink, this.onPressedLinkedIn, this.onPressedFacebook}) : super(key: key);
  final Function()? onPressedLink;
  final Function()? onPressedLinkedIn;
  final Function()? onPressedFacebook;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        shareButton(onPressed: onPressedLink, icon: FontAwesomeIcons.link),
        shareButton(onPressed: onPressedLinkedIn, icon: FontAwesomeIcons.linkedinIn),
        shareButton(onPressed: onPressedFacebook, icon: FontAwesomeIcons.facebookF),
      ],
    );
  }

  Widget shareButton({IconData? icon, Function()? onPressed}) {
    return ElevatedButton(
      onPressed: onPressed,
      child: FaIcon(icon, size: 15),
      style: ElevatedButton.styleFrom(
        alignment: Alignment.topCenter,
        shape: CircleBorder(),
        padding: EdgeInsets.all(12),
        primary: WellNestColor.wncBlue,
      ),
    );
  }
}
