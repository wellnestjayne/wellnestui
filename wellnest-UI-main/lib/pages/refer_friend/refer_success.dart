import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/pages/refer_friend/controller/referFriend_controller.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class ReferAFriendSuccess extends StatelessWidget {
  ReferAFriendSuccess({Key? key}) : super(key: key);

  final referFriendController = Get.put(ReferAFriendController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Image.asset(WellnestAsset.referFriendSuccess),
          ),
          SizedBox(height: 20),
          Text(
            'Congratulations for referring a friend/coach!',
            textAlign: TextAlign.center,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncBlue,
              25.sp,
            ),
          ),
          SizedBox(height: 60),
          BlueButtonStandard(
            bluebutton: () => Get.offNamedUntil(AppRouteName.homeView!, (route) => false, id: 3),
            title: "Check your Rewards",
            back: WellNestColor.wncBlue,
            border: WellNestColor.wncBlue,
            textColor: WellNestColor.wncWhite,
            sizeText: 15.0,
            height: 37,
            width: 334,
          ),
        ],
      ),
    );
  }
}
