import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:share_plus/share_plus.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/refer_friend/controller/referFriend_controller.dart';
import 'package:wellnest/pages/refer_friend/refer_success.dart';
import 'package:wellnest/pages/refer_friend/widgets/appbar.dart';
import 'package:wellnest/pages/refer_friend/widgets/share_buttons.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class ReferAFriendView extends StatelessWidget {
  ReferAFriendView({Key? key}) : super(key: key);

  final referFriendController = Get.put(ReferAFriendController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: WellNestColor.wncWhite,
        appBar: ReferFriendAppBar(
          function: () => Get.back(id: 3),
          color: WellNestColor.wncWhite,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Image.asset(WellnestAsset.referFriend),
            ),
            SizedBox(height: 20),
            Text(
              'Start referring friends and earn amazing rewards!',
              textAlign: TextAlign.center,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncBlue,
                25.sp,
              ),
            ),
            SizedBox(height: 40),
            Text(
              'View Rewards',
              textAlign: TextAlign.center,
              style: TextStyle(
                decoration: TextDecoration.underline,
                color: WellNestColor.wncBlue,
                fontSize: 20.sp,
              ),
            ),
            SizedBox(height: 20),
            BlueButtonStandard(
              bluebutton: () {
                Share.share("Hello Test", subject: "Wellnest");
              },
              //Get.to(() => ReferAFriendSuccess(), id: referFriendController.mainController.select.value),
              title: "Share with Friends",
              back: WellNestColor.wncBlue,
              border: WellNestColor.wncBlue,
              textColor: WellNestColor.wncWhite,
              sizeText: 15.0,
              height: 37,
              width: 334,
            ),
            SizedBox(height: 20),
            ShareButtons(
              onPressedLink: () {},
              onPressedLinkedIn: () {},
              onPressedFacebook: () {},
            ),
          ],
        ),
      ),
    );
  }
}
