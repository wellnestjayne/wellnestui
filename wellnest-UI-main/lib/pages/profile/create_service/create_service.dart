import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/pages/profile/create_service/page_view/cancellationpolicy.dart';
import 'package:wellnest/pages/profile/create_service/page_view/coaching.dart';
import 'package:wellnest/pages/profile/create_service/page_view/main_page.dart';
import 'package:wellnest/pages/profile/create_service/page_view/training.dart';
import 'package:wellnest/pages/profile/create_service/widget/appbar_image.dart';
import 'package:wellnest/pages/profile/create_service/widget/drop_down.dart';
import 'package:wellnest/pages/profile/create_service/widget/select_inclusion.dart';
import 'package:wellnest/pages/profile/create_service/widget/textField.dart';
import 'package:wellnest/pages/profile/create_service/widget/upload_video.dart';
import 'package:wellnest/pages/profile/models/inclussion.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class CreateServiceCoach extends StatelessWidget {
  final createserviceController = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SafeArea(
        child: WillPopScope(
            child: Scaffold(
                appBar: AppbarCreateServiceImage(
                  imageFile: createserviceController.image.value,
                  press: () => createserviceController.pickImage(),
                  title: createserviceController.stringtitle(createserviceController.serviceName!.value.text),
                  back: () => Get.back(id: 0),
                ),
                body: PageView(
                  //physics:,
                  controller: createserviceController.pageController,
                  onPageChanged: (index) {},
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    MainPageStart(),
                    CoachingPage(),
                    TrainingPage(),
                    CancelPolicy(),
                  ],
                )),
            onWillPop: () async {
              Get.back(id: 0);
              return true;
            }),
      ),
    );
  }
}
