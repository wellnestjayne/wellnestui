
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppbarCreateServiceImage extends StatelessWidget implements PreferredSizeWidget{
  
  final double? topsize = 261;
  final Function()? press;
  final String? imageFile;
  final Function()? back;
  final String? title;
  final String? createdBy;

  const AppbarCreateServiceImage({Key? key, this.press, this.imageFile, this.back, this.title, this.createdBy}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: topsize!,
      decoration: BoxDecoration(
        color: WellNestColor.wncAquaBlue,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10)
        )
      ),
      child: Stack(
        children: [
          imageFile == ""?
          Container() :
          Container(
            height: 261,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              // color: Colors.black,
               borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)
            ), 
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)
              ),
              child: Image.file(
                File(imageFile!),
                fit: BoxFit.cover,
                ),
            ),
          ),
           Container(
             decoration: BoxDecoration(
               borderRadius: BorderRadius.only(
                 bottomLeft: Radius.circular(10),
                 bottomRight: Radius.circular(10),
               ),
               gradient: LinearGradient(
                 begin: Alignment.center,
                 end: Alignment.bottomCenter,
                 colors: [
                 Colors.transparent,
                 Colors.black.withOpacity(0.4)
               ])
             ),
             height: 261,
             width: MediaQuery.of(context).size.width,
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child: Stack(
                 children: [
                   Align(
                     alignment: Alignment.bottomCenter,
                     child: Container(
                       height: 50,
                       width: MediaQuery.of(context).size.width,
                       child: Padding(
                         padding: const EdgeInsets.only(
                           left: 10,
                           right: 10
                         ),
                         child: Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           textDirection: TextDirection.ltr,
                           children: [
                             Text(title! == "" ? "" : title!,
                             style: WellNestTextStyle.nowMedium(
                               WellNestColor.wncWhite
                               ,18.0),
                             ),
                             SizedBox(height: 5,),
                             Text("by "+SharePref.detailData['firstName']+" "+SharePref.detailData['lastName'],
                             style: WellNestTextStyle.nowRegular(
                               WellNestColor.wncWhite
                               ,10.0),
                             ),
                           ],
                         ),
                       ),
                     ),
                   )
                 ],
               ),
             ),
           ),
          Align(
            alignment: Alignment.center,
            child: GestureDetector(
              onTap: press,
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  color: WellNestColor.wncGrey,
                  shape: BoxShape.circle
                ),
                child: Center(
                  child: FaIcon(FontAwesomeIcons.camera,
                  color: WellNestColor.wncWhite,
                  size: 20,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: GestureDetector(
              onTap: back,
              child: Padding(
                padding: const EdgeInsets.only(
                  top: 10,
                  left: 20
                ),
                child: Container(
                  height: 50,
                  width: 50,
                  child: FaIcon(FontAwesomeIcons.chevronLeft,
                  color: WellNestColor.wncWhite,
                  size: 30,
                  ),
                ),
              ),
            ),
          ),
         
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(topsize!);
}