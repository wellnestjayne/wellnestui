
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';

class CancelDays extends StatelessWidget {
  
  final createSerice = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return  Container(
      width: 349,    
      child: Column(
        children: [
          Text("How many days a user or company can cancel for free?",
          style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.0),
          ),
          SizedBox(height: 30,),
         createSerice.coaching.value ? 
         Container(
           height: 30,
           child: ListView.builder(
             scrollDirection: Axis.horizontal,
             itemCount: createSerice.listcancelCoaching.length,
             itemBuilder: (context,index)
             =>Obx(
               ()=> GestureDetector(
                 onTap: (){
                  createSerice.getWreck(createSerice.listcancelCoaching[index]);
                 },
                 child: Padding(
                   padding: const EdgeInsets.only(left:4.0,right: 4.0),
                   child: Container(
                     height: 29,
                     width: 96.21,
                     decoration: BoxDecoration(
                       color:  
                       createSerice.selectedString.value == createSerice.listcancelCoaching[index]?
                       WellNestColor.wncBlue
                       : WellNestColor.wncGrey,
                       borderRadius: BorderRadius.circular(20)
                     ),
                     child: Padding(
                       padding: const EdgeInsets.all(1.0),
                       child: Center(
                         child: Text(createSerice.listcancelCoaching[index],
                         style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite,13.0),
                         ),
                       ),
                     ),
                   ),
                 ),
               ),
             )),
         ) 
         :Container(
          height: 30,
           child: ListView.builder(
             scrollDirection: Axis.horizontal,
             itemCount: createSerice.listcancelTraining.length,
             itemBuilder: (context,index)
             =>Obx(
               ()=> GestureDetector(
                 onTap: ()=>createSerice.getWreck(createSerice.listcancelTraining[index]),
                 child: Padding(
                   padding: const EdgeInsets.only(left:4.0,right: 4.0),
                   child: Container(
                     height: 29,
                     width: 96.21,
                     decoration: BoxDecoration(
                       color: 
                       createSerice.selectedString.value == createSerice.listcancelTraining[index]?
                       WellNestColor.wncBlue
                       : WellNestColor.wncGrey,
                       borderRadius: BorderRadius.circular(20)
                     ),
                     child: Padding(
                       padding: const EdgeInsets.all(1.0),
                       child: Center(
                         child: Text(createSerice.listcancelTraining[index].toString(),
                         style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite,13.0),
                         ),
                       ),
                     ),
                   ),
                 ),
               ),
             )),
         ),

        ],
      ),
      
    );
  }
}