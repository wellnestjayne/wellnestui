

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';

class UploadvideofromGallery extends StatelessWidget {
 
  final createServiceController = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>createServiceController.pickVideo(),
      child: Obx(
        ()=>Container(
                            height: 55,
                            width: 349,
                            decoration: BoxDecoration(
                              color: WellNestColor.wncWhite,
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(3, 2),
                                  blurRadius: 1,
                                  spreadRadius: 1,
                                  color: WellNestColor.wncLightgrey.withOpacity(0.1)
                                )
                              ],
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                color: WellNestColor.wncBlue
                              )
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(createServiceController.video.isNotEmpty ?
                                    createServiceController.video.value  :"Upload",
                                    overflow: TextOverflow.ellipsis,
                                    style: WellNestTextStyle.nowRegular(
                                      WellNestColor.wncBlue
                                      , 18.0),
                                    ),
                                  ),
                                  FaIcon(FontAwesomeIcons.cloudUploadAlt,
                                  color: WellNestColor.wncBlue,
                                  size: 20,
                                  )
                                ],
                              ),
                            ),
                          ),
      ),
    );
  }
}