
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';

class TextFieldDescription extends StatelessWidget {

   final TextEditingController? controller;

  const TextFieldDescription({Key? key, this.controller}) : super(key: key); 

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 104,
      width: 349,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(3, 2),
                    blurRadius: 3,
                    spreadRadius: 3,
                    color: WellNestColor.wncLightgrey.withOpacity(0.1)
                  )
                ]
              ),
              child: TextFormField(
                validator: (String? value)=>name(value),
                autofocus: false,
                textAlignVertical: TextAlignVertical.top,
                maxLines: null,
                expands:true,
                keyboardType: TextInputType.multiline,
                controller: controller,
                textInputAction: TextInputAction.done,
                style: WellNestTextStyle.nowRegular(
                  WellNestColor.wncGrey
                  , 15.0),
               // onTap: ontap,
                decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: WellNestColor.wncBlue)
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: WellNestColor.wncBlue)
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: WellNestColor.wncBlue)
                ),
                filled: true,
                fillColor: WellNestColor.wncWhite,
                hintText: "Description of the Service",
               hintStyle: WellNestTextStyle.nowMedium(
            WellNestColor.wncGrey.withOpacity(0.3)
            , 15.0)
        ),
                  

              ),
            );
  }
}