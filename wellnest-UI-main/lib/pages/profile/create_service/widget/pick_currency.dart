
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/country/country_get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/coach/widget/flag_d.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class PickCurrencyonFlag extends StatelessWidget {

  final Alignment? alignButton;
  final Alignment? alignFlag;

  final createSer = Get.put(CreateServiceController());

   PickCurrencyonFlag({Key? key, this.alignButton, this.alignFlag}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Obx(
      ()=> GestureDetector(
        onTap: (){
          Get.defaultDialog(
            title: "",
            content: Container(
              child: Column(
                children: [
                  Container(
                              width: 244,
                              child:  TextFieldWellnestw(
                          controller:  createSer.searchCurreny,
                          validator: (value) =>
                              name(createSer.searchCurreny!.text = value!),
                          keyboardType: TextInputType.text,
                          obsecure: false,
                          onchanged: (value)=>createSer.filteredSearchCurrency(value!),
                          onsaved: (value) =>
                               createSer.searchCurreny!.text = value!,
                          hintText: "ex. PHP",
                        ),
                            ),
                   SizedBox(height: 15,),         
                   Obx(()
                   => createSer.countrySearch.isEmpty ? 
                   Container(
                    height: 300,
                    child: ListView.builder(
                      itemCount: createSer.country.length,
                      itemBuilder: (context,index)=>
                      GestureDetector(
                        onTap: ()
                        =>
                        createSer.showFlag(createSer.country[index].code,createSer.country[index].currency),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Row(
                              children: [
                                  Container(
                                    height: 35,
                                    width: 35,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle
                                    ),
                                    child: Image.asset(
                                      "lib/assets/images/countries/${createSer.country[index].code!.toString().toLowerCase()}.png"
                                    )
                                  ),
                                  SizedBox(width: 10,),
                                  Expanded(
                                    child: Text(createSer.country[index].currency!.toString()
                                    ,overflow: TextOverflow.ellipsis,
                                    style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,13.0),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        ),
                      )
                      ),
                  ): Container(
                    height: 300,
                    child: ListView.builder(
                      itemCount: createSer.countrySearch.length,
                      itemBuilder: (context,index)=>
                      GestureDetector(
                        onTap: ()
                        =>
                         createSer.showFlag(createSer.countrySearch[index].code,createSer.countrySearch[index].currency),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Row(
                              children: [
                                  Container(
                                    height: 35,
                                    width: 35,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle
                                    ),
                                    child: Image.asset(
                                      "lib/assets/images/countries/${createSer.countrySearch[index].code!.toString().toLowerCase()}.png"
                                    )
                                  ),
                                  SizedBox(width: 10,),
                                  Expanded(
                                    child: Text(createSer.countrySearch[index].currency!.toString()
                                    ,overflow: TextOverflow.ellipsis,
                                    style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,13.0),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        ),
                      )
                      ),
                  )
                   ),
                ],
              ),
            )
          );
        },
        child: Container(
          width: 160,
          height: 28,
          child: Padding(
            padding: const EdgeInsets.only(top:0),
            child: Row(
              children: [
                Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncBlue,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: FaIcon(FontAwesomeIcons.chevronDown,color: WellNestColor.wncWhite,size: 15,),
                    ),
                  ),
                SizedBox(width: 10,),
                 createSer.flagToShow.value.isEmpty ? Container(
                  height: 26,
                  width: 52,
                  child: FlagWidget(
                    code: 'ph',
                  ),
                ):
                Container(
                  height: 26,
                  width: 52,
                  child: FlagWidget(
                    code: createSer.flagToShow.value,
                  ),
                ),
                SizedBox(width: 10,),
                createSer.currencyPick.isEmpty ? Text("PHP",
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 18.0),
                  ) :
                  Text(createSer.currencyPick.value,
                  style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 18.0),
                  )
              ],
            ),
            // child: Stack(
            //   // mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     Align(
            //       alignment: alignButton!,
            //       child: Container(
            //         height: 20,
            //         width: 20,
            //         decoration: BoxDecoration(
            //           color: WellNestColor.wncBlue,
            //           shape: BoxShape.circle,
            //         ),
            //         child: Center(
            //           child: FaIcon(FontAwesomeIcons.chevronDown,color: WellNestColor.wncWhite,size: 15,),
            //         ),
            //       ),
            //     ),
            //     SizedBox(width: 10,),
            //     createSer.flagToShow.value.isEmpty ? Container(
            //       height: 26,
            //       width: 52,
            //       child: FlagWidget(
            //         code: 'ph',
            //       ),
            //     ):
            //     Align(
            //       alignment: alignFlag!,
            //       child: Container(
            //         height: 26,
            //         width: 52,
            //         child: FlagWidget(
            //           code: createSer.flagToShow.value,
            //         ),
            //       ),
            //     ),
            //     Align(
            //       alignment: Alignment.topRight,
            //       child: Padding(
            //         padding: const EdgeInsets.only(left: 15,top: 5),
            //         child:  createSer.currencyPick.isEmpty ? Text("PHP",
            //       style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 18.0),
            //       ) :
            //       Text(createSer.currencyPick.value,
            //       style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 18.0),
            //       )
            //         ),
                  
            //     ),
            //   ],
            // ),
          ),
        ),
      ),
    );
  }
}