

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';

class InclusionItem extends StatelessWidget {
  
  final String? name;
  final String? id;
  final RxBool? selected = false.obs;
  final Function(bool?)? onValue;
  


    final controller = Get.put(CreateServiceController());

  InclusionItem({Key? key, this.name, this.id, this.onValue}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(()=> 
    Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                       Checkbox(
            checkColor: WellNestColor.wncWhite,
            activeColor: WellNestColor.wncBlue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2)
            ),
            value: selected!.value,
            onChanged: (boolVak){
                selected!(!selected!.value);
                if(selected!.value == true){
                  controller.addInclusion(id,name);
                }else{
                  controller.listInclu.removeWhere((element) => element.name==name);
                }        
            }),
            //SizedBox(width: 2,),
            Text(name!,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncGrey
              , 16.0),
            )
                    ],
                  ),
                ),
    );
  }
}