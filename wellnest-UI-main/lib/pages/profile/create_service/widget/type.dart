
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';

class TypeChooser extends StatelessWidget {
  
  final controller = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      ()=> Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          textDirection: TextDirection.ltr,
          children: [
            Container(
              child: Row(
                children: [
                Checkbox(
              checkColor: WellNestColor.wncWhite,
              activeColor: WellNestColor.wncBlue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2)
              ),
              value: controller.coaching.value,
              onChanged: (bool)
              =>controller.changeCoachingCheck()),
              Text("Coaching (1 on 1)",
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,15.0),
              )
                ],
              ),
            ),
            GestureDetector(
              onTap: ()=>controller.pickFile(),
              child: Text(controller.fileText0.isNotEmpty ?
               controller.fileText0.value : "Upload Coaching Terms and Conditions",
              style: TextStyle(
                decoration: TextDecoration.underline,
                fontFamily: 'NowRegular',
                fontSize: 15.0,
                color: WellNestColor.wncBlue
              )
              ),
            ),
            SizedBox(height: 5,),
             Container(
              child: Row(
                children: [
                Checkbox(
              checkColor: WellNestColor.wncWhite,
              activeColor: WellNestColor.wncBlue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2)
              ),
              value: controller.training.value,
              onChanged: (bool)
              =>controller.changeTraingCheck()),
               Text("Training",
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey,15.0),
              )
                ],
              ),
            ),
             GestureDetector(
              onTap: ()=>controller.pickFile1(),
              child: Text(controller.fileText1.isNotEmpty ? controller.fileText1.value 
              : "Upload Training Terms and Conditions",
              style: TextStyle(
                decoration: TextDecoration.underline,
                fontFamily: 'NowRegular',
                fontSize: 15.0,
                color: WellNestColor.wncBlue
              )
              ),
            ),
          ],
        ),
      ),
    );
  }
}