import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/pages/profile/models/category_class.dart';

class DropDownCategory extends StatelessWidget {

  final createSerice = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      ()=> Container(
        height: 37,
        width: 349,
        decoration: BoxDecoration(
            color: WellNestColor.wncWhite,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: WellNestColor.wncBlue),
            boxShadow: [
              BoxShadow(
                  offset: Offset(1, 2),
                  spreadRadius: 2,
                  blurRadius: 2,
                  color: WellNestColor.wncLightgrey.withOpacity(0.2))
            ]),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                isExpanded: true,
                value: createSerice.selectedItem.isEmpty ? "" : createSerice.selectedItem.value,
                // hint: Obx(
                //   ()=> Text( createSerice.selectedItem.value.isNotEmpty ? 
                //   "Please Choose a Category" :  createSerice.selectedItem.value),
                // ),
                 icon: FaIcon(FontAwesomeIcons.chevronRight,
                 size: 15,color: WellNestColor.wncBlue,),
                 dropdownColor: WellNestColor.wncWhite,
                 onChanged: (String? object){
                   createSerice.changeValu(object);
                  //  print(object);
                   print(createSerice.selectedItem.value);
                 },
                items: createSerice.listCategory.map((e) => 
                DropdownMenuItem(
                 value: e.id,
                  child: Text(e.name!,
                                textAlign: TextAlign.center,
                              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0)
                                ))
                ).toList(),),
            ),
          ),  
      ),
    );
  }
}
