import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/pages/profile/create_service/widget/item_seclusion.dart';
import 'package:wellnest/pages/profile/models/inclussion.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class InclusionSelect extends StatelessWidget {

  final createServiceController = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return  InkWell(
                      onTap: ()
                      =>
                          Get.defaultDialog(
      title: "",
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.ltr,
        children: [
          Text("Inclusion",
          style: WellNestTextStyle.nowMedium(
            WellNestColor.wncBlue
            ,18.0),),
          SizedBox(height: 10,),
          Obx(
            ()=> createServiceController.listInclussion.isEmpty ? 
            Container():
            Container(
            height: 200,
            child: ListView.builder(
              itemCount: createServiceController.listInclussion.length,
              itemBuilder: (context,index)
              =>
                InclusionItem(
                  id: createServiceController.listInclussion[index].id.toString(),
                  name: createServiceController.listInclussion[index].name,
                ),
              ),
          )
          ),  
        ],
      ),
      confirm: BlueButtonStandard(
                        bluebutton: ()
                        =>Get.back(),
                        border: WellNestColor.wncBlue,
                        height: 37,
                        width: 349,
                        textColor: WellNestColor.wncWhite,
                        title: "DONE",
                        back: WellNestColor.wncBlue,
                        sizeText: 15.0,
                      ),
      contentPadding: const EdgeInsets.all(20),
      backgroundColor: WellNestColor.wncWhite,
      barrierDismissible: false,
    ),
                      child: Container(
                        height: 37,
                        width: 349,
                        decoration: BoxDecoration(
                          color: WellNestColor.wncWhite,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(3, 2),
                              blurRadius: 1,
                              spreadRadius: 1,
                              color: WellNestColor.wncLightgrey.withOpacity(0.1)
                            )
                          ],
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: WellNestColor.wncBlue
                          )
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Obx(
                                ()=> Expanded(
                                  child: Text(createServiceController.listInclu.isEmpty ? "Select" :
                                  createServiceController.listInclu.map((e) => e.name).toString().replaceAll("(", "").replaceAll(")",""),
                                  overflow: TextOverflow.ellipsis,
                                  style: WellNestTextStyle.nowRegular(
                                    WellNestColor.wncGrey
                                    , 15.0),
                                  ),
                                ),
                              ),
                              FaIcon(FontAwesomeIcons.chevronRight,
                              color: WellNestColor.wncBlue,
                              size: 15,
                              )
                            ],
                          ),
                        ),
                      ),
                    );
  }
}