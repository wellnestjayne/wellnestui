import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class PaymentWidget extends StatelessWidget {
  final double? coachingFee;
  final int? sessionNumber;
  final double? price;
  final int? discount;
  final double? finalPrice;
  final double? totalPrice;

  const PaymentWidget(
      {Key? key,
      this.coachingFee,
      this.sessionNumber,
      this.price,
      this.discount,
      this.finalPrice,
      this.totalPrice})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 349,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.ltr,
        children: [
          Text(
            "Payment Option",
            style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 13.0),
          ),
          SizedBox(
            height: 10,
          ),
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Coaching Fee: ",
              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: coachingFee.toString(),
              style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            ),
          ])),
          SizedBox(
            height: 10,
          ),
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Number of Session: ",
              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: sessionNumber.toString(),
              style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            ),
          ])),
          SizedBox(
            height: 10,
          ),
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Price: ",
              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: price.toString(),
              style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            ),
          ])),
          SizedBox(
            height: 10,
          ),
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Discount%: ",
              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: "${discount.toString()}%",
              style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            ),
          ])),
          SizedBox(
            height: 25,
          ),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Final Price: ",
                  style:
                      WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.0),
                ),
                TextSpan(
                  text: '$finalPrice',
                  style:
                      WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
