import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/pages/profile/create_service/widget/drop_down.dart';
import 'package:wellnest/pages/profile/create_service/widget/select_inclusion.dart';
import 'package:wellnest/pages/profile/create_service/widget/textField.dart';
import 'package:wellnest/pages/profile/create_service/widget/type.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class MainPageStart extends StatelessWidget {
  
  final createserviceController = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(
                  top: 10,
                  left: 20,
                  right: 20,
                  bottom: 20
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  textDirection: TextDirection.ltr,
                  children: [
                    Text("Service Name",
                    style: WellNestTextStyle.nowMedium(
                      WellNestColor.wncBlue
                      ,10.0),
                    ),
                    SizedBox(height: 10,),
                    TextFieldWellnestw(
                      controller:  createserviceController.serviceName,
                      validator: (value) =>
                          name(createserviceController.serviceName!.text = value!),
                      keyboardType: TextInputType.text,
                      obsecure: false,
                      onchanged: (value)=>
                          // createserviceController.stringtitle(value);
                          createserviceController.changeOnTextServiceName(value),
                      onsaved: (value) =>
                           createserviceController.serviceName!.text = value!,
                      hintText: "Ex. Fitness and Health",
                    ),
                    SizedBox(height: 15,),
                    Text("Description",
                    style: WellNestTextStyle.nowMedium(
                      WellNestColor.wncBlue
                      ,10.0),
                    ),
                    SizedBox(height: 5,),
                    TextFieldDescription(
                      controller: createserviceController.description,
                    ),
                    SizedBox(height: 15,),
                     Text("Inclusions",
                    style: WellNestTextStyle.nowMedium(
                      WellNestColor.wncBlue
                      ,10.0),
                    ),
                    SizedBox(height: 5,),
                    InclusionSelect(),
                    SizedBox(height: 15,),
                     Obx(
                       ()=> 
                       createserviceController.listInclu.isEmpty ?
                        Container()
                      : Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Container(
                          height: 40,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: createserviceController.listInclu.length,
                            itemBuilder: (context,index)
                            =>Padding(
                              padding: const EdgeInsets.only(
                                right: 10
                              ),
                              child: GestureDetector(
                                onTap: (){
                                  createserviceController.listInclu.removeWhere((element) => element.name
                                  ==createserviceController.listInclu[index].name);
                                },
                                child: Container(
                                  // width: 90,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(60),
                                    color: WellNestColor.wncOrange
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 20,
                                          child: Center(
                                            child: FaIcon(FontAwesomeIcons.times,
                                            color: WellNestColor.wncWhite,
                                            size: 12,
                                            ),
                                          ),
                                        ),
                                        Text(createserviceController.listInclu[index].name!)
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            )),
                    ),
                      ),
                     ),
                    Text("Category",
                    style: WellNestTextStyle.nowMedium(
                      WellNestColor.wncBlue
                      ,10.0),
                    ),
                    SizedBox(height: 5,),
                    DropDownCategory(),
                    SizedBox(height: 15,),
                    Text("Link",
                    style: WellNestTextStyle.nowMedium(
                      WellNestColor.wncBlue
                      ,10.0),
                    ),
                    SizedBox(height: 5,),
                    // UploadvideofromGallery(),
                     TextFieldWellnestw(
                      controller:  createserviceController.link,
                      validator: (value) =>
                          name(createserviceController.link!.text = value!),
                      keyboardType: TextInputType.text,
                      obsecure: false,
                      onchanged: (value)=>false,
                      onsaved: (value) =>
                           createserviceController.link!.text = value!,
                      hintText: "Youtube url link",
                    ),
                    SizedBox(height: 15,),
                    Text("Type",
                    style: WellNestTextStyle.nowMedium(
                      WellNestColor.wncBlue
                      ,10.0),
                    ),
                    SizedBox(height: 5,),
                    TypeChooser(),
                    SizedBox(height: 25,), 
                    BlueButtonStandard(
                      bluebutton: ()
                      =>callbacl(),
                      //=>createserviceController.forwardtoCoach(),
                      border: WellNestColor.wncBlue,
                      height: 37,
                      width: 349,
                      textColor: WellNestColor.wncWhite,
                      title: "Proceed",
                      back: WellNestColor.wncBlue,
                      sizeText: 15.0,
                    )
                  ],
                ),
              ),
            );
  }

  callbacl(){
      createserviceController.validatePagesMain();
  }

}