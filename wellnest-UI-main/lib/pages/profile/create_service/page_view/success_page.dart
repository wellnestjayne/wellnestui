


import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class SuccessServiceCreation extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.only(left: 20,right: 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                   SizedBox(height: 180,),
                   Container(width: MediaQuery.of(context).size.width,),
                  SvgPicture.asset(WellnestAsset.successFul),
                  SizedBox(height: 20,),
                  Text("You have successfully created your service.",
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowMedium(
                    WellNestColor.wncGrey,20.0),
                  ),
                  SizedBox(height: 100,),
                   BlueButtonStandard(
                        bluebutton: ()
                        { Get.keys.clear();
                          Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);},
                        border: WellNestColor.wncBlue,
                        height: 37,
                        width: 349,
                        textColor: WellNestColor.wncWhite,
                        title: "Return Home",
                        back: WellNestColor.wncBlue,
                        sizeText: 15.0,
                      ),
               SizedBox(height: 45,),
              GestureDetector(
                onTap: ()=>Get.back(),
                child: Text("Duplicate this service",
                style: TextStyle(
                  fontFamily: 'NowRegular',
                  fontSize: 15.0,
                  decoration: TextDecoration.underline,
                  color: WellNestColor.wncBlue
                ),
                ),
              )
                ],
              ),
            ),
          ),
        ),
      onWillPop: ()async=>false),
    );
  }
}