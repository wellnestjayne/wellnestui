import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/pages/profile/create_service/widget/payment.dart';
import 'package:wellnest/pages/profile/create_service/widget/pick_currency.dart';
import 'package:wellnest/pages/profile/upcomming_session/dialog/dialog.dart';
import 'package:wellnest/pages/subscription/widget/termsncondition.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';
import 'package:wellnest/terms_and_conditions/tac_dialog.dart';

class TrainingPage extends StatelessWidget {
  final createserviceController = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Obx(
        () => Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 25),
          child: Container(
              width: 349,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                children: [
                  Text(
                    "Price",
                    style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncBlue, 15.0),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        PickCurrencyonFlag(
                          alignButton: Alignment.centerRight,
                          alignFlag: Alignment.centerLeft,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 120,
                          child: TextFieldWellnestw(
                            controller: createserviceController.price,
                            validator: (value) => phone(
                                createserviceController.price!.text = value!),
                            keyboardType: TextInputType.number,
                            obsecure: false,
                            onchanged: (value) =>
                                createserviceController.priceText(value!.isEmpty
                                    ? 0.0
                                    : double.parse(value.toString())),
                            onsaved: (value) =>
                                createserviceController.price!.text = value!,
                            hintText: "0.00",
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Number of People",
                    style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncBlue, 15.0),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          "Maximum",
                          style: WellNestTextStyle.nowMedium(
                              WellNestColor.wncGrey, 15.0),
                        ),
                        Container(
                          width: 106,
                          child: TextFieldWellnestw(
                            controller: createserviceController.maximum,
                            validator: (value) => phone(
                                createserviceController.maximum!.text = value!),
                            keyboardType: TextInputType.number,
                            obsecure: false,
                            onchanged: (value) => false,
                            onsaved: (value) =>
                                createserviceController.maximum!.text = value!,
                            hintText: "0.00",
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          "Discount %",
                          style: WellNestTextStyle.nowMedium(
                              WellNestColor.wncGrey, 15.0),
                        ),
                        Container(
                          width: 106,
                          child: TextFieldWellnestw(
                            controller: createserviceController.discount,
                            validator: (value) => phone(createserviceController
                                .discount!.text = value!),
                            keyboardType: TextInputType.number,
                            obsecure: false,
                            onchanged: (value) => createserviceController
                                .discountText(value!.isEmpty
                                    ? 0
                                    : int.parse(value.toString())),
                            onsaved: (value) =>
                                createserviceController.discount!.text = value!,
                            hintText: "0%",
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  PaymentWidget(
                    coachingFee: 0.0,
                    //  createserviceController.price!.text.isEmpty ? 0.00 :
                    //  double.parse(createserviceController.price!.text.toString()),
                    sessionNumber:
                        createserviceController.numberSessionText.value == 0
                            ? 0
                            : createserviceController.numberSessionText.value,
                    price: createserviceController.priceText.value == 0.0
                        ? 0.0
                        : createserviceController.priceText.value,
                    discount: createserviceController.discountText.value == 0
                        ? 0
                        : createserviceController.discountText.value,
                    finalPrice:
                        createserviceController.priceText.value == 0.0 &&
                                createserviceController.discountText.value == 0
                            ? 0.00
                            : createserviceController.priceText.value -
                                (createserviceController.priceText.value *
                                        createserviceController
                                            .discountText.value) /
                                    100,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TearmsNCondition(
                    checkit: (b) => createserviceController.aggre(b),
                    seeMore: () => TacDialog.tacDialogShow(),
                    value: createserviceController.agreeTerms.value,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BlueButtonStandard(
                          bluebutton: () => SessionDialog.sessionBooking(
                            bookings: () {
                              Get.back();
                              createserviceController.pageController
                                  .animateToPage(0,
                                      duration: 230.milliseconds,
                                      curve: Curves.fastLinearToSlowEaseIn);
                            },
                            cancelShow: true,
                            booking: () => Get.back(),
                            donx: false,
                            cont: "Confirm",
                            imagePath: WellnestAsset.cancelSession,
                            message:
                                "Are you sure want to go back to previos screen",
                          ),
                          border: WellNestColor.wncBlue,
                          height: 37,
                          width: 159,
                          textColor: WellNestColor.wncBlue,
                          title: "Cancel",
                          back: WellNestColor.wncWhite,
                          sizeText: 15.0,
                        ),
                        SizedBox(
                          width: 2,
                        ),
                        BlueButtonStandard(
                          bluebutton: () =>
                              createserviceController.validatedTrainingPage(),
                          //=>createserviceController.gotoCancelation(),
                          border: WellNestColor.wncBlue,
                          height: 37,
                          width: 159,
                          textColor: WellNestColor.wncWhite,
                          title: "Confirm",
                          back: WellNestColor.wncBlue,
                          sizeText: 15.0,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
