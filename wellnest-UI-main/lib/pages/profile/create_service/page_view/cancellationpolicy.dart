
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/pages/profile/create_service/widget/days.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class CancelPolicy extends StatelessWidget {


    final service = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20,right: 20,top: 25),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            textDirection: TextDirection.ltr,
            children: [
              Text("Cancellation Policy",
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncBlue,18.0),
              ),
              SizedBox(height: 15,),
              Container(
                width: 349,
                child: Row(
                  children: [
                    Container(
                      height: 27,
                      width: 27,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: WellNestColor.wncAquaBlue
                      ),
                      child: Center(
                        child: FaIcon(FontAwesomeIcons.check,
                        color: WellNestColor.wncWhite,
                        size: 15,
                        ),
                      ),
                    ),
                    SizedBox(width: 20,),
                    Expanded(
                      child: Text("A user can cancel their appointment based on your preferred cancelation policy below.",
                      textAlign: TextAlign.left,
                      style: WellNestTextStyle.nowRegular(
                        WellNestColor.wncLightgrey
                        ,15.0),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 20,),
              Container(
                width: 349,
                child: Row(
                  children: [
                    Container(
                      height: 27,
                      width: 27,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: WellNestColor.wncAquaBlue
                      ),
                      child: Center(
                        child: FaIcon(FontAwesomeIcons.check,
                        color: WellNestColor.wncWhite,
                        size: 15,
                        ),
                      ),
                    ),
                    SizedBox(width: 20,),
                    Expanded(
                      child: Text("A user who cancels within the preferred duration will be eligible for a refund.",
                      textAlign: TextAlign.left,
                      style: WellNestTextStyle.nowRegular(
                        WellNestColor.wncLightgrey
                        ,15.0),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 25,),
              CancelDays(),
              SizedBox(height: 25,),
               Obx(
                 ()=> Container(
                  child: Row(
                    children: [
                        Checkbox(
              checkColor: WellNestColor.wncWhite,
              activeColor: WellNestColor.wncBlue,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(2)
              ),
              value: service.norefund.value,
              onChanged: (boolVak){
                  service.norefund(boolVak);       
              }),
              SizedBox(width: 10,),
              Text("No Refund Policy",
              style: WellNestTextStyle.nowRegular(
                WellNestColor.wncLightgrey, 15.0),
              )
                    ],
                  ),
              ),
               ),
              SizedBox(height: 25,),
               BlueButtonStandard(
                        bluebutton: ()
                        =>service.gotoSuccess(),
                        //=>service.gotoCancelationx(),
                        border: WellNestColor.wncBlue,
                        height: 37,
                        width: 349,
                        textColor: WellNestColor.wncWhite,
                        title: "Confirm",
                        back: WellNestColor.wncBlue,
                        sizeText: 15.0,
                      ),
               SizedBox(height: 15,),

            ],
          ),
        ),
      ),
    );
  }
}