import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/pages/profile/create_service/widget/payment.dart';
import 'package:wellnest/pages/profile/create_service/widget/pick_currency.dart';
import 'package:wellnest/pages/subscription/widget/termsncondition.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';
import 'package:wellnest/terms_and_conditions/tac_dialog.dart';

class CoachingPage extends StatelessWidget {
  final createserviceController = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Obx(
        () => Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10),
            child: Form(
              //autovalidateMode: AutovalidateMode.onUserInteraction,
              key: createserviceController.formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            textDirection: TextDirection.ltr,
                            children: [
                              Text(
                                "Number of Session/s",
                                style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncBlue, 13.0),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 156,
                                child: TextFieldWellnestw(
                                  controller:
                                      createserviceController.numberSession,
                                  validator: (value) => phone(
                                      createserviceController
                                          .numberSession!.text = value!),
                                  keyboardType: TextInputType.number,
                                  obsecure: false,
                                  onchanged: (value) {
                                    createserviceController.time(value);
                                    createserviceController.numberSessionText(
                                        value!.isEmpty ? 0 : int.parse(value));
                                  },
                                  onsaved: (value) => createserviceController
                                      .numberSession!.text = value!,
                                  hintText: "0",
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            textDirection: TextDirection.ltr,
                            children: [
                              Text(
                                "Duration",
                                style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncBlue, 13.0),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                  width: 156,
                                  height: 37,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: WellNestColor.wncWhite,
                                      border: Border.all(
                                          color: WellNestColor.wncBlue),
                                      boxShadow: [
                                        BoxShadow(
                                            offset: Offset(2, 1),
                                            blurRadius: 1,
                                            spreadRadius: 1,
                                            color: WellNestColor.wncLightgrey
                                                .withOpacity(0.3))
                                      ]),
                                  child: Center(
                                    child: Text(
                                      createserviceController.time.isEmpty
                                          ? "00:00"
                                          : "1:00",
                                      style: WellNestTextStyle.nowRegular(
                                          WellNestColor.wncGrey, 15.0),
                                    ),
                                  ))
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            textDirection: TextDirection.ltr,
                            children: [
                              Text(
                                "Price per session",
                                style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncBlue, 13.0),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 150,
                                child: TextFieldWellnestw(
                                  controller: createserviceController.price,
                                  validator: (value) => phone(
                                      createserviceController.price!.text =
                                          value!),
                                  keyboardType: TextInputType.number,
                                  obsecure: false,
                                  onchanged: (value) => createserviceController
                                      .priceText(value!.isEmpty
                                          ? 0.0
                                          : double.parse(value.toString())),
                                  onsaved: (value) {
                                    createserviceController.price!.text =
                                        value!;
                                  },
                                  hintText: "0.00",
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          textDirection: TextDirection.ltr,
                          children: [
                            Text(
                              "Currency",
                              style: WellNestTextStyle.nowMedium(
                                  WellNestColor.wncBlue, 13.0),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8),
                              child: PickCurrencyonFlag(
                                alignButton: Alignment.centerLeft,
                                alignFlag: Alignment.centerRight,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    child: Row(
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            textDirection: TextDirection.ltr,
                            children: [
                              Text(
                                "Discount %",
                                style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncBlue, 13.0),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 244,
                                child: TextFieldWellnestw(
                                  controller: createserviceController.discount,
                                  validator: (value) {
                                    if (value!.isEmpty)
                                      return "Please Don't leave it blank";
                                    else if (int.parse(value.toString()) >
                                        100) {
                                      return "Discount cannot be higher than 100%";
                                    } else
                                      return null;
                                  },
                                  keyboardType: TextInputType.number,
                                  obsecure: false,
                                  onchanged: (value) => createserviceController
                                      .discountText(value!.isEmpty
                                          ? 0
                                          : int.parse(value.toString())),
                                  onsaved: (value) {
                                    createserviceController.discount!.text =
                                        value!;
                                  },
                                  hintText: "0%",
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  PaymentWidget(
                    coachingFee: 0.0,
                    //  createserviceController.price!.text.isEmpty ? 0.00 :
                    //  double.parse(createserviceController.price!.text.toString()),
                    sessionNumber:
                        createserviceController.numberSessionText.value == 0
                            ? 0
                            : createserviceController.numberSessionText.value,
                    price: createserviceController.priceText.value == 0.0
                        ? 0.0
                        : createserviceController.priceText.value,
                    discount: createserviceController.discountText.value == 0
                        ? 0
                        : createserviceController.discountText.value,
                    finalPrice:
                        createserviceController.priceText.value == 0.0 &&
                                createserviceController.discountText.value == 0
                            ? 0.00
                            : createserviceController.priceText.value -
                                ((createserviceController.priceText.value *
                                        createserviceController
                                            .discountText.value) /
                                    100),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TearmsNCondition(
                    checkit: (b) => createserviceController.aggre(b),
                    seeMore: () => TacDialog.tacDialogShow(),
                    value: createserviceController.agreeTerms.value,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  BlueButtonStandard(
                    bluebutton: () => callback(),
                    //=>createserviceController.gotoCancelation(),
                    border: WellNestColor.wncBlue,
                    height: 37,
                    width: 349,
                    textColor: WellNestColor.wncWhite,
                    title: "Proceed",
                    back: WellNestColor.wncBlue,
                    sizeText: 15.0,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            )),
      ),
    );
  }

  callback() {
    if (createserviceController.formKey.currentState!.validate()) {
      createserviceController.validateSecondPageCoaching();
    }
  }
}
