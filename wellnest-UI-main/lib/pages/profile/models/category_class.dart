// To parse this JSON data, do
//
//     final coachCategories = coachCategoriesFromJson(jsonString);

import 'dart:convert';

CoachCategories coachCategoriesFromJson(String str) => CoachCategories.fromJson(json.decode(str));

String coachCategoriesToJson(CoachCategories data) => json.encode(data.toJson());

class CoachCategories {
    CoachCategories({
        this.message,
        this.data,
    });

    final String? message;
    final List<Datum>? data;

    factory CoachCategories.fromJson(Map<String, dynamic> json) => CoachCategories(
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.id,
        this.name,
        this.active,
        this.isActive,
    });

    final String? id;
    final String? name;
    final int? active;
    final bool? isActive;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        active: json["active"] == null ? null : json["active"],
        isActive: json["isActive"] == null ? null : json["isActive"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "active": active == null ? null : active,
        "isActive": isActive == null ? null : isActive,
    };
}
