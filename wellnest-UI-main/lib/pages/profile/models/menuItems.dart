
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/pages/profile/models/menuItem.dart';

class MenuItems{

   static const dashboard 
  = MenuItem(
    menuTitle: "DashBoard"
  );

  static const viewProfile 
  = MenuItem(
    menuTitle: "View Profile"
  );

  static const editProfile
  = MenuItem(
    menuTitle: "Edit Profile"
  );

  static const switchProfile 
  = MenuItem(
    menuTitle: "Switch to User"
  );

  static const switchLogout 
  = MenuItem(
    menuTitle: "Logout"
  );


  static const List<MenuItem> items =
    [ 
      //dashboard,
      // viewProfile,
      // editProfile,
      switchProfile
    ];

  // static const List<MenuItem> iteml =
  // [
  //   switchLogout
  // ];  

}