class ModelCategoryService{

  final String? imagePath;
  final String? title;

  ModelCategoryService({this.imagePath, this.title});

}