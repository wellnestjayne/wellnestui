// To parse this JSON data, do
//
//     final coachInclusions = coachInclusionsFromJson(jsonString);

import 'dart:convert';

CoachInclusions coachInclusionsFromJson(String str) => CoachInclusions.fromJson(json.decode(str));

String coachInclusionsToJson(CoachInclusions data) => json.encode(data.toJson());

class CoachInclusions {
    CoachInclusions({
        this.message,
        this.inclusion,
    });

    final String? message;
    final List<Inclusion>? inclusion;

    factory CoachInclusions.fromJson(Map<String, dynamic> json) => CoachInclusions(
        message: json["message"] == null ? null : json["message"],
        inclusion: json["data"] == null ? null : List<Inclusion>.from(json["data"].map((x) => Inclusion.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": inclusion == null ? null : List<dynamic>.from(inclusion!.map((x) => x.toJson())),
    };
}

class Inclusion {
    Inclusion({
        this.id,
        this.name,
        this.active,
        this.isActive,
    });

    final String? id;
    final String? name;
    final int? active;
    final bool? isActive;

    factory Inclusion.fromJson(Map<String, dynamic> json) => Inclusion(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        active: json["active"] == null ? null : json["active"],
        isActive: json["isActive"] == null ? null : json["isActive"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "active": active == null ? null : active,
        "isActive": isActive == null ? null : isActive,
    };
}
