var default_dailyStaet = DailyStatUIModel(
  day: 'day',
  stat: 0,
  isToday: false,
  isSelecgted: false,
  dayPosition: 1
);


class DailyStatUIModel{

  final String? day;
  final int? stat;
  final bool? isToday;
  final bool? isSelecgted;
  final int? dayPosition;

  DailyStatUIModel({
    this.day, 
    this.stat, 
    this.isToday, 
    this.isSelecgted, 
    this.dayPosition});

  DailyStatUIModel copyWith(
    {
      String? day,
   int? stat,
   bool? isToday,
   bool? isSelecgted,
   int? dayPosition,
    }
  )=>
  DailyStatUIModel(
    day: day ?? this.day,
    stat: stat ?? this.stat,
    isToday: isToday ?? this.isToday,
    isSelecgted: isSelecgted ?? this.isSelecgted,
    dayPosition: dayPosition ?? this.dayPosition
  );

}