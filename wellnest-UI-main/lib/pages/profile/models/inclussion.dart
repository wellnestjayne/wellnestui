// To parse this JSON data, do
//
//     final inclussion = inclussionFromJson(jsonString);

import 'dart:convert';

List<Inclussion> inclussionFromJson(String str) => List<Inclussion>.from(json.decode(str).map((x) => Inclussion.fromJson(x)));

String inclussionToJson(List<Inclussion> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Inclussion {
    Inclussion({
        this.id,
        this.name,
    });

    final String? id;
    final String? name;

    factory Inclussion.fromJson(Map<String, dynamic> json) => Inclussion(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}



  final listInclussion = [
    {
      "id":1,
      "name":"Assestments"
    },
    {
      "id":2,
      "name":"Handouts"
    },
    {
      "id":3,
      "name":"Discounts"
    },
    {
      "id":4,
      "name":"Free Session"
    },
  ];
