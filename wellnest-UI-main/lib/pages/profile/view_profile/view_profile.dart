import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/coach/widget/service_list.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';
import 'package:wellnest/pages/profile/edit_profile/edit_profile.dart';
import 'package:wellnest/pages/profile/user_service/user_service.dart';
import 'package:wellnest/pages/profile/view_profile/widget/CoachProfile_Image.dart';
import 'package:wellnest/pages/profile/view_profile/widget/appbar_view.dart';
import 'package:wellnest/pages/profile/view_profile/widget/flag_dicks.dart';
import 'package:wellnest/pages/profile/view_profile/widget/name_coach.dart';
import 'package:wellnest/pages/profile/view_profile/widget/serviceList.dart';
import 'package:wellnest/pages/profile/view_profile/widget/tags.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/header.dart';

class ViewProfileUser extends StatelessWidget {
  final viewProfileController = Get.put(ViewOwnProfileController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
          child: Scaffold(
            appBar: AppbarVuewProfile(
              functionX: () => Get.back(id: viewProfileController.mainController.current.value),
              extraTitle: "Edit",
              edit: () => Get.to(() => EditProfileCoach(), id: 0)
              //{},
              // Get.toNamed(
              //   AppRouteName.homeView!+
              //   AppRouteName.editProfile!
              // ),
              ,
              title: "My Profile",
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  children: [
                    CoachProfileImages(),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 349,
                      height: 60,
                      child: NameCoach(),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    // ViewFlagLanguage(),
                    FlagViewOwnProfile(),
                    SizedBox(
                      height: 15,
                    ),
                    CoachTags(),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          BlueButtonStandard(
                            bluebutton: () {},
                            title: "Message",
                            back: WellNestColor.wncAquaBlue,
                            border: WellNestColor.wncAquaBlue,
                            textColor: WellNestColor.wncWhite,
                            sizeText: 15.0,
                            height: 37,
                            width: 155.59,
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          BlueButtonStandard(
                            bluebutton: () {},
                            title: "Email",
                            back: WellNestColor.wncAquaBlue,
                            border: WellNestColor.wncAquaBlue,
                            textColor: WellNestColor.wncWhite,
                            sizeText: 15.0,
                            height: 37,
                            width: 155.59,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    BlueButtonStandard(
                      bluebutton: () {},
                      title: "Book an Appointment",
                      back: WellNestColor.wncBlue,
                      border: WellNestColor.wncBlue,
                      textColor: WellNestColor.wncWhite,
                      sizeText: 15.0,
                      height: 37,
                      width: 334,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    // Standardheader(
                    // press: ()=>
                    // Get.toNamed(
                    //   AppRouteName.homeView!+
                    // AppRouteName.coachFeaturedProfile!+
                    // AppRouteName.coachCertificate!,
                    // parameters: {"name":"Erchil's Certificates"}
                    // ),
                    // title: "Certificates",
                    // show: true,
                    // colorText: WellNestColor.wncBlue,
                    // ),
                    // SizedBox(height: 5,),
                    // Container(
                    //   height: 160,
                    //   child: ListView.builder(
                    //     scrollDirection: Axis.horizontal,
                    //     itemCount: coachController.listCertificate.length,
                    //     itemBuilder: (context,index)
                    //     =>Padding(
                    //       padding: const EdgeInsets.only(right: 10,
                    //       bottom: 10,
                    //       top: 10
                    //       ),
                    //       child: CertificateCardprofile(
                    //         imagePath: coachController.listCertificate[index].imagePath,
                    //         name: coachController.listCertificate[index].name,
                    //         width: 292,
                    //         hieght: 145,
                    //       ),
                    //     )),
                    // ),
                    SizedBox(
                      height: 15,
                    ),
                    Standardheader(
                      press: () =>
                          //   Get.toNamed(
                          //   AppRouteName.homeView!+
                          // AppRouteName.userServiceCategory!
                          // ),
                          Get.to(() => UserServiceCategory(), id: 0),
                      title: "Services",
                      show: true,
                      colorText: WellNestColor.wncBlue,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    ServiceOWnList(),
                    // Container(
                    //   height: 160,
                    //   child: ListView.builder(
                    //     scrollDirection: Axis.horizontal,
                    //     itemCount: coachController.listService.length,
                    //     itemBuilder: (context,index)
                    //     =>InkWell(
                    //       onTap: ()=>
                    //       Get.toNamed(
                    //          AppRouteName.homeView!+
                    //   AppRouteName.coachFeaturedProfile!+
                    //   AppRouteName.coachServices!+
                    //   AppRouteName.coachBookSession!
                    //       ),
                    //       child: Padding(
                    //         padding: const EdgeInsets.only(right: 10,
                    //         bottom: 10,
                    //         top: 10
                    //         ),
                    //         child: CertificateCardprofile(
                    //           imagePath: coachController.listService[index].imagePath,
                    //           name: coachController.listService[index].name,
                    //           width: 292,
                    //           hieght: 145,
                    //         ),
                    //       ),
                    //     )),
                    // ),
                    // SizedBox(height: 15,),
                    // Standardheader(
                    // press: ()=>
                    // Get.toNamed(
                    //   AppRouteName.homeView!+
                    // AppRouteName.coachFeaturedProfile!+
                    // AppRouteName.coachReviews!,
                    // parameters: {"name":"Erchil's Reviews"}
                    // ),
                    // title: "Reviews",
                    // show: true,
                    // colorText: WellNestColor.wncBlue,
                    // ),
                    // SizedBox(height: 5,),
                    //  Container(
                    //   height: 160,
                    //   child: ListView.builder(
                    //     scrollDirection: Axis.horizontal,
                    //     itemCount: coachController.listReviews.length,
                    //     itemBuilder: (context,index)
                    //     =>Padding(
                    //       padding: const EdgeInsets.only(left: 5,right: 10,
                    //       bottom: 10,
                    //       top: 10
                    //       ),
                    //       child: ReviewCardCoach(
                    //         dateString: coachController.listReviews[index].dateString,
                    //         understanding: coachController.listReviews[index].understanding,
                    //         commentorname: coachController.listReviews[index].commentorname,
                    //         description: coachController.listReviews[index].description,
                    //         starRate: coachController.listReviews[index].starRate,
                    //       ),
                    //     )),
                    // ),
                    // SizedBox(height: 10,),
                    //   BlueButtonStandard(
                    //         bluebutton: (){},
                    //         title: "Book an Appointment",
                    //         back: WellNestColor.wncBlue,
                    //         border: WellNestColor.wncBlue,
                    //         textColor: WellNestColor.wncWhite,
                    //         sizeText: 15.0,
                    //         height: 37,
                    //         width: 334,
                    //       ),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            ),
          ),
          onWillPop: () async {
            Get.back(id: viewProfileController.mainController.current.value);
            return true;
          }),
    );
  }
}
