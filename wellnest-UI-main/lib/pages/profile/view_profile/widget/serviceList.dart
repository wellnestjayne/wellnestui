import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/coach/widget/service_card.dart';
import 'package:wellnest/pages/coach/widget/shimmer/shimmer_list.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';

class ServiceOWnList extends StatelessWidget {
  final serviceController = Get.put(ViewOwnProfileController());

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      width: 349,
      child: Obx(
        () => serviceController.serviceList.isEmpty
            ? ShimmerListAll()
            : Container(
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: serviceController.serviceList.length > 3
                      ? serviceController.serviceList.length
                      : serviceController.serviceList.length,
                  itemBuilder: (_, __) => Padding(
                    padding: const EdgeInsets.only(right: 10, bottom: 10, top: 10),
                    child: ServiceCardCoach(
                      //  press: (){serviceController.sellMe(serviceController.serviceList[__].id);
                      press: () {},
                      imagePath: serviceController.serviceList[__].bannerUrl,
                      name: serviceController.forNameService.value,
                      title: serviceController.serviceList[__].name,
                      coachId: serviceController.serviceList[__].coachId,
                      id: serviceController.serviceList[__].id,
                      width: 292,
                      hieght: 145,
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
