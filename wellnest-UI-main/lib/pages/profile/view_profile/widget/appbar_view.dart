import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppbarVuewProfile extends StatelessWidget implements PreferredSizeWidget{
  
  final double? topSize = 90;
    final String? title;
    final String? extraTitle;
    final Function()? functionX;
    final Function()? edit;

  const AppbarVuewProfile({Key? key, this.title, this.extraTitle, this.functionX, this.edit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: topSize!,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 20,
            right: 20
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: functionX,
                child: FaIcon(
                  FontAwesomeIcons.times,
                  color: WellNestColor.wncGrey,
                ),
              ),
              Expanded(
                child: Container(
                  child: Text(title!,
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowMedium(
                    WellNestColor.wncGrey, 18.0),
                  ),
                ),
              ),
              GestureDetector(
                onTap: edit,
                child: Container(
                  child: Text(extraTitle!,
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowMedium(
                    WellNestColor.wncBlue
                    ,18.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(topSize!);
}