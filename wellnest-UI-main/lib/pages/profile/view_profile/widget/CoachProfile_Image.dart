import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';
import 'package:wellnest/pages/profile/view_profile/widget/shimmer/profile_imageLoad.dart';

class CoachProfileImages extends StatelessWidget {
  final viewCoachprofile = Get.put(ViewOwnProfileController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => viewCoachprofile.coachDatas.isEmpty
          ? ImageLoadingProfile()
          : Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: viewCoachprofile.coachDatas
                    .map(
                      (e) => Container(
                        child: Column(
                          children: [
                            Container(
                              height: 303,
                              width: 334,
                              child: PageView.builder(
                                controller: viewCoachprofile.pageController,
                                itemCount: e.photoUrls!.length,
                                onPageChanged: viewCoachprofile.selectedNumber,
                                itemBuilder: (context, index) => Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height: 303,
                                    width: 334,
                                    decoration: BoxDecoration(
                                        color: WellNestColor.wncBlue,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow: [
                                          BoxShadow(
                                            offset: Offset(3, 2),
                                            blurRadius: 2,
                                            spreadRadius: 2,
                                            color: WellNestColor.wncLightgrey.withOpacity(0.3),
                                          ),
                                        ]),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Image.network(e.photoUrls![index].toString(),
                                          fit: BoxFit.cover, alignment: Alignment.topCenter),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: List.generate(
                                  e.photoUrls!.length,
                                  (index) => Padding(
                                    padding: const EdgeInsets.only(left: 2, right: 2),
                                    child: Container(
                                      height: 10,
                                      width: 10,
                                      decoration: BoxDecoration(
                                        color: viewCoachprofile.selectedNumber.value == index
                                            ? WellNestColor.wncBlue
                                            : WellNestColor.wncLightgrey.withOpacity(0.3),
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
    );
  }
}
