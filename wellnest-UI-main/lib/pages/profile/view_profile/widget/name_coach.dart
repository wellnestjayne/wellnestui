import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';
import 'package:wellnest/pages/profile/models/userCoach.dart';
import 'package:wellnest/pages/profile/view_profile/widget/shimmer/title_info.dart';

class NameCoach extends StatelessWidget {
  final viewCoachprofile = Get.put(ViewOwnProfileController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => viewCoachprofile.coachDatas.isEmpty
          ? TitleInfoLoading()
          : Container(
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: viewCoachprofile.coachDatas.length,
                  itemBuilder: (context, index) => Container(
                        width: 349,
                        height: 80,
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  textDirection: TextDirection.ltr,
                                  children: [
                                    Container(
                                      child: Row(
                                        children: [
                                          Text(
                                            viewCoachprofile.coachDatas[index].name.toString(),
                                            style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 18.0),
                                          ),
                                          SizedBox(
                                            width: 15,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                      viewCoachprofile.coachDatas[index].shortDescription.toString(),
                                      style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 10.0),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 55,
                              width: 45,
                              child: Stack(
                                children: [
                                  SizedBox(
                                    child: Image.asset(WellnestAsset.chipCol),
                                  ),
                                  Positioned(
                                    top: -1,
                                    left: 2,
                                    right: 4,
                                    //alignment: Alignment.topCenter,
                                    child: Image.asset(WellnestAsset.starPol),
                                  ),
                                  Positioned(
                                    top: 27,
                                    left: 13,
                                    right: 11,
                                    //alignment: Alignment.topCenter,
                                    child: Text(
                                      "4.5",
                                      style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 10.0),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      )),
            ),
    );
  }
}
