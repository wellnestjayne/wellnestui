
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class ChipsTagLoad extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: Shimmer.fromColors(
        child: Wrap(
          alignment: WrapAlignment.center,
          spacing: 10,
          runSpacing: 10,
          children: [
            Container(
              height: 20,
              width: 90,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: WellNestColor.wncLightgrey
              ),
            ),
             Container(
              height: 20,
              width: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: WellNestColor.wncLightgrey
              ),
            ),
             Container(
              height: 20,
              width: 40,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: WellNestColor.wncLightgrey
              ),
            )
            
          ],
        ),
      baseColor: WellNestColor.wncLighBg, 
      highlightColor: WellNestColor.wncLightgrey),
    );
  }
}