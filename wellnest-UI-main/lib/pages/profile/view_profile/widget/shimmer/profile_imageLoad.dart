
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wellnest/constants/colors/colors.dart';

class ImageLoadingProfile extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 303,
      width: 334,
      child: Shimmer.fromColors(
      child: ListView.builder(
        itemCount: 1,
        itemBuilder: (_,__)
        =>
        Container(
        height: 303,
      width: 334,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: WellNestColor.wncLightgrey
      ),
        )
        ),
      baseColor: WellNestColor.wncLighBg, 
      highlightColor: WellNestColor.wncLightgrey),
    );
  }
}