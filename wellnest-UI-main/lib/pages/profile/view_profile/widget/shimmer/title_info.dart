
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wellnest/constants/colors/colors.dart';

class TitleInfoLoading extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 349,
      height: 60,
      child: Shimmer.fromColors(
      child: ListView.builder(
        itemCount: 1,
        itemBuilder: (_,__)=>
        Container(
          width: 349,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 40,
                        height: 30,
                        decoration: BoxDecoration(
                          color: WellNestColor.wncLightgrey,
                          borderRadius: BorderRadius.circular(5)
                        ),
                      ),
                      SizedBox(height: 5,),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 30,
                      decoration: BoxDecoration(
                          color: WellNestColor.wncLightgrey,
                          borderRadius: BorderRadius.circular(5)
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 10,),
              Container(
                height: 50,
                width: 50,
                color: WellNestColor.wncLightgrey,
              )
            ],
          ),
        )),
      baseColor: WellNestColor.wncLighBg, 
      highlightColor: WellNestColor.wncLightgrey),
    );
  }
}