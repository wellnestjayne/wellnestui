import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/widget/flag_d.dart';
import 'package:wellnest/pages/coach/widget/shimmer/shimmer_flag.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';

class FlagViewOwnProfile extends StatelessWidget {
  final viewController = Get.put(ViewOwnProfileController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => viewController.coachDatas.isEmpty
        ? ShimmerFlags()
        : Container(
            width: 349,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Wrap(
                    alignment: WrapAlignment.start,
                    runSpacing: 10,
                    spacing: 10,
                    children: viewController.flags
                        .map(
                          (e) => FlagWidget(
                            code: e!.toLowerCase(),
                          ),
                        )
                        .toList(),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: viewController.coachDatas.length,
                      itemBuilder: (_, __) => Text(
                            viewController.coachDatas[__].longDescription.toString(),
                            style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 15.0),
                          )),
                ),
              ],
            ),
          ));
  }
}
