
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';
import 'package:wellnest/pages/profile/view_profile/widget/shimmer/chips_load.dart';

class CoachTags extends StatelessWidget {
  
  final viewController = Get.put(ViewOwnProfileController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => 
    viewController.coachDatas.isEmpty ? 
    ChipsTagLoad() :
    Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: viewController.coachDatas.map((e) => 
        Container(
          width: MediaQuery.of(context).size.width,
          child: Wrap(
            alignment: WrapAlignment.center,
            spacing: 10,
            runSpacing: 10,
            children: e.tags!.map((ex) => 
             Container(
          decoration: BoxDecoration(
            color: WellNestColor.wncOrange,
            borderRadius: BorderRadius.circular(15),
            boxShadow:[
              BoxShadow(
                offset: Offset(1, 2),
                blurRadius: 1,
                spreadRadius: 1,
                color: WellNestColor.wncLightgrey.withOpacity(0.3)
              )
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              top: 5,
              bottom: 5
            ),
            child: Text(ex!,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncGrey, 13.0),
            ),
          ),
        )
            ).toList(),
          ),
        ),
        ).toList(),
      ),
    ),
    );
  }
}