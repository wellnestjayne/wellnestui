import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/profile/controller/profileController.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';
import 'package:wellnest/pages/profile/setAvailabilty/availability_view_c.dart';
import 'package:wellnest/pages/profile/upcomming_session/upcomming_session.dart';
import 'package:wellnest/pages/profile/widgets/appbar.dart';
import 'package:wellnest/pages/profile/widgets/associations.dart';
import 'package:wellnest/pages/profile/widgets/availabilty_button.dart';
import 'package:wellnest/pages/profile/widgets/badges.dart';
import 'package:wellnest/pages/profile/widgets/check_inButton.dart';
import 'package:wellnest/pages/profile/widgets/first_statistic.dart';
import 'package:wellnest/pages/profile/widgets/popmenu.dart';
import 'package:wellnest/pages/profile/widgets/header.dart';
import 'package:wellnest/pages/profile/widgets/private_public.dart';
import 'package:wellnest/pages/profile/widgets/recent_visitor_card.dart';
import 'package:wellnest/pages/profile/widgets/second_statistics.dart';
import 'package:wellnest/pages/profile/widgets/upcomming_sessionCard.dart';
import 'package:wellnest/pages/profile/widgets/user_bag.dart';
import 'package:wellnest/pages/profile/widgets/view_coachees_widget.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class ProfileVieww extends StatelessWidget {
  final controller = Get.put(ProfileController());
  final profileController = Get.put(ViewOwnProfileController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          // Get.back(id: controller.mainController.select.value);
          // return true;
          return true;
        },
        child: Scaffold(
            backgroundColor: WellNestColor.wncWhite,
            body: Container(
              child: Obx(
                () => ListView.builder(
                  itemCount: profileController.coachDatas.length,
                  itemBuilder: (context, index) => profileController.coachDatas.isEmpty
                      ? Container()
                      : Column(
                          children: [
                            AppbarProfile(
                              color: controller.mainController.colorType,
                              path: null,
                              firstname: controller.letter[0],
                              end: 34,
                              end2: 55,
                              item: (items) => controller.selectedItem(items),
                            ),

                            SizedBox(
                              height: 25.h,
                            ),
                            UserBagDesign(
                              name: profileController.coachDatas[index].name!,
                              photo: profileController.coachDatas[index].photoUrls![0],
                            ),
                            SizedBox(
                              height: 25.h,
                            ),
                            Obx(
                              () => Visibility(
                                visible: controller.profileShow! == "user" ? true : false,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 20,
                                    right: 20,
                                  ),
                                  child: MakeStatusProfile(
                                    colors: controller.value.value ? WellNestColor.wncBlue : WellNestColor.wncWhite,
                                    change: true,
                                    textColor: controller.value.value ? WellNestColor.wncWhite : WellNestColor.wncBlue,
                                    value: controller.value.value,
                                    functionbool: (value) => controller.functionBool(),
                                    borderColor:
                                        controller.value.value ? WellNestColor.wncWhite : WellNestColor.wncBlue,
                                    status: controller.value.value ? "Public" : "Private",
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 25.h,
                            ),
                            //Upcomming Header
                            HeaderProfile(
                              title: "Upcoming Session",
                              press: () => Get.to(() => UpcommingSessions(), id: 0),
                              show: true,
                            ),
                            SizedBox(
                              height: 15.h,
                            ),
                            //Upcomming Session Card
                            UpcommingSessionCard(
                              name: profileController.coachDatas[index].name!,
                              photo: profileController.coachDatas[index].photoUrls![0],
                            ),
                            SizedBox(
                              height: 20.h,
                            ),
                            Divider(
                              endIndent: 50,
                              indent: 50,
                              color: WellNestColor.wncLightgrey,
                            ),
                            SizedBox(
                              height: 25.h,
                            ),
                            //Check in Button
                            CheckInButton(
                              onPressed: () {},
                            ),
                            SizedBox(
                              height: 15.h,
                            ),
                            //Availabilty Button,
                            SetavailabiltyButton(
                              onpres: () =>Get.to(()=>SetAvailability(),id:0),
                              color: controller.mainController.colorType,
                            ),
                            SizedBox(
                              height: 25.h,
                            ),
                            //Recent Visitor Header,
                            HeaderProfile(
                              title: "Recent Viewers",
                              press: () {},
                              show: false,
                            ),
                            SizedBox(
                              height: 15.h,
                            ),
                            //Recent Circle Cards.
                            RecentVisitorCard(),
                            SizedBox(
                              height: 25.h,
                            ),
                            //Badeges,
                            //  HeaderProfile(
                            //    title: "Badges",
                            //    press: (){},
                            //    show: false,
                            //  ),
                            //   SizedBox(height: 15.h,),
                            //  BadgesUserProfile(),
                            //Header Associations
                            SizedBox(
                              height: 25.h,
                            ),
                            // HeaderProfile(
                            //    title: "Associations",
                            //    press: (){},
                            //    show: false,
                            //  ),
                            // SizedBox(height: 15.h,),
                            // AssociationCards(),
                            // SizedBox(height: 15.h,),
                            //  HeaderProfile(
                            //    title: "Visitor Stats",
                            //    press: (){},
                            //    show: false,
                            //  ),
                            //  SizedBox(height: 15.h,),
                            // FirstStatictic(),
                            SizedBox(
                              height: 15.h,
                            ),
                            HeaderProfile(
                              title: "View Coachees",
                              press: () {},
                              show: true,
                            ),
                            SizedBox(
                              height: 15.h,
                            ),
                            ViewCoaches(),
                            SizedBox(
                              height: 15.h,
                            ),
                            HeaderProfile(
                              title: "Session Hours",
                              press: () {},
                              show: false,
                            ),
                            SizedBox(
                              height: 15.h,
                            ),
                            SecondStatistic(),
                            SizedBox(
                              height: 15.h,
                            ),
                          ],
                        ),
                ),
              ),
            )),
      ),
    );
  }
}
