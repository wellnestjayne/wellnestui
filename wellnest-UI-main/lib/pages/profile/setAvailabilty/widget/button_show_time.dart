
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class ShowAvalabiltyBeforeSave extends StatelessWidget {
  
  final bool? valueChanger;
  final Function(bool)? toggle;
  final Function()? showTime;
  final String? weekName;
  final String? endTime;
  final String? startTime;
  final Function()? setME;

  const ShowAvalabiltyBeforeSave({Key? key, this.valueChanger, this.toggle, this.showTime, this.weekName, this.endTime, this.startTime, this.setME}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 349,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                 FlutterSwitch(
                  inactiveColor: WellNestColor.wncLightgrey,
                  activeColor: WellNestColor.wncBlue,
                  value: valueChanger!,
                  width: 45,
                  height: 25.0,
                  toggleSize: 18,
                  valueFontSize: 20.0,
                  onToggle: toggle!)
                  ,SizedBox(width: 5,),
                  Text(weekName!,
                  style: WellNestTextStyle.nowMedium(
                  WellNestColor.wncLightgrey, 13.0),
                  ),
                SizedBox(width: 15,),
                 GestureDetector(
                   onTap: showTime,
                   child: Container(
                    height: 39,
                    width: 80,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncWhite,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(2, 2),
                          blurRadius: 2,
                          spreadRadius: 2,
                          color: WellNestColor.wncLightgrey.withOpacity(0.3)
                        )
                      ],
                      border: Border.all(
                        color: WellNestColor.wncBlue
                      )
                    ),
                    child: Center(
                      child: Text(startTime!.isEmpty ? "00:00" : startTime!,
                      style: WellNestTextStyle.nowMedium(
                    WellNestColor.wncLightgrey, 15.0),
                      ),
                    ),
                ),
                 ),
                SizedBox(width: 5,),
                Container(
                  height: 39,
                 width: 80,
                  decoration: BoxDecoration(
                    color: WellNestColor.wncWhite,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(2, 2),
                        blurRadius: 2,
                        spreadRadius: 2,
                        color: WellNestColor.wncLightgrey.withOpacity(0.3)
                      )
                    ],
                    border: Border.all(
                      color: WellNestColor.wncBlue
                    )
                  ),
                  child: Center(
                    child: Text(endTime!.isEmpty ? "00:00" : endTime!,
                    style: WellNestTextStyle.nowMedium(
                  WellNestColor.wncLightgrey, 15.0),
                    ),
                  ),
                ),
                SizedBox(width: 10,),

              ],
            ),
          ),
          SizedBox(height: 15,),
          Visibility(
            visible: valueChanger!,
            child: Padding(
              padding: const EdgeInsets.only(right: 18),
              child: BlueButtonStandard(
                title: "Set",
                textColor: WellNestColor.wncWhite,
                height: 40,
                width: 170,
                back: WellNestColor.wncBlue,
                border: WellNestColor.wncBlue,
                bluebutton: setME,
              ),
            ),
          )
        ],
      ),
    );
  }
}