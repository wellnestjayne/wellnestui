

import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/set_availablity.dart';
import 'package:wellnest/pages/profile/setAvailabilty/widget/button_show_time.dart';
import 'package:wellnest/pages/write/calendar/customization/calendar_style.dart';
import 'package:wellnest/pages/write/calendar/shared/utils.dart';
import 'package:wellnest/pages/write/calendar/table_calendar.dart';
import 'package:wellnest/standard_widgets/appbar.dart';
import 'package:intl/intl.dart';
class SetAvailability extends StatelessWidget {
  
  final setController = Get.put(SetAvailabilityController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      ()=> SafeArea(
        child: Scaffold(
          appBar: StandardAppbaruse(
            function: ()=>Get.back(id: 0),
            title: "Set My Availability",
            color: WellNestColor.wncAquaBlue,
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(left:20, right: 20,top:30,bottom: 40),
              child: Column(
                children: [
                 Text(DateFormat("EEEE ,MMMM dd yyyy").format(DateTime.now()),
                 style: WellNestTextStyle.nowMedium(
                   WellNestColor.wncGrey, 18.0),),
                SizedBox(height: 20,),
                Container(
                  width: 349,
                  child: Row(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            FlutterSwitch(
                              inactiveColor: WellNestColor.wncLightgrey,
                              activeColor: WellNestColor.wncBlue,
                              value: setController.onlyWeekDays.value,
                              width: 45,
                              height: 25.0,
                              toggleSize: 18,
                              valueFontSize: 20.0,
                              onToggle: (onToggle)=>setController.makeWeekDaysOnly(onToggle))
                              ,SizedBox(width: 10,),
                              Text("Set Only Week Days.",
                              style: WellNestTextStyle.nowMedium(
                                WellNestColor.wncLightgrey, 13.0),
                              )
                            ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 35,),
                ShowAvalabiltyBeforeSave(
                  weekName: "Sunday",
                  startTime: setController.sundayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.sundayT.value)),
                  endTime: setController.sundayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.sundayT.value)
                  .add(Duration(seconds: 3600))),
                  valueChanger: setController.sunday.value,
                  toggle: (tog)=>setController.sunday(tog),
                  showTime: ()async{
                  final pickTime = await showTimePicker(
                  cancelText: "Back",
                  confirmText: "Select Time",
                  helpText: "Input Time",
                  initialEntryMode: TimePickerEntryMode.input,
                  context: context, initialTime: TimeOfDay.now());
                  setController.sundayT(pickTime!.hour.toString()+":"+pickTime.minute.toString());
                  },
                  setME: ()=>setController.setAvalabilityCoach(
                    dayNumber: "0",
                    timonly: setController.sundayT.value
                  )
                ),
                SizedBox(height: 15,),
                 //Monday
                 ShowAvalabiltyBeforeSave(
                  weekName: "Monday",
                  startTime: setController.mondayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.mondayT.value)),
                  endTime: setController.mondayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.mondayT.value)
                  .add(Duration(seconds: 3600))),
                  valueChanger: setController.monday.value,
                  toggle: (tog)=>setController.monday(tog),
                  showTime: ()async{
                  final pickTime = await showTimePicker(
                  cancelText: "Back",
                  confirmText: "Select Time",
                  helpText: "Input Time",
                  initialEntryMode: TimePickerEntryMode.input,
                  context: context, initialTime: TimeOfDay.now());
                  setController.mondayT(pickTime!.hour.toString()+":"+pickTime.minute.toString());
                  },
                  setME: ()=>setController.setAvalabilityCoach(
                    dayNumber: "1",
                    timonly: setController.mondayT.value
                  )
                ),
                SizedBox(height: 15,),
                //Tuesday
                ShowAvalabiltyBeforeSave(
                  weekName: "Tuesday",
                  startTime: setController.tuesdayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.tuesdayT.value)),
                  endTime: setController.tuesdayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.tuesdayT.value)
                  .add(Duration(seconds: 3600))),
                  valueChanger: setController.tuesday.value,
                  toggle: (tog)=>setController.tuesday(tog),
                  showTime: ()async{
                  final pickTime = await showTimePicker(
                  cancelText: "Back",
                  confirmText: "Select Time",
                  helpText: "Input Time",
                  initialEntryMode: TimePickerEntryMode.input,
                  context: context, initialTime: TimeOfDay.now());
                  setController.tuesdayT(pickTime!.hour.toString()+":"+pickTime.minute.toString());
                  },
                  setME: ()=>setController.setAvalabilityCoach(
                    dayNumber: "2",
                    timonly: setController.tuesdayT.value
                  )
                ),
                SizedBox(height: 15,),
                //Wednesday
                ShowAvalabiltyBeforeSave(
                  weekName: "Wednesday",
                  startTime: setController.wednesdayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.wednesdayT.value)),
                  endTime: setController.wednesdayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.wednesdayT.value)
                  .add(Duration(seconds: 3600))),
                  valueChanger: setController.wednesday.value,
                  toggle: (tog)=>setController.wednesday(tog),
                  showTime: ()async{
                  final pickTime = await showTimePicker(
                  cancelText: "Back",
                  confirmText: "Select Time",
                  helpText: "Input Time",
                  initialEntryMode: TimePickerEntryMode.input,
                  context: context, initialTime: TimeOfDay.now());
                  setController.wednesdayT(pickTime!.hour.toString()+":"+pickTime.minute.toString());
                  },
                  setME: ()=>setController.setAvalabilityCoach(
                    dayNumber: "3",
                    timonly: setController.wednesdayT.value
                  )
                ),
                SizedBox(height: 15,),
                //Thursday
                 ShowAvalabiltyBeforeSave(
                  weekName: "Thursday",
                  startTime: setController.thurdayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.thurdayT.value)),
                  endTime: setController.thurdayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.thurdayT.value)
                  .add(Duration(seconds: 3600))),
                  valueChanger: setController.thursday.value,
                  toggle: (tog)=>setController.thursday(tog),
                  showTime: ()async{
                  final pickTime = await showTimePicker(
                  cancelText: "Back",
                  confirmText: "Select Time",
                  helpText: "Input Time",
                  initialEntryMode: TimePickerEntryMode.input,
                  context: context, initialTime: TimeOfDay.now());
                  setController.thurdayT(pickTime!.hour.toString()+":"+pickTime.minute.toString());
                  },
                  setME: ()=>setController.setAvalabilityCoach(
                    dayNumber: "4",
                    timonly: setController.thurdayT.value
                  )
                ),
                SizedBox(height: 15,),
                //Friday
                 ShowAvalabiltyBeforeSave(
                  weekName: "Friday",
                  startTime: setController.fridayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.fridayT.value)),
                  endTime: setController.fridayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.fridayT.value)
                  .add(Duration(seconds: 3600))),
                  valueChanger: setController.friday.value,
                  toggle: (tog)=>setController.friday(tog),
                  showTime: ()async{
                  final pickTime = await showTimePicker(
                  cancelText: "Back",
                  confirmText: "Select Time",
                  helpText: "Input Time",
                  initialEntryMode: TimePickerEntryMode.input,
                  context: context, initialTime: TimeOfDay.now());
                  setController.fridayT(pickTime!.hour.toString()+":"+pickTime.minute.toString());
                  },
                  setME: ()=>setController.setAvalabilityCoach(
                    dayNumber: "5",
                    timonly: setController.fridayT.value
                  )
                ),
                SizedBox(height: 15,),
                ShowAvalabiltyBeforeSave(
                  weekName: "Saturday",
                  startTime: setController.saturdayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.saturdayT.value)),
                  endTime: setController.saturdayT.isEmpty ? ""
                  : DateFormat.jm().format(DateFormat("hh:mm").parse(setController.saturdayT.value)
                  .add(Duration(seconds: 3600))),
                  valueChanger: setController.saturday.value,
                  toggle: (tog)=>setController.saturday(tog),
                  showTime: ()async{
                  final pickTime = await showTimePicker(
                  cancelText: "Back",
                  confirmText: "Select Time",
                  helpText: "Input Time",
                  initialEntryMode: TimePickerEntryMode.input,
                  context: context, initialTime: TimeOfDay.now());
                  setController.saturdayT(pickTime!.hour.toString()+":"+pickTime.minute.toString());
                  },
                  setME: ()=>setController.setAvalabilityCoach(
                    dayNumber: "6",
                    timonly: setController.saturdayT.value
                  )
                ),
                SizedBox(height: 15,),
                
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}