import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/api/country/country_get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/model/country.dart';
import 'package:wellnest/pages/profile/controller/save_Controller.dart';
import 'package:wellnest/pages/profile/create_service/page_view/main_page.dart';
import 'package:wellnest/pages/profile/create_service/page_view/success_page.dart';
import 'package:wellnest/pages/profile/models/category_choose.dart';
import 'package:wellnest/pages/profile/models/category_class.dart';
import 'package:wellnest/pages/profile/models/inclussion.dart';
import 'package:wellnest/pages/profile/models/inclussion_class.dart';
import 'package:wellnest/standard_widgets/loading.dart';
import 'package:intl/intl.dart';

class CreateServiceController extends GetxController {
  @override
  void onInit() {
    // GetCountries.loadCountry();
    getAllCoachCateg();
    getInclussion();
    getCountryTry();
    super.onInit();

    price!.text = '0.0';
    discount!.text = '0';
    numberSession!.text = '0';
  }

  final image = "".obs;
  final fileImagebanner = "".obs;
  final currencyPick = "".obs;
  final numberSessionText = 0.obs;
  final priceText = 0.0.obs;
  final discountText = 0.obs;
  final formKey = GlobalKey<FormState>();

  TextEditingController? serviceName = TextEditingController();
  TextEditingController? description = TextEditingController();
  TextEditingController? link = TextEditingController();
  final stringtitle = "".obs;

  validatePagesMain() {
    if (serviceName!.value.text.isEmpty ||
        description!.value.text.isEmpty ||
        link!.value.text.isEmpty) {
      LoadingOrError.snackBarMessage(
          title: "Please Don't Leave some blanks",
          message: "Please Fill up all.",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    } else if (listInclu.isEmpty) {
      LoadingOrError.snackBarMessage(
          title: "Please Pick Inlussion!",
          message: "Choose at least 1 or more.",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    } else if (selectedItem.isEmpty) {
      LoadingOrError.snackBarMessage(
          title: "Please Select for Category",
          message: "Please Fill up all.",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    } else if (coaching.isTrue) {
      if (fileText0.isEmpty) {
        LoadingOrError.snackBarMessage(
            title: "Please Upload your File",
            message: "Please Fill up all.",
            icon: FaIcon(
              FontAwesomeIcons.exclamation,
              color: WellNestColor.wncWhite,
            ));
      } else {
        forwardtoCoach();
      }
    } else if (training.isTrue) {
      if (fileText1.isEmpty) {
        LoadingOrError.snackBarMessage(
            title: "Please Upload your File",
            message: "Please Fill up all.",
            icon: FaIcon(
              FontAwesomeIcons.exclamation,
              color: WellNestColor.wncWhite,
            ));
      } else {
        forwardtoCoach();
      }
    } else {
      forwardtoCoach();
    }
  }

  changeOnTextServiceName(String? value) {
    stringtitle(value!);
  }

  Future pickImage() async {
    try {
      final imagePick =
          await ImagePicker().pickImage(source: ImageSource.gallery);
      if (imagePick != null) {
        image.value = imagePick.path;
      } else {
        print("Error encountered!");
      }
    } on PlatformException catch (e) {
      print("Error encountered!");
    }
    return image;
  }

  final video = "".obs;

  Future pickVideo() async {
    try {
      final videoPick =
          await ImagePicker().pickVideo(source: ImageSource.gallery);
      if (videoPick != null) {
        video.value = videoPick.name;
      } else {
        print("Error encountered!");
      }
    } on PlatformException catch (e) {
      print("Error encountered!");
    }
  }

  final fileText0 = "".obs;
  final fileText1 = "".obs;
  final fileCoach = "".obs;
  final fileTraining = "".obs;

  void pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom, allowedExtensions: ['pdf', 'doc', '.word']);

    if (result != null) {
      File file = File(result.files.single.name);
      PlatformFile pickIt = result.files.first;
      fileCoach(pickIt.path);
      print(fileCoach);
      fileText0(file.toString());
    } else {
      print("user cancel");
    }
  }

  void pickFile1() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom, allowedExtensions: ['pdf', 'doc', '.word']);

    if (result != null) {
      File file = File(result.files.single.name);
      PlatformFile pickIt = result.files.first;
      fileTraining(pickIt.path);
      fileText1(file.toString());
    } else {
      print("user cancel");
    }
  }

  final coaching = false.obs;
  final training = false.obs;

  void changeCoachingCheck() {
    coaching(!coaching.value);
    if (coaching.value == true) {
      training(false);
    }
  }

  void changeTraingCheck() {
    training(!training.value);
    if (training.value == true) {
      coaching(false);
    }
  }

  //Inclussions List

  final listInclussion = <Inclusion>[].obs;

  getInclussion() async {
    try {
      final listInlcud = await ApiCoach.getCoachInclussion();
      if (listInlcud != null) {
        listInclussion.assignAll(listInlcud);
      }
    } finally {}
  }

  final List<Inclussion> listInclu = <Inclussion>[].obs;

  addInclusion(id, name) {
    if (isItemAlreadyAdded(name)) {
      print("exist");
    } else {
      var data = {"id": id, "name": name};
      listInclu.add(Inclussion(id: id, name: name));
      print(data['id']);
    }
  }

  removeIt(RxString? name) {
    listInclu.removeWhere((element) => element.name.obs == name!);
  }

  bool isItemAlreadyAdded(String? inclu) =>
      listInclu.where((element) => element.name == inclu).isNotEmpty;

  //  Rx<Datum> mentalHealth =Datum().obs;

  List<Datum> get categories => listCategory;

  var selectedItem = "".obs;

  changeValu(String? val) {
    // selectedItem.value = val!;
    selectedItem(val!);
    // print("Id is $selectedItem");
  }

  final pageController = PageController();
  var selectedPage = 0.obs;

  void forwardtoCoach() {
    if (coaching.value == true) {
      pageController.animateToPage(1,
          duration: 300.milliseconds, curve: Curves.fastLinearToSlowEaseIn);
    } else if (training.value == true) {
      pageController.animateToPage(2,
          duration: 300.milliseconds, curve: Curves.fastLinearToSlowEaseIn);
    }
    print("None");
  }

  //Coaching

  final time = "".obs;
  TextEditingController? numberSession = TextEditingController();
  TextEditingController? price = TextEditingController();
  TextEditingController? discount = TextEditingController();
  TextEditingController? maximum = TextEditingController();

  final currencyMoney = "".obs;
  final agreeTerms = false.obs;

  aggre(b) {
    agreeTerms(b);
  }

  validateSecondPageCoaching() {
    if (numberSession!.value.text.isEmpty ||
        price!.value.text.isEmpty ||
        discount!.value.text.isEmpty) {
      LoadingOrError.snackBarMessage(
          title: "Please Don't Leave some blanks",
          message: "Please Fill up all.",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    }
    // else if(currencyMoney.isEmpty){
    //   LoadingOrError.snackBarMessage(
    //       title: "Please Choose what currency.",
    //       message: "Please Fill up all.",
    //       icon: FaIcon(FontAwesomeIcons.exclamation,color: WellNestColor.wncWhite,)
    //     );
    // }
    else if (agreeTerms.isTrue) {
      gotoCancelation();
    } else {
      LoadingOrError.snackBarMessage(
          title: "Please Agree Our Terms and Conditions",
          message: "Please Check the box.",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    }
  }

  validatedTrainingPage() {
    if (price!.value.text.isEmpty ||
        maximum!.value.text.isEmpty ||
        discount!.value.text.isEmpty) {
      LoadingOrError.snackBarMessage(
          title: "Please Don't Leave some blanks",
          message: "Please Fill up all.",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    }
    // else if(currencyMoney.isEmpty){
    //   LoadingOrError.snackBarMessage(
    //       title: "Please Choose what currency.",
    //       message: "Please Fill up all.",
    //       icon: FaIcon(FontAwesomeIcons.exclamation,color: WellNestColor.wncWhite,)
    //     );
    // }
    else if (agreeTerms.isTrue) {
      gotoCancelation();
    } else {
      LoadingOrError.snackBarMessage(
          title: "Please Agree Our Terms and Conditions",
          message: "Please Check the box.",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    }
  }

  gotoSuccess() async {
    // if(selectedString.isEmpty){
    //   LoadingOrError.snackBarMessage(
    //       title: "Please Choose Cancellation Day",
    //       message: "Please Fill up all.",
    //       icon: FaIcon(FontAwesomeIcons.exclamation,color: WellNestColor.wncWhite,)
    //     );
    // }else
    if (image.isEmpty) {
      LoadingOrError.snackBarMessage(
          title: "Please Put A Banner image for your Service",
          message: "Upload a banner",
          icon: FaIcon(
            FontAwesomeIcons.exclamation,
            color: WellNestColor.wncWhite,
          ));
    } else {
      if (coaching.isTrue) {
        var data = {
          "servicename": serviceName!.value.text,
          "description": description!.value.text,
          "category": selectedItem.toString(),
          "link": link!.value.text,
          "type": "Coaching",
          "imagePath": image,
          "coach_file": fileCoach,
          "numberSession": numberSession!.value.text,
          "price": price!.value.text,
          "discount": discount!.value.text,
          "canceldays": selectedString.toString().isEmpty
              ? 0
              : int.parse(selectedString.toString().substring(0, 1)),
          "time": Duration(hours: 1).inSeconds,
          "refund": norefund.isFalse ? false : norefund
        };

        // print("$data $listInclu");
        var result = await ApiCoach.coachCreateService(
            data: data, listInclussion: listInclu);

        if (result == 200) {
          print("Status Result is $result");
          gotoCancelationx();
        }
      } else {
        var dataTraining = {
          "servicename": serviceName!.value.text,
          "description": description!.value.text,
          "category": selectedItem,
          "link": link!.value.text,
          "type": "Training",
          "imagePath": image,
          "training_file": fileTraining,
          "price": price!.value.text,
          "numberSession": 1,
          "maximun": maximum!.value.text,
          "discount": discount!.value.text,
          "time": Duration(hours: 1).inSeconds,
          "canceldays": selectedString.toString().isEmpty
              ? 0
              : int.parse(selectedString.toString().substring(0, 1)),
          "refund": norefund.isFalse ? false : norefund
        };

        print(dataTraining);

        var result = await ApiCoach.coachTrainingServices(
            data: dataTraining, listInclussion: listInclu);

        if (result == 200) {
          print("Status Result is $result");
          gotoCancelationx();
        }
      }
    }
  }

  TextEditingController? searchCurreny = TextEditingController();
  final List<Country> country = <Country>[].obs;
  final List<Country> countrySearch = <Country>[].obs;

  filteredSearchCurrency(String? query) {
    if (query!.isNotEmpty) {
      // country.addAll(country.where((e) => e.currency!.contains(query.toUpperCase())));
      countrySearch.assignAll(
          country.where((e) => e.currency!.contains(query.toUpperCase())));
    } else {
      getCountryTry();
    }
  }

  getCountryTry() async {
    try {
      final response = await GetCountries.loadAssets();
      if (response != null) {
        country.assignAll(response);
        print(country);
      }
    } finally {}
  }

  final flagToShow = "".obs;
  final selectedString = "".obs;
  final norefund = false.obs;

  getWreck(String? val) {
    selectedString(val);
    print(selectedString.value);
  }

  showFlag(String? val, String? currency) {
    flagToShow(val!.toLowerCase());
    print("Money is $currency!");
    currencyMoney(currency);
    currencyPick(currency);
    Get.back();
  }

  final RxList<String> listcancelCoaching = ['1 day', '4 days', '7 days'].obs;
  final RxList<String> listcancelTraining = ['5 days', '7 days', '10 days'].obs;

  gotoCancelation() {
    pageController.animateToPage(3,
        duration: 300.milliseconds, curve: Curves.fastLinearToSlowEaseIn);
  }

  gotoCancelationx() {
    pageController.animateToPage(0,
        duration: 300.milliseconds, curve: Curves.fastLinearToSlowEaseIn);
    Get.to(() => SuccessServiceCreation());
  }

  final List<Datum> listCategory = <Datum>[].obs;

  getAllCoachCateg() async {
    try {
      final category = await ApiCoach.getAllCoachCategory();
      if (category != null) {
        listCategory.assignAll(category);
      }
    } finally {
      listCategory.map((e) => selectedItem(e.id)).first;
    }
  }

  rightIcon(String? string) {
    switch (string) {
      case "Mental Health":
        return WellnestAsset.mental_health;
      case "Performance":
        return WellnestAsset.perfomace;
      case "Relationship":
        return WellnestAsset.relationship;
      case "Lifestyle":
        return WellnestAsset.lifeStyle1;
      case "Spiritual":
        return WellnestAsset.spiritual1;
      case "Financial":
        return WellnestAsset.fianancial;
    }
  }
}
