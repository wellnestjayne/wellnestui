import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/api/api_service/api_coaches/api_get_all_coach.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/coach_user_detail.dart';
import 'package:wellnest/model/coash_services.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/profile/models/userCoach.dart';

class ViewOwnProfileController extends GetxController {
  final PageController? pageController = PageController();
  final mainController = Get.find<MainController>();
  var selectedNumber = 0.obs;
  var photoInt = 0.obs;
  final List<Data> coachDatas = <Data>[].obs;
  @override
  void onInit() {
    getCoachProfiledata();
    getCoachAllService();
    super.onInit();
  }

  List<Color> colors = <Color>[WellNestColor.wncOrange, WellNestColor.wncPeach, WellNestColor.wncLightmoreAque];

  final randomColorPick = Random();

  Color colorRandom() => colors[randomColorPick.nextInt(3)];

  // final List<CoachUserDetails> coachDetails = <CoachUserDetails>[].obs;
  final List<String?> netWorkImage = <String?>[].obs;
  final List<String?> flags = <String?>[].obs;
  final List<String?> specials = <String?>[].obs;
  final List<Service> serviceList = <Service>[].obs;
  final forNameService = ''.obs;

  getCoachProfiledata() async {
    try {
      var listCoach = await ApiCoach.getCoachProfileById();
      if (listCoach != null) {
        coachDatas.assign(listCoach);
        print(coachDatas);
      }
    } finally {
      //print(photoInt);
      coachDatas.map((e) => e.photoUrls!.map((e) => netWorkImage.add(e)).toList()).toList();
      coachDatas.map((e) => e.nationalityTags!.map((e) => flags.add(e)).toList()).toList();
      forNameService(coachDatas.map((e) => e.name).first);
      // print(coachDatas.map((e) => e.nationalityTags!.map((e) => flags.add(e)).toList()).toList());
    }
  }

  getCoachAllService() async {
    try {
      final result = await CoachGetLists.getAllServicesOfCoach(SharePref.getCoachId());
      if (result != null) {
        serviceList.assignAll(result);
      }
    } finally {}
  }

  final List<Service> selectedCoach = <Service>[].obs;

  getSelected(String? serviceId) {
    final highFive = serviceList.where((e) => e.id!.contains(serviceId!)).toList();
    highFive.map((e) => selectedCoach.assign(e)).toList();
  }

  @override
  void onClose() {
    //ViewOwnProfileController().dispose();
    super.onClose();
  }
}
