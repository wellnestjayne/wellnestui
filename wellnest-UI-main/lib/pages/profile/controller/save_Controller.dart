
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class SaveControllerService extends GetxController{

  final createControllerService = Get.find<CreateServiceController>();

  
  validatePagesMain(){

      if(createControllerService.serviceName!.value.text.isEmpty || 
        createControllerService.description!.value.text.isEmpty || 
        createControllerService.link!.value.text.isEmpty){
         LoadingOrError.snackBarMessage(
          title: "Please Don't Leave some blanks",
          message: "Please Fill up all.",
          icon: FaIcon(FontAwesomeIcons.exclamation,color: WellNestColor.wncWhite,)
        );
      } else  if(createControllerService.listInclu.isEmpty){
        LoadingOrError.snackBarMessage(
          title: "Please Pick Inlussion!",
          message: "Choose at least 1 or more.",
          icon: FaIcon(FontAwesomeIcons.exclamation,color: WellNestColor.wncWhite,)
        );
      } else if(createControllerService.selectedItem.isEmpty){
        LoadingOrError.snackBarMessage(
          title: "Please Select for Category",
          message: "Please Fill up all.",
          icon: FaIcon(FontAwesomeIcons.exclamation,color: WellNestColor.wncWhite,)
        );
      }else if(createControllerService.coaching.isTrue){
        if(createControllerService.fileText0.isEmpty){
          LoadingOrError.snackBarMessage(
          title: "Please Upload your File",
          message: "Please Fill up all.",
          icon: FaIcon(FontAwesomeIcons.exclamation,color: WellNestColor.wncWhite,)
        );
        }
      }else if(createControllerService.training.isTrue){
        if(createControllerService.fileText1.isEmpty){
          LoadingOrError.snackBarMessage(
          title: "Please Upload your File",
          message: "Please Fill up all.",
          icon: FaIcon(FontAwesomeIcons.exclamation,color: WellNestColor.wncWhite,)
        );
        }
      }
    }


}