

import 'dart:math';

import 'package:get/get.dart';
import 'package:wellnest/pages/profile/dateutil/date_util.dart';
import 'package:wellnest/pages/profile/models/dafualt_dailyStat.dart';

class StatisticController extends GetxController{


  final todaystate = "".obs;
  final currentWeek = "".obs;

  final dailyStatList1 = (List<DailyStatUIModel>.of([])).obs;

  final displayNextWeekbtn = false.obs;

  int maxSelection = -1;
  int maxSelection2 = -1;
  int maxSelection3 = -1;

  DateTime? selectdDate = DateTime.now();
  DateTime? currentDate = DateTime.now();


  @override
  void onInit() {
    setCurrentWeek();
    super.onInit();
  }


  void resetMaxValue(){
    maxSelection = -1;
    maxSelection2 = -1;
    maxSelection3= -1;
  }

  void setCurrentWeek() async{
    selectdDate = DateTime.now();
    currentWeek.value = getWeekDisplayDate(selectdDate);
    getDailyStatList(selectdDate);
  }

  void setPreviousWeek(){
    selectdDate = selectdDate!.subtract(Duration(days: 7));
    setNextWeekButtonVisibility();
   currentWeek.value = getWeekDisplayDate(selectdDate);
    getDailyStatList(selectdDate);
  }

  void setNextWeek(){
    selectdDate = selectdDate!.add(Duration(days: 7));
    setNextWeekButtonVisibility();
   currentWeek.value = getWeekDisplayDate(selectdDate);
    getDailyStatList(selectdDate);
  }

  void setNextWeekButtonVisibility(){
    displayNextWeekbtn.value = !selectdDate!.isSameDate(currentDate);
  }

  String getWeekDisplayDate(DateTime? dateTime)
  =>'${AppDateDetails.firstDateofWeek(dateTime)!.toFormatStrig('dd MMM')} - ${AppDateDetails.lastDateofWeek(dateTime)!.toFormatStrig('dd MMM')}';

  Future<void> getDailyStatList(DateTime? dateTime)async{
    resetMaxValue();
    var daysInWeek = AppDateDetails.getDaysinWeek(dateTime!);

    List<DailyStatUIModel> selectionList1 = List.filled(7, default_dailyStaet);

    var today = DateTime.now();
    var todayPosition = DateTime.now().weekday -1;

    for(var i = 0 ; i <=6 ; i++){
      var date = daysInWeek[i];
      var randomStat = randomInt(100);
      selectionList1[i] = DailyStatUIModel(
        day: date.toFormatStrig('EEE'),
        stat: randomStat,
        isToday: today.isSameDate(date),
        isSelecgted: todayPosition ==i,
        dayPosition: i
      );
      if(maxSelection < randomStat){
        maxSelection = randomStat;
      }
    }
    dailyStatList1.assignAll(selectionList1);
  }

  int randomInt(int max)
  =>Random().nextInt(100) +1;

  void setselectedDayPosition(int position,int sectionNumber){
    switch(sectionNumber){
      case 1 : 
      {
        dailyStatList1.assignAll(getDailyListWithSelectedDay(
          dailyStatList1.call(),
          position));
        break;
      }
    }
  }
  

  List<DailyStatUIModel> getDailyListWithSelectedDay(
    List<DailyStatUIModel> listsX, int position
  )=> listsX.map((e)=> e.copyWith(
    isSelecgted: e.dayPosition == position,
  )).toList();

  double getStatPercentage(int? time)
  =>getStatPercentageList1(time!);

  double getStatPercentageList1(int time){
    if(time == 0){
      return 0;
    }else{
      return time/ maxSelection;
    }
  }

  void onNextWeek(){
    setNextWeek();
  }
  void onPreviosWeek(){
    setPreviousWeek();
  }

}