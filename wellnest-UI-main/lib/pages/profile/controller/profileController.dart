import 'package:get/get.dart';
import 'package:wellnest/api/services/update.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/profile/edit_profile/edit_profile.dart';
import 'package:wellnest/pages/profile/models/menuItems.dart';
import 'package:wellnest/pages/profile/view_profile/view_profile.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class ProfileController extends GetxController {
  final letter = SharePref.detailData['firstName'];
  final profileShow = SharePref.getType();
  final mainController = Get.find<MainController>();

  final value = false.obs;

  functionBool() {
    value(!value.value);
  }

  void selectedItem(items) async {
    switch (items) {
      case MenuItems.dashboard:
        Get.back(id: mainController.select.value);
        break;
      case MenuItems.viewProfile:
        // print("View Profile");
        Get.to(() => ViewProfileUser(), id: mainController.select.value);
        break;
      case MenuItems.editProfile:
        print("Edit Profile");
        Get.to(() => EditProfileCoach(), id: mainController.select.value);
        break;
      case MenuItems.switchProfile:
         LoadingOrError.dialogWithNorm(
           title: "Switch to User",
           message: "Do you want to switch account.",
           yes: ()=>switchUser(),
           no: ()=>Get.back()
         );
        break;
      case MenuItems.switchLogout:
        return LoadingOrError.logoutDialog(
          () => mainController.logoutinAnyPages(),
        );
    }
  }

  switchUser() async {
    await SharePref.setType("User");
    Get.keys.clear();
    Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
  }

  back() {
    final typeUser = SharePref.typeUser;
    Get.back(result: typeUser);
  }

  switchProfile() {
    final type = SharePref.getType();
    if (type == "user") {
      UpdateCognitoUser.updateCognito("coach");
    } else {
      UpdateCognitoUser.updateCognito("user");
    }
  }
}
