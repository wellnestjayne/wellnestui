import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/api/country/country_get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/country.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/profile/models/category.dart';
import 'package:wellnest/pages/profile/models/category_class.dart';
import 'package:wellnest/pages/profile/models/userCoach.dart';

class EditProfessionalProfile extends GetxController {
  final mainController = Get.find<MainController>();
  final name = TextEditingController();
  final shortDesc = TextEditingController();
  final longDesc = TextEditingController();
  TextEditingController? searchCurreny = TextEditingController();

  final hintTextDefault = SharePref.detailData['firstName'] + " " + SharePref.detailData['lastName'];

  List<String?> tagStrings = <String>[].obs;
  List<String?> photoUrls = <String>[].obs;
  @override
  void onInit() {
    // TODO: implement onInit
    getCoachProfiledata();
    super.onInit();
  }

  final List<Data> coachDatas = <Data>[].obs;
  getCoachProfiledata() async {
    try {
      var listCoach = await ApiCoach.getCoachProfileById();
      if (listCoach != null) {
        coachDatas.assign(listCoach);
      }
    } finally {
      coachDatas.map((e) {
        name.text = e.name!;
        shortDesc.text = e.shortDescription!;
        longDesc.text = e.longDescription!;
        e.photoUrls!.map((e) => photoUrls.add(e)).toList();
        e.tags!.map((e) => tagStrings.add(e)).toList();
        e.nationalityTags!.map((e) => viewFlagSelected.add(e)).toList();
      }).toList();
    }
  }

  final addTag = TextEditingController();
  final randomColorPick = Random();
  final List<String> viewFlagSelected = <String>[].obs;

  List<Color> colors = <Color>[WellNestColor.wncOrange, WellNestColor.wncPeach, WellNestColor.wncLightmoreAque];
  Color colorRandom() => colors[randomColorPick.nextInt(3)];

  addItemString() {
    if (addTag.value.text == "") {
      Get.snackbar("Please Input a tag", "We implore you to type your own tag for yourself.",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
    } else if (isItemStringisExist(addTag.value.text)) {
      Get.snackbar("Add Tag", "Tag already added!",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
    } else {
      Get.snackbar("Tag Added", "${addTag.value.text} has been added",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
      tagStrings.add(addTag.value.text);
    }
  }

  removeStringtag(String? string) => tagStrings.removeWhere((element) => element == string!);
  bool isItemStringisExist(String? string) => tagStrings.where((e) => e == string!).isNotEmpty;
  bool isCountryExists(String? string) => viewFlagSelected.where((e) => e.contains(string!)).isNotEmpty;

  getAddString(String? code, String? name) {
    if (isCountryExists(code!)) {
      Get.snackbar("Country Tag Added", "$name has been added.",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
    } else {
      Get.snackbar("Country Tag", "Country Tag already added!",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
      viewFlagSelected.add(code.toUpperCase());
    }
  }

  final List<Country> country = <Country>[].obs;
  final List<Country> countrySearch = <Country>[].obs;

  removeCountry(String? string) => viewFlagSelected.removeWhere((e) => e.contains(string!));

  filteredSearchCurrency(String? query) {
    if (query!.isNotEmpty) {
      countrySearch.assignAll(country.where((e) => e.name!.contains(query)));
    } else {
      getCountryTry();
    }
  }

  getCountryTry() async {
    try {
      final response = await GetCountries.loadAssets();
      if (response != null) {
        country.assignAll(response);
      }
    } finally {}
  }

  editProfile() async {
    var data = {
      "userId": SharePref.detailData['id'],
      "name": name.value.text.isEmpty ? hintTextDefault : name.value.text,
      "shortDescription": shortDesc.value.text,
      "longDescription": longDesc.value.text,
      "tags": tagStrings,
      "nationalityTags": viewFlagSelected,
      "isVerified": true,
      "photoUrls": [],
      "id": SharePref.getCoachId(),
    };
    print(data);
    await ApiCoach.createOrEditCoach(data, 2);
  }
}
