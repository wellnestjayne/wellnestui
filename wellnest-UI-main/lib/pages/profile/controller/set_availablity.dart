import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/standard_widgets/loading.dart';
class SetAvailabilityController extends GetxController{

  
  final
  onlyWeekDays = false.obs, 
  monday = false.obs,
  tuesday = false.obs,
  wednesday = false.obs,
  thursday = false.obs,
  friday = false.obs,
  saturday = false.obs,
  sunday = false.obs, 
  mondayap = false.obs,
  tuesdayap = false.obs,
  wednesdayap = false.obs,
  thursdayap = false.obs,
  fridayap = false.obs,
  saturdayap = false.obs,
  sundayap = false.obs ;
  
  final sundayT = "".obs;
  final mondayT = "".obs;
  final tuesdayT = "".obs;
  final wednesdayT = "".obs;
  final thurdayT = "".obs;
  final fridayT = "".obs;
  final saturdayT = "".obs;

  @override
  void onInit() {
    super.onInit();
  }

  makeWeekDaysOnly(given){
    monday(given);
    tuesday(given);
    wednesday(given);
    thursday(given);
    friday(given);
    onlyWeekDays(given);
  }

  setAvalabilityCoach({String? timonly,String? dayNumber}) async{
    final DateTime wow = DateFormat('HH:mm').parse("$timonly");
    var  data  = {
          "coachId" : SharePref.getCoachId(),
          "day": dayNumber,
          "startTime" : DateFormat.Hm().format(wow).toString(),
          "endTime": DateFormat.Hm().format(wow.add(Duration(seconds: 3600))).toString(),
          "isActive" : true
    };
    print(data);
    try{
      final result = await ApiCoach.setCoachAvalability(data: data);
      if(result == 200){
        LoadingOrError.snackBarMessage(
          title: "Success",
          message:"Set Avalability",
          icon: Container(),
        );
      }
    }finally{
    }
  }

    setEnd(endTime){
      
    }

     onDaySelect(context,data) async {

      //    final TimeOfDay? pickTime  = await showTimePicker(
      // cancelText: "Back",
      // confirmText: "Select Time",
      // helpText: DateFormat("EEEE, MMMM d y").format(selectedDay.value),
      // initialEntryMode: TimePickerEntryMode.input,
      // context: context, initialTime: TimeOfDay.now());
      // final dateOnly = DateFormat("yyyy-MM-dd").format(selectedDay.value);
      // if(pickTime != null){
      //   final timonly = pickTime.hour.toString()+":"+pickTime.minute.toString();
      //   final DateTime wow = DateFormat('HH:mm').parse("$timonly");

      //   print(DateFormat.Hm().format(wow));
      //  print(DateFormat.Hm().format(wow.add(Duration(seconds: 3600))));
      // var  data  = {
      //     "coachId" : SharePref.getCoachId(),
      //     "day": 0+availabilty.map((e) => e).length,
      //     "startTime" : DateFormat.Hm().format(wow).toString(),
      //     "endTime": DateFormat.Hm().format(wow.add(Duration(seconds: 3600))).toString(),
      //     "date" : DateFormat("EEEE, MMMM d y").format(selectedDay.value),
      //     "isActive" : true
      // };
      // print(data);
      // availabilty.add(data);
      // print(availabilty.map((e) => e).toList());
     // }
   } 

}