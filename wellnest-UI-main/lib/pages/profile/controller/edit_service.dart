import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/api/api_service/api_coaches/api_get_all_coach.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/coash_services.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';
import 'package:wellnest/pages/profile/edit_service/widgets/success_dialog.dart';
import 'package:wellnest/route/route_page.dart';

class EditServiceController extends GetxController {
  final viewProfile = Get.find<ViewOwnProfileController>();

  TextEditingController? serviceName = TextEditingController();
  TextEditingController? description = TextEditingController();
  TextEditingController? numberOfSession = TextEditingController();
  TextEditingController? price = TextEditingController();
  TextEditingController? discount = TextEditingController();
  TextEditingController? maximum = TextEditingController();

  final id = "".obs;
  final stringtitle = "".obs;
  final type = "".obs;
  final category = "".obs;
  final time = "".obs;
  final norefund = false.obs;
  final imageFile = "".obs;
  late final imageUrl = "".obs;

  changeOnTextServiceName(String? value) {
    stringtitle(value!);
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getCoachAllService();
  }

  Future pickImage() async {
    try {
      final imagePick =
          await ImagePicker().pickImage(source: ImageSource.gallery);
      if (imagePick != null) {
        imageFile.value = imagePick.path;
      } else {
        print("Error encountered!");
      }
    } on PlatformException catch (e) {
      print("Error encountered!");
    }
    return imageFile;
  }

  final List<Service> serviceList = <Service>[].obs;

  getCoachAllService() async {
    final myCoachId = SharePref.getCoachId();
    try {
      final result = await CoachGetLists.getAllServicesOfCoach(myCoachId);
      if (result != null) {
        serviceList.assignAll(result);
      }
    } finally {
      print('Service List ${serviceList.map((e) => e.name).toList()}');
    }
  }

  final List<Service> selectedServices = <Service>[].obs;

  getSelected(String? serviceId) {
    try {
      final highFive =
          serviceList.where((e) => e.id!.contains(serviceId!)).toList();
      highFive.map((e) => selectedServices.assign(e)).toList();
    } finally {
      print('Selected Service ${selectedServices.map((e) => e.name).toList()}');
      selectedServices.map((e) {
        serviceName!.text = e.name!;
        description!.text = e.description!;
        numberOfSession!.text = e.numberOfSessions.toString();
        time(numberOfSession!.text);
        price!.text = e.price.toString();
        serviceName!.text = e.name!;
        discount!.text = e.discount.toString();
        imageUrl.value = e.bannerUrl!;
        category.value = e.categoryId!;
        type.value = e.type!;
        norefund.value = e.canRefund!;
        id.value = e.id!;
      }).toList();
    }
  }

  updateServices(serviceId) async {
    var data = {
      "serviceName": serviceName!.value.text,
      "description": description!.value.text,
      "type": type.value,
      "category": category.value,
      "imagePath": imageUrl.value,
      "imageFile": imageFile.value,
      "numberSession": numberOfSession!.value.text,
      "price": price!.value.text,
      "discount": discount!.value.text,
      "time": Duration(hours: 1).inSeconds,
      "refund": norefund.isFalse ? false : norefund,
      "id": id,
    };
    // print("$data $listInclu");
    var result = await ApiCoach.coachEditServices(
      data: data,
      //listInclussion: listInclu
    );
    print('Update Services Success! $result');
    if (result == 200) {
      getCoachAllService();
      ServiceDialog.successUpdate(press: () {
        imageCache!.clear();
        Get.keys.clear();
        Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
      });
    }
  }
}
