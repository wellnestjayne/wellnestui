

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_coaches/api_get_upcomming_session.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/model/coach_view_upcomming_session.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/profile/models/session_trash.dart';
import 'package:wellnest/pages/profile/upcomming_session/dialog/dialog.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class UpcomingController extends GetxController{
  final maincontroller = Get.find<MainController>();
  @override
  void onInit() {
    super.onInit();
    getAllUpcommingSessions();
  }

  final List<UpcommingSession> upcomingList = <UpcommingSession>[].obs;
  getAllUpcommingSessions() async{
    try{
      final result = await ApiCoachUpcommingSession.getAllupcommingSessionwithFilter();
      upcomingList.assignAll(result);
    }finally{
      // print(upcomingList.map((e) => e.startTime).toList());
    }
  }

  makeConfirmBooking({String? id})async{
    LoadingOrError.loading();
    try{
      final result = await ApiCoachUpcommingSession.getConfirmBook(id: id!);
      if(result == 200){
        this.getAllUpcommingSessions();
        Get.back();
        SessionDialog.sessionBooking(
          booking: ()=>Get.back(),
          bookings: (){},
          cont: "",
          cancelShow: false,
          imagePath: WellnestAsset.approvedSesion,
          donx: true,
          message: "Congrats! Your Session has been confirmed!",
      );
      
      }else{
        Get.back();
        LoadingOrError.errorloading(result['responseException']['message'], "Try Again!");
      }
    }finally{

    }
  }

  makeCancelBooking({String? id}) async{
     Get.back();
     LoadingOrError.loading();
    try{
      final result = await ApiCoachUpcommingSession.getCancelBook(id: id!);
      if(result == 200){
        this.getAllUpcommingSessions();
        Get.back();
        LoadingOrError.snackBarMessage(
          title: "Booking Decline Successfull",
          message: "Booking Session Declined",
          icon: FaIcon(FontAwesomeIcons.solidThumbsUp,color: WellNestColor.wncWhite,)
        );
      }else{
        Get.back();
        LoadingOrError.errorloading(result['responseException']['message'], "Try Again!");
      }
    }finally{

    }
  }

}