import 'package:intl/intl.dart';

class AppDateDetails{


  static DateTime? firstDateofWeek(DateTime? dateTime)
  => dateTime!.subtract(Duration(days: dateTime.weekday -1));

  static DateTime? lastDateofWeek(DateTime? dateTime)
  =>dateTime!.add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));

  static List<DateTime> getDaysinWeek(DateTime? dateTime)
  { 
    var days = List<DateTime>.filled(7, DateTime.fromMillisecondsSinceEpoch(0));
    var firstDay = firstDateofWeek(dateTime!);
    for(var i = 0; i <= 6; i++){
      days[i] = firstDay!.add(Duration(days: i));
    }
    return days;
  }

}

extension DateExtension on DateTime {

  String? toFormatStrig(String? format)
  => DateFormat(format!).format(this);

  bool isSameDate(DateTime? other)
  =>this.year == other!.year 
  && this.month == other.month
  && this.day == other.day;

}