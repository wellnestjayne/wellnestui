import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/pages/profile/models/dafualt_dailyStat.dart';
import 'package:wellnest/pages/profile/widgets/first_statistic_indicator.dart';

class StatisticBuildWeek extends StatelessWidget {
  
  final List<DailyStatUIModel>? models;

  const StatisticBuildWeek({Key? key, this.models}) : super(key: key);

 

  @override
  Widget build(BuildContext context) {
    if(models!.length == 7){
      return Padding(
        padding: const EdgeInsets.all(20.0),
        child: Container(
          height: 255.h,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: StatisticIndicator(
                  models: models![0],
                ),
              ),
              StatisticIndicator(
               models: models![1],
              ),
              StatisticIndicator(
                models: models![2],
              ),
              StatisticIndicator(
               models: models![3],
              ),
              StatisticIndicator(
                models: models![4],
              ),
              StatisticIndicator(
               models: models![5],
              ),
              StatisticIndicator(
                models: models![6],
              )
            ],
          ),
        ),
        );
    }else{
      return Container();
    }
  }
}