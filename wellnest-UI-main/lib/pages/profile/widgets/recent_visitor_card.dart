import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class RecentVisitorCard extends StatelessWidget {
  const RecentVisitorCard({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20
      ),
      child: Container(
        child: Row(
          children: [
            Expanded(
              child:Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                     Container(
              height: 55.h,
              width: 55.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(WellnestAsset.testimonial)),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(1,1),
                    blurRadius: 1,
                    spreadRadius: 2,
                    color: WellNestColor.wncGrey.withOpacity(0.2)
                  )
                ]
              ),
            ),
            Container(
              height: 55.h,
              width: 55.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(WellnestAsset.testimonial)),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(1,1),
                    blurRadius: 1,
                    spreadRadius: 2,
                    color: WellNestColor.wncGrey.withOpacity(0.2)
                  )
                ]
              ),
            ),
            Container(
              height: 55.h,
              width: 55.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(WellnestAsset.testimonial)),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(1,1),
                    blurRadius: 1,
                    spreadRadius: 2,
                    color: WellNestColor.wncGrey.withOpacity(0.2)
                  )
                ]
              ),
            ),
            Container(
              height: 55.h,
              width: 55.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(WellnestAsset.testimonial)),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(1,1),
                    blurRadius: 1,
                    spreadRadius: 2,
                    color: WellNestColor.wncGrey.withOpacity(0.2)
                  )
                ]
              ),
            ),
                  ],
                ),
              ) 
              ),
            Container(
              height: 55.h,
              width: 55.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(1,1),
                    blurRadius: 1,
                    spreadRadius: 2,
                    color: WellNestColor.wncGrey.withOpacity(0.2)
                  )
                ]
              ),
              child: Center(
                child: Text("+16",
                style: WellNestTextStyle.nowWithShad(WellNestColor.wncWhite, 18.sp,
                 WellNestTextStyle.nowMed),
                ),
              ),
            ),
          ],
        ),  
      ),
    );
  }
}