

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/statisticController.dart';
import 'package:wellnest/pages/profile/models/dafualt_dailyStat.dart';

class StatisticIndicator extends StatelessWidget {
  
  final Function()? press;
  final DailyStatUIModel? models;

   StatisticIndicator({Key? key, this.press, this.models}) : super(key: key);
 final statisticController = Get.put(StatisticController());

  @override
  Widget build(BuildContext context) {
    
    return InkWell(
      onTap: press,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // Expanded(
          //   child: 
          //   NeumorphicIndicator(
          //     width: 15.w,
          //     percent: statisticController.getStatPercentage(models!.stat),
          //   ),
          //   ),
         SizedBox(height:5.0,),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4.0),
          child: Text("${models!.day}",
          style: WellNestTextStyle.nowMedium(
            WellNestColor.wncAquaBlue
            , 10.sp),
          ),
          ),  
        ],
        ),
    );
  }
}