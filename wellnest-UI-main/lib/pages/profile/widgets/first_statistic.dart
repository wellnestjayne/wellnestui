

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/statisticController.dart';
import 'package:wellnest/pages/profile/widgets/first_statistic_buildweek.dart';

class FirstStatictic extends StatelessWidget {


  final statisticController = Get.put(StatisticController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20
      ),
      child: Container(
        height: 255.h,
        width: 325.w,
        decoration: BoxDecoration(
          color: WellNestColor.wncWhite,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: WellNestColor.wncLightgrey.withOpacity(0.3),
              blurRadius: 3,
              spreadRadius: 3,

            )
          ]
        ),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            child: Row(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        height: 5,
                        width: 5,
                      ),
                      Text("110",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncAquaBlue, 12.sp),
                      ),
                      Text("90",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncAquaBlue, 12.sp),
                      ),
                      Text("70",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncAquaBlue, 12.sp),
                      ),
                      Text("50",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncAquaBlue, 12.sp),
                      ),
                      Text("30",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncAquaBlue, 12.sp),
                      ),
                      Text("10",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncAquaBlue, 12.sp),
                      ),
                      Text("0",
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncAquaBlue, 12.sp),
                      ),
                      SizedBox(
                        height: 10,
                        width: 10,
                      ),
                     

                    ],
                  ),
                ),
                 Expanded(
                   child: StatisticBuildWeek(
                          models: statisticController.dailyStatList1.call(),
                        ),
                 )
              ],
            ),
          ),
        ),
      ),
    );
  }
}