import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';

class SetavailabiltyButton extends StatelessWidget {
  
  final Function()? onpres;
  final Color? color;

  const SetavailabiltyButton({Key? key, this.onpres, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.h,
      width: 325.w,
      child: ElevatedButton(onPressed: onpres, 
      style: ElevatedButton.styleFrom(
        elevation: 1,
        primary: color!,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(60)
        )
      ),
      child: Center(
        child: Text("Set my Availabilty",
        style: TextStyle(
          fontFamily: 'NowMed',
          fontSize: 18.sp,
          shadows: [
            Shadow(
              offset: Offset(3,2),
              blurRadius: 4,
              color: WellNestColor.wncGrey.withOpacity(0.3)
            )
          ]
        ),
        ),
      )),
    );
  }
}