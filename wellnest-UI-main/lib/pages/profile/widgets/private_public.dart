
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';


class MakeStatusProfile extends StatelessWidget {

  final Color? colors;
  final bool? change;
  final Color? textColor;
  final Function(bool?)? functionbool;
  final bool? value;
  final Color? borderColor;
  final String? status;

  const MakeStatusProfile({Key? key, this.colors, this.change, this.textColor, this.functionbool, this.value, this.borderColor, this.status}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: colors!,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            offset: Offset(1,1),
            blurRadius: 2,
            spreadRadius: 2,
            color: WellNestColor.wncGrey.withOpacity(0.2)
          )
        ]
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            Expanded(
              child: Text("Make Profile User $status",
              style: WellNestTextStyle.nowMedium(textColor, 12.sp),
              ),
            ),
            FlutterSwitch(
              activeText: "Private",
              inactiveText: "Public",
              activeTextColor: WellNestColor.wncWhite,
              inactiveTextColor: WellNestColor.wncBlue,
              padding: 5,
              width: 85.w,
              height: 40.h,
              inactiveColor: WellNestColor.wncLighBg,
              activeColor: WellNestColor.wncBlue.withOpacity(0.5),
              inactiveToggleColor: WellNestColor.wncBlue,
              valueFontSize: 12.sp,
              showOnOff: change!,
              value: value!,
              switchBorder: Border.all(
                color: borderColor!,
                width: 1
              ),
              onToggle: functionbool!)            

          ],
        ),
      ),      
    );
  }
}