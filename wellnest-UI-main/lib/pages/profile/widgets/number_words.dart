import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/widgets/counter.dart';

class Numbertopbelowords extends StatelessWidget {
  final double? end;
  final Color? color;
  final double? fsize;
  final String? text;
  final double? textsize;

  const Numbertopbelowords(
      {Key? key, this.end, this.color, this.fsize, this.text,this.textsize})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Countup(
              begin: 0,
              end: end!,
              separator: ',',
              duration: Duration(seconds: 2),
              style: WellNestTextStyle.nowMedium(color, fsize),
              // style: TextStyle(
              //    color: color,
              //     fontSize : fsize,
              //     fontFamily: 'NowMed',
              //   shadows: [
              //     Shadow(
              //         offset: Offset(3, 2),
              //         blurRadius: 2,
              //         color: WellNestColor.wncGrey.withOpacity(0.2))
              //   ],
              // )
              ),
          SizedBox(
            height: 10.h,
          ),
          Text(
            text!,
            style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey,
            textsize),
          )
        ],
      ),
    );
  }
}
