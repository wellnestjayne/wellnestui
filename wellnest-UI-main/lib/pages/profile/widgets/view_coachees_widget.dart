

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class ViewCoaches extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20,
      ),
      child: Container(
        height: 255.h,
        width: 325.w,
        decoration: BoxDecoration(
          color: WellNestColor.wncWhite,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: WellNestColor.wncLightgrey.withOpacity(0.3),
              blurRadius: 3,
              spreadRadius: 3,
            )
          ]
        ),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                     Container(
              height: 55.h,
              width: 55.w,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(WellnestAsset.testimonial)),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(1,1),
                    blurRadius: 1,
                    spreadRadius: 2,
                    color: WellNestColor.wncGrey.withOpacity(0.2)
                  )
                ]
              ),
            ),
            SizedBox(width: 10.0.w,),
            Expanded(
              child: Text("Erchil Amad",
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey
                , 12.sp),
              )),
                  ],
                ),
              ),
            ],
          ),
          ),
      ),
    );
  }
}