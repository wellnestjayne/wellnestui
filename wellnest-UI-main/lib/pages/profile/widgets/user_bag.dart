import 'package:flutter/material.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/widgets/number_words.dart';

class UserBagDesign extends StatelessWidget {
  UserBagDesign({this.name, this.photo});
  final String? name;
  final String? photo;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 113,
      width: 326,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 83,
              width: 326,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: WellNestColor.wncWhite,
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(2, 1),
                        blurRadius: 2,
                        spreadRadius: 2,
                        color: WellNestColor.wncLightgrey.withOpacity(0.3))
                  ]),
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0, left: 20, right: 20),
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Numbertopbelowords(
                        end: 34,
                        fsize: 25.0,
                        color: WellNestColor.wncGrey,
                        text: "Followers",
                        textsize: 12,
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Numbertopbelowords(
                        end: 54,
                        fsize: 25.0,
                        color: WellNestColor.wncGrey,
                        text: "Following",
                        textsize: 12,
                      ),
                    ),
                    Positioned(
                      left: 0,
                      right: 0,
                      top: -40,
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      offset: Offset(0, 2),
                                      blurRadius: 2,
                                      spreadRadius: 2,
                                      color: WellNestColor.wncLightgrey.withOpacity(0.3))
                                ],
                                shape: BoxShape.circle,
                                border: Border.all(color: WellNestColor.wncWhite),
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  alignment: Alignment.topCenter,
                                  image: NetworkImage(photo!),
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            Text(
                              name!,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 10.0),
                            ),
                            SizedBox(height: 5),
                            // Text(
                            //   "Tagum City",
                            //   style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 7.0),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
