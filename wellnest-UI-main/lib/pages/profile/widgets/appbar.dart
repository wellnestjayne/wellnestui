import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/notification/notification_view.dart';
import 'package:wellnest/pages/profile/controller/view_profileController.dart';
import 'package:wellnest/pages/profile/edit_profile/edit_profile.dart';
import 'package:wellnest/pages/profile/models/menuItem.dart';
import 'package:wellnest/pages/profile/view_profile/view_profile.dart';
import 'package:wellnest/pages/profile/widgets/number_words.dart';
import 'package:wellnest/pages/profile/widgets/popmenu.dart';
import 'package:wellnest/pages/profile/widgets/switchero.dart';

final profileController = Get.put(ViewOwnProfileController());

class AppbarProfile extends StatelessWidget
//implements PreferredSizeWidget
{
  // final double? topSize = 193;
  final Color? color;
  final String? path;
  final String? firstname;
  final double? end;
  final double? end2;
  final Function(MenuItem?)? item;

  AppbarProfile({Key? key, this.color, this.path, this.firstname, this.end, this.end2, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 78,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
        color: color,
      ),
      child: Padding(
        padding: const EdgeInsets.all(13.0),
        child: Column(
          children: [
            Container(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                        child: PopMenuProfile(
                      item: item,
                      path: path,
                      firstname: firstname,
                    )),
                  ),
                  SizedBox(width: 15.w),
                  SizedBox(
                    height: 30.h,
                    width: 30.w,
                    child: InkWell(
                      onTap: () => Get.to(() => ViewProfileUser(), id: profileController.mainController.select.value),
                      child: Center(
                        child: FaIcon(
                          FontAwesomeIcons.eye,
                          color: WellNestColor.wncWhite,
                          size: 20.w,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 15.w),
                  SizedBox(
                    height: 30.h,
                    width: 30.w,
                    child: InkWell(
                      onTap: () => Get.to(() => EditProfileCoach(), id: profileController.mainController.select.value),
                      child: Center(
                        child: FaIcon(
                          FontAwesomeIcons.pen,
                          color: WellNestColor.wncWhite,
                          size: 20.w,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 15.w),
                  SizedBox(
                    height: 30.h,
                    width: 30.w,
                    child: Center(
                      child: FaIcon(
                        FontAwesomeIcons.shareAlt,
                        color: WellNestColor.wncWhite,
                        size: 20.w,
                      ),
                    ),
                  ),
                  SizedBox(width: 15.w),
                  SizedBox(
                    height: 30.h,
                    width: 30.w,
                    child: GestureDetector(
                      onTap: ()=>Get.to(() => NotificationVieww(), id: 0),
                      child: Center(
                        child: FaIcon(
                          FontAwesomeIcons.solidBell,
                          color: WellNestColor.wncWhite,
                          size: 20.w,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 15.w),
                ],
              ),
            ),
            SizedBox(height: 10.h),
            // Divider(
            //   color: WellNestColor.wncWhite,
            //   indent: 30,
            //   endIndent: 30,
            // ),
            // SizedBox(height :20.h),
            // Container(
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     children: [
            //       Numbertopbelowords(
            //         end: end,
            //         fsize: 25.sp,
            //         color: WellNestColor.wncWhite,
            //         text: "Followers",
            //         textsize: 12.sp
            //       ),
            //       SizedBox(width: 110.w,),
            //       Numbertopbelowords(
            //         end: end2,
            //         fsize: 25.sp,
            //         color: WellNestColor.wncWhite,
            //         text: "Following",
            //         textsize: 12.sp
            //       ),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  // @override
  // // TODO: implement preferredSize
  // Size get preferredSize => Size.fromHeight(topSize!);
}