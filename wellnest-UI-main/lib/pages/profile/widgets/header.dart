import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class HeaderProfile extends StatelessWidget {

  final Function()? press;
  final String? title;
  final bool? show;

  const HeaderProfile({Key? key, this.press, this.title, this.show}) : super(key: key);



  
  @override
  Widget build(BuildContext context) {
      return Padding(
      padding: const EdgeInsets.only(
        left: 20,right: 20,
      ),
      child: Container(
        child: Row(
          children: [
            Expanded(
              child: Container(
                child: Text(title!,
                style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 16.sp),
                ),
              )),
            Visibility(
              visible: show!,
              child: InkWell(
                onTap: press,
                child: Text("Show more",
                style: TextStyle(
                  color: WellNestColor.wncAquaBlue,
                  fontFamily: 'NowMed',
                  fontSize:12.sp,
                  shadows: [
                    Shadow(
                      offset: Offset(3,2),
                      blurRadius: 2,
                      color: WellNestColor.wncGrey.withOpacity(0.2)
                  )
                  ]
                ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}