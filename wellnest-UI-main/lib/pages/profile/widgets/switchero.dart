
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class FaceImageOrletter extends StatelessWidget {


  final String? path;
  final String? firstname;

  const FaceImageOrletter({Key? key, this.path, this.firstname}) : super(key: key);  

  @override
  Widget build(BuildContext context) {
    return Container(
                   width: 90,
                  child: Stack(
                    children: [
                    path != null 
                    ?  Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: WellNestColor.wncWhite
                        ),
                        child: Image.network(path!,
                        loadingBuilder: (context,child,loading)
                        =>Center(
                          child: CircularProgressIndicator(
                            backgroundColor: WellNestColor.wncBlue,
                            value: loading!.expectedTotalBytes != null ? 
                                  loading.cumulativeBytesLoaded/loading.expectedTotalBytes! :null,
                          ),
                        ),
                        ),
                      ) :
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncOrange,
                            border: Border.all(color: WellNestColor.wncWhite)
                          ),
                          child: Center(
                            child: Text(
                              firstname!,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 28.sp),
                            ),
                          ),
                        ),
                      ),
                       Positioned(
                         top: 20,
                          left: 30,
                        //bottom: 10,
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncPeach,
                            border: Border.all(color: WellNestColor.wncWhite)
                          ),
                          child: Center(
                            child: Text(
                              firstname!,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 12.sp),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 25,
                        bottom: 3,
                        child: Container(
                          height: 10,
                          width: 10,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: WellNestColor.wncAquaBlue
                          ),
                          child: Center(
                            child: FaIcon(
                              FontAwesomeIcons.exchangeAlt,
                              size: 5,
                              color: WellNestColor.wncWhite,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
  
  }
}