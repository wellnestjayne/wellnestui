
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/models/menuItem.dart';
import 'package:wellnest/pages/profile/models/menuItems.dart';
import 'package:wellnest/pages/profile/widgets/switchero.dart';

class PopMenuProfile extends StatelessWidget {


  final Function(MenuItem?)? item;
  final String? path;
  final String? firstname;

  const PopMenuProfile({Key? key, this.item, this.path, this.firstname}) : super(key: key);
 

  @override
  Widget build(BuildContext context) {
    return Container(
          child: PopupMenuButton<MenuItem>(
                  
                   onSelected: item,
                   child:  FaceImageOrletter(
                        path: path,
                        firstname : firstname
                      ),
                  offset: Offset(0.0,50.5),
                  elevation: 2,
                  
                  shape: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10),
                  
                  ),
                  itemBuilder: (context)
                  =>[
                    ...MenuItems.items.map((e) => 
                    PopupMenuItem<MenuItem>(
                      value: e,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(e.menuTitle!,
                            style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 16.sp),
                            ),
                            // Divider(
                            //   color: WellNestColor.wncAquaBlue,
                            //   endIndent: 20,
                            //   indent: 20,
                            // )
                          ],
                        ),
                      ),
                      
                      ),
                    ).toList(),
                    // ...MenuItems.iteml.map((e) => 
                    // PopupMenuItem<MenuItem>(
                    //   value: e,
                    //   child: Center(
                    //     child: Column(
                    //       mainAxisAlignment: MainAxisAlignment.center,
                    //       crossAxisAlignment: CrossAxisAlignment.center,
                    //       children: [
                    //         Container(
                    //           width: 140.w,
                    //           decoration: BoxDecoration(
                    //             color: WellNestColor.wncGrey,
                    //             borderRadius: BorderRadius.circular(900)
                    //           ),
                    //           child: Padding(
                    //             padding: const EdgeInsets.all(8.0),
                    //             child: Center(
                    //               child: Text(e.menuTitle!,
                    //               style: WellNestTextStyle.nowRegular(WellNestColor.wncWhite, 16.sp),
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                      
                    //   ),
                    // ).toList(),
                  ],
                ),
                );
  }
}