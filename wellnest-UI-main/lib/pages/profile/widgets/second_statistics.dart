

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/secondstatisticController.dart';

class SecondStatistic extends StatelessWidget {

  final seconstatisticController = Get.put(SecondStaticticController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20
      ),
      child: Container(
        height:  255.h,
        width: 325.w,
        decoration: BoxDecoration(
          color: WellNestColor.wncWhite,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: WellNestColor.wncLightgrey.withOpacity(0.3),
              blurRadius: 3,
              spreadRadius: 3,

            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
            top: 10,
            right: 10
          ),
          child: 
            LineChart(
              linearChart()
             ),
          ),
      ),
    );
  }


   linearChart(){
     return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: false,
        drawHorizontalLine: false,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: SideTitles(
        showTitles: true,
         getTextStyles: (context, value) =>
              const TextStyle(color: Colors.transparent, fontWeight: FontWeight.bold, fontSize: 16),
        reservedSize: 10,
        ),
        topTitles: SideTitles(
        showTitles: true,
         getTextStyles: (context, value) =>
              const TextStyle(color: Colors.transparent, fontWeight: FontWeight.bold, fontSize: 16),
        reservedSize: 10,
        ),
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 20,
          interval: 1,
          getTextStyles: (context, value) =>
              WellNestTextStyle.nowMedium(
                WellNestColor.wncAquaBlue
                , 11.sp),
          getTitles: (value)
          =>seconstatisticController.bottomgetTitles(value.toInt()),
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          interval: 1,
          getTextStyles: (context, value) =>  
          WellNestTextStyle.nowMedium(
                WellNestColor.wncAquaBlue
                , 11.sp),
          getTitles: (value) => seconstatisticController.letfgetTitles(value.toInt()),
          reservedSize: 20,
          //margin: ,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(color: Colors.transparent),
          left: BorderSide(color: Colors.transparent),
          right: BorderSide(color: Colors.transparent),
          top: BorderSide(color: Colors.transparent),
        ),
      ),
      minX: 0,
      maxX: 12,
      minY: 0,
      maxY: 12,
     // extraLinesData: ExtraLinesData(),
      
      lineBarsData: [
        LineChartBarData(
              
            spots: [
            FlSpot(0, 8),
            FlSpot(2.1, 5),
            FlSpot(4.10, 6),
            FlSpot(6.8, 4.1),
            FlSpot(8, 8),
            FlSpot(9.5, 11),
            FlSpot(11, 7),
            FlSpot(12, 1),
          ],
          isCurved: true,
          colors: [
            WellNestColor.wncAquaBlue
          ],
          barWidth: 1,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: true,
            getDotPainter: (spot,percent,bardata,index){
              if(index == 1){
                return FlDotCirclePainter(
                  radius: 6,
                  color: WellNestColor.wncAquaBlue,
                  strokeWidth: 1,
                  strokeColor: WellNestColor.wncWhite
                );
              }else{
                  return FlDotCirclePainter(
                  radius: 6,
                  color: WellNestColor.wncWhite,
                  strokeWidth: 1,
                  strokeColor: WellNestColor.wncAquaBlue
                );
              }
            }
          ),
          belowBarData: BarAreaData(
            show: true,
            //gradientColorStops: ,
            gradientTo: Offset(0,1.2),
            //gradientFrom: Offset(4,0),
            //cutOffY: ,
            //applyCutOffY: ,
            colors: seconstatisticController.gradients.map((color) => 
            color
            ).toList(),
          ),
          
        ),
      ],
    );
  }

}