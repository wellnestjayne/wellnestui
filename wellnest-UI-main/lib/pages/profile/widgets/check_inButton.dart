
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';

class CheckInButton extends StatelessWidget {

    final Function()? onPressed;

  const CheckInButton({Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20,right: 20
      ),
      child: Container(
        width: 325.w,
        height: 50.h,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 2,
              spreadRadius: 2,
              color: WellNestColor.wncLightgrey.withOpacity(0.2)
            )
          ],
          color: WellNestColor.wncBlue,
         borderRadius: BorderRadius.circular(200)
        ),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius:  BorderRadius.circular(200)
            )
          ),
          onPressed: onPressed,
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 4,bottom: 4),
                child: Align(
                  alignment: Alignment(-1.14,0),
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: WellNestColor.wncWhite
                    ),
                    height:50.h,
                    width: 50.w,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 4,
                        right: 2,
                        left: 2,
                        bottom: 1,
                      ),
                      child: Image.asset(WellnestAsset.dateback),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text("Check In",
                style: TextStyle(
                  color: WellNestColor.wncWhite,
                  fontFamily: 'NowMed',
                  fontSize: 18.sp,
                  shadows: [
                    Shadow(
                      offset: Offset(2,2),
                      blurRadius: 4,
                      color: WellNestColor.wncLightgrey.withOpacity(0.4)
                    )
                  ]
                ),
                ),
              )
            ],
          )),   
      ),
    );
  }
}