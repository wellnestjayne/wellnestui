import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class UpcommingSessionCard extends StatelessWidget {
  UpcommingSessionCard({this.name, this.photo});
  final String? name;
  final String? photo;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Row(
          children: [
            Expanded(
              child: Container(
                child: Row(
                  children: [
                    Container(
                      height: 55.h,
                      width: 55.w,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                                offset: Offset(0, 2),
                                blurRadius: 3,
                                spreadRadius: 3,
                                color: WellNestColor.wncLightgrey.withOpacity(0.3))
                          ],
                          image: DecorationImage(
                            image: NetworkImage(photo!),
                            fit: BoxFit.cover,
                            alignment: Alignment.topCenter,
                          )),
                    ),
                    SizedBox(
                      width: 10.w,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        textDirection: TextDirection.ltr,
                        children: [
                          Text(
                            name!,
                            style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 14.sp),
                          ),
                          SizedBox(
                            height: 4.h,
                          ),
                          // Text(
                          //   "Manila City",
                          //   style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 11.sp),
                          // )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                textDirection: TextDirection.ltr,
                children: [
                  Text(
                    "08:30 PM",
                    style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 14.sp),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Text(
                    "December 21, 2021",
                    style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 11.sp),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Text(
                    "4th Session",
                    style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey, 11.sp),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
