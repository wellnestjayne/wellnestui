

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:intl/intl.dart';
class AppbarSession extends StatelessWidget implements PreferredSizeWidget{

     final double? topSize = 105.h;
    final Color? color;
    final String? title;
    final Function()? function;

   AppbarSession({Key? key, this.color, this.title, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dateNow = DateFormat('MMMM dd, yyyy');
   return Container(
      height: topSize!,
      decoration: BoxDecoration(
        color: color!,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10)
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            InkWell(
              onTap: function!,
              child: Container(
                width: 50,
                height: topSize!,
                child: Center(
                  child: FaIcon(FontAwesomeIcons.chevronLeft,
                  color: WellNestColor.wncWhite,
                  size: 25,
                  ),
                ),
              ),
            ),
            SizedBox(width: 40.w,),
            Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                Text(title!,
                style: WellNestTextStyle.nowWithShad(
                  WellNestColor.wncWhite
                  ,20.sp,
                  WellNestTextStyle.nowMed),
                ),
                SizedBox(height: 15.h,),
                Text(dateNow.format(DateTime.now()),
                style: WellNestTextStyle.nowMedium(
                  WellNestColor.wncWhite, 10.sp),
                )  
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(topSize!);
}