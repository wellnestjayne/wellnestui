import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AssociationCards extends StatelessWidget {
  const AssociationCards({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Padding(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Wrap(
          alignment: WrapAlignment.start,
          spacing: 10.0,
          children: [
            Container(
              child: Column(
                children: [
                  Container(
                    height: 60.h,
                    width: 60.h,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncWhite,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(1,1),
                          blurRadius: 2,
                          spreadRadius: 2,
                          color: WellNestColor.wncLightgrey.withOpacity(0.2)
                        )
                      ],
                    image: DecorationImage(
                      image: AssetImage(WellnestAsset.specialLogo)
                    ),
                     shape: BoxShape.circle,
                     //border: Border.all(color: WellNestColor.wncBlue,width: 1)
                    ),
                  ),
                  SizedBox(height: 3.h,),
                  Text("Erchil Tutorial",
                  style: WellNestTextStyle.nowLight(WellNestColor.wncLightgrey, 10.sp),
                  ),
                ],
              )
            ),
          ],
        ),        
      ),
    );
  }
}