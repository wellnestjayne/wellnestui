import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/controller/create_service.dart';
import 'package:wellnest/pages/profile/user_service/widget/shimmer_grid.dart';

class GridCoachCategory extends StatelessWidget {
  final createServiceController = Get.put(CreateServiceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => createServiceController.listCategory.isEmpty
          ? GridCoachLoading()
          : Container(
              height: 600,
              child: GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  childAspectRatio: 5 / 5,
                  //crossAxisSpacing: 10,
                  //mainAxisSpacing: 15,
                ),
                itemCount: createServiceController.listCategory.length,
                itemBuilder: (_, index) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 149.46,
                    decoration: BoxDecoration(
                        color: WellNestColor.wncLightmoreAque,
                        borderRadius: BorderRadiusDirectional.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: WellNestColor.wncLightgrey.withOpacity(0.2),
                            blurRadius: 2,
                            spreadRadius: 2,
                            offset: Offset(2.0, 2.0),
                          )
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 55,
                          width: 55,
                          child: Image.asset(
                            createServiceController.rightIcon(createServiceController.listCategory[index].name!),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 10, right: 20),
                          child: Text(
                            createServiceController.listCategory[index].name!,
                            maxLines: 5,
                            textAlign: TextAlign.center,
                            style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 16.0),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
