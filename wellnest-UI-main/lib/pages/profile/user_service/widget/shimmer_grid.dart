
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wellnest/constants/colors/colors.dart';

class GridCoachLoading extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    
    return Container(
      height: 600,
      child: Shimmer.fromColors(
        baseColor: WellNestColor.wncLighBg,
        highlightColor: WellNestColor.wncLightgrey,
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                       maxCrossAxisExtent: 200,
                       childAspectRatio: 5/5,
                       //crossAxisSpacing: 10,
                       //mainAxisSpacing: 15,
        ),
          itemBuilder: (_,index)
          =>
               Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: Container(
                         height: 149.46,
                        decoration: BoxDecoration(
                    color: WellNestColor.wncLightmoreAque,
                    borderRadius: BorderRadiusDirectional.circular(10),
                       boxShadow: [
                  BoxShadow(
                    color: WellNestColor.wncLightgrey.withOpacity(0.2),
                    blurRadius: 2,
                    spreadRadius: 2,
                    offset: Offset(2.0,2.0),
                  )
                ]
                  ),
                 
              ),
                     ),
          ),
      ),    
    );
  }
}