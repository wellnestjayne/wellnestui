import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/coach/controller/featureController.dart';
import 'package:wellnest/pages/coach/widget/service_card.dart';
import 'package:wellnest/pages/profile/controller/editController.dart';
import 'package:wellnest/pages/profile/create_service/create_service.dart';
import 'package:wellnest/pages/profile/user_service/appbar/appbar.dart';
import 'package:wellnest/pages/profile/user_service/widget/grid_category.dart';
import 'package:wellnest/pages/profile/view_profile/widget/serviceList.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/appbar.dart';
import 'package:wellnest/standard_widgets/header.dart';

class UserServiceCategory extends StatelessWidget {
  // final coachController = Get.put(FeatureCoachController());
  final editController = Get.put(EditProfessionalProfile());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
          child: Scaffold(
            appBar: AppbarForUserService(
              function: () => Get.back(id: 0),
              title: SharePref.detailData['firstName'] + "'s Services",
              color: WellNestColor.wncBlue,
              create: () =>
                  //   Get.toNamed(
                  //   AppRouteName.homeView!+
                  //   AppRouteName.createServicesCoach!
                  // ),
                  Get.to(() => CreateServiceCoach(), id: 0),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  children: [
                    SizedBox(
                      height: 15,
                    ),
                    Standardheader(
                      press: () {},
                      title: "Featured Services",
                      show: false,
                      colorText: WellNestColor.wncBlue,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    ServiceOWnList(),
                    SizedBox(
                      height: 15,
                    ),
                    Standardheader(
                      press: () {},
                      title: "Category Services",
                      show: false,
                      colorText: WellNestColor.wncBlue,
                    ),
                    GridCoachCategory(),
                  ],
                ),
              ),
            ),
          ),
          onWillPop: () async {
            Get.back(id: 0);
            return true;
          }),
    );
  }
}
