
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppbarForUserService extends StatelessWidget implements PreferredSizeWidget {
  final double? topSize = 90;
    final Color? color;
    final String? title;
    final Function()? function;
    final Function()? create;

  const AppbarForUserService({Key? key, this.color, this.title, this.function, this.create}) : super(key: key);



  @override
  Widget build(BuildContext context) {
        return Container(
      height: topSize!,
      decoration: BoxDecoration(
        color: color!,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10)
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            InkWell(
              onTap: function!,
              child: Container(
                width: 50,
                height: topSize!,
                child: Center(
                  child: FaIcon(FontAwesomeIcons.chevronLeft,
                  color: WellNestColor.wncWhite,
                  size: 25,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: Text(title!,
                style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 18.0),
                ),
              ),
            ),
            InkWell(
              onTap: create,
              child: Container(
                height:29,
                width: 29,
                decoration: BoxDecoration(
                  color: WellNestColor.wncWhite,
                  shape: BoxShape.circle
                ),
                child: Center(
                  child: FaIcon(FontAwesomeIcons.plus,
                  color: WellNestColor.wncBlue,
                  size: 12,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(topSize!);
}