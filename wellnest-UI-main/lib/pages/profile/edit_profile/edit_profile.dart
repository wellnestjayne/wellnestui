import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/coach/widget/view_profile_images.dart';
import 'package:wellnest/pages/create_profile_coach/widget/diolog.dart';
import 'package:wellnest/pages/create_profile_coach/widget/tag_item.dart';
import 'package:wellnest/pages/profile/controller/editController.dart';
import 'package:wellnest/pages/profile/edit_profile/widget/edit_nationality.dart';
import 'package:wellnest/pages/profile/edit_profile/widget/textEditing.dart';
import 'package:wellnest/pages/profile/view_profile/widget/appbar_view.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';
import 'package:wellnest/standard_widgets/tex_form_field2.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class EditProfileCoach extends StatelessWidget {
  final editController = Get.put(EditProfessionalProfile());

  final GlobalKey<FormState>? keyEdit = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
          child: Scaffold(
            appBar: AppbarVuewProfile(
              title: "Edit Profile",
              extraTitle: "Next",
              functionX: () => Get.back(id: editController.mainController.current.value),
              edit: () => callback(),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 25),
                child: Form(
                  key: keyEdit!,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    textDirection: TextDirection.ltr,
                    children: [
                      Text(
                        "Name",
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 15.0),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFieldWellnestw(
                        controller: editController.name,
                        // validator: (value) =>
                        //     name(registerController.firstname.text = value!),
                        keyboardType: TextInputType.text,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) => editController.name.text = value!,
                        hintText: editController.hintTextDefault,
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        "Short Description",
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 15.0),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFieldWellnestw(
                        controller: editController.shortDesc,
                        validator: (value) => name(editController.shortDesc.text = value!),
                        keyboardType: TextInputType.text,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) => editController.shortDesc.text = value!,
                        hintText: "Ex. Therapist for almost 9 years",
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        "Long Description",
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 15.0),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFieldWellnestw2(
                        hg: 200,
                        vertical: TextAlignVertical.top,
                        controller: editController.longDesc,
                        validator: (value) => nameWithDot(editController.longDesc.text = value!),
                        keyboardType: TextInputType.multiline,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) => editController.longDesc.text = value!,
                        hintText: "Ex. Therapist for almost 9 years and foremost",
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      //  Text("Tags",
                      // style: WellNestTextStyle.nowMedium(
                      //   WellNestColor.wncLightgrey, 15.0),),
                      HeaderTitleSubs(
                        title: "Tags",
                        extra: "Add Tag",
                        extraFunction: () => DialogAddTag.addTag(
                            function: () => editController.addItemString(), textController: editController.addTag),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Obx(
                        () => editController.tagStrings.isEmpty
                            ? Container()
                            : Container(
                                width: 349,
                                child: Wrap(
                                  spacing: 10,
                                  runSpacing: 10,
                                  alignment: WrapAlignment.center,
                                  children: editController.tagStrings
                                      .map(
                                        (e) => TagItem(
                                          name: e!,
                                          color: editController.colorRandom(),
                                          functionRemoveItem: () => editController.removeStringtag(e),
                                        ),
                                      )
                                      .toList(),
                                ),
                              ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AddNationalityCoach(),
                      SizedBox(
                        height: 25,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          onWillPop: () async => false),
    );
  }

  callback() {
    if (keyEdit!.currentState!.validate()) {
      editController.editProfile();
    }
  }
}
