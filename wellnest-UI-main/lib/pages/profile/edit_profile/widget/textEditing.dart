import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class TitleTop extends StatelessWidget {
  
  final String? titleTop;

  const TitleTop({Key? key, this.titleTop}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
    width: 349,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      textDirection: TextDirection.ltr,
      children: [
        Text(titleTop!,
        style: WellNestTextStyle.nowMedium(
          WellNestColor.wncBlue
          , 10.0),
        ),
        SizedBox(height: 5,),
      ],
    ),    
    );
  }
}