import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/coach/widget/flag_d.dart';
import 'package:wellnest/pages/profile/controller/editController.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

final controller = Get.put(EditProfessionalProfile());

class AddNationalityCoach extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        width: 349,
        child: Column(
          children: [
            HeaderTitleSubs(
              title: "Nationalities",
              extra: "Add Nationality",
              extraFunction: () => Get.defaultDialog(
                title: "",
                content: Flexible(
                  flex: 1,
                  child: Container(
                    height: 350,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 15),
                                    child: Text(
                                      "Nationality",
                                      style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.0),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 15),
                                  child: BlueButtonStandard(
                                    height: 40,
                                    width: 90,
                                    sizeText: 15.0,
                                    textColor: WellNestColor.wncBlue,
                                    back: WellNestColor.wncWhite,
                                    border: WellNestColor.wncBlue,
                                    title: "Back",
                                    bluebutton: () => Get.back(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 244,
                            child: TextFieldWellnestw(
                              controller: controller.searchCurreny,
                              validator: (value) => name(controller.searchCurreny!.text = value!),
                              keyboardType: TextInputType.text,
                              obsecure: false,
                              onchanged: (value) => controller.filteredSearchCurrency(value!),
                              onsaved: (value) => controller.searchCurreny!.text = value!,
                              hintText: "ex. Philipphines",
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Obx(() => controller.countrySearch.isEmpty
                              ? Container(
                                  height: 350,
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: controller.country.length,
                                      itemBuilder: (context, index) => GestureDetector(
                                            onTap: () {},
                                            //=>
                                            // createSer.showFlag(controller.country[index].code,controller.country[index].currency),
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Container(
                                                child: Row(
                                                  children: [
                                                    Container(
                                                        height: 35,
                                                        width: 35,
                                                        decoration: BoxDecoration(shape: BoxShape.circle),
                                                        child: Image.asset(
                                                            "lib/assets/images/countries/${controller.country[index].code!.toString().toLowerCase()}.png")),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Expanded(
                                                      child: Text(
                                                        controller.country[index].name!.toString(),
                                                        overflow: TextOverflow.ellipsis,
                                                        style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 13.0),
                                                      ),
                                                    ),
                                                    ElevatedButton(
                                                        style: ElevatedButton.styleFrom(
                                                          primary: WellNestColor.wncBlue,
                                                          elevation: 1,
                                                        ),
                                                        onPressed: () => controller.getAddString(
                                                            controller.country[index].code,
                                                            controller.countrySearch[index].name),
                                                        // =>print(controller.country[index].code),
                                                        child: Center(
                                                          child: Text(
                                                            "Add",
                                                            style: WellNestTextStyle.nowMedium(
                                                                WellNestColor.wncWhite, 12.0),
                                                          ),
                                                        )),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          )),
                                )
                              : Container(
                                  height: 350,
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: controller.countrySearch.length,
                                      itemBuilder: (context, index) => GestureDetector(
                                            onTap: () {},
                                            // =>
                                            //  createSer.showFlag(createSer.countrySearch[index].code,createSer.countrySearch[index].currency),
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Container(
                                                child: Row(
                                                  children: [
                                                    Container(
                                                        height: 35,
                                                        width: 35,
                                                        decoration: BoxDecoration(shape: BoxShape.circle),
                                                        child: Image.asset(
                                                            "lib/assets/images/countries/${controller.countrySearch[index].code!.toString().toLowerCase()}.png")),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Expanded(
                                                      child: Text(
                                                        controller.countrySearch[index].name!.toString(),
                                                        overflow: TextOverflow.ellipsis,
                                                        style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 13.0),
                                                      ),
                                                    ),
                                                    ElevatedButton(
                                                        style: ElevatedButton.styleFrom(
                                                          primary: WellNestColor.wncBlue,
                                                          elevation: 1,
                                                        ),
                                                        onPressed: () => controller.getAddString(
                                                            controller.countrySearch[index].code,
                                                            controller.countrySearch[index].name),
                                                        // =>print(controller.countrySearch[index].code),
                                                        child: Center(
                                                          child: Text(
                                                            "Add",
                                                            style: WellNestTextStyle.nowMedium(
                                                                WellNestColor.wncWhite, 12.0),
                                                          ),
                                                        )),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          )),
                                )),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            controller.viewFlagSelected.isEmpty
                ? Container()
                : Container(
                    width: 349,
                    child: Wrap(
                      spacing: 10,
                      runSpacing: 10,
                      alignment: WrapAlignment.center,
                      children: controller.viewFlagSelected
                          .map((e) => GestureDetector(
                                onTap: () => controller.removeCountry(e),
                                child: FlagWidget(
                                  code: e.toLowerCase(),
                                ),
                              ))
                          .toList(),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
