import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/profile/controller/edit_service.dart';
import 'package:wellnest/pages/profile/create_service/widget/pick_currency.dart';
import 'package:wellnest/pages/profile/create_service/widget/textField.dart';
import 'package:wellnest/pages/profile/edit_service/widgets/appbar.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class EditService extends StatelessWidget {
  EditService({Key? key, this.id}) : super(key: key);
  final String? id;

  final editServiceController = Get.put(EditServiceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SafeArea(
        child: WillPopScope(
            child: Scaffold(
              appBar: AppbarEditServiceImage(
                title: '',
                press: () => editServiceController.pickImage(),
                imageFile: editServiceController.imageFile.value,
                imageUrl: editServiceController.imageUrl.value,
              ),
              body: SingleChildScrollView(
                child: AnimationLimiter(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 17),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      textDirection: TextDirection.ltr,
                      children: AnimationConfiguration.toStaggeredList(
                        duration: const Duration(seconds: 1),
                        childAnimationBuilder: (widget) => SlideAnimation(
                            verticalOffset: 150,
                            child: FadeInAnimation(
                              child: widget,
                            )),
                        children: [
                          Text(
                            "Service Name",
                            style: WellNestTextStyle.nowMedium(
                                WellNestColor.wncBlue, 10.0),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          TextFieldWellnestw(
                            controller: editServiceController.serviceName,
                            validator: (value) => name(editServiceController
                                .serviceName!.text = value!),
                            keyboardType: TextInputType.text,
                            obsecure: false,
                            onchanged: (value) =>
                                // createserviceController.stringtitle(value);
                                editServiceController
                                    .changeOnTextServiceName(value),
                            onsaved: (value) => editServiceController
                                .serviceName!.text = value!,
                            hintText: "Ex. Fitness and Health",
                          ),
                          SizedBox(height: 15),
                          Text(
                            "Description",
                            style: WellNestTextStyle.nowMedium(
                                WellNestColor.wncBlue, 10.0),
                          ),
                          SizedBox(height: 5),
                          TextFieldDescription(
                            controller: editServiceController.description,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    textDirection: TextDirection.ltr,
                                    children: [
                                      Text(
                                        "Number of Session/s",
                                        style: WellNestTextStyle.nowMedium(
                                            WellNestColor.wncBlue, 13.0),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        width: 156,
                                        child: TextFieldWellnestw(
                                          controller: editServiceController
                                              .numberOfSession,
                                          validator: (value) => phone(
                                              editServiceController
                                                  .numberOfSession!
                                                  .text = value!),
                                          keyboardType: TextInputType.number,
                                          obsecure: false,
                                          onchanged: (value) {
                                            editServiceController.time(value);
                                          },
                                          onsaved: (value) =>
                                              editServiceController
                                                  .numberOfSession!
                                                  .text = value!,
                                          hintText: "0",
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    textDirection: TextDirection.ltr,
                                    children: [
                                      Text(
                                        "Duration",
                                        style: WellNestTextStyle.nowMedium(
                                            WellNestColor.wncBlue, 13.0),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        width: 156,
                                        height: 37,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: WellNestColor.wncWhite,
                                            border: Border.all(
                                                color: WellNestColor.wncBlue),
                                            boxShadow: [
                                              BoxShadow(
                                                  offset: Offset(2, 1),
                                                  blurRadius: 1,
                                                  spreadRadius: 1,
                                                  color: WellNestColor
                                                      .wncLightgrey
                                                      .withOpacity(0.3))
                                            ]),
                                        child: Center(
                                          child: Text(
                                            editServiceController
                                                        .time.isEmpty ||
                                                    editServiceController
                                                            .time.value ==
                                                        "0"
                                                ? "00:00"
                                                : "1:30",
                                            style: WellNestTextStyle.nowRegular(
                                                WellNestColor.wncGrey, 15.0),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    textDirection: TextDirection.ltr,
                                    children: [
                                      Text(
                                        "Price per session",
                                        style: WellNestTextStyle.nowMedium(
                                            WellNestColor.wncBlue, 13.0),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        width: 244,
                                        child: TextFieldWellnestw(
                                          controller:
                                              editServiceController.price,
                                          validator: (value) => phone(
                                              editServiceController
                                                  .price!.text = value!),
                                          keyboardType: TextInputType.number,
                                          obsecure: false,
                                          onchanged: (value) => false,
                                          onsaved: (value) =>
                                              editServiceController
                                                  .price!.text = value!,
                                          hintText: "0.00",
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  textDirection: TextDirection.ltr,
                                  children: [
                                    Text(
                                      "Currency",
                                      style: WellNestTextStyle.nowMedium(
                                          WellNestColor.wncBlue, 13.0),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8),
                                      child: PickCurrencyonFlag(
                                        alignButton: Alignment.centerLeft,
                                        alignFlag: Alignment.centerRight,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    textDirection: TextDirection.ltr,
                                    children: [
                                      Text(
                                        "Discount %",
                                        style: WellNestTextStyle.nowMedium(
                                            WellNestColor.wncBlue, 13.0),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        width: 244,
                                        child: TextFieldWellnestw(
                                          controller:
                                              editServiceController.discount,
                                          validator: (value) => phone(
                                              editServiceController
                                                  .discount!.text = value!),
                                          keyboardType: TextInputType.number,
                                          obsecure: false,
                                          onchanged: (value) => false,
                                          onsaved: (value) =>
                                              editServiceController
                                                  .discount!.text = value!,
                                          hintText: "0%",
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      BlueButtonStandard(
                                        bluebutton: () => editServiceController
                                            .updateServices(id),
                                        border: WellNestColor.wncBlue,
                                        height: 37,
                                        width: 340,
                                        textColor: WellNestColor.wncWhite,
                                        title: "Save",
                                        back: WellNestColor.wncBlue,
                                        sizeText: 15.0,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            onWillPop: () async {
              Get.back(id: 0);
              return true;
            }),
      ),
    );
  }
}
