import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class ServiceDialog {
  static successUpdate({Function()? press}) {
    return Get.defaultDialog(
      barrierDismissible: false,
      contentPadding: const EdgeInsets.all(20),
      title: '',
      content: Column(
        children: [
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(shape: BoxShape.circle, color: WellNestColor.wncAquaBlue),
            child: Center(
              child: FaIcon(FontAwesomeIcons.check, color: WellNestColor.wncWhite),
            ),
          ),
          SizedBox(height: 30),
          Text('Update Successfully!',
              textAlign: TextAlign.center, style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 13.0)),
        ],
      ),
      confirm: BlueButtonStandard(
        height: 40,
        width: 92,
        sizeText: 15.0,
        textColor: WellNestColor.wncWhite,
        back: WellNestColor.wncBlue,
        border: WellNestColor.wncBlue,
        title: "Okay",
        bluebutton: press,
      ),
    );
  }
}
