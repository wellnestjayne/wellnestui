import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppbarEditServiceImage extends StatelessWidget implements PreferredSizeWidget {
  final double? topsize = 261;
  final Function()? press;
  final String? imageFile;
  final String? imageUrl;
  final Function()? back;
  final String? title;
  final String? createdBy;

  const AppbarEditServiceImage(
      {Key? key, this.press, this.imageFile, this.back, this.title, this.createdBy, this.imageUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      height: topsize!,
      decoration: BoxDecoration(
          color: WellNestColor.wncWhite,
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10))),
      child: Stack(
        children: [
          imageFile == ""
              ? imageUrl == "" ? Container():  Container(
                  height: 261,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    // color: Colors.black,
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                    child: Image.network(
                      imageUrl!,
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              : Container(
                  height: 261,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    // color: Colors.black,
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                    child: Image.file(
                      File(imageFile!),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
          Align(
            alignment: Alignment.center,
            child: GestureDetector(
              onTap: press,
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(color: WellNestColor.wncGrey, shape: BoxShape.circle),
                child: Center(
                  child: FaIcon(
                    FontAwesomeIcons.camera,
                    color: WellNestColor.wncWhite,
                    size: 20,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: GestureDetector(
              onTap: back,
              child: Padding(
                padding: const EdgeInsets.only(top: 10, left: 20),
                child: Container(
                  height: 50,
                  width: 50,
                  child: FaIcon(
                    FontAwesomeIcons.chevronLeft,
                    color: WellNestColor.wncWhite,
                    size: 30,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(topsize!);
}
