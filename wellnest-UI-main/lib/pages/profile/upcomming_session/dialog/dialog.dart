
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class SessionDialog{


  static sessionBooking(
    {
      Function()? booking,
      Function()? bookings,
      String? imagePath,
      String? message,
      String? cont,
      bool? cancelShow,
      bool? donx,
    }
  ){
    Get.defaultDialog(
    contentPadding: EdgeInsets.only(bottom: 50),
      title: "",
      backgroundColor: WellNestColor.wncWhite,
      barrierDismissible: false,
      textCustom: "Done",
    
      cancel: Visibility(
        visible: cancelShow!,
        child: BlueButtonStandard(
          bluebutton: bookings,
          title: "Cancel",
          back: WellNestColor.wncWhite,
          border: WellNestColor.wncBlue,
          textColor: WellNestColor.wncBlue,
          sizeText: 12,
          height: 37,
          width: 125,
        ),
        ),
      confirm:  Visibility(
        visible: cancelShow,
        child: BlueButtonStandard(
          bluebutton: booking,
          title: cont!.isEmpty ? "Decline" : cont,
          back: WellNestColor.wncBlue,
          border: WellNestColor.wncBlue,
          textColor: WellNestColor.wncWhite,
          sizeText: 12,
          height: 37,
          width: 125,
        ),
        ),
      content: Container(
        child:Padding(
          padding: const EdgeInsets.only(
            bottom: 20,
            left: 20,
            right: 20,
          ),
          child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(imagePath!),
                  SizedBox(height: 15,),
                  Text(message!,
                  maxLines: 10,
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowRegular( WellNestColor.wncGrey,12.0),
                  ),
                  SizedBox(height: 25,),
                   Visibility(
          visible: donx!,
          child: BlueButtonStandard(
            bluebutton: ()=>Get.back(),
            title: "Done",
            back: WellNestColor.wncBlue,
            border: WellNestColor.wncBlue,
            textColor: WellNestColor.wncWhite,
            sizeText: 12,
            height: 37,
            width: 205,
          ),
          ),
                ],
              ),
        ),
      ),
    );
  }


}