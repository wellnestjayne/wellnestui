

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/profile/upcomming_session/dialog/dialog.dart';

class SessionupcomCard extends StatelessWidget {

  final String? dateString;
  final String? timeString;
  final String? imagePath;
  final String? name;
  final String? location;
  final String? session;
  final String? type;

  const SessionupcomCard({Key? key, this.dateString, this.timeString, this.imagePath, this.name, this.location, this.session, this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
          Container(
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      textDirection: TextDirection.ltr,
                      children: [
                        Text(dateString!,
                        style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncGrey, 13.sp),
                        ),
                        SizedBox(height: 5.h,),
                        Text(timeString!,
                        style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncGrey, 13.sp),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    textDirection: TextDirection.ltr,
                    children: [
                      Visibility(
                        visible: type == "Session starts in 5 mins" ? true : false,
                        child: InkWell(
                          onTap: (){},
                          child: Text("Join Session",
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncAquaBlue, 13.sp),
                          ),
                        ),
                      ),
                      SizedBox(height: 5.h,),
                      Visibility(
                        visible: type == "Session Unconfirmed" ? true : false,
                        child: InkWell(
                          onTap: ()=>SessionDialog.sessionBooking(
                            booking: (){},
                            cancelShow: false,
                            imagePath: WellnestAsset.approvedSesion,
                            donx: true,
                            message: "Congrats! Your Session has been confirmed!",
                          ),
                          child: Text("Confirm Booking",
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncAquaBlue, 13.sp),
                          ),
                        ),
                      ),
                       SizedBox(height: 5.h,),
                      Visibility(
                        visible: type == "Session Unconfirmed" ? true : false,
                        child: InkWell(
                          onTap: ()=>SessionDialog.sessionBooking(
                            booking: (){},
                            cancelShow: true,
                            donx: false,
                            imagePath: WellnestAsset.cancelSession,
                            message: "Are you sure want to decline a session with $name",
                          ),
                          child: Text("Decline",
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncLightgrey, 13.sp),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 20.h,),
          Container(
            child: Row(
              children: [
                Container(
                height: 35.h,
                width: 35.w,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage(imagePath!)),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(4,2),
                        blurRadius: 1,
                        spreadRadius: 1,
                        color: WellNestColor.wncLightgrey.withOpacity(0.2)
                      )
                    ]
                  ),
                ),
                SizedBox(width: 15.w,),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    textDirection: TextDirection.ltr,
                    children: [
                      Text(name!,
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncGrey
                        ,13.sp),
                      ),
                      SizedBox(height: 5.h,),
                      Text(location!+"•"+session!,
                      style: WellNestTextStyle.nowMedium(
                        WellNestColor.wncLightgrey
                        ,10.sp),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15.h,),
          Container(
            child: Row(
              children: [
                Visibility(
                  visible:   type == "Session starts in 5 mins" ? true : false,
                  child: Container(
                    height: 15.h,
                    width: 15.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: WellNestColor.wncAquaBlue
                    ),
                  ),
                ),
               Visibility(
                  visible: type == "Session Confirmed" ? true : false,
                  child: Container(
                    height: 15.h,
                    width: 15.w,
                    child: Center(
                      child: FaIcon(FontAwesomeIcons.solidCheckCircle,
                      color: WellNestColor.wncBlue,
                      size: 14,
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: type == "Session Unconfirmed" ? true : false,
                  child: Container(
                    height: 15.h,
                    width: 15.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: WellNestColor.wncWhite,
                      border: Border.all(color: WellNestColor.wncLightgrey,width: 1)
                    ),
                  ),
                ),
                SizedBox(width: 5.w,),
                Expanded(
                  child: Text(type!,
                  style: WellNestTextStyle.nowMedium(
                    WellNestColor.wncGrey
                    ,13.sp),
                  ),
                ),
                 Visibility(
                        visible: type == "Session Unconfirmed" ? true :  type == "Session Confirmed" ? true : false,
                        child: InkWell(
                          onTap: (){},
                          child: Text("Cancel",
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncLightgrey, 13.sp),
                          ),
                        ),
                      ),
              ],
            ),
          )
          ],
        ),
      ),
    );
  }
}