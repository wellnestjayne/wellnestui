
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class UpcommingCardSessionFunction extends StatelessWidget {

    final String? dateString;
    final String? timeString;
    final String? timeEndString;
    final Function()? confirmBook;
    final Function()? declineBook;
    final String? usernameF;
    final String? userId;
    final Function()? cancelSession;
    final String? statusSession;
    final Function()? jsoinSession;

  const UpcommingCardSessionFunction({Key? key, this.dateString, this.timeString, this.timeEndString, this.confirmBook, this.declineBook, this.usernameF, this.userId, this.cancelSession, this.statusSession, this.jsoinSession}) : super(key: key);

 
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:12.0,left: 20,right: 20),
      child: Container(
        width: 349,
        child: Column(
          children: [
            Container(
              child: Row(
                children: [
                  Expanded(child: 
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      textDirection: TextDirection.ltr,
                      children: [
                        Text(dateString!,
                        textAlign: TextAlign.left,
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,
                         13.0),),
                         SizedBox(height: 4,),
                         Text(timeString!+" - "+timeEndString!,
                        textAlign: TextAlign.left,
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,
                         13.0),),
                      ],
                    ),
                  )),
                  statusSession! == "Confirmed" ? 
                  GestureDetector(
                    onTap: jsoinSession,
                    child: Container(
                      child: Column(
                         crossAxisAlignment: CrossAxisAlignment.end,
                      textDirection: TextDirection.ltr,
                        children: [
                          Text("Join Session",
                                textAlign: TextAlign.right,
                                style: WellNestTextStyle.nowMedium(WellNestColor.wncAquaBlue,
                                 13.0),),
                        ],
                      ),
                    ),
                  ) :
                   Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      textDirection: TextDirection.ltr,
                      children: [
                        GestureDetector(
                          onTap: confirmBook,
                          child: Text("Confirm Booking",
                          textAlign: TextAlign.right,
                          style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue,
                           13.0),),
                        ),
                         SizedBox(height: 4,),
                         GestureDetector(
                           onTap: declineBook,
                           child: Text("Decline",
                        textAlign: TextAlign.right,
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,
                           13.0),),
                         ),
                      ],
                    ),
                  )
                  
                ],
              ),
            ),
            SizedBox(height: 20,),
            Container(
              child: Row(
                children: [
                  Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                       color: WellNestColor.wncAquaBlue,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(2, 3),
                          spreadRadius: 1,
                          blurRadius: 2,
                          color: WellNestColor.wncLightgrey.withOpacity(0.3)
                        )
                      ]
                    ),
                    child: Center(
                      child: Text(usernameF![0]
                      ,style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 
                      15.0),),
                    ),
                  ),
                  SizedBox(width: 15,),
                  Expanded(child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      textDirection: TextDirection.ltr,
                      children: [
                        Text(usernameF!,
                        textAlign: TextAlign.left,
                        style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncGrey, 13.0),)
                      ],
                    ),
                  ))
                ],  
              )
            ),
            SizedBox(height: 20,),
            Container(
              child: Row(
                children: [
                  Expanded(
                    child: Container(

                    )
                  ),
                  statusSession! == "Pending" ?
                  Container() :
                  GestureDetector(
                           onTap: cancelSession,
                           child: Text("Cancel",
                        textAlign: TextAlign.right,
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey,
                           13.0),),
                         ),
                ],
              ),
            ),
            SizedBox(height: 20,),
            Divider(
              color: WellNestColor.wncLightgrey,
              endIndent: 50,
              indent: 20,
            ),
          ],
        ),
      ),
    );
  }
}