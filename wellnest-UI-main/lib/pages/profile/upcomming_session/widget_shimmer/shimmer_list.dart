import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wellnest/constants/colors/colors.dart';

class ShimmerListStagg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnimationLimiter(
      child: Shimmer.fromColors(
        child: ListView.builder(
          itemCount: 5,
          itemBuilder: (_, __) => AnimationConfiguration.staggeredList(
            position: __,
            duration: const Duration(milliseconds: 450),
            child: SlideAnimation(
              verticalOffset: 120.0,
              child: FadeInAnimation(
                curve: Curves.decelerate,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 120,
                    width: 349,
                    decoration: BoxDecoration(
                      color: WellNestColor.wncLightgrey,
                      borderRadius: BorderRadius.circular(10)
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        baseColor: WellNestColor.wncLighBg,
        highlightColor: WellNestColor.wncLightgrey,
      ),
    );
  }
}
