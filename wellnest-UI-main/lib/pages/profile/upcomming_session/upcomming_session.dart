

import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/pages/profile/controller/upcommingControler.dart';
import 'package:wellnest/pages/profile/upcomming_session/dialog/dialog.dart';
import 'package:wellnest/pages/profile/upcomming_session/session_card.dart';
import 'package:wellnest/pages/profile/upcomming_session/widget_card/card_session.dart';
import 'package:wellnest/pages/profile/upcomming_session/widget_shimmer/shimmer_list.dart';
import 'package:wellnest/pages/profile/widgets/appbar_session.dart';
import 'package:intl/intl.dart';
class UpcommingSessions extends StatelessWidget {

  final controller = Get.put(UpcomingController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
          appBar: AppbarSession(
            function: ()=>Get.back(id: 0),
            title: "Upcoming Sessions",
            color: controller.maincontroller.colorType,
          ),
          body: Obx(()=>
          controller.upcomingList.isEmpty ? ShimmerListStagg() :
          AnimationLimiter(
            child: ListView.builder(
              itemCount: controller.upcomingList.length,
              itemBuilder: (_,index)
              =>AnimationConfiguration.staggeredList(
                position: index, 
                duration: const Duration(seconds: 1),
                child: SlideAnimation(
                  verticalOffset: 140,
                  child: 
                FadeInAnimation(child:  UpcommingCardSessionFunction(
                  jsoinSession: (){},
                  statusSession: controller.upcomingList[index].status,
                  cancelSession: ()=>  SessionDialog.sessionBooking(
                booking: ()=>controller.makeCancelBooking(id: controller.upcomingList[index].id),
                bookings: ()=>Get.back(),
                cont: "",
                cancelShow: true,
                donx: false,
                imagePath: WellnestAsset.cancelSession,
                message: "Are you sure want to decline a session with No Name Yet",
                ),
            confirmBook: ()=>controller.makeConfirmBooking(id: controller.upcomingList[index].id),
                declineBook: ()=>  SessionDialog.sessionBooking(
                booking: ()=>controller.makeCancelBooking(id: controller.upcomingList[index].id),
                bookings: ()=>Get.back(),
                cont: "",
                cancelShow: true,
                donx: false,
                imagePath: WellnestAsset.cancelSession,
                message: "Are you sure want to decline a session with No Name Yet",
                ),
            usernameF: "No Names yets",
            userId: controller.upcomingList[index].userId!,
            dateString: DateFormat("EEEE, MMMM dd ").format(controller.upcomingList[index].startTime!),
            timeString: DateFormat("hh:mm aa").format(controller.upcomingList[index].startTime!),
            timeEndString: DateFormat("hh:mm aa").format(controller.upcomingList[index].endTime!),
          ),)
                )))),
          ),

        ),
      onWillPop: ()async{ Get.back(id:0); return true;}),
    );
  }
}