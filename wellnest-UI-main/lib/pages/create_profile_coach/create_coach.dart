import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/create_profile_coach/controller/create_coachController.dart';
import 'package:wellnest/pages/create_profile_coach/widget/add_natinality.dart';
import 'package:wellnest/pages/create_profile_coach/widget/diolog.dart';
import 'package:wellnest/pages/create_profile_coach/widget/tag_item.dart';
import 'package:wellnest/pages/profile/view_profile/widget/appbar_view.dart';
import 'package:wellnest/pages/subscription/widget/dialog.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/tex_form_field2.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class CreateProfileCoach extends StatelessWidget {
  final createCoach = Get.put(CreateCoachController());
  final GlobalKey<FormState>? keyCreate = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppbarVuewProfile(
              title: "Create Profile",
              extraTitle: "Next",
              functionX: () => Get.back(),
              edit: () => callback(),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 25),
                child: Form(
                  key: keyCreate!,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    textDirection: TextDirection.ltr,
                    children: [
                      Text(
                        "Name",
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 15.0),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFieldWellnestw(
                        controller: createCoach.name,
                        // validator: (value) =>
                        //     name(registerController.firstname.text = value!),
                        keyboardType: TextInputType.text,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) => createCoach.name!.text = value!,
                        hintText: createCoach.hintTextDefault,
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        "Short Description",
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 15.0),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFieldWellnestw(
                        controller: createCoach.shortDis,
                        validator: (value) => name(createCoach.shortDis!.text = value!),
                        keyboardType: TextInputType.text,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) => createCoach.shortDis!.text = value!,
                        hintText: "Ex. Therapist for almost 9 years",
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        "Long Description",
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 15.0),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFieldWellnestw2(
                        hg: 200,
                        vertical: TextAlignVertical.top,
                        controller: createCoach.longDis,
                        validator: (value) => nameWithDot(createCoach.longDis!.text = value!),
                        keyboardType: TextInputType.multiline,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) => createCoach.longDis!.text = value!,
                        hintText: "Ex. Therapist for almost 9 years and foremost",
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      //  Text("Tags",
                      // style: WellNestTextStyle.nowMedium(
                      //   WellNestColor.wncLightgrey, 15.0),),
                      HeaderTitleSubs(
                        title: "Tags",
                        extra: "Add Tag",
                        extraFunction: () => DialogAddTag.addTag(
                            function: () => createCoach.addItemString(), textController: createCoach.addTag),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Obx(
                        () => createCoach.tagStrings.isEmpty
                            ? Container()
                            : Container(
                                width: 349,
                                child: Wrap(
                                  spacing: 10,
                                  runSpacing: 10,
                                  alignment: WrapAlignment.center,
                                  children: createCoach.tagStrings
                                      .map(
                                        (e) => TagItem(
                                          name: e!,
                                          color: createCoach.colorRandom(),
                                          functionRemoveItem: () => createCoach.removeStringtag(e),
                                        ),
                                      )
                                      .toList(),
                                ),
                              ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AddNationalityCoach(),
                      SizedBox(
                        height: 25,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          onWillPop: () async => false),
    );
  }

  callback() {
    if (keyCreate!.currentState!.validate()) {
      createCoach.createProfile();
    }
  }
}
