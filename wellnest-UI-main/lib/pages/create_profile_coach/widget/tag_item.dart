
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class TagItem extends StatelessWidget {
  final Color? color;
  final String? name;
  final Function()? functionRemoveItem;

  const TagItem({Key? key, this.color, this.name, this.functionRemoveItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: functionRemoveItem,
      child: Container(
        height: 32,
        width: 128,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(90),
          color:color,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 2),
              blurRadius: 2,
              spreadRadius: 2,
              color: WellNestColor.wncLightgrey.withOpacity(0.3)
            )
          ]
        ),
        // child: Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child: Text(name!,
        //         style: WellNestTextStyle.nowMedium(
        //           WellNestColor.wncLightgrey, 12.0),
        //         )
        // ),
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Align(
              alignment: Alignment(0.95, -2),
              // right: 4,
              // top: -10,
              child: Container(
                height: 15,
                width: 15,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: WellNestColor.wncRedError
                ),
                child: Center(
                  child: FaIcon(FontAwesomeIcons.times,color: WellNestColor.wncWhite,
                  size: 10,),
                ),
              ),),
              Align(
                alignment: Alignment.center,
                child: Text(name!,
                overflow: TextOverflow.ellipsis,
                style: WellNestTextStyle.nowMedium(
                  WellNestColor.wncLightgrey, 12.0),
                ),
              ),
          ],
        ),
      ),
    );
  }
}