
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class DialogAddTag{


  static addTag({
    TextEditingController? textController,
    Function()? function,
  }){
    return Get.defaultDialog(
      title: "Tags Coach",
      titlePadding: const EdgeInsets.only(top: 25,left: 15,right: 15),
      titleStyle: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.0),
      contentPadding: const EdgeInsets.all(7),
      content: Container(
        height: 100,
        width: 349,
        child: Padding(
          padding: const EdgeInsets.only(left: 15,right: 15),
          child: Center(
            child: TextFieldWellnestw(
              controller: textController,
              validator: (value) =>
                          name( textController!.text = value!),
                      keyboardType: TextInputType.text,
                      obsecure: false,
                      onchanged: (value) => false,
                      onsaved: (value) => textController!.text = value!,
                      hintText: "Ex. Executive",
            ),
          ),
        ),
      ),
      confirm: Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: BlueButtonStandard(
          height: 40,
          width: 90,
          sizeText: 15.0,
          textColor: WellNestColor.wncWhite,
          back: WellNestColor.wncBlue,
          border: WellNestColor.wncBlue,
          title: "Add",
          bluebutton: function,
        ),
      ),
      cancel: Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: BlueButtonStandard(
          height: 40,
          width: 90,
          sizeText: 15.0,
          textColor: WellNestColor.wncBlue,
          back: WellNestColor.wncWhite,
          border: WellNestColor.wncWhite,
          title: "Back",
          bluebutton: ()=>Get.back(),
        ),
      )    
    );
  }

}