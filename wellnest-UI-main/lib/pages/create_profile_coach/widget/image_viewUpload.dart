import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/create_profile_coach/controller/upload_controller.dart';
import 'package:wellnest/pages/profile/controller/editController.dart';

class ImageViewUploads extends StatelessWidget {
  final uploadController = Get.put(UploadController());
  final editController = Get.put(EditProfessionalProfile());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Obx(
        () => uploadController.imagesX!.isEmpty
            ? Column(
                // IMAGE NETWORK URL
                children: [
                  Container(
                    height: 303,
                    width: 334,
                    child: PageView.builder(
                      controller: uploadController.pageController,
                      itemCount: editController.photoUrls.length,
                      onPageChanged: uploadController.selectImage,
                      itemBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 303,
                          width: 334,
                          decoration: BoxDecoration(
                            // color: WellNestColor.wncBlue,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(3, 2),
                                  blurRadius: 2,
                                  spreadRadius: 2,
                                  color: WellNestColor.wncLightgrey.withOpacity(0.3)),
                            ],
                          ),
                          child: Stack(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Container(
                                  height: 303,
                                  width: 334,
                                  child: Image.network(
                                    editController.photoUrls[index]!,
                                    fit: BoxFit.cover,
                                    alignment: Alignment.topCenter,
                                  ),
                                ),
                              ),
                              // Align(
                              //   alignment: Alignment.topRight,
                              //   child: Padding(
                              //     padding: const EdgeInsets.only(right: 20, top: 20),
                              //     child: GestureDetector(
                              //       onTap: () {},
                              //       // onTap: (){},
                              //       child: Container(
                              //         height: 30,
                              //         width: 30,
                              //         decoration: BoxDecoration(shape: BoxShape.circle, color: WellNestColor.wncWhite),
                              //         child: Center(
                              //           child: FaIcon(
                              //             FontAwesomeIcons.trash,
                              //             color: WellNestColor.wncGrey,
                              //             size: 18,
                              //           ),
                              //         ),
                              //       ),
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        editController.photoUrls.length,
                        (index) => Padding(
                          padding: const EdgeInsets.only(left: 2, right: 2),
                          child: AnimatedContainer(
                            duration: 230.milliseconds,
                            height: 10,
                            width: uploadController.selectImage.value == index ? 30 : 10,
                            decoration: BoxDecoration(
                                color: uploadController.selectImage.value == index
                                    ? WellNestColor.wncBlue
                                    : WellNestColor.wncLightgrey.withOpacity(0.3),
                                // shape: BoxShape.circle
                                borderRadius: BorderRadius.circular(90)),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : Column(
                // IMAGE FILE
                children: [
                  Container(
                    height: 303,
                    width: 334,
                    child: PageView.builder(
                        controller: uploadController.pageController,
                        itemCount: uploadController.imagesX!.length,
                        onPageChanged: uploadController.selectImage,
                        itemBuilder: (context, index) => Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                  height: 303,
                                  width: 334,
                                  decoration: BoxDecoration(
                                    // color: WellNestColor.wncBlue,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(3, 2),
                                          blurRadius: 2,
                                          spreadRadius: 2,
                                          color: WellNestColor.wncLightgrey.withOpacity(0.3)),
                                    ],
                                  ),
                                  child: Stack(
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Container(
                                          height: 303,
                                          width: 334,
                                          child: Image.file(
                                            File(uploadController.imagesX![index].path),
                                            fit: BoxFit.cover,
                                            alignment: Alignment.topCenter,
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Padding(
                                          padding: const EdgeInsets.only(right: 20, top: 20),
                                          child: GestureDetector(
                                            onTap: () => uploadController
                                                .removeImageonArray(uploadController.imagesX![index].path),
                                            // onTap: (){},
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration:
                                                  BoxDecoration(shape: BoxShape.circle, color: WellNestColor.wncWhite),
                                              child: Center(
                                                child: FaIcon(
                                                  FontAwesomeIcons.trash,
                                                  color: WellNestColor.wncGrey,
                                                  size: 18,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            )),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                          uploadController.imagesX!.length,
                          (index) => Padding(
                                padding: const EdgeInsets.only(left: 2, right: 2),
                                child: AnimatedContainer(
                                  duration: 230.milliseconds,
                                  height: 10,
                                  width: uploadController.selectImage.value == index ? 30 : 10,
                                  decoration: BoxDecoration(
                                      color: uploadController.selectImage.value == index
                                          ? WellNestColor.wncBlue
                                          : WellNestColor.wncLightgrey.withOpacity(0.3),
                                      // shape: BoxShape.circle
                                      borderRadius: BorderRadius.circular(90)),
                                ),
                              )),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
