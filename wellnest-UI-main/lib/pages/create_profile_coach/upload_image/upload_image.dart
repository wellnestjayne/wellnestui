import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/create_profile_coach/controller/upload_controller.dart';
import 'package:wellnest/pages/create_profile_coach/widget/image_viewUpload.dart';
import 'package:wellnest/pages/profile/view_profile/widget/appbar_view.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class UploadImageForProfile extends StatelessWidget {
  
  final uploadController = Get.put(UploadController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
          appBar: AppbarVuewProfile(
          title: "Upload Image/s",
          extraTitle: "Done",
          functionX: ()=>Get.back(),
          edit: ()
          =>uploadController.uploadImages(
            Get.parameters['data']
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.only(left: 20,right: 20,top: 25),
          child: Column(
            children: [
              ImageViewUploads(),
              SizedBox(height: 25,),
              BlueButtonStandard(
                height: 40,
                width: 240,
                sizeText: 15.0,
                textColor: WellNestColor.wncWhite,
                back: WellNestColor.wncBlue,
                border: WellNestColor.wncBlue,
                title: "Select Images",
                bluebutton: ()=>uploadController.pickMultipleImages()
              ),
            ],
          ),
        ),
        ),
        onWillPop: ()async=>false)
    );
  }
}