import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/route/route_page.dart';

class UploadController extends GetxController {
  final List<XFile>? imagesX = <XFile>[].obs;
  final pageController = PageController();
  var selectImage = 0.obs;

  uploadImages(String? id) async {
    print(SharePref.detailData['id'].toString());
    print(id);
    final result = await ApiCoach.uploadImage(file: imagesX!, uuid: id!);
    print("Result $result");
    if (result != null) {
      Get.keys.clear();
      Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false);
    }
    //    id!
    //    )
    //  imagesX!.map((e) =>
    //     ApiCoach.uploadImage(e,
    //    id!
    //    )
    //   ).toList();
    // print(id);
    // await ApiCoach.uploadImage(pick1[0],SharePref.detailData['id'] );
  }

  pickMultipleImages() async {
    try {
      List<XFile>? images = await ImagePicker().pickMultiImage();
      print(images!.length);
      print(images
          .map((e) => imagesX!.add(e)
              //e
              )
          .toList());
      print(imagesX!.map((e) => e).toList());
    } on PlatformException catch (e) {}
  }

  removeImageonArray(String? file) => imagesX!.removeWhere((el) => el.path == file!);
}
