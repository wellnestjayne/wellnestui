import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/api/country/country_get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/model/country.dart';

class CreateCoachController extends GetxController {
  List<Color> colors = <Color>[WellNestColor.wncOrange, WellNestColor.wncPeach, WellNestColor.wncLightmoreAque];

  @override
  void onInit() {
    getCountryTry();
    super.onInit();
  }

  final randomColorPick = Random();

  Color colorRandom() => colors[randomColorPick.nextInt(3)];

  final hintTextDefault = SharePref.detailData['firstName'] + " " + SharePref.detailData['lastName'];

  TextEditingController? name = TextEditingController();
  TextEditingController? shortDis = TextEditingController();
  TextEditingController? longDis = TextEditingController();

  TextEditingController? addTag = TextEditingController();

  List<String?> tagStrings = <String>[].obs;

  createProfile() async {
    var data = {
      "userId": SharePref.detailData['id'],
      "name": name!.value.text.isEmpty ? hintTextDefault : name!.value.text,
      "shortDescription": shortDis!.value.text,
      "longDescription": longDis!.value.text,
      "tags": tagStrings,
      "nationalityTags": viewFlagSelected,
      "isVerified": true,
      "photoUrls": []
    };
    print(data);
    await ApiCoach.createOrEditCoach(data, 1);
  }

  addItemString() {
    if (addTag!.value.text == "") {
      Get.snackbar("Please Input a tag", "We implore you to type your own tag for yourself.",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
    } else if (isItemStringisExist(addTag!.value.text)) {
      Get.snackbar("Add Tag", "Tag already added!",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
    } else {
      Get.snackbar("Tag Added", "${addTag!.value.text} has been added",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
      tagStrings.add(addTag!.value.text);
    }
  }

  removeStringtag(String? string) => tagStrings.removeWhere((element) => element == string!);

  bool isItemStringisExist(String? string) => tagStrings.where((e) => e == string!).isNotEmpty;

  bool isCountryExists(String? string) => viewFlagSelected.where((e) => e.contains(string!)).isNotEmpty;

  TextEditingController? searchCurreny = TextEditingController();
  final List<Country> country = <Country>[].obs;
  final List<Country> countrySearch = <Country>[].obs;
  final List<String> viewFlagSelected = <String>[].obs;

  removeCountry(String? string) => viewFlagSelected.removeWhere((e) => e.contains(string!));

  getAddString(String? code, String? name) {
    if (isCountryExists(code!)) {
      Get.snackbar("Country Tag", "Country Tag already added!",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
    } else {
      Get.snackbar("Country Tag Added", "$name has been Added.",
          icon: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: FaIcon(
              FontAwesomeIcons.tag,
              color: WellNestColor.wncWhite,
            ),
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: WellNestColor.wncBlue,
          borderRadius: 5,
          colorText: WellNestColor.wncWhite,
          shouldIconPulse: true,
          margin: EdgeInsets.all(16),
          dismissDirection: SnackDismissDirection.HORIZONTAL,
          forwardAnimationCurve: Curves.easeInOutBack,
          isDismissible: true);
      viewFlagSelected.add(code.toUpperCase());
    }
  }

  filteredSearchCurrency(String? query) {
    if (query!.isNotEmpty) {
      countrySearch.assignAll(country.where((e) => e.name!.contains(query)));
    } else {
      getCountryTry();
    }
  }

  getCountryTry() async {
    try {
      final response = await GetCountries.loadAssets();
      if (response != null) {
        country.assignAll(response);
        print(country);
      }
    } finally {}
  }
}
