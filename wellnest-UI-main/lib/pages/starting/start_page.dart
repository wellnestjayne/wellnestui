import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/pages/as_page/as_log.dart';
import 'package:wellnest/pages/login/login_view.dart';
import 'package:wellnest/pages/register/register_view.dart';
import 'package:wellnest/pages/starting/controller/start.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/line_choice.dart';
import 'package:wellnest/standard_widgets/social_button.dart';

class StartingPagew extends StatelessWidget {

 final putMore = Get.put(StarterController());

  @override 
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
          body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(WellnestAsset.backBg),
              )
            ),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 6,sigmaY: 6),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Spacer(),
                    Center(
                        child: Container(
                        width: 250.w,
                        height: 250.h,
                        child: Image.asset(
                          WellnestAsset.specialLogo,
                          fit: BoxFit.contain,
                        ),
                        ),
                      ),
                      SizedBox(height: 15.h,),
                        Text("Welcome to WellNest!",
                            style: TextStyle(
                              fontSize: 28.sp,
                              fontFamily: 'NowMed',
                              shadows: [
                                Shadow(
                                  offset: Offset(3,2),
                                  blurRadius: 2,
                                  color: WellNestColor.wncGrey.withOpacity(0.6)
                                )
                              ],
                              color: WellNestColor.wncWhite,
                            ),
                      ),
                      Spacer(flex: 1,),
                        Padding(
                          padding: const EdgeInsets.only(left: 40,right: 40),
                          child: BlueButtonStandard(
                            height: 45.h,
                            width: 236.w,
                            title: "Signup",
                            textColor: WellNestColor.wncBlue,
                            back: WellNestColor.wncWhite,
                            border: WellNestColor.wncBlue,
                            sizeText: 14.sp,
                            bluebutton: ()
                            =>Get.offNamed(AppRouteName.loginAs!),
                            //=>Get.off(()=>RegisterVieww(typeUser: "User",)
                            //),
                          ),
                        ),
                        SizedBox(height: 15.h,),
                        Padding(
                          padding: const EdgeInsets.only(left: 40,right: 40),
                          child: BlueButtonStandard(
                            height: 45.h,
                            width: 236.w,
                            title: "Login",
                            textColor: WellNestColor.wncWhite,
                            back: WellNestColor.wncBlue,
                            border: WellNestColor.wncBlue,
                            sizeText: 14.sp,
                            //bluebutton: ()=>Get.off(()=>LoginVieww()),
                            bluebutton: ()=>putMore.checkVersion(
                              context: context,
                              route: AppRouteName.loginPage!
                            ),
                          ),
                        ),
                       Spacer(),
                  ],
                ),
              ),
            ),
          ),
      ),
    );
  }
}
