import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:new_version/new_version.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class StarterController extends GetxController{

   late final newVersion = NewVersion(
    iOSId: "com.WellNest.app", 
    androidId: "com.WellNest.app"
  );

  checkVersion({BuildContext? context,String? route}) async{
    LoadingOrError.loading();
  final statusApp = await newVersion.getVersionStatus();
  if(statusApp!.localVersion == statusApp.storeVersion){
    print("The App is on the latest one!");
    
    Get.offNamed(route!);
  }else{
    Get.back();
    print("The App is Need to be updated!");
     newVersion.showUpdateDialog(
   context: context!,
   versionStatus: statusApp,
   dialogTitle: "Update WellNest to Latest Version.",
   updateButtonText: "Update the app.",
   dialogText: "New version of the app please update the app from ${statusApp.localVersion} to ${statusApp.storeVersion} as the latest version of WellNest.",
   dismissAction: ()=>Get.offNamed(route!),
   dismissButtonText: "Skip for now.",
   );
  }
 }



}