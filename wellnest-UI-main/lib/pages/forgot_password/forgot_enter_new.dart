
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/forgot_password/controller/controller.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class UpdatenewPassword extends StatelessWidget {
  

   final forgotController = Get.put(ForgotController());
   final GlobalKey<FormState> keys03= GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
   return SafeArea(
      child: Scaffold(
        backgroundColor: WellNestColor.wncWhite,
        body: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(
              left: 20,
              right: 20
            ),
            child: Obx(
              ()=>Form(
                key: keys03,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                            child: Container(
                            width: 250.w,
                            height: 250.h,
                            child: Image.asset(
                              WellnestAsset.logoWellNest,
                              fit: BoxFit.contain,
                            ),
                            ),
                          ),
                          SizedBox(height: 15.h,),
                    RichText(text:
                    TextSpan(
                      children: [
                        TextSpan(
                          text: "Put you new Password for Wellnest.",
                          style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 14.sp)
                        ),
                      ]
                    ),
                    ),
                    SizedBox(height: 25.h,),
                    TextFieldWellnestw(
                        controller: forgotController.newPassword,
                        validator: (value)=> password(forgotController.newPassword!.text = value!),
                        keyboardType: TextInputType.visiblePassword,
                        obsecure: forgotController.obse.value,
                        onchanged: (value) => false,
                        onsaved: (value)=> forgotController.newPassword!.text = value!,
                        hintText: "New Password",
                      ),
                      SizedBox(height: 15.h,),
                      TextFieldWellnestw(
                        controller: forgotController.confirmPassword,
                        validator: (value)=> confirmPassword(
                          forgotController.newPassword!.value.text,forgotController.confirmPassword!.text = value!),
                        keyboardType: TextInputType.visiblePassword,
                        obsecure: forgotController.obse.value,
                        onchanged: (value) => false,
                        onsaved: (value)=> forgotController.confirmPassword!.text = value!,
                        hintText: "Confirm Password",
                      ),
                      SizedBox(height: 15.h), 
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                            Text( 
                              forgotController.obse.value ?
                              "Hide Password." :
                              "Password is Shown."
                              ,
                          style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 12.sp),
                          ),
                           SizedBox(width: 2.w,),
                          Checkbox(
                          hoverColor: WellNestColor.wncAquaBlue,
                          value: forgotController.obse.value == false ? false: true ,
                          onChanged: (value)=>forgotController.checkShown(value!)),
                         
                         
                        ],
                      ) ,
                    SizedBox(height: 25.h),  
                     BlueButtonStandard(
                        bluebutton: ()=>callback(),
                        title: "Change Password",
                        textColor: WellNestColor.wncWhite,
                        border: WellNestColor.wncBlue,
                        back: WellNestColor.wncBlue,
                        height: 45,
                        width: 250,
                      ),
                      SizedBox(height: 15.h),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void callback(){

    if(keys03.currentState!.validate()){
      forgotController.getPasswordUpdate();
    }

  }
}