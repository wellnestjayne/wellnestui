
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/Log_Reg_Up/api_services.dart';
import 'package:wellnest/api/services/forgot_password.dart';
import 'package:wellnest/pages/forgot_password/forgot_enter_code.dart';
import 'package:wellnest/pages/forgot_password/forgot_enter_new.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class ForgotController extends GetxController{

  TextEditingController? sendEmail = TextEditingController();
  TextEditingController? newPassword = TextEditingController();
  TextEditingController? confirmPassword = TextEditingController();

  TextEditingController? n0 = TextEditingController();
  TextEditingController? n1 = TextEditingController();
  TextEditingController? n2 = TextEditingController();
  TextEditingController? n3 = TextEditingController();
  TextEditingController? n4 = TextEditingController();
  TextEditingController? n5 = TextEditingController();

  FocusNode? fn0 = FocusNode();
  FocusNode? fn1 = FocusNode();
  FocusNode? fn2 = FocusNode();
  FocusNode? fn3 = FocusNode();
  FocusNode? fn4 = FocusNode();
  FocusNode? fn5 = FocusNode();

  var obse = true.obs;

   checkShown(value){
     obse(value);
     print(value);
  }

  var sendCog;

  void getPasswordUpdate() async{
    var mergeAll = 
    n0!.value.text
    +
    n1!.value.text
    +
    n2!.value.text
    +
    n3!.value.text
    +
    n4!.value.text
    +
    n5!.value.text;

    var newPasswords = newPassword!.value.text;
    var emailSend = sendEmail!.value.text;
    // await ForgotServices.setNewPassword(emailSend, mergeAll, newPasswords); 
    await UserCoachApiOnly.setNewPassword(emailSend, mergeAll, newPasswords);

    print("$mergeAll $newPasswords");
  }


  void sendEmailtoRecieveCode() async{


    var emailSend = sendEmail!.value.text;

    // await ForgotServices.sendEmail(emailSend);
    await UserCoachApiOnly.requestForgotPass(emailSend);
  }

  verirfyCode(){
      Get.to(()=>UpdatenewPassword());
  }

  nextField(String? values,nodes,context){
    if(values!.length == 1){
      nodes.requestFocus();
    }else{
      FocusScope.of(context).previousFocus();
    }
  }

  lastInput(String? value,nodes,context){
  if(value!.length == 1){
      fn5!.unfocus();
    }else{
      FocusScope.of(context).previousFocus();
    }
  }

}