

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/forgot_password/controller/controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/pages/forgot_password/forgot_enter_new.dart';
import 'package:wellnest/pages/register/widget/custom_textfield.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
class EneterCodeNewPassword extends StatelessWidget {
  
    final String? email;

   final forgotController = Get.put(ForgotController());
   final GlobalKey<FormState> keys02 =  GlobalKey<FormState>();

   EneterCodeNewPassword({Key? key, this.email}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     var colorUse = WellNestColor.wncBlue;
    return SafeArea(
      child: Scaffold(
        backgroundColor: WellNestColor.wncWhite,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
                key: keys02,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: Container(
                      width: 150.w,
                      height:  150.h,
                      child: Image.asset(
                        WellnestAsset.logoWellNest,
                        fit: BoxFit.contain,
                      ),
                      ),
                    ),
                    SizedBox(height: 15.h,),
                    RichText(text: TextSpan(
                      children: [
                        TextSpan(
                          text: "We have sent you an Email Verification with code at your email ",
                          style: WellNestTextStyle.nowRegular(colorUse, 16.sp)
                        ),
                        TextSpan(
                         text: email != null ? email : '',
                         //text: 'a.chil666@gmail.com',
                         style: WellNestTextStyle.nowBold(colorUse, 16.sp)
                        ),
                      ]
                    )),
                    SizedBox(height: 15.h,),
                    Container(
                      width: MediaQuery.of(context).size.width.w,
                      height: 120.h,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CustomtextfieldVerification(
                            color: colorUse,
                            controller: forgotController.n0,
                            onchanged: (value)=>forgotController
                            .nextField(value!,
                            forgotController.fn1, context),
                            onSave: (value)=>forgotController.n0!.text = value!,
                            hintText: "0",
                            nodes: forgotController.fn0,
                            action: TextInputAction.next,
                            validator: (value)=>verification(forgotController.n0!.text =value!),
                          ),

                          CustomtextfieldVerification(
                            color: colorUse,
                            controller: forgotController.n1,
                            onchanged: (value)=>forgotController
                            .nextField(value!,
                            forgotController.fn2, context),
                            onSave: (value)=>forgotController.n1!.text = value!,
                            hintText: "0",
                            nodes: forgotController.fn1,
                            action: TextInputAction.next,
                             validator: (value)=>verification(forgotController.n1!.text =value!),
                          ),

                          CustomtextfieldVerification(
                            color: colorUse,
                            controller: forgotController.n2,
                            onchanged: (value)=>forgotController
                            .nextField(value!,
                            forgotController.fn3, context),
                            onSave: (value)=>forgotController.n2!.text = value!,
                            hintText: "0",
                            nodes: forgotController.fn2,
                            action: TextInputAction.next,
                             validator: (value)=>verification(forgotController.n2!.text =value!),
                          ),

                          CustomtextfieldVerification(
                            color: colorUse,
                            controller: forgotController.n3,
                            onchanged: (value)=>forgotController
                            .nextField(value!,
                            forgotController.fn4, context),
                            onSave: (value)=>forgotController.n3!.text = value!,
                            hintText: "0",
                            nodes: forgotController.fn3,
                            action: TextInputAction.next,
                             validator: (value)=>verification(forgotController.n3!.text =value!),
                          ),

                          CustomtextfieldVerification(
                            color: colorUse,
                            controller: forgotController.n4,
                            onchanged: (value)=>forgotController
                            .nextField(value!,
                            forgotController.fn5, context),
                            onSave: (value)=>forgotController.n4!.text = value!,
                            hintText: "0",
                            nodes: forgotController.fn4,
                            action: TextInputAction.next,
                             validator: (value)=>verification(forgotController.n4!.text =value!),
                          ),

                          CustomtextfieldVerification(
                            color: colorUse,
                            controller: forgotController.n5,
                            onchanged: (value)=>forgotController
                            .lastInput(value!,
                            forgotController.fn5, context),
                            onSave: (value)=>forgotController.n5!.text = value!,
                            hintText: "0",
                            nodes: forgotController.fn5,
                            action: TextInputAction.next,
                             validator: (value)=>verification(forgotController.n5!.text =value!),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 50,),
                     BlueButtonStandard(
                      bluebutton: () => callBack(),
                      title: "Submit Code",
                      textColor: WellNestColor.wncWhite,
                    border: colorUse,
                    back: colorUse,
                    ),
                     SizedBox(height: 20,),
                  
                  ],
                )),
            ),
        ),
      ),
    );
  }

  void callBack(){

    if(keys02.currentState!.validate()){
     forgotController.verirfyCode();
    }


  }
}