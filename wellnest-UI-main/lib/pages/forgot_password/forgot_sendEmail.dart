

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/forgot_password/controller/controller.dart';
import 'package:wellnest/pages/forgot_password/forgot_enter_code.dart';
import 'package:wellnest/pages/login/login_view.dart';
import 'package:wellnest/pages/login/widget/appbar.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class SendEmailForgotPassword extends StatelessWidget {
  

  final forgotController = Get.put(ForgotController());
  final GlobalKey<FormState> keys01 = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: GobacktoAsLog(
          title: "Go back!",
          icon: FontAwesomeIcons.times,
          press: ()=>Get.off(()=>LoginVieww()),
          colors: WellNestColor.wncBlue,
        ),
        backgroundColor: WellNestColor.wncWhite,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 20,
              right: 20
            ),
            child: Form(
              key: keys01,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 35.h,),
                  Center(
                          child: Container(
                          width: 250.w,
                          height: 250.h,
                          child: Image.asset(
                            WellnestAsset.logoWellNest,
                            fit: BoxFit.contain,
                          ),
                          ),
                        ),
                        SizedBox(height: 15.h,),
                  RichText(text:
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "Please put your email or phone below ",
                        style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 14.sp)
                      ),
                    ]
                  ),
                  ),
                  SizedBox(height: 25.h,),
                  TextFieldWellnestw(
                      controller: forgotController.sendEmail,
                      validator: (value)=> email(forgotController.sendEmail!.text = value!),
                      keyboardType: TextInputType.emailAddress,
                      obsecure: false,
                      onchanged: (value) => false,
                      onsaved: (value)=> forgotController.sendEmail!.text = value!,
                      hintText: "Email",
                    ),
                  SizedBox(height: 15.h),  
                   BlueButtonStandard(
                      bluebutton: ()=>callback(),
                      title: "Submit",
                      textColor: WellNestColor.wncWhite,
                      border: WellNestColor.wncBlue,
                      back: WellNestColor.wncBlue,
                      height: 45,
                      width: 250,
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void callback(){

    if(keys01.currentState!.validate()){
      forgotController.sendEmailtoRecieveCode();
    }

  }

}