

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/newsfeed/controller/newsfeed_controller.dart';
import 'package:wellnest/standard_widgets/newsfeed_widget/appbar.dart';
import 'package:wellnest/standard_widgets/newsfeed_widget/news_card.dart';

class NewFeedVieww extends GetView<NewsFeedController>{
  @override
  Widget build(BuildContext context) {
    return Obx(
      ()=> SafeArea(
        child: GestureDetector(
          onTap: ()=>controller.obtapoutSidetry(context),
          child: Scaffold(
            backgroundColor: WellNestColor.wncWhite,
            body: CustomScrollView(
              controller: controller.scroll,
              slivers: [
                SliverAppBar(
                  collapsedHeight: 100,
                  backgroundColor: Colors.transparent,
                  expandedHeight:controller.sizeH.value ,
                  flexibleSpace:AppbarNewsfeedw(
                    hintext: "Hey, What do you want to talk about?",
              controller: controller.postNews,
              ontap: ()=>controller.ontaptext(),
              expand: controller.expand.value,
              sizeHiegh: controller.sizeH.value,
            ),
                ),
                NewsCardw(
                  iconSuffix: controller.suffixTap.value,
                  icontap: ()=>controller.iconStap(),
                  sendtap: (){},
                  //press: (){},
                  press: ()=>controller.hump(),
                  showIt: controller.trial.value,
                ),
              ],
            ),
          ),
        ),
      ),
    );    
    }
}