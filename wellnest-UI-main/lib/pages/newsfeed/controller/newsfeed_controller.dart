
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/standard_widgets/newsfeed_widget/modal_sheet.dart';

class NewsFeedController extends GetxController{

  final directLike = false.obs;
  void like(){
    directLike(!directLike.value);
  }


  final expand = false.obs;
  final hideTextField = false.obs;
  final sizeH = 200.0.obs;
  Rx<bool> trial = false.obs;  
  TextEditingController? postNews = TextEditingController();
  //NewsCard
  TextEditingController? newsFeed = TextEditingController();
  Rx<bool> suffixTap = false.obs;
  void iconStap(){
    suffixTap(!suffixTap.value);
  }
  emojiSel(Emoji emoji){
    newsFeed!
    ..text += emoji.emoji
    ..selection = TextSelection.fromPosition(
      TextPosition(offset: newsFeed!.text.length)
    );
  }
  onBackpress(){
    newsFeed!
    ..text = newsFeed!.text.characters.skipLast(1).toString()
    ..selection = TextSelection.fromPosition(
      TextPosition(offset: newsFeed!.text.length)
    );
  }
  //End
  final shitme = Get.find<MainController>();

 late ScrollController scroll = ScrollController();


  void hump(){
    trial(!trial.value);
    
    // if(trial.value){
    //   shitme.listenMe(false);
      
    // }else{
    //   shitme.listenMe(true);
    //   // iconStap();
    // }
  }

  @override
  void onInit() {
      onReturnKeyboard();
      dependecies();
      // scroll.addListener(listen);
    super.onInit();
  }


  dependecies(){
    Get.lazyPut<MainController>(() => MainController());
  }


  void ontaptext(){
    expand(true);
    sizeH(400);
  }

  void onReturnKeyboard(){

    expand(false);
    sizeH(200);
  }

  void obtapoutSidetry(context){
      FocusScopeNode current = FocusScope.of(context);
              if(!current.hasPrimaryFocus){
                current.unfocus();
                onReturnKeyboard();
  }
  }

  void listen(){

    final direction = scroll.position.userScrollDirection;

    if(direction == ScrollDirection.forward){

      // shitme.listenMe(true);

    }else if(direction == ScrollDirection.reverse){

      // shitme.listenMe(false);
      onReturnKeyboard();

    }
    // if(scroll.position.pixels <=200){
    //   shitme.listenMe(true);
    // }else{
    //   shitme.listenMe(false);
    // }

  }


  @override
  void onClose() {
    super.onClose();
    scroll.dispose();
  }

}