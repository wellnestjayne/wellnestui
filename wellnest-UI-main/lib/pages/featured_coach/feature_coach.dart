import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/featured_coach/controller/featureController.dart';
import 'package:wellnest/pages/featured_coach/widget/feature_card.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/appbar.dart';

class FeatureCoach extends StatelessWidget {
  final featureController = Get.put(FeatureConroller());
  @override
  Widget build(BuildContext context) {
    print("${Get.arguments}");
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          Get.back(id: 0);
          return false;
        },
        child: Scaffold(
          appBar: StandardAppbaruse(
            function: () => Get.back(id: 0),
            title: "Featured Coaches",
            color: featureController.mainController.colorType,
          ),
          body: Obx(
            () => featureController.coachList.isEmpty
                ? Container()
                : AnimationLimiter(
                    child: GridView.builder(
                      gridDelegate:
                          SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 200,
                           childAspectRatio: 3 / 4.4),
                      itemCount: featureController.coachList.length,
                      itemBuilder: (context, index) => AnimationConfiguration.staggeredGrid(
                        position: index,
                        duration: const Duration(milliseconds: 375),
                        columnCount: 2,
                        child: ScaleAnimation(
                          child: FadeInAnimation(
                            // new-->
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10,right: 10,bottom: 25,top: 5),
                              
                              child: FeatureCardCoach(
                                press: ()=>
                                Get.toNamed(
                                  AppRouteName.homeView!+
                                  AppRouteName.coachFeaturedProfile!,
                                  arguments: featureController.coachList[index].id,
                                  id: 0,
                                  ),
                                name: featureController.coachList[index].name,
                                specialty: featureController.coachList[index].shortDescription,
                                backcolor: WellNestColor.wncAquaBlue,                                
                                imagepath: featureController.coachList[index].photoUrls![0],
                              ),
                            ),
                            // new-->
                          ),
                        ),
                      ),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
