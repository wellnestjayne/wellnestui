import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_coaches/api_get_all_coach.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/model/feature_class_view.dart';
import 'package:wellnest/pages/featured_coach/model/model_trash.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';

class FeatureConroller extends GetxController {
  final mainController = Get.find<MainController>();

  @override
  void onInit() {
    getAllFeaturedCoach();
    super.onInit();
  }

  final listTrash = [
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue,
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncAquaBlue,
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue,
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncAquaBlue,
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue,
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue,
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncAquaBlue,
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue,
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncAquaBlue,
    ),
  ];

  final List<Coachee> coachList = <Coachee>[].obs;
  getAllFeaturedCoach() async {
    try {
      final result = await CoachGetLists.coachgetAll();
      if (result != null) {
        coachList.assignAll(result);
      }
    } finally {
      print(coachList.map((e) => e.name));
    }
  }
}
