import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/src/widgets/image.dart';

class FeatureCardCoach extends StatelessWidget {
  final Function()? press;
  final Color? backcolor;
  final String? imagepath;
  final String? name;
  final String? specialty;

  const FeatureCardCoach({Key? key, this.press, this.backcolor, this.imagepath, this.name, this.specialty})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        height: 195.h,
        width: 152.w,
        decoration: BoxDecoration(
          color: backcolor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              offset: Offset(4, 2),
              blurRadius: 3,
              spreadRadius: 3,
              color: WellNestColor.wncLightgrey.withOpacity(0.2),
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Stack(
            children: [
              ClipRect(
                child: Container(
                decoration: BoxDecoration(color: WellNestColor.wncWhite, borderRadius: BorderRadius.circular(8)),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Image.network(imagepath!,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
                loadingBuilder: (_,__,___){
                  if(___ == null)
                  return __;
                  return Align(
                      alignment: Alignment.center, //c
                      child: SpinKitRotatingCircle(
                        color: WellNestColor.wncOrange,//c
                        size: 40.0,
                        duration: Duration(milliseconds: 450),
                        
                      ),
                  );
                }),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: IndexedStack(
                  children: [
                    Container(
                      height: 45.h,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(color: WellNestColor.wncWhite, borderRadius: BorderRadius.circular(0)),
                      child: Padding(
                        padding: const EdgeInsets.all(9.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          textDirection: TextDirection.ltr,
                          children: [
                            Text(
                              name!,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 10.sp),
                            ),
                            SizedBox(
                              height: 3.h,
                            ),
                            Text(
                              specialty!,
                              overflow: TextOverflow.ellipsis,
                              style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 6.sp),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
