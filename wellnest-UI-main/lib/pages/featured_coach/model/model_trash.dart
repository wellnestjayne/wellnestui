
import 'package:flutter/material.dart';

class FeatureTrash{

  final String? imagePath;
  final String? name;
  final String? speciality;
  final Color? backColor;

  FeatureTrash({this.imagePath, this.name, this.speciality, this.backColor});
}