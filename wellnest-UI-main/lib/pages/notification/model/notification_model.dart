


import 'package:flutter/material.dart';

class NotificationModel {


  final String? imagePath;
  final String? name;
  final String? dateString;
  final String? date;
  final Color? colors;
  final String? typeWord;

  NotificationModel({this.imagePath, this.name, this.date, this.dateString, this.colors,this.typeWord});



}