
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/notification/controller/controller.dart';
import 'package:wellnest/pages/notification/widgets/appbar.dart';
import 'package:wellnest/pages/notification/widgets/bottomSheet.dart';
import 'package:wellnest/pages/notification/widgets/notificationCard.dart';
import 'package:wellnest/standard_widgets/appbar.dart';

class NotificationVieww extends StatelessWidget {
  
  final controller = Get.put(NotificationController());

  @override
  Widget build(BuildContext context) {
    
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
          appBar: StandardAppbaruse(
            function: ()=>Get.back(id: 0),
            title: "Notifications",
            color: controller.mainController.colorType,
          ),
          body: 
          controller.decider == "user" ?
          ListView.builder(
            itemCount: controller.listUserCoaches.length,
            itemBuilder: (context,index)
            =>NotificationCard(
              function: (){},
              date: controller.listUserCoaches[index].date,
              dateString:controller.listUserCoaches[index].dateString,
              typeWord: controller.listUserCoaches[index].typeWord,
              name: controller.listUserCoaches[index].name,
              colors: controller.listUserCoaches[index].colors,
              imagePath: controller.listUserCoaches[index].imagePath,
            ))
          : ListView.builder(
            itemCount: controller.listCoach.length,
            itemBuilder: (context,index)
            =>NotificationCard(
              function:  ()=>BottomSheetCall.notificationBottomSheet(
                decline: (){print("Decline");},
                accept: (){print("Confirm");},
                name: controller.listCoach[index].name,
                dateString: controller.listCoach[index].dateString,
                typeword: controller.listCoach[index].typeWord,
                imagePath: controller.listCoach[index].imagePath
              ),
              date: controller.listCoach[index].date,
              dateString:controller.listCoach[index].dateString,
              typeWord: controller.listCoach[index].typeWord,
              name: controller.listCoach[index].name,
              colors: controller.listCoach[index].colors,
              imagePath: controller.listCoach[index].imagePath,
            )),
        ), 
      onWillPop: ()async{Get.back(id: 0); return Future.value(true);})
    );
  }
}