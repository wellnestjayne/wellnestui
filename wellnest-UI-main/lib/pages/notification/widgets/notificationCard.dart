
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class NotificationCard extends StatelessWidget {

  final Function()? function;
  final String? name;
  final String? dateString;
  final String? date;
  final Color? colors;
  final String? imagePath;
  final String? typeWord;

  const NotificationCard({Key? key, this.function, this.name, this.dateString, this.date, this.colors, this.imagePath, this.typeWord}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: colors,
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          left: 10,
          right: 10,
          top: 20,
          bottom: 0,
        ),
        child: Column(
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    height: 55.h,
                    width: 55.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(imagePath!)),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(4,2),
                          blurRadius: 2,
                          spreadRadius: 2,
                          color: WellNestColor.wncLightgrey.withOpacity(0.2)
                        )
                      ]
                    ),
                  ),
                  SizedBox(width: 10.w,),
                  Expanded(
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        textDirection: TextDirection.ltr,
                        children: [
                          SizedBox(height: 10.h,),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: name!,
                                  style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncGrey
                                    , 14.sp)
                                ),
                                 TextSpan(
                                  text: typeWord!,
                                  style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncLightgrey
                                    , 14.sp)
                                ),
                                 TextSpan(
                                  text: dateString!,
                                  style: WellNestTextStyle.nowMedium(
                                    WellNestColor.wncGrey
                                    , 14.sp)
                                )
                              ]
                            )),
                          SizedBox(height: 10.h),
                          Text(date!,
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncLightgrey
                            ,10.sp),
                          )
                        ],
                      ),
                    ),),
                    GestureDetector(
                      onTap: function,
                      child: Container(
                        height: 55.h,
                        width: 55.w,
                        child: Center(
                          child: FaIcon(FontAwesomeIcons.ellipsisH,
                          color: WellNestColor.wncLightgrey,
                          ),
                        ),
                      ),
                    )
                ],
              ),
            ),
            SizedBox(height: 5.h,),
            Divider(
              thickness: 1,
              endIndent: 20,
              indent: 20,
              color: WellNestColor.wncLightgrey,
            )

          ],
        ),
      ),
    );
  }
}