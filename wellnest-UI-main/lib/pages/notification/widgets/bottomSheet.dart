

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class BottomSheetCall{

  static notificationBottomSheet({
    Function()? decline,
    Function()? accept,
    String? imagePath,
    String? name,
    String? typeword,
    String? dateString}){

    Get.bottomSheet(
      Container(
        height: 265,
        decoration: BoxDecoration(
          color: WellNestColor.wncWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(4,3),
              blurRadius: 4,
              spreadRadius: 4,
              color: WellNestColor.wncLightgrey.withOpacity(0.2)
            )
          ]
        ),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                onTap: ()=>Get.back(),
                child: Container(
                  height:55,
                  width: 55,
                  child: Center(
                    child: FaIcon(FontAwesomeIcons.times,
                    color: WellNestColor.wncGrey,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
             Container(
                    height: 55.h,
                    width: 55.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(imagePath!)),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(4,2),
                          blurRadius: 2,
                          spreadRadius: 2,
                          color: WellNestColor.wncLightgrey.withOpacity(0.2)
                        )
                      ]
                    ),
                  ),
             SizedBox(height: 15.h,),  
               Padding(
                 padding: const EdgeInsets.only(
                   left: 50,right: 50
                 ),
                 child: RichText(
                   textAlign: TextAlign.center,
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: name!,
                                    style: WellNestTextStyle.nowMedium(
                                      WellNestColor.wncGrey
                                      , 14.sp)
                                  ),
                                   TextSpan(
                                    text: typeword!,
                                    style: WellNestTextStyle.nowMedium(
                                      WellNestColor.wncLightgrey
                                      , 14.sp)
                                  ),
                                   TextSpan(
                                    text: dateString!,
                                    style: WellNestTextStyle.nowMedium(
                                      WellNestColor.wncGrey
                                      , 14.sp)
                                  )
                                ]
                              )),
               ),
             SizedBox(height: 15.h,),
             Container(
               child: Row(
                 crossAxisAlignment: CrossAxisAlignment.center,
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   BlueButtonStandard(
                     bluebutton: decline,
                     title: "Decline",
                     back: WellNestColor.wncWhite,
                     border: WellNestColor.wncBlue,
                     textColor: WellNestColor.wncBlue,
                     sizeText: 16.sp,
                     height: 38.h,
                     width: 148.w,
                   ),
                   SizedBox(width: 5.w,),
                   BlueButtonStandard(
                     bluebutton: accept,
                     title: "Confirm",
                     back: WellNestColor.wncBlue,
                     border: WellNestColor.wncBlue,
                     textColor: WellNestColor.wncWhite,
                     sizeText: 16.sp,
                     height: 38.h,
                     width: 148.w,
                   ),
                 ],
               ),
             )     
          ],
        ),
      ),
      barrierColor: Colors.transparent,
      backgroundColor: Colors.transparent,
      isDismissible: false
    );

  }


}