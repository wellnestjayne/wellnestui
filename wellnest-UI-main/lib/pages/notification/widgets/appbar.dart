

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppbarNotification extends StatelessWidget implements PreferredSizeWidget{

  final double? topSize = 110.h;
  final Function()? press;
  final Color? color;

   AppbarNotification({Key? key, this.press, this.color}) : super(key: key);

 

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: color!,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(15),
          bottomRight: Radius.circular(15)
        )
      ),
      height: topSize!,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Center(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: GestureDetector(
                  onTap: press,
                  child: Container(
                    height: 20.h,
                    width: 20.w,
                    child: Center(
                      child: FaIcon(
                        FontAwesomeIcons.chevronLeft,
                        color: WellNestColor.wncWhite,
                        size: 20.h,
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Text("Notifications",
                  style: TextStyle(
                    color: WellNestColor.wncWhite,
                  fontSize : 22.sp,
                  fontFamily: 'NowMed',
                    shadows: [
                              Shadow(
                                offset: Offset(3,2),
                                blurRadius: 2,
                                color: WellNestColor.wncGrey.withOpacity(0.4)
                              )
                            ],
                  ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(topSize!);
}