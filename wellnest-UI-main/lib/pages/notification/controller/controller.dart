

import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/notification/model/notification_model.dart';

class NotificationController extends GetxController{


  final mainController = Get.find<MainController>();
  
  final listCoach = [
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "Now",
      dateString: "September 8 ,2021(Wednesday) at 3:00 PM",
      colors: WellNestColor.wncAquaBlue.withOpacity(0.1),
      typeWord: " booked a session with you on "
    ),
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "2 hours ago",
      dateString: "September 8 ,2021(Wednesday) at 1:00 PM",
      colors: WellNestColor.wncAquaBlue.withOpacity(0.1),
      typeWord: " booked a session with you on "
    ),
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "6 hours ago",
      dateString: "September 8 ,2021(Wednesday) at 9:00 AM",
      colors: WellNestColor.wncAquaBlue.withOpacity(0.1),
      typeWord: " booked a session with you on "
    ),
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "Yesterday",
      dateString: "September 8 ,2021(Wednesday) at 3:00 PM",
      colors: WellNestColor.wncWhite,
      typeWord: " booked a session with you on "
    ),
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "Yesterday",
      dateString: "September 8 ,2021(Wednesday) at 3:00 PM",
      colors: WellNestColor.wncWhite,
      typeWord: " booked a session with you on "
    ),
  ];

  final listUserCoaches = [
     NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "Now",
      dateString: "September 8 ,2021(Wednesday) at 3:00 PM",
      colors: WellNestColor.wncAquaBlue.withOpacity(0.1),
      typeWord: " confirmed a session with you on "
    ),
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "2 hours ago",
      dateString: "September 8 ,2021(Wednesday) at 1:00 PM",
      colors: WellNestColor.wncAquaBlue.withOpacity(0.1),
      typeWord: " confirmed a session with you on "
    ),
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "6 hours ago",
      dateString: "September 8 ,2021(Wednesday) at 9:00 AM",
      colors: WellNestColor.wncAquaBlue.withOpacity(0.1),
       typeWord: " confirmed a session with you on "
    ),
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "Yesterday",
      dateString: "September 8 ,2021(Wednesday) at 3:00 PM",
      colors: WellNestColor.wncWhite,
       typeWord: " confirmed a session with you on "
    ),
    NotificationModel(
      imagePath: WellnestAsset.testimonial,
      name: "John Dick",
      date: "Yesterday",
      dateString: "September 8 ,2021(Wednesday) at 3:00 PM",
      colors: WellNestColor.wncWhite,
      typeWord: " confirmed a session with you on "
    ),
  ];


  @override
  void onInit() {
    
    super.onInit();
  }

  final decider = SharePref.getType();

}