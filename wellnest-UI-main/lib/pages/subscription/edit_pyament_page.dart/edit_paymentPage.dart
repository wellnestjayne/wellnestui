
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/subscription/controller/subscriptionController.dart';
import 'package:wellnest/pages/subscription/model/card.dart';
import 'package:wellnest/standard_widgets/appbar.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class EditPaymentPage extends StatelessWidget {
  
  final subsController = Get.put(SubscriptionController());
  final GlobalKey<FormState>? keys = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    return SafeArea(
        child: WillPopScope(
          child: Scaffold(
            appBar: StandardAppbaruse(
              title: "Edit Payment Option",
              function: ()=>Get.back(),
              color: WellNestColor.wncBlue,
            ),
            body: Padding(
              padding: const EdgeInsets.all(20.0),
              child: SingleChildScrollView(
                child: Form(
                  key: keys,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                   Container(
                     child: Row(
                       children: [
                         Container(
                           height: 27,
                           width: 40,
                           decoration: BoxDecoration(
                             color: WellNestColor.wncGrey.withOpacity(0.5),
                             borderRadius: BorderRadius.circular(2)
                           ),
                         ),
                         SizedBox(width: 10,),
                          Obx(
                   ()=> Container(
                         height: 40,
                      width: 133,
                      decoration: BoxDecoration(
                        color: WellNestColor.wncAquaBlue,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(1,2),
                            spreadRadius: 2,
                            blurRadius: 2,
                            color: WellNestColor.wncLightgrey.withOpacity(0.3)
                          )
                        ]
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: DropdownButton(
                          isExpanded: true,
                           onChanged: (String? value)
                            =>
                           subsController.changeValueCard(value!),
                          icon: FaIcon(FontAwesomeIcons.chevronDown,
                          size: 15,
                          color: WellNestColor.wncWhite,
                          ),
                          dropdownColor: WellNestColor.wncBlue,
                          value: subsController.dropCardValue!.value ,
                          items:  subsController.listCards.map((e){
                          return DropdownMenuItem(
                            value: e.cardTypeName!.value,
                            child: Text(e.cardTypeName!.value,
                            textAlign: TextAlign.center,
                          style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 15.0)
                            ));
                          }).toList()),
                      ),
                        ),
               ),
                       ],
                     ),
                   ),   
                    SizedBox(height: 35),
                  TextFieldWellnestw(
                        controller:  subsController.cardNumber,
                        validator: (value) =>
                            phone(subsController.cardNumber!.text = value!),
                        keyboardType: TextInputType.number,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) =>
                             subsController.cardNumber!.text = value!,
                        hintText: "Card Number",
                      ),
                      SizedBox(height: 20),
                  Container(
                    width: 150,
                    child: TextFieldWellnestw(
                          controller:  subsController.date,
                          validator: (value) =>
                              phone(subsController.date!.text = value!),
                          keyboardType: TextInputType.text,
                          obsecure: false,
                          onchanged: (value) => false,
                          onsaved: (value) =>
                               subsController.date!.text = value!,
                          hintText: "MM/YY",
                        ),
                  ),
                   SizedBox(height: 20),
                  Container(
                    width: 150,
                    child: TextFieldWellnestw(
                          controller:  subsController.cvc,
                          validator: (value) =>
                              phone(subsController.cvc!.text = value!),
                          keyboardType: TextInputType.number,
                          obsecure: false,
                          onchanged: (value) => false,
                          onsaved: (value) =>
                               subsController.cvc!.text = value!,
                          hintText: "CVC",
                        ),
                  ),
                  SizedBox(height: 45,),
                  Container(
                    width: 337,
                    child: Row(
                      children: [
                        Container(
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                            color: WellNestColor.wncGrey,
                            shape: BoxShape.circle
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: Text("I acknowledge that by saving my account I acknowldge that by saving my account I xcacknowledge that by saving my account",
                          textAlign: TextAlign.justify,
                          maxLines: 6,
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncGrey
                            , 13.0),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 15,),
                   Container(
                    width: 337,
                    child: Row(
                      children: [
                        Container(
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                            color: WellNestColor.wncGrey,
                            shape: BoxShape.circle
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: Text("I acknowledge that by saving my account I acknowldge that by saving my account I xcacknowledge that by saving my account",
                          textAlign: TextAlign.justify,
                          maxLines: 6,
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncGrey
                            , 13.0),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 65,),
                  BlueButtonStandard(
                    height: 37,
                    width: 334,
                    bluebutton: ()=>callback(),
                    title: "Save",
                    textColor: WellNestColor.wncWhite,
                    border: WellNestColor.wncBlue,
                    back: WellNestColor.wncBlue,
                  ),
                  SizedBox(height: 15,),
                    ],
                  ),
                ),
              ),
            ),
          ),
        onWillPop: ()async=>false),
    );
  }

  void callback(){
    if(keys!.currentState!.validate()){

    }
  }

}