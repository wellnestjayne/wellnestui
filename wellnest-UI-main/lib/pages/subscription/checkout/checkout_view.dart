
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/subscription/controller/subscriptionController.dart';
import 'package:wellnest/pages/subscription/edit_pyament_page.dart/edit_paymentPage.dart';
import 'package:wellnest/pages/subscription/successPage/succes_buy.dart';
import 'package:wellnest/pages/subscription/widget/BoxText.dart';
import 'package:wellnest/pages/subscription/widget/boxPremium.dart';
import 'package:wellnest/pages/subscription/widget/dialog.dart';
import 'package:wellnest/pages/subscription/widget/edit_payment.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';
import 'package:wellnest/pages/subscription/widget/termsncondition.dart';
import 'package:wellnest/pages/subscription/widget/totalPayment_withoff.dart';
import 'package:wellnest/standard_widgets/appbar.dart';

class Checkoutpageview extends StatelessWidget {


  final subsController = Get.put(SubscriptionController());
  final GlobalKey<FormState> keys = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
          backgroundColor: WellNestColor.wncWhite,
          appBar: StandardAppbaruse(
            color: WellNestColor.wncBlue,
            function: ()=>Get.back(),
            title: "Checkout",
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 25,),
                  Center(
                    child: BoxPremiumMonAn()
                    ),
                  SizedBox(height: 20,),
                  HeaderTitleSubs(
                    title: "Duration",
                    extra: "",
                  ),
                  SizedBox(height: 10,),
                  Obx(
                    ()=>BoxText(
                      text: subsController.pickDate.value == "" 
                      ? subsController.formatter.format(subsController.dateNow)
                      +" - "+
                      subsController.formatter.format(subsController.nowAddMonnth)
                      : subsController.pickDate.value,
                    )
                  ),
                  SizedBox(height: 20,),
                  HeaderTitleSubs(
                    title: "Voucher",
                    extra: "Add",
                    
                    extraFunction: ()=>DialogSubscriptions.inputPromoCode(
                          function: ()=>callback(),
                          titleDialog: "Input Voucher Code",
                          textEditingController: subsController.inputVoucher,
                          buttonTitle: "Add",
                          globalKey: keys
                        ),
                  ),
                  SizedBox(height: 10,),
                  BoxText(
                    text: "None",
                  ),
                  SizedBox(height: 20,),
                  HeaderTitleSubs(
                    title: "Payment Option",
                    extra: "Edit",
                    extraFunction: ()=>Get.to(()=>EditPaymentPage()),
                  ),
                  SizedBox(height: 10,),
                  EditPayment(
                    cardText: "*4543",
                    sessionFee: "P 100.00",
                    appFee: "P 0.00",
                    discount: "P 100.00",
                    voucher: "P 0.00",
                    totalPayment: 500.00,
                  ),
                  SizedBox(height: 25,),
                  Obx(
                    ()=> TearmsNCondition(
                      checkit: (b)=>subsController.changeCheck(subsController.value.value =b!),
                      seeMore: (){},
                      value: subsController.value.value,
                    ),
                  ),
                  SizedBox(height: 15,),
                  TotalPaymentWithOff(
                    goto: ()=>Get.to(()=>SuccessSubscription()),
                  ),
                  SizedBox(height:25,),
                ],
              ),
            ),
          ),
        ),
        onWillPop: ()async=>false),
    );
  }
   void callback(){
    if (keys.currentState!.validate()) {
      Get.back();
      DialogSubscriptions.errorInputCode(
        iconPath: WellnestAsset.cancelSession,
        messageText: "The Voucher code you entered is expired or already used.",
        buttonTitle: "Okay",
        function: (){
          Get.back();
        }
      );
    }
  }
}