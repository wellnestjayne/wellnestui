import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/subscription/checkout/checkout_view.dart';
import 'package:wellnest/pages/subscription/controller/subscriptionController.dart';
import 'package:wellnest/pages/subscription/widget/dialog.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class SubscriptionView extends StatelessWidget {
  final subsController = Get.put(SubscriptionController());
  final GlobalKey<FormState> keys = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
          child: Scaffold(
            body: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(fit: BoxFit.fill, image: AssetImage(WellnestAsset.backimagePng))),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(color: WellNestColor.wncWhite.withOpacity(0.2)),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 38.0, right: 38.0),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 15.h,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: GestureDetector(
                            onTap: () {
                              LoadingOrError.loading();
                              Future.delayed(2.seconds, () => Get.offAllNamed(AppRouteName.dashBoard!));
                            },
                            child: Text("Not Now",
                                style: WellNestTextStyle.nowWithShad(
                                    WellNestColor.wncBlue, 15.sp, WellNestTextStyle.nowReg)),
                          ),
                        ),
                        Spacer(),
                        SizedBox(
                          height: 120,
                          width: 120,
                          child: Image.asset(WellnestAsset.specialLogo),
                        ),
                        SizedBox(height: 45.h),
                        Text(
                          "Welcome to WellNest! Enjoy your 7-day free trial.",
                          textAlign: TextAlign.center,
                          style: WellNestTextStyle.nowWithShad(WellNestColor.wncWhite, 28.sp, WellNestTextStyle.nowMed),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        BlueButtonStandard(
                          bluebutton: () {},
                          //() =>Get.to(()=>Checkoutpageview()),
                          title: "Get 1 Month of Premium",
                          back: WellNestColor.wncBlue,
                          border: WellNestColor.wncBlue,
                          textColor: WellNestColor.wncWhite,
                          sizeText: 15.sp,
                          height: 55.h,
                          width: 266.w,
                        ),
                        SizedBox(
                          height: 25.h,
                        ),
                        GestureDetector(
                          onTap: () => Get.toNamed(AppRouteName.subscription! + AppRouteName.subscriptionMonthly!),
                          child: Center(
                            child: Text(
                              "See other plans",
                              style: WellNestTextStyle.nowRegular(WellNestColor.wncWhite, 13.sp),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 35.h,
                        ),
                        GestureDetector(
                          onTap: () => DialogSubscriptions.inputPromoCode(
                              function: () => callback(),
                              titleDialog: "Input Promo Code",
                              textEditingController: subsController.inputPromo,
                              buttonTitle: "Verify",
                              globalKey: keys),
                          child: Center(
                            child: Text(
                              "Input promo code",
                              style: WellNestTextStyle.nowRegular(WellNestColor.wncWhite, 13.sp),
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          onWillPop: () async => false),
    );
  }

  void callback() {
    if (keys.currentState!.validate()) {
      Get.back();
      DialogSubscriptions.errorInputCode(
          iconPath: WellnestAsset.cancelSession,
          messageText: "The Promo code you entered is expired.",
          buttonTitle: "Okay",
          function: () {
            Get.back();
          });
    }
  }
}
