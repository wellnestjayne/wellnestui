
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:wellnest/pages/subscription/model/card.dart';

class SubscriptionController extends GetxController{
  
@override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  
  final pickDate = "".obs;
  final dateNow  = DateTime.now();
  final nowAddMonnth = DateTime.now().add(Duration(days: 30));
  final formatter = DateFormat("MMMM dd yyyy");

  TextEditingController? inputPromo = TextEditingController();
  TextEditingController? inputVoucher = TextEditingController();

  TextEditingController? cardNumber = TextEditingController();
  TextEditingController? date = TextEditingController();
  TextEditingController? cvc = TextEditingController();
  

 RxString? dropCardValue = "Debit".obs;
  final listCards = [
    CardX(
      id: 0,
      cardTypeName: "Debit".obs,
    ),
    CardX(
      id: 1,
      cardTypeName: "Credit".obs,
    ),
  ].obs;
  
  changeValueCard(String? value){
    dropCardValue!.value = value!; 
  }


  final value = false.obs;

  changeCheck(bool? b ){
    value(b);
  }

}