import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/subscription/controller/subscriptionController.dart';
import 'package:wellnest/pages/subscription/widget/boxPremium.dart';
import 'package:wellnest/pages/subscription/widget/boxText.dart';
import 'package:wellnest/pages/subscription/widget/header_title.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/loading.dart';

class SuccessSubscription extends StatelessWidget {

    final subsController = Get.put(SubscriptionController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: WillPopScope(
        child: Scaffold(
          backgroundColor: WellNestColor.wncWhite,
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 50,),
                  SizedBox(
                    height: 60,
                    width: 60,
                    child: FaIcon(
                      FontAwesomeIcons.solidCheckCircle,
                      color: WellNestColor.wncAquaBlue,
                      size: 60,
                    ),
                  ),
                  SizedBox(height: 20,),
                  Text("You have successfully purchased Wellnest Premium",
                  textAlign: TextAlign.center,
                  style: WellNestTextStyle.nowMedium(
                    WellNestColor.wncGrey
                    , 20.0),
                  ),
                  SizedBox(height: 40,),
                  Center(child: BoxPremiumMonAn()),
                   SizedBox(height: 40,),
                  HeaderTitleSubs(
                    title: "Duration",
                    extra: "",
                  ),
                  SizedBox(height: 10,),
                  Obx(
                    ()=>BoxText(
                      text: subsController.pickDate.value == "" 
                      ? subsController.formatter.format(subsController.dateNow)
                      +" - "+
                      subsController.formatter.format(subsController.nowAddMonnth)
                      : subsController.pickDate.value,
                    )
                  ),
                  SizedBox(height: 30,),
                  Text("Enjoy your 1 month premium",
                  style: WellNestTextStyle.nowRegular(
                    WellNestColor.wncLightgrey
                    , 13.0),
                  ),
                   SizedBox(height: 80,),
                    BlueButtonStandard(
                // bluebutton: ()=>
                // Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false),
                bluebutton: (){
                            LoadingOrError.loading();
                            Future.delayed(2.seconds,()
                            =>Get.offNamedUntil(AppRouteName.dashBoard!, (route) => false));
                          },
                title: "Return Home",
                back: WellNestColor.wncBlue,
                textColor: WellNestColor.wncWhite,
                sizeText: 13.0,
                border: WellNestColor.wncBlue,
                height: 37,
                width: 334,
              )
                ],
              ),
            ),
          ),
        ),
        onWillPop: ()async=>false),
    );
  }
}