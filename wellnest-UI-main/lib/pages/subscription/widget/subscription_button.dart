

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class SubscriptionButton extends StatelessWidget {

  final Function()? monthly;
  final Function()? annual;

  const SubscriptionButton({Key? key, this.monthly, this.annual}) : super(key: key);  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          InkWell(
            splashColor: WellNestColor.wncWhite,
            onTap: monthly,
            child: Container(
              height: 95,
              width: 315,
              clipBehavior: Clip.none,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: WellNestColor.wncBlue,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(3,2),
                    blurRadius: 2,
                    spreadRadius: 2,
                    color: WellNestColor.wncLightgrey.withOpacity(0.4)
                  )
                ]
              ),
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  //  Positioned(
                  //   top: -20,
                  //   right: 4,
                  //   child: Container(
                  //     height: 45,
                  //     width: 45,
                  //     decoration: BoxDecoration(
                  //       shape: BoxShape.circle,
                  //       color: WellNestColor.wncRedError,
                  //     ),
                  //     child: Center(
                  //       child: Text("-41%",
                  //       style: WellNestTextStyle.nowMedium(
                  //         WellNestColor.wncWhite,13.0),
                  //       ),
                  //     ),
                  // ),),
                  Positioned(
                    top: -10,
                    right: 45,
                    bottom: 10,
                    child: SizedBox(
                      height:101.41,
                      width: 49.29,
                      child: SvgPicture.asset(WellnestAsset.subscription1),
                    ),
                  ),
                 
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 20,
                        top: 15,
                        bottom: 10
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        textDirection: TextDirection.ltr,
                        children: [
                          Text("Monthly Plan",
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncWhite,22.0),
                          ),
                          SizedBox(height: 5.h,),
                          Text("₱ 100/Monthly",
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncWhite,13.0),
                          ),
                          SizedBox(height: 5.h,),
                          Text("All Access!",
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncWhite,13.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                 
                ],
              ),
              
            ),
          ),
          //Break
          SizedBox(height: 30.h,),
          //Break
           InkWell(
            splashColor: WellNestColor.wncWhite,
            onTap: monthly,
            child: Container(
              height: 95,
              width: 315,
              clipBehavior: Clip.none,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: WellNestColor.wncAquaBlue,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(3,2),
                    blurRadius: 2,
                    spreadRadius: 2,
                    color: WellNestColor.wncLightgrey.withOpacity(0.4)
                  )
                ]
              ),
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                   Positioned(
                    top: -20,
                    right: 4,
                    child: Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: WellNestColor.wncRedError,
                      ),
                      child: Center(
                        child: Text("-41%",
                        style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncWhite,13.0),
                        ),
                      ),
                  ),),
                  // Positioned(
                  //   top: -10,
                  //   right: 45,
                  //   bottom: 10,
                  //   child: SizedBox(
                  //     height:101.41,
                  //     width: 49.29,
                  //     child: SvgPicture.asset(WellnestAsset.subscription2),
                  //   ),
                  // ),
                  Positioned(
                    top: -10,
                    right: 48,
                    bottom: 10,
                    child: SizedBox(
                      height:84,
                      width: 101.55,
                      child: SvgPicture.asset(WellnestAsset.subscription2),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 20,
                        top: 15,
                        bottom: 10
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        textDirection: TextDirection.ltr,
                        children: [
                          Text("Annual Plan",
                          style: WellNestTextStyle.nowMedium(
                            WellNestColor.wncWhite,22.0),
                          ),
                          SizedBox(height: 5.h,),
                          Text("₱ 1200/Year",
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncWhite,13.0),
                          ),
                          SizedBox(height: 5.h,),
                          Text("Save ₱200!",
                          style: WellNestTextStyle.nowRegular(
                            WellNestColor.wncWhite,13.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                 
                ],
              ),
              
            ),
          ),
        ],
      ),
    );
  }
}