
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class BoxPremiumMonAn extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 174,
      width: 337,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        // image: DecorationImage(
        //   fit: BoxFit.fill,
        //   image: AssetImage(WellnestAsset.subbannerPng))
      ),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: 158,
              width: 337,
              child: Image.asset(WellnestAsset.subbannerPng,
              fit: BoxFit.fill,
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 40,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: WellNestColor.wncBlue,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
                )
              ),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 15,
                    right: 15,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text("WellNest Premium",
                        style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncWhite, 13.0),
                        ),
                      ),
                      Text("₱500",
                        style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncWhite, 13.0),
                        ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}