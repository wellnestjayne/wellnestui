import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class EditPayment extends StatelessWidget {
  final String? cardText;
  final String? sessionFee;
  final String? appFee;
  final String? discount;
  final String? voucher;
  final double? totalPayment;

  const EditPayment(
      {Key? key,
      this.cardText,
      this.sessionFee,
      this.appFee,
      this.discount,
      this.voucher,
      this.totalPayment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 337,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.ltr,
        children: [
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Credit/Debit Card ",
              style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: cardText,
              style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.0),
            ),
          ])),
          SizedBox(height: 30),
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Session Fee: ",
              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: sessionFee,
              style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            ),
          ])),
          SizedBox(height: 10),
          RichText(
              text: TextSpan(children: [
            // TextSpan(
            //   text: "App Fee: ",
            //   style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
            // ),
            // TextSpan(
            //   text: appFee,
            //   style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            // ),
          ])),
          SizedBox(height: 10),
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Discount% : ",
              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: discount,
              style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            ),
          ])),
          SizedBox(height: 10),
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Voucher: ",
              style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: voucher,
              style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            ),
          ])),
          SizedBox(
            height: 30,
          ),
          SizedBox(height: 10),
          RichText(
              text: TextSpan(children: [
            TextSpan(
              text: "Total fees: ",
              style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.0),
            ),
            TextSpan(
              text: totalPayment.toString(),
              style: WellNestTextStyle.nowMedium(WellNestColor.wncBlue, 15.0),
            ),
          ])),
        ],
      ),
    );
  }
}
