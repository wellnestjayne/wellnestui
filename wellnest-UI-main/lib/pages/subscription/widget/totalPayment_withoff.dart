import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class TotalPaymentWithOff extends StatelessWidget {
  final Function()? goto;
  final double? total;

  const TotalPaymentWithOff({Key? key, this.goto, this.total})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 337,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.ltr,
        children: [
          Text(
            "Total Payment with Discount",
            style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 15.0),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            child: Row(
              children: [
                Text(
                  total!.toString(),
                  style: WellNestTextStyle.nowBold(WellNestColor.wncBlue, 25.0),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      textDirection: TextDirection.ltr,
                      children: [
                        // Text(
                        //   "P 500 ",
                        //   textAlign: TextAlign.center,
                        //   style: TextStyle(
                        //       decoration: TextDecoration.lineThrough,
                        //       fontFamily: 'NowReg',
                        //       fontSize: 13.0,
                        //       color: WellNestColor.wncLightgrey),
                        //),
                        // SizedBox(
                        //   height: 5,
                        // ),
                        // Text(
                        //   "P 100 % Off",
                        //   style: WellNestTextStyle.nowRegular(
                        //       WellNestColor.wncLightgrey, 13.0),
                        // ),
                      ],
                    ),
                  ),
                ),
                BlueButtonStandard(
                  bluebutton: goto,
                  title: "Confirm",
                  back: WellNestColor.wncBlue,
                  textColor: WellNestColor.wncWhite,
                  sizeText: 13.0,
                  border: WellNestColor.wncBlue,
                  height: 37,
                  width: 117,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
