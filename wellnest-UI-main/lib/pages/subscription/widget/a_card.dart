
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';

class DropDownCard extends StatelessWidget {

   final list;
   final String? value;
   final Function(String?)? stringFunction;

  const DropDownCard({Key? key, this.list, this.value, this.stringFunction}) : super(key: key); 

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 133,
      decoration: BoxDecoration(
        color: WellNestColor.wncBlue,
        borderRadius: BorderRadius.circular(10)
      ),
      child: DropdownButton(items: list),
    );
  }
}