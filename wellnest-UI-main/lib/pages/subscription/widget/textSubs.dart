

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class TextSubscriptionwithCheck extends StatelessWidget {

   final String? text1;
   final String? text2;
   final String? text3;

  const TextSubscriptionwithCheck({Key? key, this.text1, this.text2, this.text3}) : super(key: key);

 
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 32,
        right: 32
      ),
      child: Container(
       child: Column(
         children: [
           SizedBox(
             height: 27.h,
             width: 27.w,
             child: FaIcon(
               FontAwesomeIcons.solidCheckCircle,
               color: WellNestColor.wncAquaBlue,
             ),
           ),
           SizedBox(height: 15,),
           RichText(
             textAlign: TextAlign.center,
             text: 
           TextSpan(
             children: [
               TextSpan(
                 text: text1,
                 style: WellNestTextStyle.nowRegular(
                   WellNestColor.wncLightgrey, 15.sp)
               ),
               TextSpan(
                 text: text2,
                 style: WellNestTextStyle.nowBold(
                   WellNestColor.wncGrey, 15.sp)
               ),
               TextSpan(
                 text: text3,
                 style: WellNestTextStyle.nowRegular(
                   WellNestColor.wncLightgrey, 15.sp)
               ),
             ]
           )
           ),
         ],
       ),    
      ),
    );
  }
}