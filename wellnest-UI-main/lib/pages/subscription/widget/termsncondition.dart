
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class TearmsNCondition extends StatelessWidget {

    final Function(bool?)? checkit;
    final Function()? seeMore;
    final bool? value;

  const TearmsNCondition({Key? key, this.checkit, this.seeMore, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 337,
      child: Row(
        children: [
          Checkbox(
          checkColor: WellNestColor.wncWhite,
          activeColor: WellNestColor.wncBlue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(2)
          ),
          value: value!,
          onChanged: checkit),
          SizedBox(width: 5,),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                children: [
                  Text("I agree to the Terms and Conditions.",
                  style: WellNestTextStyle.nowRegular(
                    WellNestColor.wncLightgrey, 13.0),
                  ),
                  SizedBox(height: 5,),
                  GestureDetector(
                    onTap: seeMore,
                    child: Text("See More",
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'NowReg',
                      fontSize: 13.0,
                      color: WellNestColor.wncLightgrey
                    ),
                    ),
                  ),
                ],
              ),
            ))
        ],
      ),
    );
  }
}