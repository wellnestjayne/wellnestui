import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class HeaderTitleSubs extends StatelessWidget {

  final String? title;
  final String? extra;
  final Function()? extraFunction;
 final bool? showFunction;

  const HeaderTitleSubs({Key? key, this.title, this.extra, this.extraFunction, this.showFunction}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      width: 337,
      child: Row(
        children: [
          Expanded(
            child: Text(title!,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncBlue
              , 15.sp),
            ),
          ),
          Visibility(
            visible: showFunction == null ? true : showFunction!,
            child: GestureDetector(
              onTap: extraFunction,
              child: Text(extra!,
              style: WellNestTextStyle.nowMedium(
                WellNestColor.wncGrey
                , 15.sp),
              ),
            ),
          )
        ],
      ),
    );
  }
}