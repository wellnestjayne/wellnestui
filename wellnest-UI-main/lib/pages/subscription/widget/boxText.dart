
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class BoxText extends StatelessWidget {
 
  final String? text;

  const BoxText({Key? key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 337,
      decoration: BoxDecoration(
        color: WellNestColor.wncLighBg,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            offset: Offset(3,2),
            blurRadius: 2,
            spreadRadius: 2,
            color: WellNestColor.wncLightgrey.withOpacity(0.3)
          )
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0
        ),
        child: Text(text!,
        style: WellNestTextStyle.nowMedium(
          WellNestColor.wncGrey
          , 13.0),
        ),
      ),
    );
  }
}