
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';

class DialogSubscriptions{


  static inputPromoCode({
    TextEditingController? textEditingController,
    String? titleDialog,
    String? buttonTitle,
    Function()? function,
    GlobalKey<FormState>? globalKey
  }){
    Get.defaultDialog(
      title: "",
      barrierDismissible: true,
      backgroundColor: WellNestColor.wncWhite,
      contentPadding: const EdgeInsets.all(20),
      content: Form(
        key: globalKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          textDirection: TextDirection.ltr,
          children: [
            Text(titleDialog!,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncBlue, 15.0),
            ),
            SizedBox(height: 20,),
            TextFieldWellnestw(
                        controller: textEditingController,
                        validator: (value) =>
                            name(textEditingController!.text = value!),
                        keyboardType: TextInputType.text,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value) =>
                            textEditingController!.text = value!,
                        hintText: "Enter Code",
                      ),
          SizedBox(height: 30,),
          Center(
            child: BlueButtonStandard(
                bluebutton: function,
                title: buttonTitle!,
                back: WellNestColor.wncBlue,
                textColor: WellNestColor.wncWhite,
                sizeText: 13.0,
                border: WellNestColor.wncBlue,
                height: 37,
                width: 194,
              ),
          )            
          ],),
      )
    );
  }

  static errorInputCode({
    String? iconPath,
    String? messageText,
    String? buttonTitle,
    Function()? function,
  }){
    Get.defaultDialog(
      title: "",
      barrierDismissible: true,
      backgroundColor: WellNestColor.wncWhite,
      contentPadding: const EdgeInsets.only(bottom:50,
      left: 20,
      right: 20,
      top: 20
      ),
      content: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(iconPath!),
            SizedBox(height: 25,),
            Text(messageText!,
            textAlign: TextAlign.center,
            style: WellNestTextStyle.nowMedium(
              WellNestColor.wncGrey
              , 18.0),),
          ],
        ),
      ),
    confirm: BlueButtonStandard(
                bluebutton: function,
                title: buttonTitle!,
                back: WellNestColor.wncBlue,
                textColor: WellNestColor.wncWhite,
                sizeText: 13.0,
                border: WellNestColor.wncBlue,
                height: 37,
                width: 272,
              ),     
    );
  }


}