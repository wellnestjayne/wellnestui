
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/subscription/checkout/checkout_view.dart';
import 'package:wellnest/pages/subscription/widget/subscription_button.dart';
import 'package:wellnest/pages/subscription/widget/textSubs.dart';

class SubscriptionMonthy extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: Scaffold(
          backgroundColor: WellNestColor.wncWhite,
          body: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 202.h,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(WellnestAsset.mountain))
                  ),
                  child: Container(
                    height: 202.h,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: GestureDetector(
                              onTap: ()=>Get.back(),
                              child: Container(
                                height: 40,
                                width: 40,
                                child: Center(
                                  child: FaIcon(FontAwesomeIcons.arrowLeft,
                                  color: WellNestColor.wncWhite,
                                  size: 18.h,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                          height: 100.h,
                          width: 100.w,
                          child: Image.asset(WellnestAsset.specialLogo),
                        ),
                        ],
                      ),
                    ),
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      colors: [
                        Colors.transparent,
                        WellNestColor.wncWhite
                      ]),
                    ),
                  ),
                ),
                SizedBox(height: 20.h,),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 25.0,
                      right: 25.0
                    ),
                    child: Column(
                      children: [
                        // Text("Unlock your ideas that will change your life",
                        // textAlign: TextAlign.center,
                        // style: WellNestTextStyle.nowMedium(
                        //   WellNestColor.wncGrey
                        //   ,22.sp),
                        // ),
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Replace the “I” in Illness with “WE,” and it now spells",
                              style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncGrey
                          ,22.sp),
                            ),
                            TextSpan(
                              text: " Wellness",
                              style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncBlue
                          ,22.sp),
                            ),
                            TextSpan(
                              text: ".",
                              style: WellNestTextStyle.nowMedium(
                          WellNestColor.wncGrey
                          ,22.sp),
                            ),
                          ]
                        )),
                        SizedBox(height: 25.h,),
                        TextSubscriptionwithCheck(
                            text1: "We at WellNest believe that if all these little lights are put together, we can create a guiding star, to unite us and help us get through this time and beyond.",
                            text2: "",
                            text3: "",
                        ),  
                        SizedBox(height: 40.h,),
                        SubscriptionButton(
                          monthly: (){},
                          //=>Get.to(()=>Checkoutpageview()),
                          annual: (){},
                        ),
                        SizedBox(height: 15.h,)            
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        onWillPop:()async=>false),
    );
  }
}