

import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/categories/model/model_category.dart';
import 'package:wellnest/pages/featured_coach/model/model_trash.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';

class CategoryController extends GetxController{


  final maincontroller = Get.find<MainController>();  

   final listTrash = [
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncAquaBlue
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncAquaBlue
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue
    ),
     FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncAquaBlue
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncBlue
    ),
    FeatureTrash(
      imagePath: WellnestAsset.pssuy,
      speciality: "Therapist",
      name: "Erchil",
      backColor: WellNestColor.wncAquaBlue
    ),
  ];


  final listrash1 = [
    TrashCategory(
      title: "Erchil Tutorial",
      name: "By Erchil",
      hieght: 135,
      width: 292,
      imagePath: WellnestAsset.trash1,
    ),
     TrashCategory(
      title: "Erchil Tutorial",
      name: "By Erchil",
      hieght: 135,
      width: 292,
      imagePath: WellnestAsset.trash1,
    ),
  ];

}