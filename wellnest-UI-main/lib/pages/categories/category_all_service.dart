import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/pages/categories/controller/categoryserviceController.dart';
import 'package:wellnest/pages/categories/top_services/top_services.dart';
import 'package:wellnest/pages/categories/widget/appbar.dart';
import 'package:wellnest/pages/categories/widget/category_servicecard.dart';
import 'package:wellnest/pages/categories/widget/top_coachCard.dart';
import 'package:wellnest/pages/top_coaches/top_coach.dart';
import 'package:wellnest/standard_widgets/header.dart';

class CategoryAllService extends StatelessWidget {
  final categoryServices = Get.put(CategoryController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: AppBarCategoryServices(
            function: () => Get.back(),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Standardheader(
                      press: () => Get.to(() => TopCoachView()),
                      title: "Top Coaches",
                      show: true,
                      colorText: categoryServices.maincontroller.colorType,
                    ),
                  ),
                  Container(
                    height: 200,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: categoryServices.listTrash.length,
                      itemBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: TopCoachCard(
                          press: () {},
                          name: categoryServices.listTrash[index].name,
                          specialty: categoryServices.listTrash[index].speciality,
                          backcolor: categoryServices.listTrash[index].backColor,
                          imagepath: categoryServices.listTrash[index].imagePath,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Standardheader(
                      press: () => Get.to(() => TopServiceViewList()),
                      title: "Top Services",
                      show: true,
                      colorText: categoryServices.maincontroller.colorType,
                    ),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Container(
                    height: 155,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: categoryServices.listrash1.length,
                      itemBuilder: (context, index) => Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: CategoryServicesCard(
                            hieght: categoryServices.listrash1[index].hieght,
                            width: categoryServices.listrash1[index].width,
                            name: categoryServices.listrash1[index].name,
                            imagePath: categoryServices.listrash1[index].imagePath,
                            title: categoryServices.listrash1[index].title,
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
