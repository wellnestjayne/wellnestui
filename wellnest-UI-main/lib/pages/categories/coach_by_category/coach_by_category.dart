import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/categories/coach_by_category/controller/coachCategoryController.dart';
import 'package:wellnest/pages/categories/widget/category_servicecard.dart';
import 'package:wellnest/pages/home/controller/home_controller.dart';
import 'package:wellnest/pages/home/widget/featured_coach.dart';
import 'package:wellnest/standard_widgets/appbar.dart';
import 'package:wellnest/standard_widgets/header.dart';

class CoachByCategory extends StatelessWidget {
  CoachByCategory({Key? key}) : super(key: key);
  final catController = Get.put(CoachCategoryController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: StandardAppbaruse(
            function: () => Get.back(id: 0),
            title2: Row(
              children: [
                Image.asset(
                  catController.rightIcon(Get.arguments[0]),
                  scale: 15,
                ),
                SizedBox(width: 5),
                Text(Get.arguments[0], style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 18.sp)),
              ],
            ),
            color: WellNestColor.wncBlue,
          ),
          body: SingleChildScrollView(
            child: SafeArea(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Standardheader(
                        press: () {},
                        show: true,
                        colorText: WellNestColor.wncBlue,
                        title: "Top Coaches",
                      ),
                      SizedBox(height: 15),
                      FeatureCoachw(),
                      SizedBox(height: 15),
                      Standardheader(
                        press: () {},
                        show: true,
                        colorText: WellNestColor.wncBlue,
                        title: "Top Services",
                      ),
                      Obx(
                        () => catController.servicesCat.isEmpty
                            ? Container()
                            : Container(
                                width: 349,
                                height: 161.34,
                                child: AnimationLimiter(
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount:
                                        catController.servicesCat.length > 3 ? 3 : catController.servicesCat.length,
                                    itemBuilder: (_, __) => AnimationConfiguration.staggeredList(
                                      position: __,
                                      duration: 375.milliseconds,
                                      child: SlideAnimation(
                                        verticalOffset: 50.0,
                                        child: FadeInAnimation(
                                          child: Padding(
                                            padding: const EdgeInsets.only(top: 5, bottom: 10, right: 10),
                                            child: CategoryServicesCard(
                                              hieght: 135,
                                              width: 292,
                                              name: catController.servicesCat[__].description,
                                              imagePath: catController.servicesCat[__].bannerUrl,
                                              title: catController.servicesCat[__].name,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
