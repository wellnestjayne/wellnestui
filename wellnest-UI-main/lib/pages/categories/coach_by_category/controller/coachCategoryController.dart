import 'package:get/get.dart';
import 'package:wellnest/api/api_service/api_coaches/api_coach.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/model/coash_services.dart';
import 'package:wellnest/pages/home/controller/home_controller.dart';

class CoachCategoryController extends GetxController {
  final homeController = Get.find<HomeController>();

  @override
  void onInit() {
    super.onInit();
  }

  rightIcon(String? string) {
    switch (string) {
      case "Mental Health":
        return WellnestAsset.mental_health;
      case "Performance":
        return WellnestAsset.perfomace;
      case "Relationship":
        return WellnestAsset.relationship;
      case "Lifestyle":
        return WellnestAsset.lifeStyle1;
      case "Spiritual":
        return WellnestAsset.spiritual1;
      case "Financial":
        return WellnestAsset.fianancial;
    }
  }

  final List<Service> servicesCat = <Service>[].obs;
  final categoryId = ''.obs;
  getCatID(catID) {
    categoryId(catID);
    getByCategory(catID);
  }

  getByCategory(catID) async {
    print(catID);
    try {
      final result = await ApiCoach.getAllByCategory(catID);
      if (result != null) {
        servicesCat.assignAll(result);
      }
    } finally {
      //print(servicesCat);
    }
  }
}
