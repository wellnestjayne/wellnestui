
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/pages/categories/top_services/controller/topservicessController.dart';
import 'package:wellnest/pages/categories/widget/category_servicecard.dart';
import 'package:wellnest/standard_widgets/appbar.dart';

class TopServiceViewList extends StatelessWidget {
  
  final topController = Get.put(TopServicexController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: WillPopScope(
          child: Scaffold(
            appBar: StandardAppbaruse(
              function: ()=>Get.back(),
              title: "Top Services",
              color: topController.mainController.colorType,
            ),
            body: Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20,
              ),
            child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: topController.listrash1.length,
                      itemBuilder: (context,index)
                      => Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: CategoryServicesCard(
                          hieght: topController.listrash1[index].hieght,
                          width: topController.listrash1[index].width,
                          name: topController.listrash1[index].name,
                          imagePath: topController.listrash1[index].imagePath,
                          title: topController.listrash1[index].title,
                        )
                      ),
                      ),
              ),
          ),
    onWillPop: ()async=>false),
    );
  }
}