

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class TopCoachCard extends StatelessWidget {

  final Function()? press;
  final Color? backcolor;
  final String? imagepath;
  final String? name;
  final String? specialty;

  const TopCoachCard({Key? key, this.press, this.backcolor, this.imagepath, this.name, this.specialty}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
                   height: 135.h,
                   width: 135.w,
                   decoration: BoxDecoration(
                     color: backcolor,
                     borderRadius: BorderRadius.circular(10),
                     boxShadow: [
                       BoxShadow(
                         offset: Offset(4,2),
                         blurRadius: 3,
                         spreadRadius: 3,
                         color: WellNestColor.wncLightgrey.withOpacity(0.2)
                       )
                     ],
                     image: DecorationImage(
                       image: AssetImage(imagepath!))
                   ),
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Stack(
                       children: [
                         Align(
                           alignment: Alignment.bottomCenter,
                           child: Container(
                             height: 45,
                             width: MediaQuery.of(context).size.width,
                             decoration: BoxDecoration(
                               color: WellNestColor.wncWhite,
                               borderRadius: BorderRadius.circular(8)
                             ),
                             child: Padding(
                               padding: const EdgeInsets.all(10.0),
                               child: Column(
                                 mainAxisAlignment: MainAxisAlignment.start,
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 textDirection: TextDirection.ltr,
                                 children: [
                                   Text(name!,
                                   style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 10.0),
                                   ),
                                   SizedBox(height: 3.h,),
                                   Text(specialty!,
                                   style: WellNestTextStyle.nowMedium(WellNestColor.wncLightgrey, 6.0),
                                   ),
                                 ],
                               ),
                             ),
                           ),
                         ),
                       ],
                     ),
                   ),
                 ),
    );
  }
}