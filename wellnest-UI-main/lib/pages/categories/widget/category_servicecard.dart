import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class CategoryServicesCard extends StatelessWidget {
  final String? imagePath;
  final String? name;
  final String? title;
  final double? hieght;
  final double? width;

  const CategoryServicesCard({Key? key, this.imagePath, this.name, this.title, this.hieght, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: hieght,
      width: width,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            offset: Offset(4, 2),
            blurRadius: 2,
            spreadRadius: 2,
            color: WellNestColor.wncLightgrey.withOpacity(0.2),
          )
        ],
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(imagePath!),
        ),
      ),
      child: Container(
        height: hieght,
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.transparent,
              Colors.black.withOpacity(0.4),
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  height: 35,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    textDirection: TextDirection.ltr,
                    children: [
                      Text(
                        title!,
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 14.sp),
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      Text(
                        name!,
                        style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 10.sp),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
