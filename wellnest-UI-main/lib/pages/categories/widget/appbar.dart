import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppBarCategoryServices extends StatelessWidget implements PreferredSizeWidget {
  
  final double? topSize = 262.h;
  final Function()? function;

  AppBarCategoryServices({Key? key, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: topSize!,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10)
        ),
        color: WellNestColor.wncPeach
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 15.h,),
          Align(
            alignment: Alignment.topLeft,
            child:  InkWell(
              onTap: function!,
              child: Container(
                width: 50,
                child: Center(
                  child: FaIcon(FontAwesomeIcons.chevronLeft,
                  color: WellNestColor.wncWhite,
                  size: 25,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 15.h,),
          Container(
            height: 120.h,
            width: 120.w,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.contain,
                image: AssetImage(WellnestAsset.logoWellNest))
            ),
          ),
          SizedBox(height: 15.h,),
          Text("All Services",
          style: WellNestTextStyle.nowMedium(WellNestColor.wncWhite, 25.sp),
          )
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(topSize!);
}