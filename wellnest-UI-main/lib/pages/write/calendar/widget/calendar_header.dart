
import 'package:flutter/material.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/write/calendar/customization/header_style.dart';
import 'package:wellnest/pages/write/calendar/shared/utils.dart';
import 'package:wellnest/pages/write/calendar/widget/custom_icon_button.dart';
import 'package:intl/intl.dart';
class CalendarHeader extends StatelessWidget {
  final dynamic locale;
  final DateTime focusedMonth;
  final CalendarFormat calendarFormat;
  final HeaderStyle headerStyle;
  final VoidCallback onLeftChevronTap;
  final VoidCallback onRightChevronTap;
  final VoidCallback onHeaderTap;
  final VoidCallback onHeaderLongPress;
  final ValueChanged<CalendarFormat> onFormatButtonTap;
  final Map<CalendarFormat, String> availableCalendarFormats;
  final DayBuilder? headerTitleBuilder;

  const CalendarHeader({
    Key? key,
    this.locale,
    required this.focusedMonth,
    required this.calendarFormat,
    required this.headerStyle,
    required this.onLeftChevronTap,
    required this.onRightChevronTap,
    required this.onHeaderTap,
    required this.onHeaderLongPress,
    required this.onFormatButtonTap,
    required this.availableCalendarFormats,
    this.headerTitleBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final text = headerStyle.titleTextFormatter?.call(focusedMonth, locale) ??
        DateFormat.yMMMM(locale).format(focusedMonth);

    return Container(
      decoration: headerStyle.decoration,
      margin: headerStyle.headerMargin,
      padding: headerStyle.headerPadding,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 10,
              left: 5,
              right: 5,
              bottom: 10,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                 
                if (headerStyle.leftChevronVisible)
                  CustomIconButton(
                    icon: headerStyle.leftChevronIcon,
                    onTap: onLeftChevronTap,
                    margin: headerStyle.leftChevronMargin,
                    padding: headerStyle.leftChevronPadding,
                  ),
                Expanded(
                  child: Center(
                    child: headerTitleBuilder?.call(context, focusedMonth) ??
                        GestureDetector(
                          onTap: onHeaderTap,
                          onLongPress: onHeaderLongPress,
                          child: Text(
                            text,
                            style: headerStyle.titleTextStyle,
                            textAlign: headerStyle.titleCentered
                                ? TextAlign.center
                                : TextAlign.start,
                          ),
                        ),
                  ),
                ),
                if (headerStyle.formatButtonVisible &&
                    availableCalendarFormats.length > 1)
                  // Padding(
                  //   padding: const EdgeInsets.only(left: 8.0),
                  //   child: FormatButton(
                  //     onTap: onFormatButtonTap,
                  //     availableCalendarFormats: availableCalendarFormats,
                  //     calendarFormat: calendarFormat,
                  //     decoration: headerStyle.formatButtonDecoration,
                  //     padding: headerStyle.formatButtonPadding,
                  //     textStyle: headerStyle.formatButtonTextStyle,
                  //     showsNextFormat: headerStyle.formatButtonShowsNext,
                  //   ),
                  // ),
                if (headerStyle.rightChevronVisible)
                  CustomIconButton(
                    icon: headerStyle.rightChevronIcon,
                    onTap: onRightChevronTap,
                    margin: headerStyle.rightChevronMargin,
                    padding: headerStyle.rightChevronPadding,
                  ),
              ],
            ),
          ),
          SizedBox(height: 15,),
          Divider(color: WellNestColor.wncGrey,),
          SizedBox(height: 5,),
        ],
      ),
    );
  }
}