
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/main/controller/main_controller.dart';
import 'package:wellnest/pages/write/models/model_mornEve.dart';
import 'package:wellnest/route/route_page.dart';
class WriteController extends GetxController{

  final TextEditingController? morningAnswer1 = TextEditingController();
   final TextEditingController? morningAnswer2 = TextEditingController();
   final TextEditingController? morningAnswer3 = TextEditingController();
   final TextEditingController? morningAnswer4 = TextEditingController();

   final TextEditingController? morningEvening1 = TextEditingController();
   final TextEditingController? morningEvening2 = TextEditingController();
   final TextEditingController? morningEvening3 = TextEditingController();
   final TextEditingController? morningEvening4 = TextEditingController();

    final mainControll = Get.find<MainController>();

    final name = SharePref.detailData['given_name'];

    Rx<DateTime> focusDay = DateTime.now().obs;
    Rx<DateTime> selectedDay = DateTime.now().obs;  

    @override
  void onInit() {
    
    super.onInit();
  } 

    var selectedPageNumber = 0.obs;
    var showBack = false.obs;
   var pageControl = PageController();
   bool get isLastpagemorning => selectedPageNumber.value == morningQuestion.length -1;
   bool get isLastpageevning => selectedPageNumber.value == eveningQuestion.length -1;

    forwardActionMorning(){
    if(isLastpagemorning){
     //Morning Function
    }
    else if(selectedPageNumber.value == 0){
      showBack(true);
      pageControl.nextPage(duration: 300.milliseconds, curve: Curves.easeInCubic);
    }
    else {
      pageControl.nextPage(duration: 300.milliseconds, curve: Curves.easeInCubic);
    }
  }
  backwardActionMorning(){
    if(selectedPageNumber.value == 1){
      showBack(false);
      pageControl.previousPage(duration: 300.milliseconds, curve: Curves.easeInCubic);
    }else{
      pageControl.previousPage(duration: 300.milliseconds, curve: Curves.easeInCubic);
    }
    
  }


  forwardActionEvening(){
    if(isLastpageevning){
     //MorniEveningng Function
    }
    else if(selectedPageNumber.value == 0){
      showBack(true);
      pageControl.nextPage(duration: 300.milliseconds, curve: Curves.easeInCubic);
    }
    else {
      pageControl.nextPage(duration: 300.milliseconds, curve: Curves.easeInCubic);
      }
  }

    knowingTimeHour() async{
      final hour = DateTime.now().toLocal().hour;
      print(hour);
      if(hour < 12 ) return Get.toNamed(AppRouteName.writeJournal!+AppRouteName.morningQuestion!) ;
      else if((hour > 12 ) || (hour <= 16)) return Get.toNamed(AppRouteName.writeJournal!+AppRouteName.morningQuestion!);
      else if((hour > 16 )  || (hour < 20)) return Get.toNamed(AppRouteName.writeJournal!+AppRouteName.eveningQuestion!);
      else return print('You Should Probably Sleep');

    //Get.toNamed(AppRouteName.writeJournal!+AppRouteName.morningQuestion!);
      
    }


    greetingTime(){
      final hour = DateTime.now().toLocal().hour;
      if(hour < 12 ) return 'Good Morning ';
      else if((hour > 12 ) || (hour <= 16)) return 'Good Afternoon ';
      else if((hour > 16 ) || (hour < 20)) return 'Good Evening ';
      else return 'You Should Probably Sleep';
    }

    vodoo(quest){
      if(quest == "What are your wins today?") return morningAnswer1;
      else if(quest == "What makes you happy today?") return morningAnswer2;
      else if(quest == "What do you want to accomplish today?") return morningAnswer3;
      else return morningAnswer4;  
    }

    vodoox(quest){
      if(quest == "What did i accomplish today?") return morningEvening1;
      else if(quest == "What did i learn today?") return morningEvening2;
      else if(quest == "What did i enjoy today?") return morningEvening3;
      else return morningEvening4;
    }

    witch(){
      var firstData = {
        "question" : "What are your wins today?",
        "answer" : morningAnswer1!.value.text
      };
      var secondData = {
        "question" : "What makes you happy today?",
        "answer" : morningAnswer2!.value.text
      };
      var thirdData = {
        "question" : "What do you want to accomplish today?",
        "answer" : morningAnswer3!.value.text
      };
      var fourthData = {
        "question" : "What else do you want to express today?",
        "answer" : morningAnswer4!.value.text
      };

      print("$firstData /n $secondData /n $thirdData /n $fourthData");
    }


    witchx(){
      var firstData = {
        "question" : "What are your wins today?",
        "answer" : morningEvening1!.value.text
      };
      var secondData = {
        "question" : "What makes you happy today?",
        "answer" : morningEvening2!.value.text
      };
      var thirdData = {
        "question" : "What do you want to accomplish today?",
        "answer" : morningEvening3!.value.text
      };
      var fourthData = {
        "question" : "What else do you want to express today?",
        "answer" : morningEvening4!.value.text
      };

      print("$firstData /n $secondData /n $thirdData /n $fourthData");
    }

    List<QuestionMornEve> morningQuestion = [
    QuestionMornEve(
        question: "What are your wins today?",
        imagepath: WellnestAsset.svgf,
        description: "Write a few words to keep you on track.",
        controller: 1
        ),
    QuestionMornEve(
        question: "What makes you happy today?",
        imagepath: WellnestAsset.svgu,
        description: "Write a few words to keep you on track.",
        controller: 2
        ),
    QuestionMornEve(
        question: "What do you want to accomplish today?",
        imagepath: WellnestAsset.svgc,
        description: "Write a few words to keep you on track.",
        controller: 3
        ),  
    QuestionMornEve(
        question: "What else do you want to express today?",
        imagepath: WellnestAsset.svgus,
        description: "Write a few words to keep you on track.",
        controller: 4
        ),      
    ];

    List<QuestionMornEve> eveningQuestion = [
    QuestionMornEve(
        question: "What did i accomplish today?",
        imagepath: WellnestAsset.svgf,
        description: "Write a few words to keep you on track.",
        controller: 1),
    QuestionMornEve(
        question: "What did i learn today?",
        imagepath: WellnestAsset.svgu,
        description: "Write a few words to keep you on track.",
        controller: 2),
    QuestionMornEve(
        question: "What did i enjoy today?",
        imagepath: WellnestAsset.svgc,
        description: "Write a few words to keep you on track.",
        controller: 3),  
    QuestionMornEve(
        question: "How can i get better tomorrow",
        imagepath: WellnestAsset.svgus,
        description: "Write a few words to keep you on track.",
        controller: 4), 
    ];


    @override
  void onClose() {
    super.onClose();
  }

} 