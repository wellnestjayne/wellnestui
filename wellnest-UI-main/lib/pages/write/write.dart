import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/write/calendar/customization/calendar_style.dart';
import 'package:wellnest/pages/write/calendar/shared/utils.dart';
import 'package:wellnest/pages/write/calendar/table_calendar.dart';
import 'package:wellnest/pages/write/controller/write_controller.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class WriteVieww extends GetView<WriteController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: WellNestColor.wncWhite,
        body: Padding(
          padding: const EdgeInsets.only(
            left: 20,
            right: 20,
          ),
          child: Column(
            children: [
              TableCalendar(
                rowHeight: 55,
                calendarFormat: CalendarFormat.month,
                firstDay: DateTime.utc(2021, 8, 2),
                lastDay: DateTime.utc(2030, 3, 14),
                // focusedDay:controller.focusDay.value,
                focusedDay: DateTime.now(),
                calendarStyle: CalendarStyle(isTodayHighlighted: true),
                // selectedDayPredicate: (day)=>isSameDay(controller.selectedDay.value, day),
                // onDaySelected: (selected,focusselectd)=>
                // controller.ondaySelect(selected, focusselectd),
              ),
              SizedBox(
                height: 30,
              ),
              BlueButtonStandard(
                bluebutton: () => controller.knowingTimeHour(),
                title: "Create Journal",
                textColor: WellNestColor.wncWhite,
                border: controller.mainControll.colorType,
                back: controller.mainControll.colorType,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
