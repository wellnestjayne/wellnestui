

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/pages/write/controller/write_controller.dart';
import 'package:wellnest/pages/write/widget/appbar.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';

class JournalEvening extends GetView<WriteController>{
  @override
  Widget build(BuildContext context) {
      return SafeArea(
        child: Scaffold(  
          backgroundColor: WellNestColor.wncWhite,
          appBar: AppbarJournal(
            back: ()=>Get.back(),
            name: controller.name,
            welcoming: controller.greetingTime(),
          ),
        body: Padding(
          padding: const EdgeInsets.only(
            left: 20,
            right: 20
          ),
          child: PageView.builder(
            itemCount: controller.eveningQuestion.length,
            controller: controller.pageControl,
            onPageChanged: controller.selectedPageNumber,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context,index)=>
            SingleChildScrollView(
              child: Column(
                children: [
                SizedBox(
                  height: 15.h,
                ),  
                Text(controller.eveningQuestion[index].question,
                style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey,16.sp),
                ),
                Container(
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          height: 250,
                          width: MediaQuery.of(context).size.width,
                          child: SvgPicture.asset(
                           controller.eveningQuestion[index].imagepath,
                            fit: BoxFit.contain,
                            
                           // alignment: Alignment(0.0,0.1),
                            semanticsLabel: '',
                            ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 100),
                        child: Container(
                          height: 328,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(3, 2),
                                blurRadius: 3,
                                spreadRadius: 3,
                                color: WellNestColor.wncLightgrey.withOpacity(0.1)
                              )
                            ]
                          ),
                          child: TextFormField(
                            
                            autofocus: false,
                            textAlignVertical: TextAlignVertical.top,
                            maxLines: null,
                            expands:true,
                            keyboardType: TextInputType.multiline,
                            controller: controller.vodoox(controller.eveningQuestion[index].question),
                            textInputAction: TextInputAction.none,
                            decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: WellNestColor.wncAquaBlue)
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: WellNestColor.wncAquaBlue)
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: WellNestColor.wncAquaBlue)
                            ),
                            filled: true,
                            
                            fillColor: WellNestColor.wncWhite,
                          //  hintText: hintext!,
                            hintStyle: TextStyle(
                           //   color: expand! ? Colors.transparent : WellNestColor.wncLightgrey,
                              fontSize: 16.sp,
                            ),
                          ),
                              

                          ),
                        ),
                      ) ,  
                    ],
                  ),
                ),  
                SizedBox(height: 15,),
               
                ],
              ),
            ),
            ),
        ),  
         bottomNavigationBar: Padding(
           padding: const EdgeInsets.all(8.0),
           child: Container(
             height: 45,
             child: Padding(
               padding: const EdgeInsets.only(
                 left: 20,
                 right: 20
               ),
               child: Row(
                 children: [
                    Expanded(
                      child: Container(
                        child: Row(
                          children: List.generate(
                          controller.eveningQuestion.length, 
                          (index) =>  Obx(
                                    () => AnimatedContainer(
                                      duration: 200.milliseconds,
                                      margin: EdgeInsets.only(right: 5),
                                      height: 10.h,
                                      width:
                                          controller.selectedPageNumber.value ==
                                                  index
                                              ? 25.w
                                              : 10.w,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(100),
                                          color: WellNestColor.wncBlue),
                                    ),
                                  )),
                        ),
                      ),
                    ),
                     Obx(
                       ()=> Visibility(
                         visible: controller.showBack.value,
                         child: BlueButtonStandard(
                            bluebutton: ()=>controller.backwardActionMorning(),
                            title: "Back",
                            textColor: WellNestColor.wncWhite,
                            border: WellNestColor.wncBlue,
                            back: WellNestColor.wncBlue,
                            height: 45,
                            width: 100,
                          ),
                       ),
                     ),
                    SizedBox(width: 10.w,),
                    BlueButtonStandard(
                        bluebutton: ()=>controller.forwardActionEvening(),
                        title: "Next",
                        textColor: WellNestColor.wncWhite,
                        border: WellNestColor.wncBlue,
                        back: WellNestColor.wncBlue,
                        height: 45,
                        width: 100,
                      ),
                 ],
               ),
             ),
           ),
         ),
        ),
        );

  }

}