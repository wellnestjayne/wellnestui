
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class QuestionMornEve {

  final  question;
  final  imagepath;
  final  description;
  final   controller;

  const QuestionMornEve({this.question, this.imagepath, this.description,this.controller});

}