

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class AppbarJournal extends StatelessWidget implements PreferredSizeWidget {

    final double? frieght = 50;
    final String? welcoming;
    final String? name;
    final Function()? back;

  const AppbarJournal({Key? key, this.welcoming, this.name, this.back}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: frieght,
      decoration: BoxDecoration(
        color: WellNestColor.wncWhite
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            GestureDetector(
              onTap: back,
              child: Container(
                height: 50,
                width: 50,
                child: Center(
                  child: FaIcon(FontAwesomeIcons.arrowLeft,
                  color: WellNestColor.wncGrey,
                  size: 15,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: Center(
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: welcoming,
                          style: WellNestTextStyle.nowMedium(WellNestColor.wncGrey, 16.sp)
                        ),
                        TextSpan(
                          text: name,
                          style: WellNestTextStyle.nowRegular(WellNestColor.wncGrey, 16.sp)
                        ),

                      ]
                    )),
                ),
              )),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(frieght!);
}