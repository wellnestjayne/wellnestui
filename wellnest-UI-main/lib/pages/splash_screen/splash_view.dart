
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/pages/splash_screen/controller/splash_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class SplashScreenw extends StatelessWidget {
   static const splash_id = "/";

  final splashController = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: MediaQuery.of(context).size.height.h,
      width:MediaQuery.of(context).size.width.w,
      decoration: BoxDecoration(
        color: WellNestColor.wncWhite
      ),
      child: Center(
      child: FadeTransition(
      opacity: splashController.animationController,
      child:  Container(
      width: 400.w,
      height: 400.h,
      child: Image.asset(
        WellnestAsset.logoWellNest,
        fit: BoxFit.cover,
      ),
      ),
        )
          ),
    );
  }
}