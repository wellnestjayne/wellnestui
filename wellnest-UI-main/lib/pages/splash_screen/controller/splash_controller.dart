

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wellnest/constants/pref/shared_userpref.dart';
import 'package:wellnest/pages/starting/start_page.dart';
import 'package:wellnest/route/route_page.dart';

class SplashController extends GetxController with SingleGetTickerProviderMixin{

  late AnimationController animationController = AnimationController(
    vsync: this,
    duration: Duration(seconds: 1)
  );

  late Animation<double> animation = Tween(begin: 0.0,end: 1.0).animate(
    CurvedAnimation(parent: animationController, 
    curve: Curves.elasticInOut)
    );

  

  @override
  void onInit() {
    animationController.forward();
    gotoLogin();
    super.onInit();
    
  }


  gotoLogin() async{
    
    animationController.addStatusListener((status) { 
      if(status == AnimationStatus.completed){
       var sharepref = SharePref.getType() != null ?  "have" :  "empty";
    if(sharepref == "empty"){
       Get.offAllNamed(AppRouteName.startingpage!);
    }else{
       Get.offAllNamed(AppRouteName.dashBoard!);
     // Get.offAllNamed(AppRouteName.subscription!);
    } 
  // print(sharepref);
      }
     });
  }



}