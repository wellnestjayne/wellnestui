

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wellnest/api/api_service/Log_Reg_Up/api_services.dart';
import 'package:wellnest/api/services/cognito_service.dart';
import 'package:wellnest/pages/main/main_home.dart';
import 'package:wellnest/route/route_page.dart';

class LoginController extends GetxController{

   TextEditingController? email = TextEditingController();
   TextEditingController? password =  TextEditingController();

   final showPass = false.obs;

   showThePassword(){
     showPass(!showPass.value);
   }


  // @override
  // void onInit() {
  //   email = TextEditingController();
  //   password = TextEditingController();
  //   super.onInit();
  // }

  void signIn() async{

    var data = 
    //<String,String>
    {
      "username": email!.value.text,
      "password": password!.value.text
    };

    // await CognitoService.signInCognito(data);

    await UserCoachApiOnly.loginUserOrCoach(data);

    // Get.offNamed(AppRouteName.dashBoard!);
  }
   


}