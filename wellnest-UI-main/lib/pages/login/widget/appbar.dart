
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';

class GobacktoAsLog extends StatelessWidget implements PreferredSizeWidget {

  final ghieght = 50.h;

  final String? title;
  final IconData? icon;
  final Function()? press;
  final Color? colors;

  GobacktoAsLog({Key? key, this.title, this.icon, this.press, this.colors}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20,right: 20),
        child: Stack(
          children: [
            GestureDetector(
              onTap: press,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    FaIcon(icon,
                    color: colors!,
                    size: 15.h,
                    ),
                    SizedBox(width: 10.w,),
                    Text(title!,
                    style: WellNestTextStyle.nowLight(colors!, 14.sp),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(ghieght);
}