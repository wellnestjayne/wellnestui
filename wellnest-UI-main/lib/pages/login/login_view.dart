

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:wellnest/assets/constant/asset_imagepath.dart';
import 'package:wellnest/constants/colors/colors.dart';
import 'package:wellnest/constants/texts/style_Text.dart';
import 'package:wellnest/constants/validator/validator.dart';
import 'package:wellnest/pages/as_page/as_log.dart';
import 'package:wellnest/pages/forgot_password/forgot_sendEmail.dart';
import 'package:wellnest/pages/login/controller/login_controller.dart';
import 'package:wellnest/pages/login/widget/appbar.dart';
import 'package:wellnest/pages/register/register_view.dart';
import 'package:wellnest/route/route_page.dart';
import 'package:wellnest/standard_widgets/button_standard.dart';
import 'package:wellnest/standard_widgets/line_choice.dart';
import 'package:wellnest/standard_widgets/social_button.dart';
import 'package:wellnest/standard_widgets/textfield_standard.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class LoginVieww extends StatelessWidget {

  // final String? typeUser;

  final loginController = Get.put(LoginController());
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  // LoginVieww({Key? key, this.typeUser}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //  final colorUse = typeUser == "user" ? WellNestColor.wncAquaBlue : WellNestColor.wncBlue;
    return SafeArea(
      child: Scaffold(
        backgroundColor: WellNestColor.wncWhite,
        // appBar: GobacktoAsLog(
        //   title: "Go back!",
        //   icon: FontAwesomeIcons.times,
        //   press: ()=>Get.offNamed(AppRouteName.startingpage!),
        //   colors: WellNestColor.wncBlue,
        // ),
          body: WillPopScope(
            onWillPop: ()async=> await Get.offNamed(AppRouteName.startingpage!),
            child: SafeArea(
        child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 30
              ),
              child: Obx(()
                => Form(
                  key: _form,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                       Container(
                        height: 160.h,
                        width: 160.w,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                          WellnestAsset.logoWellNest,
                        ))),
                      ),
                      Text("Login to your Account",
                      style: WellNestTextStyle.nowBold(WellNestColor.wncGrey, 16.sp),
                      ),
                      SizedBox(height: 40.h,),
                       SizedBox(height: 30.h,),
                      TextFieldWellnestw(
                        controller: loginController.email,
                        validator: (value)=> nameWithDot(loginController.email!.text = value!),
                        keyboardType: TextInputType.emailAddress,
                        obsecure: false,
                        onchanged: (value) => false,
                        onsaved: (value)=> loginController.email!.text = value!,
                        hintText: "Email",
                      ),
                      SizedBox(height: 25.h,),
                      TextFieldWellnestw(
                        controller: loginController.password,
                        validator: (value)=> password(loginController.password!.text = value!),
                        keyboardType: TextInputType.visiblePassword,
                        obsecure: loginController.showPass.isFalse ? true : false,
                        onchanged: (value) => false,
                        onsaved: (value)=> loginController.password!.text = value!,
                        hintText: "Password",
                      ),
                      Container(
                        width: 349,
                        child: Row(
                          children: [
                            Checkbox(
                            value: loginController.showPass.value,
                            tristate: true,
                            checkColor: WellNestColor.wncWhite,
                            activeColor: WellNestColor.wncBlue,
                            onChanged: (bolVal)=>loginController.showThePassword()),
                            Text("Show Password.",
                            style: WellNestTextStyle.nowRegular(WellNestColor.wncLightgrey,
                            13.0),),
                          ],
                        ),
                      ),
                      SizedBox(height: 70.h,),
                    //  SocialButtonStandard(
                    //     facebook: () => print("this is f"),
                    //     google: () => print("this is g"),
                    //     linkedin: () => print("this is l"),
                    //     twitter: () => print("this is t"),
                    //     bonusText: "",
                    //   ),
                      SizedBox(height: 30.h,),
                       BlueButtonStandard(
                        bluebutton: ()=>callback(),
                        title: "Log In",
                        textColor: WellNestColor.wncWhite,
                        border: WellNestColor.wncBlue,
                        back: WellNestColor.wncBlue,
                        height: 45,
                        width: 250,
                      ),
                       SizedBox(
                        height: 20,
                      ),
                      LineChoiceForgotorSome(
                        title1: "Forgot Password",
                        title2: "Sign Up",
                        forgot: () => Get.off(()=>SendEmailForgotPassword()),
                        some: () 
                        // => Get.off(
                        //   ()=>RegisterVieww(typeUser: "User",)
                        => Get.offNamed(AppRouteName.loginAs!)
                        
                      ),
                      SizedBox(height: 40.h,),
                    ],
                  ),
                ),
              ) ,
              ),
            ),
              ),
          ),
      ),
    );
  }

  void callback(){
    if(_form.currentState!.validate()){
      loginController.signIn();
    }
  }
  
}